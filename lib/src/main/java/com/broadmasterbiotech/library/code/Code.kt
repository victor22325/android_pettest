package com.broadmasterbiotech.library.code

object Code {

    object Type {
        const val BLUETOOTH_SETTING_TYPE: Int = 10
        const val VERIFY_SN_TYPE: Int = 11
        const val CREATE_ACCOUNT_TYPE: Int = 12
        const val LOGIN_TYPE: Int = 13
        const val PROFILE_ACCOUNT_TYPE: Int = 14
        const val PICK_PET_TYPE: Int = 21
        const val CREATE_PET_TYPE: Int = 22
        const val DELETE_PET_TYPE: Int = 23
        const val EDIT_PET_TYPE: Int = 24
        const val SYNC_GLU_TYPE: Int = 31
        const val ADD_GLU_TYPE: Int = 32
        const val DELETE_GLU_TYPE: Int = 33
        const val EDIT_GLU_TYPE: Int = 34
        const val SYNC_NEW: Int = 41
        const val CREATE_NEW: Int = 42
        const val SYNC_INTRODUCTION = 43
    }

    // request success: 200, reload: 300, local error: 400, server error: 500

    // verifySN
    const val VERIFY_SUCCESS: Int = 200
    const val VERIFY_ERROR: Int = -405
    const val VERIFY_SN_IS_NULL: Int = -406
    const val VERIFY_SN_IS_EMPTY: Int = -407

    // createAccount
    const val CREATE_ACCOUNT_REQUEST_SEND_CERTIFICATION_EMAIL_SUCCESS: Int = 200
    const val CREATE_ACCOUNT_CONNECT_ERROR: Int = -401
    const val CREATE_ACCOUNT_EMAIL_FORMAT_ERROR: Int = -411
    const val CREATE_ACCOUNT_PASSWORD_FORMAT_ERROR: Int = -412
    const val CREATE_ACCOUNT_CONFIRM_FORMAT_ERROR: Int = -413
    const val CREATE_ACCOUNT_SERVER_ERROR: Int = -501
    const val CREATE_ACCOUNT_SEND_CERTIFICATION_EMAIL_FAIL: Int = -511

    // loginAccount
    const val LOGIN_REQUEST_SUCCESS: Int = 200
    const val LOGIN_REQUEST_SUCCESS_NEED_CREATE_PET: Int = 211
    const val LOGIN_SEND_CERTIFICATION_EMAIL_SUCCESS: Int = 212
    const val LOGIN_CONNECT_ERROR: Int = -401
    const val LOGIN_EMAIL_FORMAT_ERROR: Int = -414
    const val LOGIN_PASSWORD_FORMAT_ERROR: Int = -415
    const val LOGIN_SERVER_ERROR: Int = -501
    const val LOGIN_FIRE_BASE_AUTH_ERROR: Int = -512
    const val LOGIN_EMAIL_NOT_CERTIFIED: Int = -513
    const val LOGIN_SEND_CERTIFICATION_EMAIL_FAIL: Int = -514

    // profile
    const val PROFILE_REQUEST_SUCCESS: Int = 200
    const val PROFILE_REQUEST_GET_ACCOUNT_INFO_SUCCESS: Int = 213
    const val PROFILE_REQUEST_UPDATE_PASSWORD_SUCCESS: Int = 214
    const val PROFILE_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS: Int = 215
    const val PROFILE_CONNECT_ERROR: Int = -401
    const val PROFILE_NEW_PASSWORD_FORMAT_ERROR: Int = -417
    const val PROFILE_CONFIRM_FORMAT_ERROR: Int = -418
    const val PROFILE_SERVER_ERROR: Int = -501
    const val PROFILE_READ_ACCOUNT_DATA_FAIL: Int = -515
    const val PROFILE_RE_AUTH_FAIL: Int = -516
    const val PROFILE_UPDATE_PASSWORD_FAIL: Int = -517
    const val PROFILE_AVATAR_PHOTO_UPLOAD_FAIL: Int = -518

    // createPet
    const val CREATE_PET_REQUEST_SUCCESS: Int = 200
    const val CREATE_PET_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS: Int = 221
    const val CREATE_PET_CONNECT_ERROR: Int = -401
    const val CREATE_PET_NAME_FORMAT_ERROR: Int = -421
    const val CREATE_PET_SPECIES_FORMAT_ERROR: Int = -422
    const val CREATE_PET_GLUCOSE_UPPER_FORMAT_ERROR: Int = -423
    const val CREATE_PET_GLUCOSE_LOWER_FORMAT_ERROR: Int = -424
    const val CREATE_PET_UPPER_MORE_THAN_LOWER_ERROR: Int = -425
    const val CREATE_PET_BIRTHDAY_AT_FUTURE_ERROR: Int = -426
    const val CREATE_PET_SERVER_ERROR: Int = -501
    const val CREATE_PET_ALREADY_EXIST: Int = -521
    const val CREATE_PET_SERVER_HAVE_NO_DATA: Int = -522
    const val CREATE_PET_AVATAR_PHOTO_UPLOAD_FAIL: Int = -523

    // pickPet
    const val SELECT_PET_PICK_REQUEST_SUCCESS: Int = 200
    const val SELECT_PET_PICK_SERVER_ERROR: Int = -501

    // deletePet
    const val SELECT_PET_DELETE_REQUEST_SUCCESS: Int = 200
    const val SELECT_PET_DELETE_CURRENT_PET_ERROR: Int = -431
    const val SELECT_PET_DELETE_SERVER_ERROR: Int = -501
    const val SELECT_PET_DELETE_NOT_EXIST: Int = -531
    const val SELECT_PET_DELETE_PET_INFO_ERROR: Int = -532

    // editPet
    const val EDIT_PET_REQUEST_SUCCESS: Int = 200
    const val EDIT_PET_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS: Int = 241
    const val EDIT_PET_CONNECT_ERROR: Int = -401
    const val EDIT_PET_NAME_FORMAT_ERROR: Int = -441
    const val EDIT_PET_SPECIES_FORMAT_ERROR: Int = -442
    const val EDIT_PET_GLUCOSE_UPPER_FORMAT_ERROR: Int = -443
    const val EDIT_PET_GLUCOSE_LOWER_FORMAT_ERROR: Int = -444
    const val EDIT_PET_UPPER_MORE_THAN_LOWER_ERROR: Int = -445
    const val EDIT_PET_BIRTHDAY_AT_FUTURE_ERROR: Int = -446
    const val EDIT_PET_SERVER_ERROR: Int = -501
    const val EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL: Int = -541

    // glucoseLogSync
    const val GLUCOSE_LOG_SYNC_REQUEST_SUCCESS: Int = 200
    const val GLUCOSE_LOG_SYNC_SERVER_ERROR: Int = -501

    // glucoseLogAdd
    const val GLUCOSE_LOG_ADD_REQUEST_SUCCESS: Int = 200
    const val GLUCOSE_LOG_ADD_CONNECT_ERROR: Int = -401
    const val GLUCOSE_LOG_ADD_VALUE_FORMAT_ERROR: Int = -451
    const val GLUCOSE_LOG_ADD_SERVER_ERROR: Int = -501

    // glucoseLogDelete
    const val GLUCOSE_LOG_DELETE_REQUEST_SUCCESS: Int = 200
    const val GLUCOSE_LOG_DELETE_CONNECT_ERROR: Int = -401
    const val GLUCOSE_LOG_DELETE_SERVER_ERROR: Int = -501

    // glucoseLogEdit
    const val GLUCOSE_LOG_EDIT_REQUEST_SUCCESS: Int = 200
    const val GLUCOSE_LOG_EDIT_CONNECT_ERROR: Int = -401
    const val GLUCOSE_LOG_EDIT_VALUE_FORMAT_ERROR: Int = -452
    const val GLUCOSE_LOG_EDIT_SERVER_ERROR: Int = -501

    // syncNewsList
    const val NEWS_LIST_SYNC_REQUEST_SUCCESS: Int = 200
    const val NEWS_LIST_SYNC_CONNECT_ERROR: Int = -401
    const val NEWS_LIST_SYNC_SERVER_ERROR: Int = -501

    // createNewsList
    const val CREATE_NEWS_REQUEST_SUCCESS: Int = 200
    const val CREATE_NEWS_REQUEST_PHOTO_UPLOAD_SUCCESS: Int = 261
    const val CREATE_NEWS_CONNECT_ERROR: Int = -401
    const val CREATE_NEWS_TITLE_FORMAT_ERROR: Int = -461
    const val CREATE_NEWS_DATE_FORMAT_ERROR: Int = -462
    const val CREATE_NEWS_IMAGE_HAVE_NO_DATA: Int = -463
    const val CREATE_NEWS_SERVER_ERROR: Int = -501
    const val CREATE_NEWS_IMAGE_UPLOAD_FAIL: Int = -561
    const val CREATE_NEWS_SERVER_SAVE_FAIL: Int = -562

    // introduction
    const val INTRODUCTION_LIST_SYNC_REQUEST_SUCCESS: Int = 200
    const val INTRODUCTION_LIST_SYNC_CONNECT_ERROR: Int = -401
    const val INTRODUCTION_LIST_SYNC_SERVER_ERROR: Int = -501

    // setting
    const val SETTING_BLE_REQUEST_SUCCESS: Int = 200
    const val SETTING_BLE_VERIFY_SN_SUCCESS: Int = 205
    const val SETTING_BLE_NON_AVAILABLE: Int = -401
    const val SETTING_BLE_LOCAL_PERMISSION_NOT_GRANTED: Int = -402
    const val SETTING_BLE_NOT_ENABLE: Int = -403
    const val SETTING_BLE_SERVICE_NOT_ENABLE: Int = -404
    const val SETTING_BLE_SEND_SN_FAIL: Int = -505
    const val SETTING_BLE_VERIFY_SN_FAIL: Int = -506
    const val SETTING_BLE_SCAN_WIFI_FAIL: Int = -507
    const val SETTING_BLE_SEND_TOKEN_FAIL: Int = -508
}