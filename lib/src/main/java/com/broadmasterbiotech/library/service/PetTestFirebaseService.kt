package com.broadmasterbiotech.library.service

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class PetTestFirebaseService: FirebaseMessagingService() {

    companion object {
        val TAG = PetTestFirebaseService::class.java.simpleName
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if(remoteMessage.notification != null) {
            Log.d(TAG, "title:${remoteMessage.notification?.title}, body:${remoteMessage.notification?.body}")
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "onNewToken:$token")
    }
}