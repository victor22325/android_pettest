package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.NewsListSyncEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class NewsViewModel(application: Application): BaseViewModel(application) {

    companion object {
        val TAG = NewsViewModel::class.java.simpleName
    }

    var isProcessing = MutableLiveData<Boolean>()
    var syncNewsLiveEvent = SingleLiveEvent<BaseEvent>()

    fun syncCurrentNews() {
        isProcessing.value = true
        databaseReference.child("info").child("news").addValueEventListener(newsValueEventListener)
    }

    private val newsValueEventListener = object : ValueEventListener {
        override fun onCancelled(databaseError: DatabaseError) {
            syncNewsLiveEvent.value = createSyncNewsEvent(Code.NEWS_LIST_SYNC_SERVER_ERROR)
            Log.d(TAG, "newsValueEventListener onCancelled:${databaseError.message}")
        }

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val newsInfoList = ArrayList<NewsInfo>()
            val newsCount = dataSnapshot.childrenCount
            val newsIterator = dataSnapshot.children.iterator()
            for(i in 0 until newsCount) {
                val newsSnap = newsIterator.next()
                val newsInfo = NewsInfo(
                    title = newsSnap.child("title").value.toString(),
                    subTitle = newsSnap.child("subTitle").value.toString(),
                    content = newsSnap.child("content").value.toString(),
                    thumbnailUri = newsSnap.child("thumbnailUri").value.toString(),
                    contentUri = newsSnap.child("contentUri").value.toString(),
                    date = newsSnap.child("date").value.toString(),
                    updateTime = newsSnap.child("updateTime").value.toString().toLong()
                )
                newsInfoList.add(newsInfo)
            }
            syncNewsLiveEvent.value = createSyncNewsEvent(status = Code.NEWS_LIST_SYNC_REQUEST_SUCCESS, newsInfoList = newsInfoList)
        }
    }

    private fun createSyncNewsEvent(status: Int, message: String? = null, newsInfoList: ArrayList<NewsInfo>? = null): BaseEvent {
        return when(status) {
            Code.NEWS_LIST_SYNC_REQUEST_SUCCESS -> NewsListSyncEvent.Success(status = status, newsInfoList = newsInfoList!!)
            Code.NEWS_LIST_SYNC_CONNECT_ERROR -> NewsListSyncEvent.ConnectError(status = status)
            Code.NEWS_LIST_SYNC_SERVER_ERROR -> NewsListSyncEvent.ServerError(status = status, message = message)
            else -> NewsListSyncEvent.OtherError(status = status)
        }
    }
}