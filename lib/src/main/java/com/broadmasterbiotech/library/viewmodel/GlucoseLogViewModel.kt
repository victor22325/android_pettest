package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.event.*
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlin.collections.ArrayList

class GlucoseLogViewModel(application: Application) : BaseViewModel(application) {

    companion object {

        private val TAG = GlucoseLogViewModel::class.java.simpleName
    }

    var isBusy = MutableLiveData<Boolean>()
    var glucoseLogSyncLiveEvent = SingleLiveEvent<BaseEvent>()
    var createGlucoseValue = SingleLiveEvent<String>()
    var glucoseLogAddLiveEvent = SingleLiveEvent<BaseEvent>()
    var glucoseLogDeleteEvent = SingleLiveEvent<BaseEvent>()
    var editGlucoseValue = SingleLiveEvent<String>()
    var glucoseLogEditEvent = SingleLiveEvent<BaseEvent>()

    init {
        isBusy.value = true
        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).child("data").addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d(TAG, "syncGlucose onCancelled :${databaseError.message}")
                glucoseLogSyncLiveEvent.value = createGlucoseLogSyncEvent(Code.GLUCOSE_LOG_SYNC_SERVER_ERROR, databaseError.message)
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val glucoseInfoList = ArrayList<GlucoseInfo>()
                val yearCount = dataSnapshot.childrenCount
                val yearIterator = dataSnapshot.children.iterator()
                for(year in  0 until yearCount) {
                    val yearSnap = yearIterator.next()
                    val monthCount = dataSnapshot.child(yearSnap.key!!).childrenCount
                    val monthIterator = dataSnapshot.child(yearSnap.key!!).children.iterator()
                    for(month in 0 until monthCount) {
                        val monthSnap = monthIterator.next()
                        val dayCount = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).childrenCount
                        val dayIterator = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).children.iterator()
                        for(day in 0 until dayCount) {
                            val daySnap = dayIterator.next()
                            val dataCount =  dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).child(daySnap.key!!).childrenCount
                            val dataIterator = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).child(daySnap.key!!).children.iterator()
                            for(data in 0 until dataCount) {
                                val dataSnap = dataIterator.next()
                                val glucoseInfo = GlucoseInfo(
                                    createTime = dataSnap.child("createTime").value.toString().toLong(),
                                    year = dataSnap.child("year").value.toString(),
                                    month = dataSnap.child("month").value.toString(),
                                    day = dataSnap.child("day").value.toString(),
                                    hour = dataSnap.child("hour").value.toString(),
                                    minute = dataSnap.child("minute").value.toString(),
                                    value = dataSnap.child("value").value.toString().toInt(),
                                    updateTime = dataSnap.child("updateTime").value.toString().toLong()
                                )
                                glucoseInfoList.add(glucoseInfo)
                            }
                        }
                    }
                }
                petTestRepository.glucoseListLiveEvent.value = glucoseInfoList
                glucoseLogSyncLiveEvent.value = createGlucoseLogSyncEvent(Code.GLUCOSE_LOG_SYNC_REQUEST_SUCCESS, glucoseInfoList = glucoseInfoList)
            }
        })
    }

    private fun createGlucoseLogSyncEvent(status: Int, message: String? = null, glucoseInfoList: ArrayList<GlucoseInfo>? = null): BaseEvent {
        return when(status) {
            Code.GLUCOSE_LOG_SYNC_REQUEST_SUCCESS -> GlucoseLogSyncEvent.Success(status = Code.GLUCOSE_LOG_SYNC_REQUEST_SUCCESS, glucoseInfoList = glucoseInfoList)
            Code.GLUCOSE_LOG_SYNC_SERVER_ERROR -> GlucoseLogSyncEvent.ServerError(status = Code.GLUCOSE_LOG_SYNC_SERVER_ERROR, message = message)
            else -> GlucoseLogSyncEvent.OtherError(status = status)
        }
    }

    fun addGlucoseInsertOnClick(datePicker: DatePicker, timePicker: TimePicker) {
        val year: String = datePicker.year.toString()
        val month: String = if((datePicker.month + 1) < 10) {
            "0${datePicker.month + 1}"
        } else {
            (datePicker.month + 1).toString()
        }
        val day: String = if(datePicker.dayOfMonth < 10) {
            "0" + datePicker.dayOfMonth
        } else {
            datePicker.dayOfMonth.toString()
        }
        val hour: String = if(timePicker.hour < 10) {
            "0" + timePicker.hour
        } else {
            timePicker.hour.toString()
        }
        val minute: String = if(timePicker.minute < 10) {
            "0" + timePicker.minute
        } else {
            timePicker.minute.toString()
        }
        if(createGlucoseValue.value!=null && createGlucoseValue.value?.isNotEmpty()!!) {
            val glucoseInfo = GlucoseInfo(year = year, month = month, day = day, hour = hour, minute = minute, value = createGlucoseValue.value?.toInt()!!)
            Log.d(TAG, "addGlucoseInsertOnClick glucoseInfo:$glucoseInfo")
            databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).child("data").child(glucoseInfo.year).child(glucoseInfo.month).child(glucoseInfo.day).child(glucoseInfo.createTime.toString()).setValue(glucoseInfo).addOnCompleteListener { task->
                if(task.isSuccessful) {
                    petTestRepository.glucoseListLiveEvent.value?.add(glucoseInfo)
                    petTestRepository.glucoseListLiveEvent.value!!.sort()
                    glucoseLogAddLiveEvent.value = createGlucoseLogAddEvent(status = Code.GLUCOSE_LOG_ADD_REQUEST_SUCCESS, glucoseInfoList = petTestRepository.glucoseListLiveEvent.value)
                } else {
                    glucoseLogAddLiveEvent.value = createGlucoseLogAddEvent(Code.GLUCOSE_LOG_ADD_CONNECT_ERROR, task.exception?.message)
                    Log.d(TAG, "addGlucoseInfo is fail :${task.exception}")
                }
            }
        } else {
            glucoseLogAddLiveEvent.value = createGlucoseLogAddEvent(Code.GLUCOSE_LOG_ADD_VALUE_FORMAT_ERROR)
        }
    }

    private fun createGlucoseLogAddEvent(status: Int, message: String? = null, glucoseInfoList: ArrayList<GlucoseInfo>? = null): BaseEvent {
        return when(status) {
            Code.GLUCOSE_LOG_ADD_REQUEST_SUCCESS -> GlucoseLogAddEvent.Success(status = Code.GLUCOSE_LOG_ADD_REQUEST_SUCCESS, glucoseInfoList = glucoseInfoList!!)
            Code.GLUCOSE_LOG_ADD_CONNECT_ERROR -> GlucoseLogAddEvent.ConnectError(status = Code.GLUCOSE_LOG_ADD_CONNECT_ERROR)
            Code.GLUCOSE_LOG_ADD_VALUE_FORMAT_ERROR -> GlucoseLogAddEvent.GlucoseValueFormatError(status = Code.GLUCOSE_LOG_ADD_VALUE_FORMAT_ERROR)
            Code.GLUCOSE_LOG_ADD_SERVER_ERROR -> GlucoseLogAddEvent.ServerError(status = Code.GLUCOSE_LOG_ADD_SERVER_ERROR, message = message)
            else -> GlucoseLogAddEvent.OtherError(status = status)
        }
    }

    fun deleteGlucoseInfo(glucoseInfo: GlucoseInfo) {
        Log.d(TAG, "deleteGlucoseInfo$glucoseInfo")
        isBusy.value = true
        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d(TAG, "onCancelled databaseError:$databaseError")
                glucoseLogDeleteEvent.value = createGlucoseLogDeleteEvent(status = Code.GLUCOSE_LOG_DELETE_CONNECT_ERROR)
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.d(TAG, "onDataChange dataSnapshot:$dataSnapshot")
                if(dataSnapshot.child("data").child(glucoseInfo.year).child(glucoseInfo.month).child(glucoseInfo.day).child(glucoseInfo.createTime.toString()).exists()) {
                    databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).child("data").child(glucoseInfo.year).child(glucoseInfo.month).child(glucoseInfo.day).child(glucoseInfo.createTime.toString()).removeValue().addOnCompleteListener { task->
                        if(task.isSuccessful) {
                            petTestRepository.glucoseListLiveEvent.value?.remove(glucoseInfo)
                            petTestRepository.glucoseListLiveEvent.value!!.sort()
                            glucoseLogDeleteEvent.value = createGlucoseLogDeleteEvent(status = Code.GLUCOSE_LOG_DELETE_REQUEST_SUCCESS, deletedGlucoseInfo = glucoseInfo, glucoseInfoList = petTestRepository.glucoseListLiveEvent.value)
                        } else {
                            Log.d(TAG, "delete glucose is fail: ${task.exception?.message}")
                            glucoseLogDeleteEvent.value = createGlucoseLogDeleteEvent(status = Code.GLUCOSE_LOG_DELETE_SERVER_ERROR, message = task.exception?.message)
                        }
                    }
                } else {
                    Log.d(TAG, "delete glucose is fail: server folder is not exist")
                    glucoseLogDeleteEvent.value = createGlucoseLogDeleteEvent(status = Code.GLUCOSE_LOG_DELETE_SERVER_ERROR, message = "delete glucose is fail. (server folder is not exist)")
                }
            }
        })
    }

    private fun createGlucoseLogDeleteEvent(status: Int, message: String? = null, deletedGlucoseInfo: GlucoseInfo? = null, glucoseInfoList: ArrayList<GlucoseInfo>? = null): BaseEvent {
        return when(status) {
            Code.GLUCOSE_LOG_DELETE_REQUEST_SUCCESS -> GlucoseLogDeleteEvent.Success(status = Code.GLUCOSE_LOG_DELETE_REQUEST_SUCCESS, deletedGlucoseInfo = deletedGlucoseInfo!!, glucoseInfoList = glucoseInfoList!!)
            Code.GLUCOSE_LOG_DELETE_CONNECT_ERROR -> GlucoseLogDeleteEvent.ConnectError(status = Code.GLUCOSE_LOG_DELETE_CONNECT_ERROR)
            Code.GLUCOSE_LOG_DELETE_SERVER_ERROR -> GlucoseLogDeleteEvent.ServerError(status = Code.GLUCOSE_LOG_DELETE_SERVER_ERROR, message = message)
            else -> GlucoseLogDeleteEvent.OtherError(status = status)
        }
    }

    fun editGlucoseSaveOnClick(editGlucoseInfo: GlucoseInfo, datePicker: DatePicker, timePicker: TimePicker) {
        val year: String = datePicker.year.toString()
        val month: String = if((datePicker.month + 1) < 10) {
            "0${(datePicker.month + 1)}"
        } else {
            (datePicker.month + 1).toString()
        }
        val day: String = if(datePicker.dayOfMonth < 10) {
            "0" + datePicker.dayOfMonth
        } else {
            datePicker.dayOfMonth.toString()
        }
        val hour: String = if(timePicker.hour < 10) {
            "0" + timePicker.hour
        } else {
            timePicker.hour.toString()
        }
        val minute: String = if(timePicker.minute < 10) {
            "0" + timePicker.minute
        } else {
            timePicker.minute.toString()
        }
        if(editGlucoseValue.value != null && editGlucoseValue.value?.isNotEmpty()!!) {
            val glucoseInfo = GlucoseInfo(createTime = editGlucoseInfo.createTime, year = year, month = month, day = day, hour = hour, minute = minute, value = editGlucoseValue.value!!.toInt())
            Log.d(TAG, "editGlucoseSaveOnClick glucoseInfo:$glucoseInfo")
            if(editGlucoseInfo.year == year && editGlucoseInfo.month == month && editGlucoseInfo.day == day && editGlucoseInfo.hour == hour && editGlucoseInfo.minute == minute) {
                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).child("data").child(editGlucoseInfo.year).child(editGlucoseInfo.month).child(editGlucoseInfo.day).child(editGlucoseInfo.createTime.toString()).setValue(glucoseInfo).addOnCompleteListener { task->
                    if(task.isSuccessful) {
                        for(i in 0 until petTestRepository.glucoseListLiveEvent.value!!.size) {
                            if(petTestRepository.glucoseListLiveEvent.value!![i].createTime?.equals(glucoseInfo.createTime)!!) {
                                petTestRepository.glucoseListLiveEvent.value!![i] = glucoseInfo
                                break
                            }
                        }
                        glucoseLogEditEvent.value = createGlucoseLogEditEvent(status = Code.GLUCOSE_LOG_EDIT_REQUEST_SUCCESS, editGlucoseInfo = editGlucoseInfo, glucoseInfoList = petTestRepository.glucoseListLiveEvent.value)
                    } else {
                        glucoseLogEditEvent.value = createGlucoseLogEditEvent(Code.GLUCOSE_LOG_EDIT_SERVER_ERROR, task.exception?.message)
                        Log.d(TAG, "editGlucoseInfo is fail :${task.exception}")
                    }
                }
            } else {
                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).child("data").child(editGlucoseInfo.year).child(editGlucoseInfo.month).child(editGlucoseInfo.day).child(editGlucoseInfo.createTime.toString()).removeValue().addOnCompleteListener { task->
                    if(task.isSuccessful) {
                        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).child("data").child(glucoseInfo.year).child(glucoseInfo.month).child(glucoseInfo.day).child(glucoseInfo.createTime.toString()).setValue(glucoseInfo).addOnCompleteListener { mission ->
                            if(mission.isSuccessful) {
                                for(i in 0 until petTestRepository.glucoseListLiveEvent.value!!.size) {
                                    if(petTestRepository.glucoseListLiveEvent.value!![i].createTime?.equals(glucoseInfo.createTime)!!) {
                                        petTestRepository.glucoseListLiveEvent.value!![i] = glucoseInfo
                                        break
                                    }
                                }
                                petTestRepository.glucoseListLiveEvent.value!!.sort()
                                glucoseLogEditEvent.value = createGlucoseLogEditEvent(status = Code.GLUCOSE_LOG_EDIT_REQUEST_SUCCESS, editGlucoseInfo = editGlucoseInfo, glucoseInfoList = petTestRepository.glucoseListLiveEvent.value)
                            } else {
                                glucoseLogEditEvent.value = createGlucoseLogEditEvent(Code.GLUCOSE_LOG_EDIT_SERVER_ERROR, task.exception?.message)
                                Log.d(TAG, "editGlucoseInfo  add ${glucoseInfo.createTime} is fail (${mission.exception?.message})")
                            }
                        }
                    } else {
                        glucoseLogEditEvent.value = createGlucoseLogEditEvent(Code.GLUCOSE_LOG_EDIT_SERVER_ERROR, task.exception?.message)
                        Log.d(TAG, "editGlucoseInfo  delete ${editGlucoseInfo.createTime} is fail (${task.exception?.message})")
                    }
                }
            }
        } else {
            glucoseLogEditEvent.value = createGlucoseLogEditEvent(Code.GLUCOSE_LOG_EDIT_VALUE_FORMAT_ERROR)
        }
    }

    private fun createGlucoseLogEditEvent(status: Int, message: String? = null, editGlucoseInfo: GlucoseInfo? = null, glucoseInfoList: ArrayList<GlucoseInfo>? = null): BaseEvent {
        return when(status) {
            Code.GLUCOSE_LOG_EDIT_REQUEST_SUCCESS -> GlucoseLogEditEvent.Success(status = Code.GLUCOSE_LOG_EDIT_REQUEST_SUCCESS, editGlucoseInfo =  editGlucoseInfo, glucoseInfoList = glucoseInfoList)
            Code.GLUCOSE_LOG_EDIT_CONNECT_ERROR -> GlucoseLogEditEvent.ConnectError(status = Code.GLUCOSE_LOG_EDIT_CONNECT_ERROR)
            Code.GLUCOSE_LOG_EDIT_VALUE_FORMAT_ERROR -> GlucoseLogEditEvent.GlucoseValueFormatError(status = Code.GLUCOSE_LOG_EDIT_VALUE_FORMAT_ERROR)
            Code.GLUCOSE_LOG_EDIT_SERVER_ERROR -> GlucoseLogEditEvent.ServerError(status = Code.GLUCOSE_LOG_EDIT_SERVER_ERROR, message = message)
            else -> GlucoseLogEditEvent.OtherError(status =status)
        }
    }
}