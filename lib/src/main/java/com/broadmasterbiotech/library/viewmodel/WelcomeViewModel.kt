package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.broadmasterbiotech.library.util.SingleLiveEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class WelcomeViewModel(application: Application) : AndroidViewModel(application) {

    companion object {

        private val TAG = WelcomeViewModel::class.java.simpleName
    }

    val toNextPage = SingleLiveEvent<Boolean>()

    private var disposable: Disposable? = null

    fun startGoToNextPage() {
        Log.d(TAG, "startGoToNextPage")
        disposable = Observable
            .just(true)
            .delay(4500, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { aBoolean -> toNextPage.setValue(aBoolean) }
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.let { if (!it.isDisposed) it.dispose()}
    }
}