package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.PetTestRepository
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage


abstract class BaseViewModel(application: Application): AndroidViewModel(application) {

    companion object {

        val TAG = BaseViewModel::class.simpleName
    }

    var petTestRepository: PetTestRepository = PetTestRepository.providePetTestRepository(application.applicationContext)

    var mFireBaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    var databaseReference = FirebaseDatabase.getInstance().reference

    var mFireBaseStorage: FirebaseStorage = FirebaseStorage.getInstance()

    var storageReference = FirebaseStorage.getInstance().reference

    fun logout() {
        if(mFireBaseAuth.currentUser!=null) {
            mFireBaseAuth.signOut()
        }
    }
}