package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.IntroductionSyncEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class IntroductionViewModel(application: Application): BaseViewModel(application) {

    companion object {
        val TAG = IntroductionViewModel::class.java.simpleName
    }

    var isProcessing = MutableLiveData<Boolean>(false)
    var syncIntroductionLiveEvent = SingleLiveEvent<BaseEvent>()

    fun syncInstructions() {
        isProcessing.value = true
        databaseReference.child("info").child("introduction").addValueEventListener(introductionListValueEventListener)
    }

    private val introductionListValueEventListener = object : ValueEventListener {
        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "introductionListValueEventListener onCancelled:${databaseError.message}")
        }

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val newsInfoList = ArrayList<NewsInfo>()
            val newsCount = dataSnapshot.childrenCount
            val newsIterator = dataSnapshot.children.iterator()
            for(i in 0 until newsCount) {
                val newsSnap = newsIterator.next()
                val newsInfo = NewsInfo(
                    title = newsSnap.child("title").value.toString(),
                    subTitle = newsSnap.child("subTitle").value.toString(),
                    content = newsSnap.child("content").value.toString(),
                    thumbnailUri = newsSnap.child("thumbnailUri").value.toString(),
                    contentUri = newsSnap.child("contentUri").value.toString(),
                    date = newsSnap.child("date").value.toString(),
                    updateTime = newsSnap.child("updateTime").value.toString().toLong()
                )
                newsInfoList.add(newsInfo)
            }
            syncIntroductionLiveEvent.value = createSyncIntroductionEvent(status = Code.INTRODUCTION_LIST_SYNC_REQUEST_SUCCESS, newsInfoList = newsInfoList)
        }
    }

    private fun createSyncIntroductionEvent(status: Int, message: String? = null, newsInfoList: ArrayList<NewsInfo>? = null): BaseEvent {
        return when(status) {
            Code.INTRODUCTION_LIST_SYNC_REQUEST_SUCCESS -> IntroductionSyncEvent.Success(status = status, introductionList = newsInfoList!!)
            Code.INTRODUCTION_LIST_SYNC_CONNECT_ERROR -> IntroductionSyncEvent.ConnectError(status = status)
            Code.INTRODUCTION_LIST_SYNC_SERVER_ERROR -> IntroductionSyncEvent.ServerError(status = status, message = message)
            else -> IntroductionSyncEvent.OtherError(status = status)
        }
    }
}