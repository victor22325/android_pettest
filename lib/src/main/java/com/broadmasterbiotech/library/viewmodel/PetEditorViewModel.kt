package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.PetTestRepository
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.EditPetEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.StorageMetadata
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import kotlin.collections.ArrayList

class PetEditorViewModel(application: Application): BaseViewModel(application) {

    companion object {

        val TAG = PetEditorViewModel::class.java.simpleName
    }

    var isProcessing = MutableLiveData<Boolean>()
    var petName = SingleLiveEvent<String>()
    var petSpecies = SingleLiveEvent<Int>()
    var petBreed = SingleLiveEvent<String>()
    var petWeight = SingleLiveEvent<String>()
    var petWeightUnit = SingleLiveEvent<Int>()
    var petDiabetesType = SingleLiveEvent<Int>()
    var petDiabetesYears = SingleLiveEvent<String>()
    var petGender = SingleLiveEvent<Int>()
    var petBirthday = SingleLiveEvent<String>()
    var petGlucoseUpperLimit = SingleLiveEvent<String>()
    var petGlucoseLowerLimit = SingleLiveEvent<String>()
    var petAvatarPhotoURI = SingleLiveEvent<String>()
    var petIsSelected = SingleLiveEvent<Boolean>()

    var uploadFileName: String? = null
    var uploadImageData: ByteArray? = null
    var uploadMetadata: StorageMetadata? = null

    private var getUserPhotoDisposable: Disposable? = null

    var editPetLiveEvent = SingleLiveEvent<BaseEvent>()

    init {
        isProcessing.value = false
    }

    fun setPetInfo(petInfo: PetInfo) {
        petName.value = petInfo.name
        petSpecies.value = petInfo.species
        petBreed.value = petInfo.breed
        petWeight.value = petInfo.weight
        petWeightUnit.value = petInfo.weightUnit
        petDiabetesType.value = petInfo.diabetesType
        petDiabetesYears.value = petInfo.diabetesYearDiagnosed
        petGender.value = petInfo.gender
        petBirthday.value = petInfo.birth
        petGlucoseUpperLimit.value = petInfo.glucoseUpper
        petGlucoseLowerLimit.value = petInfo.glucoseLower
        petAvatarPhotoURI.value = petInfo.avatarPhoto
        petIsSelected.value = petInfo.isSelected
    }

    private fun isPetNameMatch():Boolean {
        if(petName.value!=null) {
            if(petName.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isSpeciesMatch():Boolean {
        if(petSpecies.value!=null) {
            return true
        }
        return false
    }

    private fun isGlucoseUpperMatch(): Boolean {
        if(petGlucoseUpperLimit.value!=null) {
            if(petGlucoseUpperLimit.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isGlucoseLowerMatch(): Boolean {
        if(petGlucoseLowerLimit.value!=null) {
            if(petGlucoseLowerLimit.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isBirthdayMatch(): Boolean {
        if(petBirthday.value!=null) {
            if(petBirthday.value!!.isNotEmpty()) {
                val splitList = petBirthday.value!!.split("/")
                val birthdayCalendar = Calendar.getInstance()
                birthdayCalendar.set(splitList[0].toInt(), splitList[1].toInt(), splitList[2].toInt())
                val nowCalendar = Calendar.getInstance()
                nowCalendar.time = Date(System.currentTimeMillis())
                return !birthdayCalendar.after(nowCalendar)
            }
        }
        return true
    }

    fun saveOnClick() {
        isProcessing.value = true
        if(isPetNameMatch()) {
            if(isSpeciesMatch()) {
                if(isGlucoseUpperMatch()){
                    if(isGlucoseLowerMatch()) {
                        if(petGlucoseUpperLimit.value!!.toInt() > petGlucoseLowerLimit.value!!.toInt()) {
                            if(isBirthdayMatch()) {
                                checkPhotoInfo(uploadFileName, uploadImageData, uploadMetadata)
                            } else {
                                editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_BIRTHDAY_AT_FUTURE_ERROR)
                            }
                        } else {
                            editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_UPPER_MORE_THAN_LOWER_ERROR)
                        }
                    } else {
                        editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_GLUCOSE_LOWER_FORMAT_ERROR)
                    }
                } else {
                    editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_GLUCOSE_UPPER_FORMAT_ERROR)
                }
            } else {
                editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_SPECIES_FORMAT_ERROR)
            }
        } else {
            editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_NAME_FORMAT_ERROR)
        }
    }

    private fun checkPhotoInfo(fileName: String?, imageData: ByteArray?, storageMetadata: StorageMetadata?) {
        if (fileName != null && imageData != null && storageMetadata != null) {
            storageReference.child("users").child(mFireBaseAuth.currentUser?.uid!!).child("pets")
                .child(petName.value!!).listAll().addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result?.items?.size!! > 0) {
                        var needUpload = true
                        for (i in 0 until it.result?.items?.size!!) {
                            if (fileName != it.result?.items?.get(i)?.name) {
                                Log.d(TAG, "checkPhotoInfo name[${i}]:${it.result?.items?.get(i)?.name} is delete")
                                it.result?.items?.get(i)?.delete()
                            } else {
                                needUpload = false
                            }
                        }
                        if (needUpload) {
                            uploadPhotoToFireBase(fileName, imageData, storageMetadata)
                        } else {
                            editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = "This photo has been uploaded.")
                        }
                    } else {
                        uploadPhotoToFireBase(fileName, imageData, storageMetadata)
                    }
                } else {
                    Log.d(TAG, "checkPhotoIsExist fail ${it.exception?.message}")
                    editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = it.exception?.message)
                }
            }
        } else {
            Log.d(TAG, "checkPhotoInfo avatar imageData is null")
            checkPetInfo()
        }
    }

    private fun uploadPhotoToFireBase(fileName: String, imageData: ByteArray, storageMetadata: StorageMetadata) {
        val filePath = storageReference.child("users").child(mFireBaseAuth.currentUser?.uid!!).child("pets").child(petName.value!!).child(fileName)
        filePath.putBytes(imageData, storageMetadata)
            .continueWith { task->
                if(!task.isSuccessful) {
                    Log.d(TAG, "uploadPhotoToFireBase continueWith downloadUrl task fail :${task.exception?.message}")
                }
                return@continueWith filePath.downloadUrl
            }
            .addOnCompleteListener{
                if(it.isSuccessful) {
                    filePath.downloadUrl.addOnCompleteListener { task->
                        if(task.isSuccessful) {
                            val userProfileChangeRequest = UserProfileChangeRequest.Builder().setPhotoUri(task.result).build()
                            mFireBaseAuth.currentUser?.updateProfile(userProfileChangeRequest)?.addOnCompleteListener { mission->
                                if(mission.isSuccessful) {
                                    databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("account").setValue(petTestRepository.accountInfo)
                                        .addOnCompleteListener { saveAccountTask->
                                            if(saveAccountTask.isSuccessful) {
                                                Log.d(TAG, "save avatarPhoto isSuccessful uri:${task.result?.toString()}")
                                                petAvatarPhotoURI.value = task.result?.toString()
                                                editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS, uri = task.result?.toString())
                                            } else {
                                                Log.d(TAG, "uploadPhotoToFireBase savePetUrl is fail")
                                                editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = saveAccountTask.exception?.message)
                                            }
                                        }
                                } else {
                                    Log.d(TAG, "uploadPhotoToFireBase upload fireBase account fail reason:${mission.exception?.message}")
                                    editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = mission.exception?.message)
                                }
                            }
                        } else {
                            Log.d(TAG, "uploadPhotoToFireBase downloadUrl fail reason:${task.exception?.message}")
                            editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = task.exception?.message)
                        }
                    }
                } else {
                    Log.d(TAG, "uploadPhotoToFireBase upload fail reason:${it.exception?.message}")
                    editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = it.exception?.message)
                }
            }
    }

    fun checkPetInfo() {
        isProcessing.value = true
        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petName.value!!).child("info").addListenerForSingleValueEvent(checkPetInfoListener)
    }

    private val checkPetInfoListener = object: ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "checkPetInfoListener onDataChange:${dataSnapshot.value}")
            val pet = PetInfo(name = petName.value!!,
                              species = petSpecies.value!!,
                              breed = petBreed.value,
                              weight = editNumOptimize(petWeight.value),
                              weightUnit = petWeightUnit.value,
                              diabetesType = petDiabetesType.value,
                              diabetesYearDiagnosed = editNumOptimize(petDiabetesYears.value),
                              gender = petGender.value,
                              birth = petBirthday.value, glucoseUpper = editNumOptimize(petGlucoseUpperLimit.value!!)!!,
                              glucoseLower = editNumOptimize(petGlucoseLowerLimit.value!!)!!,
                              avatarPhoto = petAvatarPhotoURI.value,
                              isSelected = petIsSelected.value)
            databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petName.value!!).child("info").setValue(pet.toMap())
                .addOnCompleteListener { result->
                    if(result.isSuccessful) {
                        val temp: ArrayList<PetInfo> = petTestRepository.petInfoListLiveEvent.value!!
                        for(i  in 0..petTestRepository.petInfoListLiveEvent.value?.size!!) {
                            if(petTestRepository.petInfoListLiveEvent.value!![i].name == pet.name) {
                                temp[i] = pet
                                break
                            }
                        }
                        petTestRepository.petInfoListLiveEvent.value = temp
                        editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_REQUEST_SUCCESS, petInfo = pet)
                    } else {
                        Log.d(TAG, "save ${petName.value}'s petName fail message:${result.exception?.message}")
                        editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_SERVER_ERROR, message = result.exception?.message)
                    }
                }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "checkPetInfoListener onCancelled:${databaseError.message}")
            editPetLiveEvent.value = createEditPetEvent(status = Code.EDIT_PET_SERVER_ERROR, message = databaseError.message)
        }
    }

    private fun editNumOptimize(num :String?): String? {
        return if (num !== null) {
            try {
                Integer.parseInt(num).toString()
            } catch (e: Exception) {
                null
            }
        } else {
            null
        }
    }

    private fun createEditPetEvent(status: Int, message: String? = null, uri: String? = null, petInfo: PetInfo? = null):BaseEvent {
        return when (status) {
            Code.EDIT_PET_REQUEST_SUCCESS -> EditPetEvent.Success(status = status, petInfo = petInfo)
            Code.EDIT_PET_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS -> EditPetEvent.Success(status = status, uri = uri)
            Code.EDIT_PET_CONNECT_ERROR -> EditPetEvent.ConnectError(status = Code.EDIT_PET_CONNECT_ERROR)
            Code.EDIT_PET_SERVER_ERROR -> EditPetEvent.ServerError(status = Code.EDIT_PET_SERVER_ERROR, message = message)
            Code.EDIT_PET_NAME_FORMAT_ERROR -> EditPetEvent.PetNameFormatError(status = status)
            Code.EDIT_PET_SPECIES_FORMAT_ERROR -> EditPetEvent.PetSpeciesFormatError(status = status)
            Code.EDIT_PET_GLUCOSE_UPPER_FORMAT_ERROR -> EditPetEvent.PetGlucoseUpperLimitFormatError(status = status)
            Code.EDIT_PET_GLUCOSE_LOWER_FORMAT_ERROR -> EditPetEvent.PetGlucoseLowerLimitFormatError(status = status)
            Code.EDIT_PET_UPPER_MORE_THAN_LOWER_ERROR -> EditPetEvent.PetUpperMoreThanLowerError(status = status)
            Code.EDIT_PET_BIRTHDAY_AT_FUTURE_ERROR -> EditPetEvent.PetBirthdayAtFuture(status = status)
            Code.EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL -> EditPetEvent.UpdateAvatarPhotoFail(status = status, message = message)
            else -> EditPetEvent.OtherError(status)
        }
    }

    override fun onCleared() {
        super.onCleared()
        getUserPhotoDisposable?.let { if (!it.isDisposed) it.dispose()}
    }
}