package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.PetTestRepository
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.CreatePetEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.StorageMetadata
import java.util.*
import kotlin.collections.ArrayList

class CreatePetViewModel(application: Application): BaseViewModel(application) {

    companion object {

        private val TAG = CreatePetViewModel::class.java.simpleName
    }

    var isProcessing = MutableLiveData<Boolean>()
    var petName = SingleLiveEvent<String>()
    var petSpecies = SingleLiveEvent<Int>()
    var petBreed = SingleLiveEvent<String>()
    var petWeight = SingleLiveEvent<String>()
    var petWeightUnit = SingleLiveEvent<Int>()
    var petDiabetesType = SingleLiveEvent<Int>()
    var petDiabetesYears = SingleLiveEvent<String>()
    var petGender = SingleLiveEvent<Int>()
    var petBirthday = SingleLiveEvent<String>()
    var petGlucoseUpperLimit = SingleLiveEvent<String>()
    var petGlucoseLowerLimit = SingleLiveEvent<String>()
    var createPetLiveEvent = SingleLiveEvent<BaseEvent>()
    private var avatarPhotoURI: String? = null
    var uploadFileName: String? = null
    var uploadImageData: ByteArray? = null
    var uploadMetadata: StorageMetadata? = null

    init {
        isProcessing.value = false
        avatarPhotoURI = ""
    }

    private fun isPetNameMatch():Boolean {
        if(petName.value!=null) {
            if(petName.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isSpeciesMatch():Boolean {
        if(petSpecies.value!=null) {
            return true
        }
        return false
    }

    private fun isGlucoseUpperMatch(): Boolean {
        if(petGlucoseUpperLimit.value!=null) {
            if(petGlucoseUpperLimit.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isGlucoseLowerMatch(): Boolean {
        if(petGlucoseLowerLimit.value!=null) {
            if(petGlucoseLowerLimit.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isBirthdayMatch(): Boolean {
        if(petBirthday.value!=null) {
            if(petBirthday.value!!.isNotEmpty()) {
                val splitList = petBirthday.value!!.split("/")
                val birthdayCalendar = Calendar.getInstance()
                birthdayCalendar.set(splitList[0].toInt(), splitList[1].toInt(), splitList[2].toInt())
                val nowCalendar = Calendar.getInstance()
                nowCalendar.time = Date(System.currentTimeMillis())
                return !birthdayCalendar.after(nowCalendar)
            }
        }
        return true
    }

    fun createPetOnClick() {
        isProcessing.value = true
        if(isPetNameMatch()) {
            if(isSpeciesMatch()) {
                if(isGlucoseUpperMatch()){
                    if(isGlucoseLowerMatch()) {
                        if(petGlucoseUpperLimit.value!!.toInt() > petGlucoseLowerLimit.value!!.toInt()) {
                            if(isBirthdayMatch()) {
                                checkPhotoInfo(uploadFileName, uploadImageData, uploadMetadata)
                            } else {
                                createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_BIRTHDAY_AT_FUTURE_ERROR)
                            }
                        } else {
                            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_UPPER_MORE_THAN_LOWER_ERROR)
                        }
                    } else {
                        createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_GLUCOSE_LOWER_FORMAT_ERROR)
                    }
                } else {
                    createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_GLUCOSE_UPPER_FORMAT_ERROR)
                }
            } else {
                createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_SPECIES_FORMAT_ERROR)
            }
        } else {
            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_NAME_FORMAT_ERROR)
        }
    }

    private fun checkPhotoInfo(fileName: String?, imageData: ByteArray?, storageMetadata: StorageMetadata?){
        if(fileName!=null && imageData!=null && storageMetadata!=null) {
            storageReference.child("users").child(mFireBaseAuth.currentUser?.uid!!).child("pets").child(petName.value!!).listAll().addOnCompleteListener {
                if(it.isSuccessful) {
                    if(it.result?.items?.size!! > 0) {
                        var needUpload = true
                        for(i in 0 until it.result?.items?.size!!) {
                            if(fileName != it.result?.items?.get(i)?.name) {
                                Log.d(TAG, "checkPhotoInfo name[${i}]:${it.result?.items?.get(i)?.name} is delete")
                                it.result?.items?.get(i)?.delete()
                            } else {
                                needUpload = false
                            }
                        }
                        if(needUpload) {
                            uploadPhotoToFireBase(fileName, imageData, storageMetadata)
                        } else {
                            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = "This photo has been uploaded.")
                        }
                    } else {
                        uploadPhotoToFireBase(fileName, imageData, storageMetadata)
                    }
                } else {
                    Log.d(TAG,"checkPhotoIsExist fail ${it.exception?.message}")
                    createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = it.exception?.message)
                }
            }
        } else {
            Log.d(TAG, "checkPhotoInfo avatar imageData is null")
            checkPetInfo()
        }
    }

    private fun uploadPhotoToFireBase(fileName: String, imageData: ByteArray, storageMetadata: StorageMetadata) {
        val filePath = storageReference.child("users").child(mFireBaseAuth.currentUser?.uid!!).child("pets").child(petName.value!!).child(fileName)
        filePath.putBytes(imageData, storageMetadata)
            .continueWith { task->
                if(!task.isSuccessful) {
                    Log.d(TAG, "uploadPhotoToFireBase continueWith downloadUrl task fail :${task.exception?.message}")
                }
                return@continueWith filePath.downloadUrl
            }
            .addOnCompleteListener{
                if(it.isSuccessful) {
                    filePath.downloadUrl.addOnCompleteListener { task->
                        if(task.isSuccessful) {
                            avatarPhotoURI = task.result?.toString()
                            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS, uri = task.result?.toString())
                        } else {
                            Log.d(TAG, "uploadPhotoToFireBase downloadUrl fail reason:${task.exception?.message}")
                            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = task.exception?.message)
                        }
                    }
                } else {
                    Log.d(TAG, "uploadPhotoToFireBase upload fail reason:${it.exception?.message}")
                    createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_AVATAR_PHOTO_UPLOAD_FAIL, message = it.exception?.message)
                }
            }
    }

    fun checkPetInfo() {
        isProcessing.value = true
        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").addListenerForSingleValueEvent(checkPetInfoListener)
    }

    private val checkPetInfoListener = object: ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "checkPetInfoListener onDataChange${dataSnapshot.value}")
            if(dataSnapshot.childrenCount > 0) {
                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petName.value!!).child("info").addListenerForSingleValueEvent(createAnotherPetInfoListener)
            } else {
                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petName.value!!).child("info").addListenerForSingleValueEvent(createNewPetInfoListener)
            }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "checkPetInfoListener onCancelled:${databaseError.message}")
            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_SERVER_ERROR, message = databaseError.message)
        }

    }

    private val createNewPetInfoListener = object: ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "createNewPetInfoListener onDataChange:${dataSnapshot.value}")
            if(!dataSnapshot.child("name").exists()) {
                val pet = PetInfo(
                    name = petName.value!!,
                    species = petSpecies.value!!,
                    breed = petBreed.value,
                    weight = editNumOptimize(petWeight.value),
                    weightUnit = petWeightUnit.value,
                    diabetesType = petDiabetesType.value,
                    diabetesYearDiagnosed = editNumOptimize(petDiabetesYears.value),
                    gender = petGender.value,
                    birth = petBirthday.value,
                    glucoseUpper = editNumOptimize(petGlucoseUpperLimit.value!!)!!,
                    glucoseLower = editNumOptimize(petGlucoseLowerLimit.value!!)!!,
                    avatarPhoto = avatarPhotoURI,
                    isSelected = true)
                    databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petName.value!!).child("info").setValue(pet.toMap())
                    .addOnCompleteListener { result->
                        if(result.isSuccessful) {
                            petTestRepository.petInfoListLiveEvent.value?.clear()
                            petTestRepository.petInfoListLiveEvent.value?.add(pet)
                            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_REQUEST_SUCCESS)
                        } else {
                            Log.d(TAG, "create ${petName.value} is fail message:${result.exception?.message}")
                            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_SERVER_ERROR, message = result.exception?.message)
                        }
                    }
            } else {
                createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_ALREADY_EXIST)
            }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "createNewPetInfoListener onCancelled:${databaseError.details}")
            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_SERVER_ERROR, message = databaseError.message)
        }
    }

    private val createAnotherPetInfoListener = object: ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "createAnotherPetInfoListener onDataChange:${dataSnapshot.value}")
            if(!dataSnapshot.child("name").exists()) {
                val pet = PetInfo(
                    name = petName.value!!,
                    species = petSpecies.value!!,
                    breed = petBreed.value,
                    weight = editNumOptimize(petWeight.value),
                    weightUnit = petWeightUnit.value,
                    diabetesType = petDiabetesType.value,
                    diabetesYearDiagnosed = editNumOptimize(petDiabetesYears.value),
                    gender = petGender.value,
                    birth = petBirthday.value,
                    glucoseUpper = editNumOptimize(petGlucoseUpperLimit.value!!)!!,
                    glucoseLower = editNumOptimize(petGlucoseLowerLimit.value!!)!!,
                    avatarPhoto = avatarPhotoURI,
                    isSelected = false)
                    databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petName.value!!).child("info").setValue(pet.toMap())
                    .addOnCompleteListener { result->
                        if(result.isSuccessful) {
                            petTestRepository.petInfoListLiveEvent.value?.add(pet)
                            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_REQUEST_SUCCESS, petInfoList = petTestRepository.petInfoListLiveEvent.value)
                        } else {
                            Log.d(TAG, "create ${petName.value} is fail message:${result.exception?.message}")
                            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_SERVER_ERROR, message = result.exception?.message)
                        }
                    }
            } else {
                createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_ALREADY_EXIST)
            }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "createAnotherPetInfoListener onCancelled:${databaseError.details}")
            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_SERVER_ERROR, message = databaseError.message)
        }
    }

    fun cancelOnClick() {
        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").addListenerForSingleValueEvent(cancelPetInfoListener)
    }

    private val cancelPetInfoListener = object: ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "cancelPetInfoListener onDataChange:${dataSnapshot.value}")
            if(dataSnapshot.value==null) {
                createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_SERVER_HAVE_NO_DATA)
            } else {
                createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_REQUEST_SUCCESS)
            }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "cancelPetInfoListener onCancelled:${databaseError.details}")
            createPetLiveEvent.value = createCreatePetEvent(status = Code.CREATE_PET_SERVER_ERROR, message = databaseError.message)
        }

    }

    private fun editNumOptimize(num :String?): String? {
        return if (num !== null) {
            try {
                Integer.parseInt(num).toString()
            } catch (e: Exception) {
                null
            }
        } else {
            null
        }
    }

    private fun createCreatePetEvent(status: Int, message: String? = null, uri: String? = null, petInfoList: ArrayList<PetInfo>? = null):BaseEvent {
        return when (status) {
            Code.CREATE_PET_REQUEST_SUCCESS -> CreatePetEvent.Success(status = status, petInfoList = petInfoList)
            Code.CREATE_PET_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS -> CreatePetEvent.Success(status = status, uri = uri)
            Code.CREATE_PET_CONNECT_ERROR -> CreatePetEvent.ConnectError(status = status)
            Code.CREATE_PET_SERVER_ERROR -> CreatePetEvent.ServerError(status = status, message = message)
            Code.CREATE_PET_NAME_FORMAT_ERROR -> CreatePetEvent.PetNameFormatError(status = status)
            Code.CREATE_PET_SPECIES_FORMAT_ERROR -> CreatePetEvent.PetSpeciesFormatError(status = status)
            Code.CREATE_PET_GLUCOSE_UPPER_FORMAT_ERROR -> CreatePetEvent.PetGlucoseUpperLimitFormatError(status = status)
            Code.CREATE_PET_GLUCOSE_LOWER_FORMAT_ERROR -> CreatePetEvent.PetGlucoseLowerLimitFormatError(status = status)
            Code.CREATE_PET_UPPER_MORE_THAN_LOWER_ERROR -> CreatePetEvent.PetUpperMoreThanLowerError(status = status)
            Code.CREATE_PET_BIRTHDAY_AT_FUTURE_ERROR -> CreatePetEvent.PetBirthdayAtFutureError(status = status)
            Code.CREATE_PET_ALREADY_EXIST -> CreatePetEvent.PetAlreadyExist(status = status)
            Code.CREATE_PET_SERVER_HAVE_NO_DATA -> CreatePetEvent.ServerHaveNoData(status = status)
            Code.CREATE_PET_AVATAR_PHOTO_UPLOAD_FAIL -> CreatePetEvent.UpdateAvatarPhotoFail(status = status, message = message)
            else -> CreatePetEvent.OtherError(status)
        }
    }

    override fun onCleared() {
        super.onCleared()
        databaseReference.removeEventListener(checkPetInfoListener)
        databaseReference.removeEventListener(createNewPetInfoListener)
        databaseReference.removeEventListener(createAnotherPetInfoListener)
        databaseReference.removeEventListener(cancelPetInfoListener)
    }

}