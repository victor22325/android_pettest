package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.CreateNewsEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.StorageMetadata
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CreateNewsViewModel(application: Application): BaseViewModel(application) {

    companion object {
        val TAG = CreateAccountViewModel::class.java.simpleName
    }

    var isProcessing = MutableLiveData<Boolean>()
    var newsTitleSingleLiveEvent = SingleLiveEvent<String>()
    var newsSubTitleSingleLiveEvent = SingleLiveEvent<String>()
    var newsContentSingleLiveEvent = SingleLiveEvent<String>()
    var newsDateSingleLiveEvent = SingleLiveEvent<String>()
    var uploadFileName: String? = null
    var uploadImageData: ByteArray? = null
    var uploadMetadata: StorageMetadata? = null
    private var thumbnailUri: String? = null
    private var contentUri: String? = null
    var createNewsLiveEvent = SingleLiveEvent<BaseEvent>()

    init {
        isProcessing.value = false
        thumbnailUri = ""
        contentUri = ""
    }

    fun createNewsOnClick() {
        isProcessing.value = true
        if (isTitleMatch()) {
            if (isDateMatch()) {
                checkPhotoInfo(uploadFileName, uploadImageData, uploadMetadata)
            } else {
                createNewsLiveEvent.value = createCreateNewsEvent(Code.CREATE_NEWS_DATE_FORMAT_ERROR)
            }
        } else {
            createNewsLiveEvent.value = createCreateNewsEvent(Code.CREATE_NEWS_TITLE_FORMAT_ERROR)
        }
    }

    private fun checkPhotoInfo(fileName: String?, imageData: ByteArray?, storageMetadata: StorageMetadata?){
        if(fileName!=null && imageData!=null && storageMetadata!=null) {
            storageReference.child("info").child("news").listAll().addOnCompleteListener {
                if(it.isSuccessful) {
                    uploadPhotoToFireBase(fileName, imageData, storageMetadata)
                } else {
                    Log.d(TAG,"checkPhotoIsExist fail ${it.exception?.message}")
                    createNewsLiveEvent.value = createCreateNewsEvent(status = Code.CREATE_NEWS_CONNECT_ERROR)
                }
            }
        } else {
            Log.d(TAG, "checkPhotoInfo avatar imageData is null")
            createNewsLiveEvent.value = createCreateNewsEvent(status = Code.CREATE_NEWS_IMAGE_HAVE_NO_DATA)
        }
    }

    private fun uploadPhotoToFireBase(fileName: String, imageData: ByteArray, storageMetadata: StorageMetadata) {
        val filePath = storageReference.child("info").child("news").child(fileName)
        filePath.putBytes(imageData, storageMetadata)
            .continueWith { task->
                if(!task.isSuccessful) {
                    Log.d(TAG, "uploadPhotoToFireBase continueWith downloadUrl task fail :${task.exception?.message}")
                }
                return@continueWith filePath.downloadUrl
            }
            .addOnCompleteListener{
                if(it.isSuccessful) {
                    filePath.downloadUrl.addOnCompleteListener { task->
                        if(task.isSuccessful) {
                            thumbnailUri = task.result?.toString()
                            createNewsLiveEvent.value = createCreateNewsEvent(status = Code.CREATE_NEWS_REQUEST_PHOTO_UPLOAD_SUCCESS)
                        } else {
                            Log.d(TAG, "uploadPhotoToFireBase downloadUrl fail reason:${task.exception?.message}")
                            createNewsLiveEvent.value = createCreateNewsEvent(
                                status = Code.CREATE_NEWS_IMAGE_UPLOAD_FAIL,
                                message = task.exception?.message
                            )
                        }
                    }
                } else {
                    Log.d(TAG, "uploadPhotoToFireBase upload fail reason:${it.exception?.message}")
                    createNewsLiveEvent.value = createCreateNewsEvent(
                        status = Code.CREATE_NEWS_IMAGE_UPLOAD_FAIL,
                        message = it.exception?.message
                    )
                }
            }
    }

    fun checkNewsInfo() {
        isProcessing.value = true
        databaseReference.child("info").child("news").addListenerForSingleValueEvent(checkNewsInfoListener)
    }

    private val checkNewsInfoListener = object: ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "checkNewsInfoListener onDataChange${dataSnapshot.value}")
            val newsInfo = NewsInfo(
                title = newsTitleSingleLiveEvent.value!!,
                subTitle = newsSubTitleSingleLiveEvent.value!!,
                date = newsDateSingleLiveEvent.value!!,
                content = newsContentSingleLiveEvent.value!!,
                thumbnailUri = thumbnailUri,
                contentUri = contentUri,
                updateTime = System.currentTimeMillis()
            )
            databaseReference.child("info").child("news").child(newsTitleSingleLiveEvent.value!!).setValue(newsInfo.toMap()).addOnCompleteListener { result->
                if(result.isSuccessful) {
                    petTestRepository.newsInfoListLiveEvent.value?.add(newsInfo)
                    createNewsLiveEvent.value = createCreateNewsEvent(status = Code.CREATE_NEWS_REQUEST_SUCCESS)
                } else {
                    Log.d(TAG, "create news (${newsTitleSingleLiveEvent.value}) is fail message:${result.exception?.message}")
                    createNewsLiveEvent.value = createCreateNewsEvent(
                        status = Code.CREATE_NEWS_SERVER_SAVE_FAIL,
                        message = result.exception?.message
                    )
                }
            }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "checkNewsInfoListener onCancelled:${databaseError.message}")
            createNewsLiveEvent.value = createCreateNewsEvent(
                status = Code.CREATE_NEWS_SERVER_ERROR,
                message = databaseError.message
            )
        }

    }

    private fun isTitleMatch(): Boolean {
        if (newsTitleSingleLiveEvent.value != null) {
            if (newsTitleSingleLiveEvent.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isDateMatch(): Boolean {
        if (newsDateSingleLiveEvent.value != null) {
            if(newsDateSingleLiveEvent.value!!.isNotEmpty()) {
                val simpleDateFormat = SimpleDateFormat("YYYY/MM/dd", Locale.US)
                return try {
                    simpleDateFormat.parse(newsDateSingleLiveEvent.value!!)
                    true
                } catch (e: ParseException) {
                    false
                }
            }
        }
        return false
    }

    private fun createCreateNewsEvent(status: Int, message: String? = null, uri: String? = null): BaseEvent {
        return when(status) {
            Code.CREATE_NEWS_REQUEST_SUCCESS -> CreateNewsEvent.Success(status = status)
            Code.CREATE_NEWS_REQUEST_PHOTO_UPLOAD_SUCCESS -> CreateNewsEvent.Success(status = status, uri = uri)
            Code.CREATE_NEWS_CONNECT_ERROR -> CreateNewsEvent.ConnectError(status = status)
            Code.CREATE_NEWS_TITLE_FORMAT_ERROR -> CreateNewsEvent.TitleFormatError(status = status)
            Code.CREATE_NEWS_DATE_FORMAT_ERROR -> CreateNewsEvent.DateFormatError(status = status)
            Code.CREATE_NEWS_IMAGE_HAVE_NO_DATA -> CreateNewsEvent.ImageHaveNoData(status = status)
            Code.CREATE_NEWS_SERVER_ERROR -> CreateNewsEvent.ServerError(status = status, message = message)
            Code.CREATE_NEWS_IMAGE_UPLOAD_FAIL -> CreateNewsEvent.ImageUploadFail(status = status, message = message)
            Code.CREATE_NEWS_SERVER_SAVE_FAIL -> CreateNewsEvent.ServerSaveFail(status = status, message = message)
            else -> CreateNewsEvent.OtherError(status)
        }
    }
}