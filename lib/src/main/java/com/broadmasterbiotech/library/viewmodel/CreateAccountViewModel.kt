package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.net.Uri
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.PetTestRepository
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.CreateAccountEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.storage.StorageMetadata


class CreateAccountViewModel(application: Application): BaseViewModel(application) {

    companion object {

        private val TAG = CreateAccountViewModel::class.java.simpleName
    }

    var isProcessing = MutableLiveData<Boolean>()
    var emailEditText = SingleLiveEvent<String>()
    var passwordEditText = SingleLiveEvent<String>()
    var confirmPasswordEditText = SingleLiveEvent<String>()
    val createAccountLiveEvent = SingleLiveEvent<BaseEvent>()

    init {
        isProcessing.value = false
    }

    fun setupOnClick() {
        isProcessing.value = true
        if(isEmailMatch()) {
            if(isPasswordMatch()) {
                if(isConfirmMatch()) {
                    mFireBaseAuth.createUserWithEmailAndPassword(emailEditText.value!!, passwordEditText.value!!)
                        .addOnCompleteListener {
                            if(it.isSuccessful) {
                                val displayName = emailEditText.value!!.trim().split("@")

                                val userProfileChangeRequest = UserProfileChangeRequest.Builder().setDisplayName(displayName[0]).build()
                                mFireBaseAuth.currentUser?.updateProfile(userProfileChangeRequest)?.addOnCompleteListener { task->
                                    if(task.isSuccessful) {
                                        Log.d(TAG, "userProfileChangeRequest isSuccessful")
                                    } else {
                                        Log.d(TAG, "userProfileChangeRequest failure ${task.exception?.message}")
                                    }
                                }
                                sendCertificationEmail()
                            } else {
                                Log.d(TAG, "setupOnClick exception message:${it.exception?.message}")
                                createAccountLiveEvent.value = createAccountLiveEvent(status = Code.CREATE_ACCOUNT_SERVER_ERROR, message = it.exception?.message )
                            }
                        }
                } else {
                    createAccountLiveEvent.value = createAccountLiveEvent(status = Code.CREATE_ACCOUNT_CONFIRM_FORMAT_ERROR)
                }
            } else {
                createAccountLiveEvent.value = createAccountLiveEvent(status = Code.CREATE_ACCOUNT_PASSWORD_FORMAT_ERROR)
            }
        } else {
            createAccountLiveEvent.value = createAccountLiveEvent(status = Code.CREATE_ACCOUNT_EMAIL_FORMAT_ERROR)
        }
    }

    fun sendCertificationEmail() {
        Log.d(TAG, "sendCertificationEmail")
        isProcessing.value = true
        mFireBaseAuth.currentUser!!.sendEmailVerification().addOnCompleteListener { result->
            if(result.isSuccessful) {
                createAccountLiveEvent.value = createAccountLiveEvent(status = Code.CREATE_ACCOUNT_REQUEST_SEND_CERTIFICATION_EMAIL_SUCCESS)
            } else {
                Log.d(TAG, "sendEmailVerification fail message:${result.exception?.message}")
                createAccountLiveEvent.value = createAccountLiveEvent(status = Code.CREATE_ACCOUNT_SEND_CERTIFICATION_EMAIL_FAIL, message = result.exception?.message)
            }
        }
    }

    private fun isEmailMatch(): Boolean {
        if(emailEditText.value!=null) {
            if(emailEditText.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isPasswordMatch(): Boolean {
        if(passwordEditText.value!=null) {
            if(passwordEditText.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isConfirmMatch(): Boolean {
        if(confirmPasswordEditText.value!=null) {
            if(confirmPasswordEditText.value!!.isNotEmpty()) {
                if(confirmPasswordEditText.value.equals(passwordEditText.value)) {
                    return true
                }
            }
        }
        return false
    }

    private fun createAccountLiveEvent(status: Int, message: String? = null): BaseEvent {
        return when (status) {
            Code.CREATE_ACCOUNT_REQUEST_SEND_CERTIFICATION_EMAIL_SUCCESS -> CreateAccountEvent.Success
            Code.CREATE_ACCOUNT_CONNECT_ERROR -> CreateAccountEvent.ConnectError(status = status)
            Code.CREATE_ACCOUNT_EMAIL_FORMAT_ERROR -> CreateAccountEvent.EmailFormatError(status = status)
            Code.CREATE_ACCOUNT_PASSWORD_FORMAT_ERROR -> CreateAccountEvent.PasswordFormatError(status = status)
            Code.CREATE_ACCOUNT_CONFIRM_FORMAT_ERROR -> CreateAccountEvent.ConfirmFormatError(status = status)
            Code.CREATE_ACCOUNT_SERVER_ERROR -> CreateAccountEvent.ServerError(status = status, message = message)
            Code.CREATE_ACCOUNT_SEND_CERTIFICATION_EMAIL_FAIL -> CreateAccountEvent.SendCertificationEmailFail(status = status, message = message)
            else -> CreateAccountEvent.OtherError(status = status)
        }
    }


}