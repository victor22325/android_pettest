package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.PetTestRepository
import com.broadmasterbiotech.library.database.account.AccountInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.ProfileEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.*
import com.google.firebase.storage.StorageMetadata
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.net.URL
import java.util.concurrent.Executors


class ProfileViewModel(application: Application): BaseViewModel(application) {

    companion object {

        private val TAG = ProfileViewModel::class.java.simpleName
    }

    var isProcessing = MutableLiveData<Boolean>()
    private var avatarPhotoURI: String? = null
    var firstNameEditText = SingleLiveEvent<String>()
    var countryText = SingleLiveEvent<String>()
    var stateText = SingleLiveEvent<String>()
    var cityTest = SingleLiveEvent<String>()
    var lastNameEditText = SingleLiveEvent<String>()
    var postalCodeEditText = SingleLiveEvent<String>()
    var emailEditText = SingleLiveEvent<String>()
    var passwordEditText = SingleLiveEvent<String>()
    var newPasswordEditText = SingleLiveEvent<String>()
    var confirmPasswordEditText = SingleLiveEvent<String>()
    val profileLiveEvent = SingleLiveEvent<BaseEvent>()
    private var getUserPhotoDisposable: Disposable? = null
    var userPhotoLiveEvent = SingleLiveEvent<Drawable>()
    val uploadLiveEvent = SingleLiveEvent<BaseEvent>()
    var uploadFileName: String? = null
    var uploadImageData: ByteArray? = null
    var uploadMetadata: StorageMetadata? = null

    init {
        isProcessing.value = false
        newPasswordEditText.value = ""
        confirmPasswordEditText.value = ""
        uploadFileName = null
        uploadImageData = null
        uploadMetadata = null
    }

    fun getAccountInfo() {
        isProcessing.value = true
        getUserPhotoDisposable =  Observable.create<Bitmap> { emitter->
            val url = URL(petTestRepository.accountInfo?.avatarPhoto.toString())
            val connection = url.openConnection()
            connection.doInput = true
            connection.connect()
            val inputStream = connection.getInputStream()
            emitter.onNext(BitmapFactory.decodeStream(inputStream))
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.from(Executors.newFixedThreadPool(1)))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({ bitmap ->
                userPhotoLiveEvent.value = BitmapDrawable(Resources.getSystem(), bitmap)
                avatarPhotoURI = mFireBaseAuth.currentUser?.photoUrl.toString()
            }, { throwable: Throwable? ->
                Log.d(TAG, "getUserPhoto throwable:${throwable?.message}")
                avatarPhotoURI = ""
                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("account").addListenerForSingleValueEvent(readAccountListener)
            }, {
                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("account").addListenerForSingleValueEvent(readAccountListener)
            })
    }

    fun saveOnClick() {
        isProcessing.value = true
        checkPhotoInfo(fileName = uploadFileName, imageData = uploadImageData, storageMetadata = uploadMetadata)
    }

    private fun isConfirmPasswordMatch(): Boolean {
        if (confirmPasswordEditText.value!=null) {
            if(confirmPasswordEditText.value!!.isNotEmpty()) {
                if(confirmPasswordEditText.value.equals(newPasswordEditText.value)) {
                    return true
                }
            }
        }
        return false
    }

    private fun isNewPasswordMatch(): Boolean {
        // todo newPassword constrain
        return true
    }

    private fun isNewPasswordExist(): Boolean {
        if(newPasswordEditText.value!=null) {
            if (newPasswordEditText.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private val readAccountListener = object: ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "accountInfoListener onDataChange dataSnapshot${dataSnapshot.value}")
            emailEditText.value = dataSnapshot.child("emailAddress").value.toString()
            if(dataSnapshot.child("password").exists()) { passwordEditText.value = dataSnapshot.child("password").value.toString() }
            if(dataSnapshot.child("firstName").exists()) { firstNameEditText.value = dataSnapshot.child("firstName").value.toString() }
            if(dataSnapshot.child("lastName").exists()) { lastNameEditText.value = dataSnapshot.child("lastName").value.toString() }
            if(dataSnapshot.child("country").exists()) { countryText.value = dataSnapshot.child("country").value.toString() }
            if(dataSnapshot.child("state").exists()) { stateText.value = dataSnapshot.child("state").value.toString() }
            if(dataSnapshot.child("city").exists()) { cityTest.value = dataSnapshot.child("city").value.toString() }
            if(dataSnapshot.child("postalCode").exists())  { postalCodeEditText.value = dataSnapshot.child("postalCode").value.toString() }
            profileLiveEvent.value = createProfileEvent(status = Code.PROFILE_REQUEST_GET_ACCOUNT_INFO_SUCCESS)
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "accountInfoListener onCancelled databaseError:${databaseError.details}")
            profileLiveEvent.value = createProfileEvent(
                status = Code.PROFILE_READ_ACCOUNT_DATA_FAIL,
                message = databaseError.message
            )
        }
    }

    private fun checkPhotoInfo(fileName: String?, imageData: ByteArray?, storageMetadata: StorageMetadata?){
        Log.d(TAG, "checkPhotoInfo")
        if(fileName!=null && imageData!=null && storageMetadata!=null) {
            storageReference.child("users").child(mFireBaseAuth.currentUser?.uid!!).listAll().addOnCompleteListener {
                if(it.isSuccessful) {
                    if(it.result?.items?.size!! > 0) {
                        var needUpload = true
                        for(i in 0 until it.result?.items?.size!!) {
                            if(fileName != it.result?.items?.get(i)?.name) {
                                Log.d(TAG, "checkPhotoInfo name[${i}]:${it.result?.items?.get(i)?.name} is delete")
                                it.result?.items?.get(i)?.delete()
                            } else {
                                needUpload = false
                            }
                        }
                        if(needUpload) {
                            uploadPhotoToFireBase(fileName, imageData, storageMetadata)
                        } else {
                            uploadLiveEvent.value = createProfileUploadEvent(status = Code.PROFILE_AVATAR_PHOTO_UPLOAD_FAIL, message = "This photo has been uploaded.")
                        }
                    } else {
                        uploadPhotoToFireBase(fileName, imageData, storageMetadata)
                    }
                } else {
                    Log.d(TAG,"checkPhotoIsExist fail ${it.exception?.message}")
                    uploadLiveEvent.value = createProfileUploadEvent(status = Code.PROFILE_AVATAR_PHOTO_UPLOAD_FAIL, message = it.exception?.message)
                }
            }
        } else {
            checkAccountInfo()
            Log.d(TAG, "checkPhotoInfo avatar imageData is null")
        }
    }

    private fun uploadPhotoToFireBase(fileName: String, imageData: ByteArray, storageMetadata: StorageMetadata) {
        val filePath = storageReference.child("users").child(mFireBaseAuth.currentUser?.uid!!).child(fileName)
        filePath.putBytes(imageData, storageMetadata)
            .continueWith { task->
                if(!task.isSuccessful) {
                    Log.d(TAG, "uploadPhotoToFireBase continueWith downloadUrl task fail :${task.exception?.message}")
                }
                return@continueWith filePath.downloadUrl
            }
            .addOnCompleteListener{
            if(it.isSuccessful) {
                filePath.downloadUrl.addOnCompleteListener { task->
                    if(task.isSuccessful) {
                        val userProfileChangeRequest = UserProfileChangeRequest.Builder().setPhotoUri(task.result).build()
                        mFireBaseAuth.currentUser?.updateProfile(userProfileChangeRequest)?.addOnCompleteListener { mission->
                            if(mission.isSuccessful) {
                                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("account").setValue(petTestRepository.accountInfo)
                                    .addOnCompleteListener { saveAccountTask->
                                        if(saveAccountTask.isSuccessful) {
                                            Log.d(TAG, "save avatarPhoto isSuccessful uri:${task.result?.toString()}")
                                            avatarPhotoURI = task.result?.toString()
                                            uploadLiveEvent.value = createProfileUploadEvent(status = Code.PROFILE_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS, uri = task.result?.toString())
                                        } else {
                                            Log.d(TAG, "uploadPhotoToFireBase saveAccountUrl is fail")
                                            uploadLiveEvent.value = createProfileUploadEvent(status = Code.PROFILE_AVATAR_PHOTO_UPLOAD_FAIL, message = saveAccountTask.exception?.message)
                                        }
                                    }
                            } else {
                                Log.d(TAG, "uploadPhotoToFireBase upload fireBase account fail reason:${mission.exception?.message}")
                                uploadLiveEvent.value = createProfileUploadEvent(status = Code.PROFILE_AVATAR_PHOTO_UPLOAD_FAIL, message = mission.exception?.message)
                            }
                        }
                    } else {
                        Log.d(TAG, "uploadPhotoToFireBase downloadUrl fail reason:${task.exception?.message}")
                        uploadLiveEvent.value = createProfileUploadEvent(status = Code.PROFILE_AVATAR_PHOTO_UPLOAD_FAIL, message = task.exception?.message)
                    }
                }
            } else {
                Log.d(TAG, "uploadPhotoToFireBase upload fail reason:${it.exception?.message}")
                uploadLiveEvent.value = createProfileUploadEvent(status = Code.PROFILE_AVATAR_PHOTO_UPLOAD_FAIL, message = it.exception?.message)
            }
        }
    }

    private fun createProfileUploadEvent(status: Int, message: String? = null, uri: String? = null):BaseEvent {
        return when(status) {
            Code.PROFILE_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS -> ProfileEvent.Success(status =  status, uri = uri)
            Code.PROFILE_AVATAR_PHOTO_UPLOAD_FAIL -> ProfileEvent.UploadAvatarPhotoFail(status = status, message =  message)
            else -> ProfileEvent.OtherError(status)
        }
    }

    fun checkAccountInfo() {
        Log.d(TAG, "checkAccountInfo")
        if(isNewPasswordExist()) {
            if(isNewPasswordMatch()) {
                if(isConfirmPasswordMatch()) {
                    val credential = EmailAuthProvider.getCredential(emailEditText.value!!, passwordEditText.value!!)
                    mFireBaseAuth.currentUser?.reauthenticate(credential)?.addOnCompleteListener {
                        if (it.isSuccessful) {
                            mFireBaseAuth.currentUser?.updatePassword(newPasswordEditText.value!!)?.addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    updateAccountInfo(password = newPasswordEditText.value!!, isUpdatePassword = true)
                                } else {
                                    Log.d(TAG, "updatePassword is fail message:${task.exception?.message}")
                                    profileLiveEvent.value = createProfileEvent(
                                        status = Code.PROFILE_UPDATE_PASSWORD_FAIL,
                                        message = task.exception?.message
                                    )
                                }
                            }
                        } else {
                            Log.d(TAG, "re-authenticate is fail message:${it.exception?.message}")
                            profileLiveEvent.value = createProfileEvent(
                                status = Code.PROFILE_RE_AUTH_FAIL,
                                message = it.exception?.message
                            )
                        }
                    }
                } else {
                    profileLiveEvent.value = createProfileEvent(status = Code.PROFILE_CONFIRM_FORMAT_ERROR)
                }
            } else {
                profileLiveEvent.value = createProfileEvent(status = Code.PROFILE_NEW_PASSWORD_FORMAT_ERROR)
            }
        } else {
            updateAccountInfo(password = passwordEditText.value!!, isUpdatePassword = false)
        }
    }

    private fun updateAccountInfo(password: String, isUpdatePassword: Boolean) {
        val accountInfo = AccountInfo(
            emailAddress = petTestRepository.accountInfo?.emailAddress!!,
            password = password,
            sn = petTestRepository.accountInfo?.sn!!,
            firstName = firstNameEditText.value,
            lastName = lastNameEditText.value,
            country = countryText.value,
            state = stateText.value,
            city = cityTest.value,
            postalCode = postalCodeEditText.value,
            avatarPhoto = avatarPhotoURI
        )
        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("account").setValue(accountInfo.toMap())
            .addOnCompleteListener { result->
                if(result.isSuccessful) {
                    petTestRepository.setAccountInfo(accountInfo = accountInfo, writeDB = true)
                    if(isUpdatePassword) {
                        profileLiveEvent.value = createProfileEvent(status = Code.PROFILE_REQUEST_UPDATE_PASSWORD_SUCCESS)
                    } else {
                        profileLiveEvent.value = createProfileEvent(status = Code.PROFILE_REQUEST_SUCCESS)
                    }
                } else {
                    Log.d(TAG, "save account fail message:${result.exception?.message}")
                    profileLiveEvent.value = createProfileEvent(
                        status = Code.PROFILE_SERVER_ERROR,
                        message = result.exception?.message
                    )
                }
            }
    }


    private fun createProfileEvent(status: Int, message: String? = null):BaseEvent {
        return when(status) {
            Code.PROFILE_REQUEST_SUCCESS -> ProfileEvent.Success(status = status)
            Code.PROFILE_REQUEST_GET_ACCOUNT_INFO_SUCCESS -> ProfileEvent.Success(status = status)
            Code.PROFILE_REQUEST_UPDATE_PASSWORD_SUCCESS -> ProfileEvent.Success(status = status)
            Code.PROFILE_CONNECT_ERROR -> ProfileEvent.ConnectError(status = status)
            Code.PROFILE_SERVER_ERROR -> ProfileEvent.ServerError(status = status, message = message)
            Code.PROFILE_NEW_PASSWORD_FORMAT_ERROR -> ProfileEvent.NewPasswordFormatError(status = status)
            Code.PROFILE_CONFIRM_FORMAT_ERROR -> ProfileEvent.ConfirmFormatError(status = status)
            Code.PROFILE_READ_ACCOUNT_DATA_FAIL -> ProfileEvent.ReadAccountDataFail(status = status, message = message)
            Code.PROFILE_RE_AUTH_FAIL -> ProfileEvent.ReAuthFail(status = status, message = message)
            Code.PROFILE_UPDATE_PASSWORD_FAIL -> ProfileEvent.UpdatePasswordFail(status = status, message = message)
            else -> ProfileEvent.OtherError(status)
        }
    }

    override fun onCleared() {
        super.onCleared()
        getUserPhotoDisposable?.let { if (!it.isDisposed) it.dispose()}
        databaseReference.removeEventListener(readAccountListener)
    }
}