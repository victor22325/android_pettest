package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import com.broadmasterbiotech.library.database.PetTestDataSource
import com.broadmasterbiotech.library.database.reminder.ReminderInfo
import com.broadmasterbiotech.library.util.SingleLiveEvent
import kotlin.collections.ArrayList

class ReminderViewModel(application: Application): BaseViewModel(application) {

    companion object {
        val TAG = ReminderViewModel::class.java.simpleName
    }

    lateinit var reminderDateSingleLiveEvent: SingleLiveEvent<String>
    lateinit var reminderTimeSingleLiveEvent: SingleLiveEvent<String>
    lateinit var reminderRepeatTypeSingleLiveEvent: SingleLiveEvent<Int>
    lateinit var reminderNotificationTypeSingleLiveEvent: SingleLiveEvent<Int>
    var reminderLabelSingleLiveEvent = SingleLiveEvent<String>()

    fun getCurrentReminderList() {
        petTestRepository.getAllReminderList(object: PetTestDataSource.LoadReminderListCallback{
            override fun onReminderListLoaded(reminderList: List<ReminderInfo>) {
                petTestRepository.reminderInfoListLiveEvent.value = reminderList as ArrayList<ReminderInfo>
            }
        })
    }

    fun initialInsulin(timePickerString: String, repeatType: Int, notificationType: Int, label: String? = null) {
        reminderTimeSingleLiveEvent = SingleLiveEvent()
        reminderRepeatTypeSingleLiveEvent = SingleLiveEvent()
        reminderNotificationTypeSingleLiveEvent = SingleLiveEvent()
        if(label == null) {
            reminderLabelSingleLiveEvent.value = String()
        } else {
            reminderLabelSingleLiveEvent.value = label
        }
        reminderTimeSingleLiveEvent.value = timePickerString
        reminderRepeatTypeSingleLiveEvent.value = repeatType
        reminderNotificationTypeSingleLiveEvent.value = notificationType
    }

    fun initialVeterinarian(dataPickerString: String, timePickerString: String, notificationType: Int, label: String? = null) {
        reminderDateSingleLiveEvent = SingleLiveEvent()
        reminderTimeSingleLiveEvent = SingleLiveEvent()
        reminderNotificationTypeSingleLiveEvent = SingleLiveEvent()
        if(label == null) {
            reminderLabelSingleLiveEvent.value = String()
        } else {
            reminderLabelSingleLiveEvent.value = label
        }

        reminderDateSingleLiveEvent.value = dataPickerString
        reminderTimeSingleLiveEvent.value = timePickerString
        reminderNotificationTypeSingleLiveEvent.value = notificationType
    }

    fun createReminderOnClick(flag: Int) {
        val reminderInfo: ReminderInfo
        if(flag == 0) {
            reminderInfo = ReminderInfo(
                createTime = System.currentTimeMillis(),
                type = flag,
                status = 1,
                time = reminderTimeSingleLiveEvent.value!!,
                repeat = reminderRepeatTypeSingleLiveEvent.value!!,
                notificationType = reminderNotificationTypeSingleLiveEvent.value!!,
                label = reminderLabelSingleLiveEvent.value!!,
                updateTime = System.currentTimeMillis())
        } else {
            reminderInfo = ReminderInfo(
                createTime = System.currentTimeMillis(),
                type = flag,
                status = 1,
                date = reminderDateSingleLiveEvent.value!!,
                time = reminderTimeSingleLiveEvent.value!!,
                repeat = 0,
                notificationType = reminderNotificationTypeSingleLiveEvent.value!!,
                label = reminderLabelSingleLiveEvent.value!!,
                updateTime = System.currentTimeMillis())
        }
        petTestRepository.saveReminder(reminderInfo)
        val temp = petTestRepository.reminderInfoListLiveEvent.value
        temp?.add(reminderInfo)
        petTestRepository.reminderInfoListLiveEvent.value = temp
    }

    fun updateReminderOnClick(reminderInfo: ReminderInfo) {
        val newReminderInfo: ReminderInfo
        if(reminderInfo.type == 0) {
            newReminderInfo = ReminderInfo(
                createTime = reminderInfo.createTime,
                type = reminderInfo.type,
                status = reminderInfo.status,
                time = reminderTimeSingleLiveEvent.value!!,
                repeat = reminderRepeatTypeSingleLiveEvent.value!!,
                notificationType = reminderNotificationTypeSingleLiveEvent.value!!,
                label = reminderLabelSingleLiveEvent.value!!,
                updateTime = System.currentTimeMillis())
        } else {
            newReminderInfo = ReminderInfo(
                createTime = reminderInfo.createTime,
                type = reminderInfo.type,
                status = reminderInfo.status,
                date = reminderDateSingleLiveEvent.value!!,
                time = reminderTimeSingleLiveEvent.value!!,
                notificationType = reminderNotificationTypeSingleLiveEvent.value!!,
                label = reminderLabelSingleLiveEvent.value!!,
                updateTime = System.currentTimeMillis())
        }
        petTestRepository.updateReminder(newReminderInfo)
        var temp = petTestRepository.reminderInfoListLiveEvent.value
        if (temp != null) {
            var index = 0
            for(i in 0 until temp.size) {
                if(newReminderInfo.createTime == temp[i].createTime) {
                    index = i
                }
            }
            temp[index] = newReminderInfo
        } else {
            temp = ArrayList()
            temp.add(newReminderInfo)
        }
        petTestRepository.reminderInfoListLiveEvent.value = temp
    }

    fun deleteReminderOnClick(reminderInfo: ReminderInfo) {
        petTestRepository.deleteReminder(reminderInfo)
        val temp = petTestRepository.reminderInfoListLiveEvent.value
        temp?.remove(reminderInfo)
        petTestRepository.reminderInfoListLiveEvent.value = temp
    }

    fun switchUpdateReminder(reminderInfo: ReminderInfo, isChecked: Boolean) {
        if(isChecked) {
            reminderInfo.status = 1
        } else {
            reminderInfo.status = 0
        }
        petTestRepository.updateReminder(reminderInfo)
        val temp = petTestRepository.reminderInfoListLiveEvent.value!!
        var index = 0
        for(i in 0 until temp.size) {
            if(reminderInfo.createTime == temp[i].createTime) {
                index = i
            }
        }
        temp[index] = reminderInfo
        petTestRepository.reminderInfoListLiveEvent.value = temp
    }
}