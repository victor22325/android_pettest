package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.PetTestDataSource
import com.broadmasterbiotech.library.database.account.AccountInfo
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.LoginEvent
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import io.reactivex.disposables.Disposable
import kotlin.collections.ArrayList


class LoginViewModel(application: Application) : BaseViewModel(application) {

    companion object {

        private val TAG = LoginViewModel::class.java.simpleName
    }

    var emailAddress = SingleLiveEvent<String>()
    var password = SingleLiveEvent<String>()

    private var loginDisposable: Disposable? = null
    val loginLiveEvent = SingleLiveEvent<BaseEvent>()
    var isProcessing = MutableLiveData<Boolean>()

    init {
        isProcessing.value = false
    }

    fun getEmailAddressAndPassword() {
        petTestRepository.getAccountList(object: PetTestDataSource.LoadAccountListCallback{
            override fun onAccountListLoaded(accountList: List<AccountInfo>) {
                if(accountList.isNotEmpty()) {
                    emailAddress.value = accountList[accountList.size - 1].emailAddress
                    password.value = accountList[accountList.size - 1].password
                }
            }
        })
    }

    fun logIn() {
        isProcessing.value = true
        Log.d(TAG, "emailLogin")
        if(isEmailMatch()) {
            if(isPasswordMatch()) {
                mFireBaseAuth.signInWithEmailAndPassword(emailAddress.value!!, password.value!!)
                    .addOnCompleteListener { task ->
                        if(task.isSuccessful) {
                            if(!mFireBaseAuth.currentUser?.isEmailVerified!!) {
                                loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_EMAIL_NOT_CERTIFIED)
                            } else {
                                petTestRepository.accountInfo?.emailAddress = emailAddress.value!!
                                petTestRepository.accountInfo?.password = password.value!!
                                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("uid").setValue("${mFireBaseAuth.currentUser?.uid}")
                                    .addOnCompleteListener {
                                        if(it.isSuccessful) {
                                            databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("account").addListenerForSingleValueEvent(emailSignInListener)
                                        } else {
                                            Log.d(TAG, "createUid failure message:${it.exception?.message}")
                                            loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SERVER_ERROR, message = it.exception?.message)
                                        }
                                    }
                            }
                        } else {
                            Log.d(TAG, "emailLogin signInWithEmailAndPassword failure message:${task.exception}")
                            loginLiveEvent.value = createLoginEvent(Code.LOGIN_FIRE_BASE_AUTH_ERROR, message = task.exception?.message)
                        }
                    }
            } else {
                loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_PASSWORD_FORMAT_ERROR)
            }
        } else {
            loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_EMAIL_FORMAT_ERROR)
        }
    }

    fun sendCertificationEmail() {
        Log.d(TAG, "sendCertificationEmail")
        isProcessing.value = true
        mFireBaseAuth.currentUser!!.sendEmailVerification()
            .addOnCompleteListener {
                if(it.isSuccessful) {
                    loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SEND_CERTIFICATION_EMAIL_SUCCESS, uuid = mFireBaseAuth.currentUser?.uid)
                } else {
                    Log.d(TAG, "sendEmailVerification addOnFailureListener message:${it.exception?.message}")
                    loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SEND_CERTIFICATION_EMAIL_FAIL, message = it.exception?.message)
                }
            }
    }

    private fun isEmailMatch(): Boolean {
        if(emailAddress.value!=null) {
            if(emailAddress.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun isPasswordMatch(): Boolean {
        if(password.value!=null) {
            if(password.value!!.isNotEmpty()) {
                return true
            }
        }
        return false
    }

    fun fireBaseAuthWithGoogle(account: GoogleSignInAccount) {
        isProcessing.value = true
        Log.d(TAG, "fireBaseAuthWithGoogle")
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        mFireBaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                petTestRepository.accountInfo?.emailAddress = account.email!!
                Log.d(TAG, "currentUser uid:$${mFireBaseAuth.currentUser?.uid}")
                databaseReference.child("users/${mFireBaseAuth.currentUser?.uid}/uid").setValue("${mFireBaseAuth.currentUser?.uid}")
                    .addOnCompleteListener{ result->
                        if(result.isSuccessful) {
                            databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("account").addListenerForSingleValueEvent(googleSignInListener)
                        } else {
                            Log.d(TAG, "fireBaseAuthWithGoogle failure message:${result.exception?.message}")
                            loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SERVER_ERROR, message = result.exception?.message)
                        }
                    }
            } else {
                loginLiveEvent.value = createLoginEvent(Code.LOGIN_FIRE_BASE_AUTH_ERROR)
            }
        }
    }

    private val emailSignInListener = object: ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "emailSignInListener onDataChange:${dataSnapshot.value}")
            val accountInfo = AccountInfo(
                sn = petTestRepository.accountInfo?.sn!!,
                emailAddress = petTestRepository.accountInfo?.emailAddress!!,
                password = petTestRepository.accountInfo?.password!!,
                firstName = if(dataSnapshot.child("firstName").exists()) { dataSnapshot.child("firstName").value } else { "" } as String,
                lastName = if(dataSnapshot.child("lastName").exists()) { dataSnapshot.child("lastName").value } else { "" } as String,
                country = if(dataSnapshot.child("country").exists()) { dataSnapshot.child("country").value } else { "" } as String,
                state = if(dataSnapshot.child("state").exists()) { dataSnapshot.child("state").value } else { "" } as String,
                city = if(dataSnapshot.child("city").exists()) { dataSnapshot.child("city").value } else { "" } as String,
                postalCode = if(dataSnapshot.child("postalCode").exists()) { dataSnapshot.child("postalCode").value } else { "" } as String,
                avatarPhoto = if(dataSnapshot.child("avatarPhoto").exists()) { dataSnapshot.child("avatarPhoto").value } else { mFireBaseAuth.currentUser?.photoUrl.toString() } as String
            )
            databaseReference.child("users/${mFireBaseAuth.currentUser?.uid}/account").setValue(accountInfo.toMap())
                .addOnCompleteListener {
                    if(it.isSuccessful) {
                        petTestRepository.setAccountInfo(accountInfo = accountInfo, writeDB = true)
                        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").addListenerForSingleValueEvent(checkPetsDataListener)
                    } else {
                        Log.d(TAG, "emailSignInListener failure message:${it.exception?.message}")
                        loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SERVER_ERROR, message = it.exception?.message)
                    }
                }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "emailSignInListener onCancelled:${databaseError.details}")
            loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SERVER_ERROR, message = databaseError.message)
        }
    }

    private val googleSignInListener = object: ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "googleSignInListener onDataChange:${dataSnapshot.value}")
            val accountInfo = AccountInfo(
                sn = petTestRepository.accountInfo?.sn!!,
                emailAddress = petTestRepository.accountInfo?.emailAddress!!,
                password = if(dataSnapshot.child("password").exists()) { dataSnapshot.child("password").value } else { "" } as String,
                firstName = if(dataSnapshot.child("firstName").exists()) { dataSnapshot.child("firstName").value } else { "" } as String,
                lastName = if(dataSnapshot.child("lastName").exists()) { dataSnapshot.child("lastName").value } else { "" } as String,
                country = if(dataSnapshot.child("country").exists()) { dataSnapshot.child("country").value } else { "" } as String,
                state = if(dataSnapshot.child("state").exists()) { dataSnapshot.child("state").value } else { "" } as String,
                city = if(dataSnapshot.child("city").exists()) { dataSnapshot.child("city").value } else { "" } as String,
                postalCode = if(dataSnapshot.child("postalCode").exists()) { dataSnapshot.child("postalCode").value } else { "" } as String,
                avatarPhoto = if(dataSnapshot.child("avatarPhoto").exists()) { dataSnapshot.child("avatarPhoto").value } else { mFireBaseAuth.currentUser?.photoUrl.toString() } as String
            )
            databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("account").setValue(accountInfo.toMap())
                .addOnCompleteListener {
                    if(it.isSuccessful) {
                        petTestRepository.setAccountInfo(accountInfo = accountInfo, writeDB = true)
                        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").addListenerForSingleValueEvent(checkPetsDataListener)
                    } else {
                        Log.d(TAG, "googleSignInListener failure message:${it.exception?.message}")
                        loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SERVER_ERROR, message = it.exception?.message)
                    }
                }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "googleSignInListener onCancelled:${databaseError.details}")
            loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SERVER_ERROR, message = databaseError.message)
        }
    }

    private val checkPetsDataListener = object: ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.d(TAG, "checkPetsDataListener onDataChange dataSnapshot:${dataSnapshot.value}")
            if(dataSnapshot.value!=null) {
                if(dataSnapshot.hasChildren()) {
                    val petInfoList = ArrayList<PetInfo>()
                    val iterator = dataSnapshot.children.iterator()
                    for(i in 0 until dataSnapshot.childrenCount) {
                        val snap = iterator.next()
                        val petInfo = PetInfo (
                            name = snap.key!!,
                            species = if(snap.child("info").child("species").exists()) { snap.child("info").child("species").value.toString().toInt() } else { 0 },
                            breed = if(snap.child("info").child("breed").exists()) { snap.child("info").child("breed").value.toString() } else { "" },
                            weight = if(snap.child("info").child("weight").exists()) { snap.child("info").child("weight").value.toString() } else { "" },
                            weightUnit = if(snap.child("info").child("weightUnit").exists()) { snap.child("info").child("weightUnit").value.toString().toInt() } else { null },
                            gender = if(snap.child("info").child("gender").exists()) { snap.child("info").child("gender").value.toString().toInt() } else { null },
                            birth = if(snap.child("info").child("birth").exists()) { snap.child("info").child("birth").value.toString() } else { "" },
                            glucoseUpper = if(snap.child("info").child("glucoseUpper").exists()) { snap.child("info").child("glucoseUpper").value.toString() } else { "" },
                            glucoseLower = (if(snap.child("info").child("glucoseLower").exists()) { snap.child("info").child("glucoseLower").value.toString() } else { "" }),
                            diabetesType = if(snap.child("info").child("diabetesYearDiagnosed").exists()) { snap.child("info").child("diabetesYearDiagnosed").value.toString().toInt() } else { null },
                            diabetesYearDiagnosed = if(snap.child("info").child("diabetesYearDiagnosed").exists()) { snap.child("info").child("diabetesYearDiagnosed").value } else { "" } as String,
                            avatarPhoto = if(snap.child("info").child("avatarPhoto").exists()) { snap.child("info").child("avatarPhoto").value } else { "" } as String,
                            isSelected = if(snap.child("info").child("isSelected").exists()) { snap.child("info").child("isSelected").value.toString().toBoolean() } else { null },
                            updateTime = if(snap.child("info").child("updateTime").exists()) { snap.child("info").child("updateTime").value.toString().toLong() } else { System.currentTimeMillis() }
                        )
                        petInfoList.add(petInfo)
                    }
                    Log.d(TAG, "petInfoList:$petInfoList")
                    petTestRepository.petInfoListLiveEvent.value = petInfoList
                    setCurrentPetInfo(petInfoList)
                    syncCurrentGlucoseInfoList()
                } else {
                    loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_REQUEST_SUCCESS_NEED_CREATE_PET, uuid = mFireBaseAuth.currentUser?.uid)
                }
            } else {
                loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_REQUEST_SUCCESS_NEED_CREATE_PET, uuid = mFireBaseAuth.currentUser?.uid)
            }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.d(TAG, "checkPetsDataListener onCancelled:${databaseError.details}")
            loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SERVER_ERROR, message = databaseError.message)
        }
    }

    private fun setCurrentPetInfo(petInfoList: ArrayList<PetInfo>) {
        for(i in 0 until petInfoList.size) {
            if(petInfoList[i].isSelected == true) {
                petTestRepository.setCurrentPetInfo(petInfo = petInfoList[i])
                break
            }
        }
        if(petTestRepository.petInfoLiveEvent.value == null) {
            petTestRepository.petInfoLiveEvent.value = petInfoList[0]
            databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petInfoList[0].name).child("info").child("isSelected").setValue("true").addOnCompleteListener { it->
                if(!it.isSuccessful) {
                    loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_SERVER_ERROR, message = it.exception?.message)
                }
            }
        }
    }

    fun syncCurrentGlucoseInfoList() {
        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).child("data").addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d(TAG, "syncCurrentGlucoseInfoList onCancelled:${databaseError.message}")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val glucoseInfoList = ArrayList<GlucoseInfo>()
                val yearCount = dataSnapshot.childrenCount
                val yearIterator = dataSnapshot.children.iterator()
                for(year in  0 until yearCount) {
                    val yearSnap = yearIterator.next()
                    val monthCount = dataSnapshot.child(yearSnap.key!!).childrenCount
                    val monthIterator = dataSnapshot.child(yearSnap.key!!).children.iterator()
                    for(month in 0 until monthCount) {
                        val monthSnap = monthIterator.next()
                        val dayCount = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).childrenCount
                        val dayIterator = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).children.iterator()
                        for(day in 0 until dayCount) {
                            val daySnap = dayIterator.next()
                            val dataCount =  dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).child(daySnap.key!!).childrenCount
                            val dataIterator = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).child(daySnap.key!!).children.iterator()
                            for(data in 0 until dataCount) {
                                val dataSnap = dataIterator.next()
                                val glucoseInfo = GlucoseInfo(
                                    createTime = dataSnap.child("createTime").value.toString().toLong(),
                                    year = dataSnap.child("year").value.toString(),
                                    month = dataSnap.child("month").value.toString(),
                                    day = dataSnap.child("day").value.toString(),
                                    hour = dataSnap.child("hour").value.toString(),
                                    minute = dataSnap.child("minute").value.toString(),
                                    value = dataSnap.child("value").value.toString().toInt(),
                                    updateTime = dataSnap.child("updateTime").value.toString().toLong()
                                )
                                glucoseInfoList.add(glucoseInfo)
                            }
                        }
                    }
                }
                petTestRepository.glucoseListLiveEvent.value = glucoseInfoList
                Log.d(TAG, "syncCurrentGlucoseInfoList sync is successful. glucoseList:$glucoseInfoList")
                syncCurrentNewsInfoList()
            }
        })
    }

    fun syncCurrentNewsInfoList() {
        databaseReference.child("info").child("news").addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d(TAG, "syncCurrentNewsInfoList onCancelled:${databaseError.message}")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val newsInfoList = ArrayList<NewsInfo>()
                val newsCount = dataSnapshot.childrenCount
                val newsIterator = dataSnapshot.children.iterator()
                for(count in 0 until newsCount) {
                    val newsSnap = newsIterator.next()
                    val newsInfo = NewsInfo(
                        title = newsSnap.child("title").value.toString(),
                        subTitle = newsSnap.child("subTitle").value.toString(),
                        content = newsSnap.child("content").value.toString(),
                        thumbnailUri = newsSnap.child("thumbnailUri").value.toString(),
                        contentUri = newsSnap.child("contentUri").value.toString(),
                        date = newsSnap.child("date").value.toString(),
                        updateTime = newsSnap.child("updateTime").value.toString().toLong()
                    )
                    newsInfoList.add(newsInfo)
                }
                petTestRepository.newsInfoListLiveEvent.value = newsInfoList
                Log.d(TAG, "syncCurrentNewsInfoList sync is successful. newsList:$newsInfoList")
                loginLiveEvent.value = createLoginEvent(status = Code.LOGIN_REQUEST_SUCCESS, uuid = mFireBaseAuth.currentUser?.uid)
            }
        })
    }

    private fun createLoginEvent(status: Int, uuid: String? = null, message: String? = null): BaseEvent {
        return when (status) {
            Code.LOGIN_REQUEST_SUCCESS -> LoginEvent.Success(status = status, uuid = uuid)
            Code.LOGIN_REQUEST_SUCCESS_NEED_CREATE_PET -> LoginEvent.Success(status = status, uuid = uuid)
            Code.LOGIN_SEND_CERTIFICATION_EMAIL_SUCCESS -> LoginEvent.Success(status = status, uuid = uuid)
            Code.LOGIN_CONNECT_ERROR -> LoginEvent.ConnectError(status = status)
            Code.LOGIN_EMAIL_FORMAT_ERROR -> LoginEvent.EmailFormatError(status = status)
            Code.LOGIN_PASSWORD_FORMAT_ERROR -> LoginEvent.PasswordFormatError(status = status)
            Code.LOGIN_SERVER_ERROR -> LoginEvent.ServerError(status = status, message = message)
            Code.LOGIN_EMAIL_NOT_CERTIFIED -> LoginEvent.EmailNotCertified(status = status)
            Code.LOGIN_SEND_CERTIFICATION_EMAIL_FAIL -> LoginEvent.SendCertificationEmailFail(status = status, message = message)
            Code.LOGIN_FIRE_BASE_AUTH_ERROR -> LoginEvent.FireBaseAuthSignInError(status = status, message = message)
            else -> LoginEvent.OtherError(status = status)
        }
    }

    override fun onCleared() {
        super.onCleared()
        loginDisposable?.let { if (!it.isDisposed) it.dispose()}
        databaseReference.removeEventListener(googleSignInListener)
        databaseReference.removeEventListener(emailSignInListener)
        databaseReference.removeEventListener(checkPetsDataListener)
    }
}