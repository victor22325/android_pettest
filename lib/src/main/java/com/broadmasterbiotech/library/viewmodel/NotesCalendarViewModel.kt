package com.broadmasterbiotech.library.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.util.Log
import android.widget.DatePicker
import com.broadmasterbiotech.library.database.PetTestDataSource
import com.broadmasterbiotech.library.database.note.NoteInfo
import com.broadmasterbiotech.library.util.SingleLiveEvent
import java.util.*
import kotlin.collections.ArrayList

class NotesCalendarViewModel(application: Application): BaseViewModel(application) {

    companion object {
        val TAG = NotesCalendarViewModel::class.java.simpleName
    }

    // calendar
    var noteInfoListSingleLiveEvent = SingleLiveEvent<ArrayList<NoteInfo>>()
    var notesDateListSingleLiveEvent = SingleLiveEvent<ArrayList<String>>()
    val selectedCalendar: Calendar = Calendar.getInstance()

    // createNote
    var datePickerSingleLiveEvent = SingleLiveEvent<String>()
    var timePickerSingleLiveEvent = SingleLiveEvent<String>()
    var typeSingleLiveEvent = SingleLiveEvent<Int>()
    var topicSingleLiveEvent = SingleLiveEvent<String>()
    var contentSingleLiveEvent = SingleLiveEvent<String>()
    var mediaUriLiveEvent = SingleLiveEvent<String>()

    // editNote
    var editCreateTimeSingleLiveEvent = SingleLiveEvent<Long>()
    var editDatePickerSingleLiveEvent = SingleLiveEvent<String>()
    var editTimePickerSingleLiveEvent = SingleLiveEvent<String>()
    var editTypeSingleLiveEvent = SingleLiveEvent<Int>()
    var editTopicSingleLiveEvent = SingleLiveEvent<String>()
    var editContentSingleLiveEvent = SingleLiveEvent<String>()
    var editMediaUriLiveEvent = SingleLiveEvent<String>()

    fun getSelectDate(datePicker: DatePicker): String {
        val month: String = when (datePicker.month) {
            0 -> "Jan"
            1 -> "Feb"
            2 -> "Mar"
            3 -> "Apr"
            4 -> "May"
            5 -> "Jun"
            6 -> "Jul"
            7 -> "Aug"
            8 -> "Sep"
            9 -> "Oct"
            10 -> "Nov"
            11 -> "Dec"
            else -> "None"
        }
        val date: String = if(datePicker.dayOfMonth < 10) {
            "0${datePicker.dayOfMonth}"
        } else {
            "${datePicker.dayOfMonth}"
        }
        return "$month/$date/${datePicker.year}"
    }

    fun getNum(num: Int): String {
        return if (num < 10) {
            "0$num"
        } else {
            "$num"
        }
    }

    fun getSelectHour(hour: Int): String {
        return if (hour < 10) {
            "0$hour"
        } else {
            "$hour"
        }
    }

    fun getSelectMinute(minute: Int): String {
        return if (minute < 10) {
            "0$minute"
        } else {
            "$minute"
        }
    }

    fun createNoteDoneOnClick() {
        val topic = if(topicSingleLiveEvent.value == null) {
            "Topic"
        } else {
            topicSingleLiveEvent.value!!
        }
        val noteInfo = NoteInfo(createTime = System.currentTimeMillis(),
                                type = typeSingleLiveEvent.value!!,
                                topic = topic,
                                content = contentSingleLiveEvent.value,
                                mediaUri = mediaUriLiveEvent.value,
                                date = datePickerSingleLiveEvent.value!!,
                                time = timePickerSingleLiveEvent.value!!)
        petTestRepository.saveNote(noteInfo)
        val selectDate = "${selectedCalendar.get(Calendar.YEAR)}/${getNum(selectedCalendar.get(Calendar.MONTH)+1)}/${getNum(selectedCalendar.get(Calendar.DAY_OF_MONTH))}"
        if(noteInfo.date == selectDate) {
            val noteList: ArrayList<NoteInfo> = noteInfoListSingleLiveEvent.value!!
            noteList.add(noteInfo)
            noteInfoListSingleLiveEvent.value = noteList
        }
        val dateList: ArrayList<String> = notesDateListSingleLiveEvent.value!!
        dateList.add(noteInfo.date)
        val hashSet = HashSet<String>(dateList)
        dateList.clear()
        dateList.addAll(hashSet)
        notesDateListSingleLiveEvent.value = dateList
    }

    @SuppressLint("CheckResult")
    fun getCurrentNoteList(date: String) {
        petTestRepository.getNoteListByDate(date).subscribe{ list->
            noteInfoListSingleLiveEvent.value = list as ArrayList
        }
    }

    fun getAllNotesDateList(){
        petTestRepository.getAllNoteList(object: PetTestDataSource.LoadNoteListCallback{
            override fun onNoteListLoaded(noteList: List<NoteInfo>) {
                val notesDateList = ArrayList<String>()
                for(element in noteList) {
                    notesDateList.add(element.date)
                }
                notesDateListSingleLiveEvent.value = notesDateList
            }
        })
    }

    fun editNoteDoneOnClick(old_noteInfo: NoteInfo) {
        val topic = if(editTopicSingleLiveEvent.value == null) {
            "Topic"
        } else {
            editTopicSingleLiveEvent.value!!
        }
        val noteInfo = NoteInfo(createTime = editCreateTimeSingleLiveEvent.value,
                                type = editTypeSingleLiveEvent.value!!,
                                topic = topic,
                                content = editContentSingleLiveEvent.value,
                                mediaUri = editMediaUriLiveEvent.value,
                                date = editDatePickerSingleLiveEvent.value!!,
                                time = editTimePickerSingleLiveEvent.value!!)
        petTestRepository.updateNote(noteInfo)
        val selectDate = "${selectedCalendar.get(Calendar.YEAR)}/${getNum(selectedCalendar.get(Calendar.MONTH)+1)}/${getNum(selectedCalendar.get(Calendar.DAY_OF_MONTH))}"
        val noteList: ArrayList<NoteInfo> = noteInfoListSingleLiveEvent.value!!
        if(old_noteInfo.date == selectDate) {
            noteList.remove(old_noteInfo)
        }
        if(noteInfo.date == selectDate) {
            noteList.add(noteInfo)
        }
        noteInfoListSingleLiveEvent.value = noteList
        val dateList: ArrayList<String> = notesDateListSingleLiveEvent.value!!
        dateList.remove(old_noteInfo.date)
        dateList.add(noteInfo.date)
        val hashSet = HashSet<String>(dateList)
        dateList.clear()
        dateList.addAll(hashSet)
        notesDateListSingleLiveEvent.value = dateList
    }

    fun deleteNoteOnClick(noteInfo: NoteInfo) {
        petTestRepository.deleteNote(noteInfo)
        val selectDate = "${selectedCalendar.get(Calendar.YEAR)}/${getNum(selectedCalendar.get(Calendar.MONTH)+1)}/${getNum(selectedCalendar.get(Calendar.DAY_OF_MONTH))}"
        val noteList: ArrayList<NoteInfo> = noteInfoListSingleLiveEvent.value!!
        if(noteInfo.date == selectDate) {
            noteList.remove(noteInfo)
        }
        noteInfoListSingleLiveEvent.value = noteList
        val dateList: ArrayList<String> = notesDateListSingleLiveEvent.value!!
        if(noteList.size == 0) {
            dateList.remove(noteInfo.date)
        }
        val hashSet = HashSet<String>(dateList)
        dateList.clear()
        dateList.addAll(hashSet)
        notesDateListSingleLiveEvent.value = dateList
    }
}