package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.PetTestDataSource
import com.broadmasterbiotech.library.database.PetTestRepository
import com.broadmasterbiotech.library.database.account.AccountInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.VerifySNEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import java.util.regex.Matcher
import java.util.regex.Pattern

class VerifySNViewModel(application: Application) : AndroidViewModel(application) {

    companion object {

        private val TAG = VerifySNViewModel::class.java.simpleName
    }

    var serialNumber = SingleLiveEvent<String>()

    private var verifyDisposable: Disposable? = null

    var verifySNLiveEvent = SingleLiveEvent<BaseEvent>()

    var isProcessing = MutableLiveData<Boolean>()

    private var petTestRepository: PetTestRepository = PetTestRepository.providePetTestRepository(application.applicationContext)

    init {
        isProcessing.value = false
    }

    fun getSN() {
        petTestRepository.getAccountList(object: PetTestDataSource.LoadAccountListCallback{
            override fun onAccountListLoaded(accountList: List<AccountInfo>) {
                if(accountList.isNotEmpty()) {
                    serialNumber.value = accountList[accountList.size - 1].sn
                }
            }
        })
    }

    fun verifySN() {
        isProcessing.value = true

        verifyDisposable = Observable
            .just(confirmSN())
            .delay(500, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{ result->
                when (result) {
                    Code.VERIFY_SUCCESS -> {
                        val accountInfo = AccountInfo(emailAddress = serialNumber.value!!, sn = serialNumber.value!!)
                        petTestRepository.setAccountInfo(accountInfo = accountInfo, writeDB = false)
                        verifySNLiveEvent.value = createVerifySNEvent(Code.VERIFY_SUCCESS)
                    }
                    Code.VERIFY_SN_IS_NULL -> {
                        verifySNLiveEvent.value = createVerifySNEvent(Code.VERIFY_SN_IS_NULL)
                    }
                    Code.VERIFY_SN_IS_EMPTY -> {
                        verifySNLiveEvent.value = createVerifySNEvent(Code.VERIFY_SN_IS_EMPTY)
                    }
                    else -> {
                        verifySNLiveEvent.value = createVerifySNEvent(Code.VERIFY_ERROR)
                    }
                }
            }
    }

    private fun confirmSN(): Int {
        return if(serialNumber.value == null) {
            Code.VERIFY_SN_IS_NULL
        } else {
            if(serialNumber.value!!.isEmpty()) {
                Code.VERIFY_SN_IS_EMPTY
            } else {
                val regex = Regex("\\d{2}[a-zA-Z]\\d{10}\$", RegexOption.IGNORE_CASE);
                if(regex.matches(serialNumber.value!!)) {
                    Code.VERIFY_SUCCESS
                } else {
                    Code.LOGIN_SERVER_ERROR
                }
            }
        }
    }

    private fun createVerifySNEvent(status: Int): BaseEvent {
        return when (status) {
            Code.VERIFY_SUCCESS -> VerifySNEvent.Success
            Code.VERIFY_ERROR -> VerifySNEvent.Fail(status = Code.VERIFY_ERROR)
            Code.VERIFY_SN_IS_EMPTY -> VerifySNEvent.SNisEmpty(status = Code.VERIFY_SN_IS_EMPTY)
            Code.VERIFY_SN_IS_NULL -> VerifySNEvent.SNisNull(status = Code.VERIFY_SN_IS_NULL)
            else -> VerifySNEvent.Fail(status = Code.VERIFY_ERROR)
        }
    }

    override fun onCleared() {
        super.onCleared()
        verifyDisposable?.let { if (!it.isDisposed) it.dispose()}
    }
}