package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.DeletePetEvent
import com.broadmasterbiotech.library.event.PickPetEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.internal.FirebaseInstanceIdInternal
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import kotlin.collections.ArrayList


class PetTestViewModel(application: Application): BaseViewModel(application) {

    companion object {

        private val TAG = PetTestViewModel::class.java.simpleName
    }

    var isProcessing = MutableLiveData<Boolean>()
    var userPhotoLiveEvent = SingleLiveEvent<Drawable>()
    var selectPickPetLiveData =  SingleLiveEvent<BaseEvent>()
    var selectDeletePetLiveData = SingleLiveEvent<BaseEvent>()
    private var getUserPhotoDisposable: Disposable? = null

    init {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task->
            if(!task.isSuccessful) { return@addOnCompleteListener }
            if(!task.isSuccessful) { return@addOnCompleteListener }
            val token = task.result?.token
            Log.d(TAG, "token:$token")
        }
    }

    fun checkIsBirthday(format: String): Boolean {
        val simpleDateFormat = SimpleDateFormat(format, Locale.US)
        val currentDate = Calendar.getInstance().time
        val date = simpleDateFormat.format(currentDate)
        for(i in 0 until petTestRepository.petInfoListLiveEvent.value?.size!!) {
            if(!petTestRepository.petInfoListLiveEvent.value!![i].birth?.isEmpty()!!) {
                val birthdaySplit = petTestRepository.petInfoListLiveEvent.value!![i].birth.toString().split("/")
                val birthdayCalendar = Calendar.getInstance()
                birthdayCalendar.set(birthdaySplit[0].toInt(), birthdaySplit[1].toInt()-1, birthdaySplit[2].toInt())
                val birthdayDate = simpleDateFormat.format(birthdayCalendar.time)
                if(birthdayDate == date) {
                    return true
                }
            }
        }
        return false
    }

    fun updatePetInfoToDataBase(petInfoList: ArrayList<PetInfo>?) {
        if(petInfoList != null) {
            petTestRepository.petInfoListLiveEvent.value = petInfoList
        }
    }

    fun pickPetInfo(petInfo: PetInfo) {
        Log.d(TAG, "updatePetInfoSelectedStatus:$petInfo")
        val newPetInfoList: ArrayList<PetInfo> = petTestRepository.petInfoListLiveEvent.value!!
        var haveError = false
        for(i in 0 until petTestRepository.petInfoListLiveEvent.value?.size!!) {
            if(petTestRepository.petInfoListLiveEvent.value!![i] == petInfo) {
                petTestRepository.setCurrentPetInfo(petInfo = petInfo)
                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}")
                    .child("pets").child(petTestRepository.petInfoListLiveEvent.value!![i].name).child("info")
                    .child("isSelected").setValue("true").addOnCompleteListener {
                        if(!it.isSuccessful) {
                            haveError = true
                        }
                    }
                newPetInfoList[i].isSelected = true
            } else {
                databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}")
                    .child("pets").child(petTestRepository.petInfoListLiveEvent.value!![i].name).child("info")
                    .child("isSelected").setValue("false").addOnCompleteListener {
                        if(!it.isSuccessful) {
                            haveError = true
                        }
                    }
                newPetInfoList[i].isSelected = false
            }
        }
        databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petTestRepository.petInfoLiveEvent.value!!.name).child("data").addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d(TAG, "syncCurrentGlucoseInfoList onCancelled:${databaseError.message}")
                selectPickPetLiveData.value = createSelectPetEvent(Code.SELECT_PET_PICK_SERVER_ERROR, petInfoList = newPetInfoList, message = databaseError.message)
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val glucoseInfoList = ArrayList<GlucoseInfo>()
                val yearCount = dataSnapshot.childrenCount
                val yearIterator = dataSnapshot.children.iterator()
                for(year in  0 until yearCount) {
                    val yearSnap = yearIterator.next()
                    val monthCount = dataSnapshot.child(yearSnap.key!!).childrenCount
                    val monthIterator = dataSnapshot.child(yearSnap.key!!).children.iterator()
                    for(month in 0 until monthCount) {
                        val monthSnap = monthIterator.next()
                        val dateCount = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).childrenCount
                        val dateIterator = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).children.iterator()
                        for(date in 0 until dateCount) {
                            val dateSnap = dateIterator.next()
                            val dataCount =  dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).child(dateSnap.key!!).childrenCount
                            val dataIterator = dataSnapshot.child(yearSnap.key!!).child(monthSnap.key!!).child(dateSnap.key!!).children.iterator()
                            for(data in 0 until dataCount) {
                                val dataSnap = dataIterator.next()
                                val glucoseInfo = GlucoseInfo(
                                    createTime = dataSnap.child("createTime").value.toString().toLong(),
                                    year = dataSnap.child("year").value.toString(),
                                    month = dataSnap.child("month").value.toString(),
                                    day = dataSnap.child("day").value.toString(),
                                    hour = dataSnap.child("hour").value.toString(),
                                    minute = dataSnap.child("minute").value.toString(),
                                    value = dataSnap.child("value").value.toString().toInt(),
                                    updateTime = dataSnap.child("updateTime").value.toString().toLong()
                                )
                                glucoseInfoList.add(glucoseInfo)
                            }
                        }
                    }
                }
                petTestRepository.glucoseListLiveEvent.value = glucoseInfoList
                Log.d(TAG, "syncCurrentGlucoseInfoList sync is successful. glucoseList:$glucoseInfoList")
                if(!haveError) {
                    selectPickPetLiveData.value = createSelectPetEvent(Code.SELECT_PET_PICK_REQUEST_SUCCESS, petInfoList = newPetInfoList)
                } else {
                    selectPickPetLiveData.value = createSelectPetEvent(Code.SELECT_PET_PICK_SERVER_ERROR, petInfoList = newPetInfoList)
                }
            }
        })
    }

    fun deletePetInfo(petInfo: PetInfo) {
        Log.d(TAG, "deletePetInfo:$petInfo")
        if(petInfo != petTestRepository.petInfoLiveEvent.value) {
            databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petInfo.name).child("info").addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(databaseError: DatabaseError) {
                        Log.d(TAG, "deletePetInfo onCancelled${databaseError.message}")
                        selectDeletePetLiveData.value = createDeletePetEvent(status = Code.SELECT_PET_DELETE_SERVER_ERROR, message = databaseError.message)
                    }

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        Log.d(TAG, "deletePetInfo onDataChange:${dataSnapshot.value}")
                        if(dataSnapshot.exists()) {
                            databaseReference.child("users").child("${mFireBaseAuth.currentUser?.uid}").child("pets").child(petInfo.name).child("info").removeValue().addOnCompleteListener {
                                if(it.isSuccessful) {
                                    Log.d(TAG, "deletePetInfo remove ${petInfo.name} is successful")
                                    storageReference.child("users").child(mFireBaseAuth.currentUser?.uid!!).child("pets").child(petInfo.name).listAll().addOnCompleteListener { task->
                                        if(task.isSuccessful) {
                                            if(task.result?.items?.size!! > 0) {
                                                for(i in 0 until task.result?.items?.size!!) {
                                                    task.result?.items?.get(i)?.delete()?.addOnCompleteListener { mission->
                                                        if(!mission.isSuccessful) {
                                                            Log.d(TAG, "delete ${petInfo.name}'s storage photo(url:${task.result?.items?.get(i).toString()})")
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            Log.d(TAG,"assess ${petInfo.name}'s storage is fail: ${it.exception?.message}")
                                        }
                                    }
                                    val newPetInfoList = petTestRepository.petInfoListLiveEvent.value
                                    newPetInfoList?.remove(petInfo)
                                    selectDeletePetLiveData.value = createDeletePetEvent(status = Code.SELECT_PET_DELETE_REQUEST_SUCCESS, petInfoList = newPetInfoList, removePetInfo = petInfo)
                                } else {
                                    Log.d(TAG, "deletePetInfo remove fail :${it.exception?.message}")
                                    selectDeletePetLiveData.value = createDeletePetEvent(status = Code.SELECT_PET_DELETE_PET_INFO_ERROR, message = it.exception?.message)
                                }
                            }
                        } else {
                            Log.d(TAG, "deletePetInfo ${petInfo.name} is not exist")
                            selectDeletePetLiveData.value = createDeletePetEvent(status = Code.SELECT_PET_DELETE_NOT_EXIST, message = "${petInfo.name} folder is not exist")
                        }
                    }
                }
            )
        } else {
            Log.d(TAG, "deletePetInfo cannot delete")
            selectDeletePetLiveData.value = createDeletePetEvent(status = Code.SELECT_PET_DELETE_CURRENT_PET_ERROR)
        }
    }

    fun getUserPhoto() {
        getUserPhotoDisposable =  Observable.create<Bitmap> { emitter->
            val url = URL(petTestRepository.accountInfo?.avatarPhoto.toString())
            Log.d(TAG, "url:$url")
            val connection = url.openConnection()
            connection.doInput = true
            connection.connect()
            val inputStream = connection.getInputStream()
            emitter.onNext(BitmapFactory.decodeStream(inputStream))
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.from(Executors.newFixedThreadPool(2)))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({ bitmap ->
                userPhotoLiveEvent.value = BitmapDrawable(Resources.getSystem(), bitmap)
            }, { throwable: Throwable? ->
                Log.d(TAG, "getUserPhoto throwable:${throwable?.message}")
            })
    }

    private fun createSelectPetEvent(status: Int, message: String? = null, petInfoList: ArrayList<PetInfo>? = null): BaseEvent {
        return when (status) {
            Code.SELECT_PET_PICK_REQUEST_SUCCESS -> PickPetEvent.PickPetSuccess(status = status, petInfoList = petInfoList)
            Code.SELECT_PET_PICK_SERVER_ERROR -> PickPetEvent.PickPetServerError(status = status, message = message)
            else -> BaseEvent.OtherError(code = status)
        }
    }

    private fun createDeletePetEvent(status: Int, message: String? = null, petInfoList: ArrayList<PetInfo>? = null, removePetInfo: PetInfo? = null): BaseEvent {
        return when (status) {
            Code.SELECT_PET_DELETE_REQUEST_SUCCESS -> DeletePetEvent.Success(status = status, petInfoList = petInfoList, removePetInfo = removePetInfo)
            Code.SELECT_PET_DELETE_CURRENT_PET_ERROR -> DeletePetEvent.CurrentPetError(status = status)
            Code.SELECT_PET_DELETE_SERVER_ERROR -> DeletePetEvent.ServerError(status = status, message = message)
            Code.SELECT_PET_DELETE_NOT_EXIST -> DeletePetEvent.IsExistError(status = status, message =  message)
            Code.SELECT_PET_DELETE_PET_INFO_ERROR -> DeletePetEvent.PetInfoError(status = status, message = message)
            else -> DeletePetEvent.OtherError(status = status)
        }
    }

    override fun onCleared() {
        super.onCleared()
        getUserPhotoDisposable?.let { if (!it.isDisposed) it.dispose()}
    }
}