package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.net.wifi.ScanResult
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.BlePairEvent
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.github.pwittchen.reactivewifi.ReactiveWifi
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

class SettingViewModel(application: Application): BaseViewModel(application) {

    companion object {
        val TAG = SettingViewModel::class.java.simpleName

        val UUID_SN_RX: UUID = UUID.fromString("2babf174-7421-462d-bc4d-fe4cb083bb54")
        val UUID_SN_TX: UUID = UUID.fromString("fdc55954-cc80-46a6-900a-4ee662c41d8f")
        val UUID_TOKEN_TX: UUID = UUID.fromString("4eda69ee-2aa0-4152-ae50-206f0968e7da")
    }

    var isProcessing = MutableLiveData<Boolean>()
    var isPaired = MutableLiveData<Boolean>(false)
    var bluetoothStatusLiveData = SingleLiveEvent<Boolean>()
    var bleClientStatus = SingleLiveEvent<RxBleClient.State>()
    private var bleConnectionDisposable: Disposable? = null
    var blePairSingleLiveEvent = SingleLiveEvent<BaseEvent>()
    private var scanningWifiDisposable: Disposable? = null
    var scanResultListSingleLiveEvent = SingleLiveEvent<ArrayList<ScanResult>>()
    private var sendTokenDisposable: Disposable? = null
    var wifiSSID = MutableLiveData<String>()
    var wifiPassword = MutableLiveData<String>()
    var firebaseUUID = MutableLiveData<String>()
    var currentPetName = MutableLiveData<String>()

    private val rxBleClient: RxBleClient = RxBleClient.create(getApplication())

    init {
        firebaseUUID.value = mFireBaseAuth.currentUser?.uid.toString()
        currentPetName.value = petTestRepository.petInfoLiveEvent.value?.name!!
    }

    fun scanBLEDevice() {
        Log.d(TAG, "scanBLEDevice")
        val scanSubscription = rxBleClient.scanBleDevices(ScanSettings.Builder().build())
            .subscribe ({ result->
            Log.d(TAG, "result:${result.bleDevice.name} address:${result.bleDevice.macAddress}") },
                { throwable->
                Log.d(TAG, "throwable:${throwable.message}")
            })
        scanSubscription.dispose()
    }

    fun connectToDevice(bluetoothDevice: BluetoothDevice) {
        Log.d(TAG, "connectToDevice:${bluetoothDevice.address}")
        val rxBleDevice = rxBleClient.getBleDevice(bluetoothDevice.address)
        bleConnectionDisposable =
            rxBleDevice.establishConnection(false)
            .subscribeOn(Schedulers.io())
            .flatMapSingle { rxBleConnection ->
                rxBleConnection.writeCharacteristic(UUID_SN_TX, petTestRepository.accountInfo?.sn!!.toByteArray())
                    .flatMap{ rxBleConnection.readCharacteristic(UUID_SN_RX) }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ byteArray->
                val serverSN = byteArray.toString(Charsets.UTF_8)
                Log.d(TAG, "serverSN:${serverSN}")
                if(serverSN == petTestRepository.accountInfo?.sn!!) {
                    blePairSingleLiveEvent.value = createBlePairEvent(status = Code.SETTING_BLE_VERIFY_SN_SUCCESS)
                } else {
                    Log.d(TAG, "App SN:${petTestRepository.accountInfo?.sn!!} and dongle SN:$serverSN are not match")
                    blePairSingleLiveEvent.value = createBlePairEvent(status = Code.SETTING_BLE_VERIFY_SN_FAIL)
                }
            },{ throwable->
                Log.d(TAG, "throwable:${throwable.message}")
                blePairSingleLiveEvent.value = createBlePairEvent(status = Code.SETTING_BLE_SEND_SN_FAIL, message = throwable.message)
            })
    }

    fun scanWifi(context: Context) {
        scanningWifiDisposable =
            ReactiveWifi.observeWifiAccessPoints(context)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ results->
                    for(i in 0 until results.size) {
                        Log.d(TAG, "scanning success result[$i]:${results[i].SSID}")
                    }
                    scanResultListSingleLiveEvent.value = results as ArrayList<ScanResult>
                }, { throwable->
                    Log.d(TAG, "scanWifi is fail:${throwable.message}")
                    blePairSingleLiveEvent.value = createBlePairEvent(status = Code.SETTING_BLE_SCAN_WIFI_FAIL, message = throwable.message)
                })
    }

    private fun createBlePairEvent(status: Int, message: String? = null): BaseEvent {
        return when (status) {
            Code.SETTING_BLE_REQUEST_SUCCESS -> BlePairEvent.PairSuccess()
            Code.SETTING_BLE_VERIFY_SN_SUCCESS -> BlePairEvent.VerifySuccess()
            Code.SETTING_BLE_SEND_SN_FAIL -> BlePairEvent.SendSNError(Code.SETTING_BLE_SEND_SN_FAIL, message)
            Code.SETTING_BLE_VERIFY_SN_FAIL -> BlePairEvent.VerifySNError(Code.SETTING_BLE_VERIFY_SN_FAIL)
            Code.SETTING_BLE_SCAN_WIFI_FAIL -> BlePairEvent.ScanWifiError(Code.SETTING_BLE_SCAN_WIFI_FAIL, message)
            Code.SETTING_BLE_SEND_TOKEN_FAIL -> BlePairEvent.SendTokenError(Code.SETTING_BLE_SEND_TOKEN_FAIL, message)
            else -> BlePairEvent.OtherError(status)
        }
    }

    fun sendTokenToDongle(bluetoothDevice: BluetoothDevice) {
        Log.d(TAG, "sendTokenToDongle:${bluetoothDevice.address}")
        val rxBleDevice = rxBleClient.getBleDevice(bluetoothDevice.address)
        sendTokenDisposable =
            rxBleDevice.establishConnection(false)
                .subscribeOn(Schedulers.io())
                .flatMapSingle { rxBleConnection ->
                    val token = "${wifiSSID.value}-${wifiPassword.value}-${firebaseUUID.value}-${currentPetName.value}"
                    Log.d(TAG, "token:$token")
                    rxBleConnection.writeCharacteristic(UUID_TOKEN_TX, token.toByteArray())
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    blePairSingleLiveEvent.value = createBlePairEvent(status = Code.SETTING_BLE_REQUEST_SUCCESS)
                },{ throwable->
                    Log.d(TAG, "throwable:${throwable.message}")
                    blePairSingleLiveEvent.value = createBlePairEvent(status = Code.SETTING_BLE_SEND_TOKEN_FAIL, message = throwable.message)
                })
    }

    fun closeBleConnection() {
        bleConnectionDisposable?.let { if (!it.isDisposed) it.dispose() }
    }

    fun closeScanWifi() {
        scanningWifiDisposable?.let { if (!it.isDisposed) it.dispose() }
    }

    fun closeSendToken() {
        sendTokenDisposable?.let { if (!it.isDisposed) it.dispose() }
    }

    override fun onCleared() {
        closeBleConnection()
        closeScanWifi()
        closeSendToken()
    }
}