package com.broadmasterbiotech.library.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.broadmasterbiotech.library.database.PetTestRepository
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import java.util.*
import kotlin.collections.ArrayList

class RecordViewModel(application: Application): AndroidViewModel(application) {

    companion object {

        val TAG = RecordViewModel::class.java.simpleName
    }

    var petTestRepository: PetTestRepository = PetTestRepository.providePetTestRepository(application.applicationContext)
    var isProcessing = MutableLiveData<Boolean>()
    var isExpand = MutableLiveData<Boolean>(false)
    var currentGlucoseDataPeriod = MutableLiveData(0)

    fun petInfoExpandOnClick() {
        isExpand.value = !isExpand.value!!
    }

    fun filterGlucoseDataPeriod(glucoseInfoList: ArrayList<GlucoseInfo>): ArrayList<GlucoseInfo> {
        if(glucoseInfoList.size > 0) {
            return when (currentGlucoseDataPeriod.value) {
                0 -> glucoseInfoList
                else -> {
                    val filterGlucoseInfoList = ArrayList<GlucoseInfo>()
                    val latestCalendar = Calendar.getInstance()
                    latestCalendar.set(Calendar.YEAR, glucoseInfoList[glucoseInfoList.size-1].year.toInt())
                    latestCalendar.set(Calendar.MONTH, glucoseInfoList[glucoseInfoList.size-1].month.toInt()-1)
                    latestCalendar.set(Calendar.DAY_OF_MONTH, glucoseInfoList[glucoseInfoList.size-1].day.toInt())
                    latestCalendar.set(Calendar.HOUR_OF_DAY, glucoseInfoList[glucoseInfoList.size-1].hour.toInt())
                    latestCalendar.set(Calendar.MINUTE, glucoseInfoList[glucoseInfoList.size-1].minute.toInt())
                    if(currentGlucoseDataPeriod.value == 1) {
                        latestCalendar.add(Calendar.MONTH, -1)
                    } else if (currentGlucoseDataPeriod.value == 2) {
                        latestCalendar.add(Calendar.MONTH, -3)
                    }
                    for(i in 0 until glucoseInfoList.size) {
                        val tempCalendar = Calendar.getInstance()
                        tempCalendar.set(Calendar.YEAR, glucoseInfoList[i].year.toInt())
                        tempCalendar.set(Calendar.MONTH, glucoseInfoList[i].month.toInt()-1)
                        tempCalendar.set(Calendar.DAY_OF_MONTH, glucoseInfoList[i].day.toInt())
                        tempCalendar.set(Calendar.HOUR_OF_DAY, glucoseInfoList[i].hour.toInt())
                        tempCalendar.set(Calendar.MINUTE, glucoseInfoList[i].minute.toInt())
                        if(tempCalendar > latestCalendar) {
                            filterGlucoseInfoList.add(glucoseInfoList[i])
                        }
                    }
                    return filterGlucoseInfoList
                }
            }
        } else {
            return glucoseInfoList
        }
    }
}
