package com.broadmasterbiotech.library.database.note

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName =  NoteInfo.TABLE_NAME)
data class NoteInfo(

    @ColumnInfo(name="createTime") @PrimaryKey(autoGenerate = true) var createTime: Long? = System.currentTimeMillis(),

    @ColumnInfo(name="type") var type: Int,

    @ColumnInfo(name="topic") var topic: String? = "",

    @ColumnInfo(name="content") var content: String? = "",

    @ColumnInfo(name="mediaUri") var mediaUri: String? = "",

    @ColumnInfo(name="date") var date: String,

    @ColumnInfo(name="time") var time: String,

    @ColumnInfo(name="updateTime") var updateTime: Long? = System.currentTimeMillis()

) : Parcelable, Comparable<NoteInfo> {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeLong(createTime!!)
        dest?.writeInt(type)
        dest?.writeString(topic)
        dest?.writeString(content)
        dest?.writeString(date)
        dest?.writeString(time)
        dest?.writeString(mediaUri)
        dest?.writeLong(updateTime!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun compareTo(other: NoteInfo): Int {
        val timeSplit = this.time.split(":")
        val otherSplit = other.time.split(":")
        val compareToHour = timeSplit[0].toInt().compareTo(otherSplit[0].toInt())
        if(compareToHour == 0 ) {
            val compareToMinute = timeSplit[1].toInt().compareTo(otherSplit[1].toInt())
            if(compareToMinute == 0) {
                return updateTime!!.compareTo(other.updateTime!!)
            }
            return compareToMinute
        }
        return compareToHour
    }

    companion object CREATOR : Parcelable.Creator<NoteInfo> {

        const val TABLE_NAME = "NoteInfo"

        override fun createFromParcel(parcel: Parcel): NoteInfo {
            return NoteInfo(parcel)
        }

        override fun newArray(size: Int): Array<NoteInfo?> {
            return arrayOfNulls(size)
        }
    }

}
