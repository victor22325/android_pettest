package com.broadmasterbiotech.library.database

import com.broadmasterbiotech.library.database.account.AccountInfoDAO
import com.broadmasterbiotech.library.database.note.NoteInfoDAO
import com.broadmasterbiotech.library.database.reminder.ReminderInfoDAO

data class DaoList constructor(
    var accountInfoDAO: AccountInfoDAO,
    var noteInfoDAO: NoteInfoDAO,
    var reminderInfoDAO: ReminderInfoDAO
)