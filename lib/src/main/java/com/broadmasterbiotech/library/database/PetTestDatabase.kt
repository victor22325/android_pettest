package com.broadmasterbiotech.library.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.broadmasterbiotech.library.database.account.AccountInfo
import com.broadmasterbiotech.library.database.account.AccountInfoDAO
import com.broadmasterbiotech.library.database.note.NoteInfo
import com.broadmasterbiotech.library.database.note.NoteInfoDAO
import com.broadmasterbiotech.library.database.reminder.ReminderInfo
import com.broadmasterbiotech.library.database.reminder.ReminderInfoDAO
import com.broadmasterbiotech.library.util.SingletonHolder
import com.broadmasterbiotech.library.util.Util

/**
 * The Room Database that contains the Nas table.
 */


@Database(entities = [AccountInfo::class, NoteInfo::class, ReminderInfo::class], version = 1, exportSchema = false)
abstract class PetTestDatabase : RoomDatabase() {

    lateinit var daoList: DaoList
    abstract fun accountInfoDao(): AccountInfoDAO
    abstract fun noteInfoDao(): NoteInfoDAO
    abstract fun reminderInfoDao(): ReminderInfoDAO

    companion object : SingletonHolder<PetTestDatabase, Context>({
        val database = Room.databaseBuilder(it.applicationContext, PetTestDatabase::class.java, Util.dbNamePetTestInfo).build()
        database.daoList = DaoList(database.accountInfoDao(), database.noteInfoDao(), database.reminderInfoDao())
        database
    })
}