package com.broadmasterbiotech.library.database.news

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.database.Exclude

@Entity(tableName =  NewsInfo.TABLE_NAME)
class NewsInfo(

    @ColumnInfo(name="title") @PrimaryKey(autoGenerate = true) var title: String,

    @ColumnInfo(name="subTitle") var subTitle: String? = null,

    @ColumnInfo(name="content") var content: String? = null,

    @ColumnInfo(name="thumbnailUri") var thumbnailUri: String? = null,

    @ColumnInfo(name="contentUri") var contentUri: String? = null,

    @ColumnInfo(name="date") var date: String,

    @ColumnInfo(name="updateTime") var updateTime: Long? = System.currentTimeMillis()

    ): Parcelable, Comparable<NewsInfo> {

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "title" to title,
            "subTitle" to subTitle,
            "content" to content,
            "thumbnailUri" to thumbnailUri,
            "contentUri" to contentUri,
            "date" to date
        )
    }

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()!!,
        parcel.readLong()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(title)
        dest?.writeString(subTitle)
        dest?.writeString(content)
        dest?.writeString(thumbnailUri)
        dest?.writeString(contentUri)
        dest?.writeString(date)
        dest?.writeLong(updateTime!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun compareTo(other: NewsInfo): Int {
        val thisSplit = date.split("/")
        val otherSplit = other.date.split("/")
        val compareToYear = thisSplit[0].toInt().compareTo(otherSplit[0].toInt())
        if(compareToYear == 0) {
            val compareToMonth = thisSplit[1].toInt().compareTo(otherSplit[1].toInt())
            if(compareToMonth == 0) {
                val compareToDate = thisSplit[2].toInt().compareTo(otherSplit[2].toInt())
                if(compareToDate == 0) {
                    return this.updateTime!!.compareTo(other.updateTime!!) * -1
                }
                return compareToDate * -1
            }
            return compareToMonth * -1
        }
        return compareToYear * -1
    }

    companion object CREATOR : Parcelable.Creator<NewsInfo> {

        const val TABLE_NAME = "NewsInfo"

        override fun createFromParcel(parcel: Parcel): NewsInfo {
            return NewsInfo(parcel)
        }

        override fun newArray(size: Int): Array<NewsInfo?> {
            return arrayOfNulls(size)
        }
    }
}