package com.broadmasterbiotech.library.database.reminder

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*

@Entity(tableName =  ReminderInfo.TABLE_NAME)
data class ReminderInfo(

    @ColumnInfo(name="createTime") @PrimaryKey(autoGenerate = true) var createTime: Long? = System.currentTimeMillis(),

    @ColumnInfo(name="type") var type: Int,

    @ColumnInfo(name="status") var status: Int,

    @ColumnInfo(name="date") var date: String? = "",

    @ColumnInfo(name="time") var time: String,

    @ColumnInfo(name="repeat") var repeat: Int? = 0,

    @ColumnInfo(name="notificationType") var notificationType: Int,

    @ColumnInfo(name="label") var label: String? = "",

    @ColumnInfo(name="updateTime") var updateTime: Long? = System.currentTimeMillis()

    ) : Parcelable, Comparable<ReminderInfo> {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readLong()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(createTime!!)
        parcel.writeInt(type)
        parcel.writeInt(status)
        parcel.writeString(date)
        parcel.writeString(time)
        parcel.writeInt(repeat!!)
        parcel.writeInt(notificationType)
        parcel.writeString(label)
        parcel.writeLong(updateTime!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun compareTo(other: ReminderInfo): Int {
        val timeSplit = time.split(":")
        val otherSplit = other.time.split(":")
        val compareToHour = timeSplit[0].toInt().compareTo(otherSplit[0].toInt())
        if(compareToHour == 0) {
            val compareToMinute = timeSplit[1].toInt().compareTo(otherSplit[1].toInt())
            if(compareToHour == 0) {
                return this.updateTime!!.compareTo(other.updateTime!!)
            }
            return compareToMinute
        }
        return compareToHour
    }

    companion object CREATOR : Parcelable.Creator<ReminderInfo> {

        const val TABLE_NAME = "ReminderInfo"

        override fun createFromParcel(parcel: Parcel): ReminderInfo {
            return ReminderInfo(parcel)
        }

        override fun newArray(size: Int): Array<ReminderInfo?> {
            return arrayOfNulls(size)
        }
    }
}
