package com.broadmasterbiotech.library.database

import android.annotation.SuppressLint
import android.util.Log
import com.broadmasterbiotech.library.database.account.AccountInfo
import com.broadmasterbiotech.library.database.note.NoteInfo
import com.broadmasterbiotech.library.database.reminder.ReminderInfo
import com.broadmasterbiotech.library.util.SingletonHolder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@SuppressLint("CheckResult")
class PetTestLocalDataSource private constructor(private val daoList: DaoList) : PetTestDataSource {

    companion object : SingletonHolder<PetTestLocalDataSource, DaoList>(::PetTestLocalDataSource){

        private val TAG = PetTestLocalDataSource::class.java.simpleName
    }

    override fun getAccountList(callback: PetTestDataSource.LoadAccountListCallback) {
        Observable.create<List<AccountInfo>> { emitter ->
            val accountList = daoList.accountInfoDAO.allAccount()
            emitter.onNext(accountList)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                callback.onAccountListLoaded(it)
            }
    }

    override fun getAccountByEmailAddress(emailAddress: String): Observable<AccountInfo> {
        return Observable
            .create<AccountInfo> { emitter ->
                val accountInfo = daoList.accountInfoDAO.getAccountByEmailAddress(emailAddress)
                emitter.onNext(accountInfo)
                emitter.onComplete()
            }.subscribeOn(Schedulers.io())
    }

    override fun saveAccountInfo(accountInfo: AccountInfo) {
        Observable.create<Any> { emitter ->
            val newAccountInfo = accountInfo.copy()
            val allAccountInfo = daoList.accountInfoDAO.allAccount() as ArrayList
            var index = -1
            for (i in 0 until allAccountInfo.size) {
                if (allAccountInfo[i].emailAddress == newAccountInfo.emailAddress) {
                    index = i
                }
            }
            if (index != -1) {
                allAccountInfo[index] = newAccountInfo
            } else {
                allAccountInfo.add(newAccountInfo)
            }
            daoList.accountInfoDAO.insertAllAccount(*allAccountInfo.toTypedArray())
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun deleteAccountByEmailAddress(emailAddress: String) {
        Observable.create<Any> { emitter ->
            val index = daoList.accountInfoDAO.deleteAccountByEmailAddress(emailAddress)
            Log.d(TAG, String.format("deleteAccountByEmail: %d", index))
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun deleteAllAccount() {
        Observable.create<Any> { emitter ->
            daoList.accountInfoDAO.deleteAllAccount()
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun deleteAccountByAccountInfo(vararg accountInfo: AccountInfo) {
        Observable.create<Any> { emitter ->
            daoList.accountInfoDAO.deleteAccountByAccountInfo(*accountInfo)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun updateAccount(accountInfo: AccountInfo): Observable<Int> {
        return Observable.create<Int> { emitter ->
            emitter.onNext(daoList.accountInfoDAO.updateAccount(accountInfo))
            emitter.onComplete()
        }.subscribeOn(Schedulers.io())
    }

    override fun getAllNoteList(callback: PetTestDataSource.LoadNoteListCallback) {
        Observable.create<List<NoteInfo>> { emitter ->
            val noteList = daoList.noteInfoDAO.allNoteInfo()
            emitter.onNext(noteList)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                callback.onNoteListLoaded(it)
            }
    }

    override fun getNoteListByDate(date: String): Observable<List<NoteInfo>> {
        return Observable.create<List<NoteInfo>> { emitter ->
            val noteList = daoList.noteInfoDAO.getNoteListByDate(date)
            emitter.onNext(noteList)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun saveNote(noteInfo: NoteInfo) {
        Observable.create<Any> { emitter ->
            val newNoteInfo = noteInfo.copy()
            val allNoteInfoList = daoList.noteInfoDAO.allNoteInfo() as ArrayList
            var index = -1
            for (i in 0 until allNoteInfoList.size) {
                if (allNoteInfoList[i].createTime == newNoteInfo.createTime) {
                    index = i
                }
            }
            if (index != -1) {
                allNoteInfoList[index] = newNoteInfo
            } else {
                allNoteInfoList.add(newNoteInfo)
            }
            daoList.noteInfoDAO.insertAllNote(*allNoteInfoList.toTypedArray())
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun updateNote(noteInfo: NoteInfo) {
        Observable.create<Int> { emitter ->
            emitter.onNext(daoList.noteInfoDAO.updateNote(noteInfo))
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun deleteNote(vararg noteInfo: NoteInfo) {
        Observable.create<Any> { emitter ->
            daoList.noteInfoDAO.deleteNote(*noteInfo)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun getAllReminderList(callback: PetTestDataSource.LoadReminderListCallback) {
        Observable.create<List<ReminderInfo>> { emitter ->
            val reminderList = daoList.reminderInfoDAO.getAllReminderInfoList()
            emitter.onNext(reminderList)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                callback.onReminderListLoaded(it)
            }
    }

    override fun getReminderListByTime(time: String): Observable<List<ReminderInfo>> {
        return Observable.create<List<ReminderInfo>> { emitter ->
            val reminderList = daoList.reminderInfoDAO.getReminderInfoByTime(time)
            emitter.onNext(reminderList)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun saveReminder(reminderInfo: ReminderInfo) {
        Observable.create<Any> { emitter ->
            val newReminderInfo = reminderInfo.copy()
            val allReminderInfoList = daoList.reminderInfoDAO.getAllReminderInfoList() as ArrayList
            var index = -1
            for (i in 0 until allReminderInfoList.size) {
                if (allReminderInfoList[i].createTime == newReminderInfo.createTime) {
                    index = i
                }
            }
            if (index != -1) {
                allReminderInfoList[index] = newReminderInfo
            } else {
                allReminderInfoList.add(newReminderInfo)
            }
            daoList.reminderInfoDAO.insertAllReminderInfo(*allReminderInfoList.toTypedArray())
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun updateReminder(reminderInfo: ReminderInfo) {
        Observable.create<Int> { emitter ->
            emitter.onNext(daoList.reminderInfoDAO.updateReminderInfo(reminderInfo))
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun deleteReminder(vararg reminderInfo: ReminderInfo) {
        Observable.create<Any> { emitter ->
            daoList.reminderInfoDAO.deleteReminderInfo(*reminderInfo)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }
}