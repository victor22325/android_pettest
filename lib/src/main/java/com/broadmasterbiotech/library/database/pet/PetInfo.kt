package com.broadmasterbiotech.library.database.pet

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.database.Exclude

@Entity(tableName =  PetInfo.TABLE_NAME)
data class PetInfo(

    @ColumnInfo(name="name")@PrimaryKey var name: String,

    @ColumnInfo(name="species") var species: Int,

    @ColumnInfo(name="breed") var breed: String? = "",

    @ColumnInfo(name="weight") var weight: String? = "",

    @ColumnInfo(name="weightUnit") var weightUnit: Int? = 0,

    @ColumnInfo(name="gender") var gender: Int? = 0,

    @ColumnInfo(name="birth") var birth: String? = "",

    @ColumnInfo(name="glucoseUpper") var glucoseUpper: String,

    @ColumnInfo(name="glucoseLower") var glucoseLower: String,

    @ColumnInfo(name="diabetesType") var diabetesType: Int? = 0,

    @ColumnInfo(name="diabetesYearDiagnosed") var diabetesYearDiagnosed: String? = "",

    @ColumnInfo(name="avatarPhoto") var avatarPhoto: String? = "",

    @ColumnInfo(name="isSelected") var isSelected: Boolean? = false,

    @ColumnInfo(name="updateTime") var updateTime: Long? = System.currentTimeMillis()

    ) :Parcelable {

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "name" to name,
            "species" to species,
            "breed" to breed,
            "weight" to weight,
            "weightUnit" to weightUnit,
            "gender" to gender,
            "birth" to birth,
            "glucoseUpper" to glucoseUpper,
            "glucoseLower" to glucoseLower,
            "diabetesType" to diabetesType,
            "avatarPhoto" to avatarPhoto,
            "isSelected" to isSelected,
            "diabetesYearDiagnosed" to diabetesYearDiagnosed
        )
    }

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readBoolean(),
        parcel.readLong()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(species)
        parcel.writeString(breed)
        parcel.writeString(weight)
        parcel.writeInt(weightUnit?:0)
        parcel.writeInt(gender?:0)
        parcel.writeString(birth)
        parcel.writeString(glucoseUpper)
        parcel.writeString(glucoseLower)
        parcel.writeInt(diabetesType?:0)
        parcel.writeString(diabetesYearDiagnosed?:"")
        parcel.writeString(avatarPhoto?:"")
        parcel.writeBoolean(isSelected?:false)
        parcel.writeLong(updateTime!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PetInfo> {

        const val TABLE_NAME = "PetInfo"

        override fun createFromParcel(parcel: Parcel): PetInfo {
            return PetInfo(parcel)
        }

        override fun newArray(size: Int): Array<PetInfo?> {
            return arrayOfNulls(size)
        }
    }
}