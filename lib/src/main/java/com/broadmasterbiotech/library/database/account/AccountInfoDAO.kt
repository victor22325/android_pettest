package com.broadmasterbiotech.library.database.account

import androidx.room.*
import com.broadmasterbiotech.library.database.account.AccountInfo.CREATOR.TABLE_NAME

/**
 * Immutable model class for a PetTest.
 */

@Dao
interface AccountInfoDAO {

    @Query("SELECT * FROM $TABLE_NAME ORDER BY updateTime DESC")
    fun allAccount(): List<AccountInfo>

    @Query("SELECT * FROM $TABLE_NAME WHERE emailAddress = :emailAddress")
    fun getAccountByEmailAddress(emailAddress: String): AccountInfo

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllAccount(vararg accountInfoList: AccountInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAccount(accountInfo: AccountInfo)

    @Query("DELETE FROM $TABLE_NAME WHERE emailAddress = :emailAddress")
    fun deleteAccountByEmailAddress(emailAddress: String): Int

    @Query("DELETE FROM $TABLE_NAME")
    fun deleteAllAccount()

    @Update
    fun updateAccount(accountInfo: AccountInfo): Int

    @Delete
    fun deleteAccountByAccountInfo(vararg accountInfo: AccountInfo)
}