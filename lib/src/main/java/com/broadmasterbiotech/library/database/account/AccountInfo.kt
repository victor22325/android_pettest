package com.broadmasterbiotech.library.database.account

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.database.Exclude

@Entity(tableName =  AccountInfo.TABLE_NAME)
data class AccountInfo(

    @ColumnInfo(name="emailAddress")@PrimaryKey var emailAddress: String,

    @ColumnInfo(name = "password") var password: String? ="",

    @ColumnInfo(name="sn") var sn: String,

    @ColumnInfo(name="firstName") var firstName: String? = "",

    @ColumnInfo(name="lastName") var lastName: String? = "",

    @ColumnInfo(name="country") var country: String? = "",

    @ColumnInfo(name="state") var state: String? = "",

    @ColumnInfo(name="city") var city: String? = "",

    @ColumnInfo(name="postalCode") var postalCode: String? = "",

    @ColumnInfo(name="avatarPhoto") var avatarPhoto: String? = "",

    @ColumnInfo(name="updateTime") var updateTime: Long? = System.currentTimeMillis()

) : Parcelable {

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf (
            "emailAddress" to emailAddress,
            "password" to password,
            "sn" to sn,
            "firstName" to firstName,
            "lastName" to lastName,
            "country" to country,
            "state" to state,
            "city" to city,
            "postalCode" to postalCode,
            "avatarPhoto" to avatarPhoto
        )
    }

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString(),
        parcel.readString()!!,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong()
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(emailAddress)
        parcel.writeString(password)
        parcel.writeString(sn)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(country)
        parcel.writeString(state)
        parcel.writeString(city)
        parcel.writeString(postalCode)
        parcel.writeString(avatarPhoto)
        parcel.writeLong(updateTime!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AccountInfo> {

        const val TABLE_NAME = "AccountInfo"

        override fun createFromParcel(parcel: Parcel): AccountInfo {
            return AccountInfo(parcel)
        }

        override fun newArray(size: Int): Array<AccountInfo?> {
            return arrayOfNulls(size)
        }
    }
}