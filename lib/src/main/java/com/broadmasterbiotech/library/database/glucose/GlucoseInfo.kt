package com.broadmasterbiotech.library.database.glucose

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName =  GlucoseInfo.TABLE_NAME)
data class GlucoseInfo(

    @ColumnInfo(name="createTime") @PrimaryKey(autoGenerate = true) var createTime: Long? = System.currentTimeMillis(),

    @ColumnInfo(name="year") var year: String,

    @ColumnInfo(name="month") var month: String,

    @ColumnInfo(name="day") var day: String,

    @ColumnInfo(name="hour") var hour: String,

    @ColumnInfo(name = "minute") var minute: String,

    @ColumnInfo(name="value") var value: Int,

    @ColumnInfo(name="updateTime") var updateTime: Long? = System.currentTimeMillis()

) : Parcelable, Comparable<GlucoseInfo> {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readLong()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeLong(createTime!!)
        dest?.writeString(year)
        dest?.writeString(month)
        dest?.writeString(day)
        dest?.writeString(hour)
        dest?.writeString(minute)
        dest?.writeInt(value)
        dest?.writeLong(updateTime!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun compareTo(other: GlucoseInfo): Int {
        val compareToYear = this.year.compareTo(other.year)
        if(compareToYear == 0) {
            val compareToMonth = this.month.compareTo(other.month)
            if(compareToMonth == 0) {
                val compareToDate = this.day.compareTo(other.day)
                if(compareToDate == 0) {
                    val compareToHour = this.hour.compareTo(other.hour)
                    if(compareToHour == 0) {
                        val compareToMinute = this.minute.compareTo(other.minute)
                        if(compareToMinute == 0) {
                            return this.updateTime!!.compareTo(other.updateTime!!)
                        }
                        return compareToMinute
                    }
                    return compareToHour
                }
                return compareToDate
            }
            return compareToMonth
        }
        return compareToYear
    }

    companion object CREATOR : Parcelable.Creator<GlucoseInfo> {

        const val TABLE_NAME = "GlucoseInfo"

        override fun createFromParcel(parcel: Parcel): GlucoseInfo {
            return GlucoseInfo(parcel)
        }

        override fun newArray(size: Int): Array<GlucoseInfo?> {
            return arrayOfNulls(size)
        }
    }
}