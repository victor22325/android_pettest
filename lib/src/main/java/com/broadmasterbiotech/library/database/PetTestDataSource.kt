package com.broadmasterbiotech.library.database

import androidx.annotation.NonNull
import com.broadmasterbiotech.library.database.account.AccountInfo
import com.broadmasterbiotech.library.database.note.NoteInfo
import com.broadmasterbiotech.library.database.reminder.ReminderInfo
import io.reactivex.Observable


interface PetTestDataSource {

    //account
    interface LoadAccountListCallback {
        fun onAccountListLoaded(accountList: List<AccountInfo>)
    }
    fun getAccountList(callback: LoadAccountListCallback)
    fun getAccountByEmailAddress(@NonNull emailAddress: String): Observable<AccountInfo>
    fun saveAccountInfo(@NonNull accountInfo: AccountInfo)
    fun deleteAccountByEmailAddress(@NonNull emailAddress: String)
    fun deleteAllAccount()
    fun updateAccount(@NonNull accountInfo: AccountInfo): Observable<Int>
    fun deleteAccountByAccountInfo(@NonNull vararg accountInfo: AccountInfo)

    //note
    interface LoadNoteListCallback {
        fun onNoteListLoaded(noteList: List<NoteInfo>)
    }
    fun getAllNoteList(callback: LoadNoteListCallback)
    fun getNoteListByDate(@NonNull date: String): Observable<List<NoteInfo>>
    fun saveNote(@NonNull noteInfo: NoteInfo)
    fun updateNote(@NonNull noteInfo: NoteInfo)
    fun deleteNote(@NonNull vararg noteInfo: NoteInfo)

    //reminder
    interface LoadReminderListCallback {
        fun onReminderListLoaded(reminderList: List<ReminderInfo>)
    }
    fun getAllReminderList(callback: LoadReminderListCallback)
    fun getReminderListByTime(@NonNull time: String): Observable<List<ReminderInfo>>
    fun saveReminder(@NonNull reminderInfo: ReminderInfo)
    fun updateReminder(@NonNull reminderInfo: ReminderInfo)
    fun deleteReminder(@NonNull vararg  reminderInfo: ReminderInfo)


}