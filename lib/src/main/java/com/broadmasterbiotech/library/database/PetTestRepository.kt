package com.broadmasterbiotech.library.database

import android.content.Context
import com.broadmasterbiotech.library.database.PetTestDataSource.LoadAccountListCallback
import com.broadmasterbiotech.library.database.account.AccountInfo
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.library.database.note.NoteInfo
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.database.reminder.ReminderInfo
import com.broadmasterbiotech.library.util.SingleLiveEvent
import com.broadmasterbiotech.library.util.SingletonHolder
import com.google.android.gms.common.util.Strings
import io.reactivex.Observable
import kotlin.collections.ArrayList


/**
 * Concrete implementation to load PetTest from the data sources into a cache.
 */

class PetTestRepository
private constructor(private val petTestLocalDataSource: PetTestLocalDataSource) : PetTestDataSource{

    companion object : SingletonHolder<PetTestRepository, PetTestLocalDataSource>(::PetTestRepository){

        private val TAG = PetTestRepository::class.java.simpleName

        fun providePetTestRepository(context: Context): PetTestRepository {
//            create ROOM database
            val database = PetTestDatabase.getInstance(context.applicationContext)
            return getInstance(
                PetTestLocalDataSource.getInstance(database.daoList))
        }
    }

    var accountInfo: AccountInfo? = null
        private set

    var petInfoLiveEvent = SingleLiveEvent<PetInfo>()

    var petInfoListLiveEvent = SingleLiveEvent<ArrayList<PetInfo>>()

    var glucoseListLiveEvent = SingleLiveEvent<ArrayList<GlucoseInfo>>()

    var reminderInfoListLiveEvent = SingleLiveEvent<ArrayList<ReminderInfo>>()

    var newsInfoListLiveEvent = SingleLiveEvent<ArrayList<NewsInfo>>()

    // account
    fun setAccountInfo(accountInfo: AccountInfo, writeDB: Boolean) {
        this.accountInfo = accountInfo
        if (writeDB && !Strings.isEmptyOrWhitespace(accountInfo.emailAddress)) {
            saveAccountInfo(accountInfo)
        }
    }

    // pet
    fun setCurrentPetInfo(petInfo: PetInfo) {
        var isFound = false
        for(i in 0 until  petInfoListLiveEvent.value?.size!!) {
            if(petInfoListLiveEvent.value?.get(i) == petInfo) {
                isFound = true
                petInfoListLiveEvent.value?.get(i)?.isSelected = true
            } else {
                petInfoListLiveEvent.value?.get(i)?.isSelected = false
            }
        }
        if(!isFound) {
            petInfoListLiveEvent.value?.get(petInfoListLiveEvent.value?.size!!)?.isSelected = true
            this.petInfoLiveEvent.value = petInfoListLiveEvent.value?.get(petInfoListLiveEvent.value?.size!!)
        } else {
            this.petInfoLiveEvent.value = petInfo
        }
    }

    override fun getAccountList(callback: LoadAccountListCallback) {
        petTestLocalDataSource.getAccountList(object : LoadAccountListCallback {
            override fun onAccountListLoaded(accountList: List<AccountInfo>) {
                callback.onAccountListLoaded(accountList)
            }
        })
    }

    override fun getAccountByEmailAddress(emailAddress: String): Observable<AccountInfo> {
        return petTestLocalDataSource.getAccountByEmailAddress(emailAddress)
    }

    override fun saveAccountInfo(accountInfo: AccountInfo) {
        accountInfo.updateTime = System.currentTimeMillis()
        petTestLocalDataSource.saveAccountInfo(accountInfo)
    }

    override fun deleteAccountByEmailAddress(emailAddress: String) {
        petTestLocalDataSource.deleteAccountByEmailAddress(emailAddress)
    }

    override fun deleteAllAccount() {
        petTestLocalDataSource.deleteAllAccount()
    }

    override fun deleteAccountByAccountInfo(vararg accountInfo: AccountInfo) {
        petTestLocalDataSource.deleteAccountByAccountInfo(*accountInfo)
    }

    override fun updateAccount(accountInfo: AccountInfo): Observable<Int> {
        return petTestLocalDataSource.updateAccount(accountInfo)
    }

    override fun getAllNoteList(callback: PetTestDataSource.LoadNoteListCallback) {
        petTestLocalDataSource.getAllNoteList(object : PetTestDataSource.LoadNoteListCallback {
            override fun onNoteListLoaded(noteList: List<NoteInfo>) {
                callback.onNoteListLoaded(noteList)
            }
        })
    }

    override fun getNoteListByDate(date: String): Observable<List<NoteInfo>> {
        return petTestLocalDataSource.getNoteListByDate(date)
    }

    override fun saveNote(noteInfo: NoteInfo) {
        noteInfo.updateTime = System.currentTimeMillis()
        petTestLocalDataSource.saveNote(noteInfo)
    }

    override fun updateNote(noteInfo: NoteInfo) {
        noteInfo.updateTime = System.currentTimeMillis()
        petTestLocalDataSource.updateNote(noteInfo)
    }

    override fun deleteNote(vararg noteInfo: NoteInfo) {
        return petTestLocalDataSource.deleteNote(*noteInfo)
    }

    override fun getAllReminderList(callback: PetTestDataSource.LoadReminderListCallback) {
        petTestLocalDataSource.getAllReminderList(object : PetTestDataSource.LoadReminderListCallback{
            override fun onReminderListLoaded(reminderList: List<ReminderInfo>) {
                callback.onReminderListLoaded(reminderList)
            }
        })
    }

    override fun getReminderListByTime(time: String): Observable<List<ReminderInfo>> {
        return petTestLocalDataSource.getReminderListByTime(time)
    }

    override fun saveReminder(reminderInfo: ReminderInfo) {
        reminderInfo.updateTime = System.currentTimeMillis()
        petTestLocalDataSource.saveReminder(reminderInfo)
    }

    override fun updateReminder(reminderInfo: ReminderInfo) {
        reminderInfo.updateTime = System.currentTimeMillis()
        petTestLocalDataSource.updateReminder(reminderInfo)
    }

    override fun deleteReminder(vararg reminderInfo: ReminderInfo) {
        return petTestLocalDataSource.deleteReminder(*reminderInfo)
    }

}