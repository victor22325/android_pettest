package com.broadmasterbiotech.library.database.note

import androidx.room.*
import com.broadmasterbiotech.library.database.note.NoteInfo.CREATOR.TABLE_NAME

@Dao
interface NoteInfoDAO {

    @Query("SELECT * FROM $TABLE_NAME ORDER BY updateTime DESC")
    fun allNoteInfo(): List<NoteInfo>

    @Query("SELECT * FROM $TABLE_NAME WHERE date = :date")
    fun getNoteListByDate(date: String): List<NoteInfo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllNote(vararg noteInfoList: NoteInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(noteInfo: NoteInfo)

    @Delete
    fun deleteNote(vararg noteInfo: NoteInfo)

    @Update
    fun updateNote(noteInfo: NoteInfo): Int
}