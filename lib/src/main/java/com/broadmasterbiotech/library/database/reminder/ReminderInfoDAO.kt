package com.broadmasterbiotech.library.database.reminder

import androidx.room.*
import com.broadmasterbiotech.library.database.reminder.ReminderInfo.CREATOR.TABLE_NAME

@Dao
interface ReminderInfoDAO {

    @Query("SELECT * FROM $TABLE_NAME ORDER BY updateTime DESC")
    fun getAllReminderInfoList(): List<ReminderInfo>

    @Query("SELECT * FROM $TABLE_NAME WHERE time = :time")
    fun getReminderInfoByTime(time: String): List<ReminderInfo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllReminderInfo(vararg reminderInfo: ReminderInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReminderInfo(reminderInfo: ReminderInfo)

    @Delete
    fun deleteReminderInfo(vararg reminderInfo: ReminderInfo)

    @Update
    fun updateReminderInfo(reminderInfo: ReminderInfo): Int
}