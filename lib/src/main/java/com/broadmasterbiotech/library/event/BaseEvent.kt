package com.broadmasterbiotech.library.event

import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.library.database.pet.PetInfo

sealed class BaseEvent {
    open class Success<T: Any>(var item: T? = null): BaseEvent()
    open class ConnectError(val code: Int = -401) : BaseEvent()
    open class ServerError(val code: Int = -501): BaseEvent()
    open class OtherError(var code: Int): BaseEvent()
}

sealed class VerifySNEvent: BaseEvent() {
    object Success: VerifySNEvent()
    class Fail(var status: Int? = Code.VERIFY_ERROR): VerifySNEvent()
    class SNisEmpty(var status: Int? = Code.VERIFY_SN_IS_EMPTY): VerifySNEvent()
    class SNisNull(var status: Int? = Code.VERIFY_SN_IS_NULL): VerifySNEvent()
}

sealed class CreateAccountEvent: BaseEvent() {
    object Success: CreateAccountEvent()
    class ConnectError(var status: Int? = Code.CREATE_ACCOUNT_CONNECT_ERROR): BaseEvent.ConnectError()
    class ServerError(var status: Int? = Code.CREATE_ACCOUNT_SERVER_ERROR, var message: String? = null): BaseEvent.ServerError()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class EmailFormatError(var status: Int? = Code.CREATE_ACCOUNT_EMAIL_FORMAT_ERROR): CreateAccountEvent()
    class PasswordFormatError(var status: Int? = Code.CREATE_ACCOUNT_PASSWORD_FORMAT_ERROR): CreateAccountEvent()
    class ConfirmFormatError(var status: Int? = Code.CREATE_ACCOUNT_CONFIRM_FORMAT_ERROR): CreateAccountEvent()
    class SendCertificationEmailFail(var status: Int? = Code.CREATE_ACCOUNT_SEND_CERTIFICATION_EMAIL_FAIL, var message: String? = null): CreateAccountEvent()
}

sealed class LoginEvent: BaseEvent() {
    class Success(var status: Int, var uuid: String? = null): BaseEvent.Success<String>()
    class ConnectError(var status: Int? = Code.LOGIN_CONNECT_ERROR): BaseEvent.ConnectError()
    class ServerError(var status: Int? = Code.LOGIN_SERVER_ERROR, var message: String? = null): BaseEvent.ServerError()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class EmailFormatError(var status: Int? = Code.LOGIN_EMAIL_FORMAT_ERROR): LoginEvent()
    class PasswordFormatError(var status: Int? = Code.LOGIN_PASSWORD_FORMAT_ERROR): LoginEvent()
    class FireBaseAuthSignInError(var status: Int? = Code.LOGIN_FIRE_BASE_AUTH_ERROR, var message: String? = null): LoginEvent()
    class EmailNotCertified(var status: Int? = Code.LOGIN_EMAIL_NOT_CERTIFIED, var message: String? = null): LoginEvent()
    class SendCertificationEmailFail(var status: Int? = Code.LOGIN_SEND_CERTIFICATION_EMAIL_FAIL, var message: String? = null): LoginEvent()
}

sealed class CreatePetEvent: BaseEvent() {
    class Success(var status: Int, var uri: String? = null, var petInfoList: ArrayList<PetInfo>? = null): CreatePetEvent()
    class ConnectError(var status: Int? = Code.PROFILE_CONNECT_ERROR): BaseEvent.ConnectError()
    class ServerError(var status: Int? = Code.PROFILE_SERVER_ERROR, var message: String? = null): BaseEvent.ServerError()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class PetNameFormatError(var status: Int? = Code.CREATE_PET_NAME_FORMAT_ERROR): CreatePetEvent()
    class PetSpeciesFormatError(var status: Int? = Code.CREATE_PET_SPECIES_FORMAT_ERROR): CreatePetEvent()
    class PetGlucoseUpperLimitFormatError(var status: Int? = Code.CREATE_PET_GLUCOSE_UPPER_FORMAT_ERROR): CreatePetEvent()
    class PetGlucoseLowerLimitFormatError(var status: Int? = Code.CREATE_PET_GLUCOSE_LOWER_FORMAT_ERROR): CreatePetEvent()
    class PetUpperMoreThanLowerError(var status: Int? = Code.CREATE_PET_UPPER_MORE_THAN_LOWER_ERROR): CreatePetEvent()
    class PetBirthdayAtFutureError(var status: Int? = Code.CREATE_PET_BIRTHDAY_AT_FUTURE_ERROR): CreatePetEvent()
    class PetAlreadyExist(var status: Int? = Code.CREATE_PET_ALREADY_EXIST): CreatePetEvent()
    class ServerHaveNoData(var status: Int? = Code.CREATE_PET_SERVER_HAVE_NO_DATA): CreatePetEvent()
    class UpdateAvatarPhotoFail(var status: Int? = Code.CREATE_PET_AVATAR_PHOTO_UPLOAD_FAIL, var message: String? = null): CreatePetEvent()
}

sealed class ProfileEvent: BaseEvent() {
    class Success(var status: Int, var uri: String? = null): ProfileEvent()
    class ConnectError(var status: Int? = Code.PROFILE_CONNECT_ERROR): BaseEvent.ConnectError()
    class ServerError(var status: Int? = Code.PROFILE_SERVER_ERROR, var message: String? = null): BaseEvent.ServerError()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class NewPasswordFormatError(var status: Int? = Code.PROFILE_NEW_PASSWORD_FORMAT_ERROR): ProfileEvent()
    class ConfirmFormatError(var status: Int? = Code.PROFILE_CONFIRM_FORMAT_ERROR): ProfileEvent()
    class ReadAccountDataFail(var status: Int? = Code.PROFILE_READ_ACCOUNT_DATA_FAIL, var message: String? = null): ProfileEvent()
    class ReAuthFail(var status: Int? = Code.PROFILE_RE_AUTH_FAIL, var message: String? = null): ProfileEvent()
    class UpdatePasswordFail(var status: Int? = Code.PROFILE_UPDATE_PASSWORD_FAIL, var message: String? = null): ProfileEvent()
    class UploadAvatarPhotoFail(var status: Int? = Code.PROFILE_AVATAR_PHOTO_UPLOAD_FAIL, var message: String? = null): ProfileEvent()
}

sealed class PickPetEvent: BaseEvent() {
    class PickPetSuccess(var status: Int? = Code.SELECT_PET_PICK_REQUEST_SUCCESS, var petInfoList: ArrayList<PetInfo>? = null): PickPetEvent()
    class PickPetServerError(var status: Int? = Code.SELECT_PET_PICK_SERVER_ERROR, var message: String? = null): PickPetEvent()
}

sealed class DeletePetEvent: BaseEvent() {
    class Success(var status: Int? = Code.SELECT_PET_DELETE_REQUEST_SUCCESS, var petInfoList: ArrayList<PetInfo>? = null, var removePetInfo: PetInfo? = null): DeletePetEvent()
    class CurrentPetError(var status: Int? = Code.SELECT_PET_DELETE_CURRENT_PET_ERROR): DeletePetEvent()
    class ServerError(var status: Int? = Code.SELECT_PET_PICK_SERVER_ERROR, var message: String? = null): DeletePetEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class IsExistError(var status: Int? = Code.SELECT_PET_DELETE_NOT_EXIST, var message: String? = null): DeletePetEvent()
    class PetInfoError(var status: Int? = Code.SELECT_PET_DELETE_PET_INFO_ERROR, var message: String? = null): DeletePetEvent()
}

sealed class EditPetEvent: BaseEvent() {
    class Success(var status: Int, var uri: String? = null, var petInfo: PetInfo? = null): EditPetEvent()
    class ConnectError(var status: Int? = Code.EDIT_PET_CONNECT_ERROR): BaseEvent.ConnectError()
    class ServerError(var status: Int? = Code.EDIT_PET_SERVER_ERROR, var message: String? = null): BaseEvent.ServerError()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class PetNameFormatError(var status: Int? = Code.EDIT_PET_NAME_FORMAT_ERROR): EditPetEvent()
    class PetSpeciesFormatError(var status: Int? = Code.EDIT_PET_SPECIES_FORMAT_ERROR): EditPetEvent()
    class PetGlucoseUpperLimitFormatError(var status: Int? = Code.EDIT_PET_GLUCOSE_UPPER_FORMAT_ERROR): EditPetEvent()
    class PetGlucoseLowerLimitFormatError(var status: Int? = Code.EDIT_PET_GLUCOSE_LOWER_FORMAT_ERROR): EditPetEvent()
    class PetUpperMoreThanLowerError(var status: Int? = Code.EDIT_PET_UPPER_MORE_THAN_LOWER_ERROR): EditPetEvent()
    class PetBirthdayAtFuture(var status: Int? = Code.EDIT_PET_BIRTHDAY_AT_FUTURE_ERROR): EditPetEvent()
    class UpdateAvatarPhotoFail(var status: Int? = Code.EDIT_PET_AVATAR_PHOTO_UPLOAD_FAIL, var message: String? = null): EditPetEvent()
}

sealed class GlucoseLogSyncEvent: BaseEvent() {
    class Success(var status: Int? = Code.GLUCOSE_LOG_SYNC_REQUEST_SUCCESS, var glucoseInfoList: ArrayList<GlucoseInfo>? = null): GlucoseLogSyncEvent()
    class ServerError(var status: Int? = Code.GLUCOSE_LOG_SYNC_SERVER_ERROR, var message: String? = null): GlucoseLogSyncEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
}

sealed class GlucoseLogAddEvent: BaseEvent() {
    class Success(var status: Int? = Code.GLUCOSE_LOG_ADD_REQUEST_SUCCESS, var glucoseInfoList: ArrayList<GlucoseInfo>): GlucoseLogAddEvent()
    class ConnectError(var status: Int? = Code.GLUCOSE_LOG_ADD_CONNECT_ERROR): GlucoseLogAddEvent()
    class ServerError(var status: Int? = Code.GLUCOSE_LOG_ADD_SERVER_ERROR, var message: String? = null): GlucoseLogAddEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class GlucoseValueFormatError(var status: Int? = Code.GLUCOSE_LOG_ADD_VALUE_FORMAT_ERROR): GlucoseLogAddEvent()
}

sealed class GlucoseLogDeleteEvent: BaseEvent() {
    class Success(var status: Int? = Code.GLUCOSE_LOG_DELETE_REQUEST_SUCCESS, var deletedGlucoseInfo: GlucoseInfo, var glucoseInfoList: ArrayList<GlucoseInfo>): GlucoseLogDeleteEvent()
    class ConnectError(var status: Int? = Code.GLUCOSE_LOG_DELETE_CONNECT_ERROR): GlucoseLogDeleteEvent()
    class ServerError(var status: Int? = Code.GLUCOSE_LOG_DELETE_SERVER_ERROR, var message: String? = null): GlucoseLogDeleteEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
}

sealed class GlucoseLogEditEvent: BaseEvent() {
    class Success(var status: Int? = Code.GLUCOSE_LOG_EDIT_REQUEST_SUCCESS, var editGlucoseInfo: GlucoseInfo? = null, var glucoseInfoList: ArrayList<GlucoseInfo>? = null): GlucoseLogEditEvent()
    class ConnectError(var status: Int? = Code.GLUCOSE_LOG_EDIT_CONNECT_ERROR): GlucoseLogEditEvent()
    class ServerError(var status: Int? = Code.GLUCOSE_LOG_EDIT_SERVER_ERROR, var message: String? = null): GlucoseLogEditEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class GlucoseValueFormatError(var status: Int? = Code.GLUCOSE_LOG_EDIT_VALUE_FORMAT_ERROR): GlucoseLogEditEvent()
}

sealed class NewsListSyncEvent: BaseEvent() {
    class Success(var status: Int? = Code.NEWS_LIST_SYNC_REQUEST_SUCCESS, var newsInfoList: ArrayList<NewsInfo>): NewsListSyncEvent()
    class ConnectError(var status: Int? = Code.NEWS_LIST_SYNC_CONNECT_ERROR): NewsListSyncEvent()
    class ServerError(var status: Int? = Code.NEWS_LIST_SYNC_SERVER_ERROR, var message: String? = null): NewsListSyncEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
}

sealed class CreateNewsEvent: BaseEvent() {
    class Success(var status: Int? = Code.CREATE_NEWS_REQUEST_SUCCESS, var uri: String? = null): CreateNewsEvent()
    class ConnectError(var status: Int? = Code.CREATE_NEWS_CONNECT_ERROR): CreateNewsEvent()
    class ServerError(var status: Int? = Code.CREATE_NEWS_SERVER_ERROR, var message: String? = null): CreateNewsEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
    class TitleFormatError(var status: Int? = Code.CREATE_NEWS_TITLE_FORMAT_ERROR): CreateNewsEvent()
    class DateFormatError(var status: Int? = Code.CREATE_NEWS_DATE_FORMAT_ERROR): CreateNewsEvent()
    class ImageHaveNoData(var status: Int? = Code.CREATE_NEWS_IMAGE_HAVE_NO_DATA): CreateNewsEvent()
    class ImageUploadFail(var status: Int? = Code.CREATE_NEWS_IMAGE_UPLOAD_FAIL, var message: String? = null): CreateNewsEvent()
    class ServerSaveFail(var status: Int? = Code.CREATE_NEWS_SERVER_SAVE_FAIL, var message: String? = null): CreateNewsEvent()
}

sealed class IntroductionSyncEvent: BaseEvent() {
    class Success(var status: Int? = Code.INTRODUCTION_LIST_SYNC_REQUEST_SUCCESS, var introductionList: ArrayList<NewsInfo>): IntroductionSyncEvent()
    class ConnectError(var status: Int? = Code.INTRODUCTION_LIST_SYNC_CONNECT_ERROR): IntroductionSyncEvent()
    class ServerError(var status: Int? = Code.INTRODUCTION_LIST_SYNC_SERVER_ERROR, var message: String? = null): IntroductionSyncEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
}

sealed class BlePairEvent: BaseEvent() {
    class PairSuccess(var status: Int? = Code.SETTING_BLE_REQUEST_SUCCESS): BlePairEvent()
    class VerifySuccess(var status: Int? = Code.SETTING_BLE_VERIFY_SN_SUCCESS): BlePairEvent()
    class SendSNError(var status: Int? = Code.SETTING_BLE_SEND_TOKEN_FAIL, var message: String? = null): BlePairEvent()
    class VerifySNError(var status: Int? = Code.SETTING_BLE_VERIFY_SN_FAIL): BlePairEvent()
    class ScanWifiError(var status: Int? = Code.SETTING_BLE_SCAN_WIFI_FAIL, var message: String? = null): BlePairEvent()
    class SendTokenError(var status: Int? = Code.SETTING_BLE_SEND_TOKEN_FAIL, var message: String? = null): BlePairEvent()
    class OtherError(var status: Int): BaseEvent.OtherError(status)
}

