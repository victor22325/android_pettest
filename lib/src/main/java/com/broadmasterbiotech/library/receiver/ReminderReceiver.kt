package com.broadmasterbiotech.library.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.broadmasterbiotech.library.R

class ReminderReceiver: BroadcastReceiver() {

    companion object {
        val TAG = ReminderReceiver::class.java.simpleName

        const val REMINDER_RECEIVED_ACTION = "ReminderReceivedAction"
        const val REMINDER_RECEIVED_TYPE_KEY = "ReminderReceivedTypeKey"
        const val REMINDER_RECEIVED_LABEL_KEY = "ReminderReceivedLabelKey"
        const val REMINDER_RECEIVED_NOTIFICATION_TYPE_KEY = "ReminderReceivedNotificationType"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if(intent?.action.equals(REMINDER_RECEIVED_ACTION)) {
            val type = intent?.getIntExtra(REMINDER_RECEIVED_TYPE_KEY, 0)
            val label = intent?.getStringExtra(REMINDER_RECEIVED_LABEL_KEY)
            val notificationType = intent?.getIntExtra(REMINDER_RECEIVED_NOTIFICATION_TYPE_KEY, 0)
            Log.d(TAG, "onReceive label:$label, notificationType:$notificationType")

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                TODO("VERSION.SDK_INT > O")
//            } else {
//                TODO("VERSION.SDK_INT < O")
//            }
        }
    }

    private fun getNotificationTitle(type: Int): String {
        return if (type == 0) {
            R.string.reminder_receiver_medicine_notification_title.toString()
        } else {
            R.string.reminder_receiver_veterinarian_notification_title.toString()
        }
    }
}