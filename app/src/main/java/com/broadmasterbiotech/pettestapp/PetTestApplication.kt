package com.broadmasterbiotech.pettestapp

import android.app.Application
import android.util.Log

class PetTestApplication : Application() {

    companion object {

        private val TAG = PetTestApplication::class.java.simpleName
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")
    }
}