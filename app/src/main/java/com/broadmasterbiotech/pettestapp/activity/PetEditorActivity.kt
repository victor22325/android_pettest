package com.broadmasterbiotech.pettestapp.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.EditPetEvent
import com.broadmasterbiotech.library.viewmodel.PetEditorViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.adapter.SpinnerDialogAdapter
import com.broadmasterbiotech.pettestapp.databinding.ActivityPetEditorBinding
import com.broadmasterbiotech.pettestapp.dialog.SelectPetDialog
import com.broadmasterbiotech.pettestapp.dialog.SpinnerDialog
import com.broadmasterbiotech.pettestapp.enums.pet.DiabetesType
import com.broadmasterbiotech.pettestapp.enums.pet.Gender
import com.broadmasterbiotech.pettestapp.enums.pet.Species
import com.broadmasterbiotech.pettestapp.enums.pet.WeightUnit
import com.broadmasterbiotech.pettestapp.fragment.RecordFragment
import com.bumptech.glide.Glide
import com.google.firebase.storage.StorageMetadata
import com.jeremyliao.liveeventbus.LiveEventBus
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import kotlinx.android.synthetic.main.progressbar_dialog.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity
import java.io.ByteArrayOutputStream
import java.util.*

class PetEditorActivity: SwipeBackActivity() {

    companion object {

        val TAG = PetEditorActivity::class.java.simpleName

        const val IMAGE_REQUEST_CODE: Int = 1
        const val RESIZE_REQUEST_CODE: Int = 2
    }

    private lateinit var petEditorBinding: ActivityPetEditorBinding
    private lateinit var petEditorViewModel: PetEditorViewModel
    private lateinit var speciesDialog: SpinnerDialog
    private lateinit var genderDialog: SpinnerDialog
    private val calendar: Calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val petInfo = intent.getParcelableExtra<PetInfo>(SelectPetDialog.INTENT_EXTRA_SELECT_PET_DIALOG_TO_EDIT_PET_KEY)
        petEditorViewModel = ViewModelProvider(this@PetEditorActivity).get(PetEditorViewModel::class.java)
        petEditorBinding = DataBindingUtil.setContentView(this, R.layout.activity_pet_editor)
        petEditorBinding.lifecycleOwner = this
        petEditorBinding.activity = this
        petEditorBinding.viewModel = petEditorViewModel
        petEditorBinding.editPetProgressDialog.progressDialog_content_text.text = resources.getString(R.string.edit_pet_progressbar_saving_text)
        petEditorBinding.petEditorTopBarLayout.pettest_advocateLogo_back_layout.setOnClickListener {
            finish()
        }
        petEditorViewModel.setPetInfo(petInfo!!)
        petEditorViewModel.petAvatarPhotoURI.observe(this@PetEditorActivity, androidx.lifecycle.Observer { uri->
            if(uri.isNotEmpty()) {
                Glide.with(this@PetEditorActivity).load(uri).into(petEditorBinding.petEditorAvatarImage)
            } else {
                Glide.with(this@PetEditorActivity).load(ContextCompat.getDrawable(this, R.drawable.user)).into(petEditorBinding.petEditorAvatarImage)
            }
        })
        setDiabetesTypesCheckBoxListener()
        petEditorViewModel.editPetLiveEvent.observe(this@PetEditorActivity, androidx.lifecycle.Observer { event -> event?.let { baseEventHandler(it) } })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode != Activity.RESULT_OK) {
            return
        } else {
            when (requestCode) {
                CreatePetActivity.IMAGE_REQUEST_CODE -> {
                    if(data!=null) {
                        resizeImage(data.data!!)
                    }
                }
                CreatePetActivity.RESIZE_REQUEST_CODE -> {
                    if (data!= null) {
                        Glide.with(this@PetEditorActivity).load(data.data).into(petEditorBinding.petEditorAvatarImage)
                        val uri = data.data!!
                        val contentResolver = this.contentResolver
                        val bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(uri))
                        val byteArrayOutputStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
                        val imageData = byteArrayOutputStream.toByteArray()
                        val storageMetadata = StorageMetadata.Builder()
                            .setCustomMetadata("accountInfo", queryFileName(uri))
                            .setContentType("image/jpg")
                            .build()
                        petEditorViewModel.uploadFileName = queryFileName(uri)
                        petEditorViewModel.uploadImageData = imageData
                        petEditorViewModel.uploadMetadata = storageMetadata
                    }
                }
            }
        }
    }

    private fun queryFileName(uri: Uri): String {
        var name = ""
        this.contentResolver.query(uri, null, null, null, null)?.use { cursor->
            if(cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
            }
        }
        return name
    }

    private fun resizeImage(uri: Uri) {
        Log.d(TAG, "resizeImage path:${uri.path}")
        try {
            val intent = Intent("com.android.camera.action.CROP")
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            intent.setDataAndType(uri, "image/*")
            intent.putExtra("crop", "true")
            intent.putExtra("aspectX", 1)
            intent.putExtra("aspectY", 1)
            intent.putExtra("outputX", 500)
            intent.putExtra("outputY", 500)
            intent.putExtra("scale", true)
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG)
            intent.putExtra("noFaceDetection", true)
            intent.putExtra("return-data", false)
            startActivityForResult(intent, RESIZE_REQUEST_CODE)
        } catch (e: Exception) {
            Log.d(TAG, "resizeImage exception:${e.message}")
        }
    }

    fun pickerAvatar() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        val destIntent = Intent.createChooser(intent, null)
        startActivityForResult(destIntent, IMAGE_REQUEST_CODE)
    }

    fun speciesOnClick() {
        val speciesList = arrayListOf(Species.Canine.name, Species.Feline.name)
        val speciesListener = object: SpinnerDialogAdapter.SpinnerDialogRecyclerViewOptionListener {
            override fun onClicked(option: String) {
                speciesDialog.dismiss()
                when (option) {
                    Species.Canine.name -> {
                        petEditorBinding.petEditorSpeciesText.text = Species.Canine.name
                        petEditorViewModel.petSpecies.value = Species.Canine.num
                    }
                    Species.Feline.name -> {
                        petEditorBinding.petEditorSpeciesText.text = Species.Feline.name
                        petEditorViewModel.petSpecies.value = Species.Feline.num
                    }
                    else -> {
                        petEditorBinding.petEditorSpeciesText.text = Species.Canine.name
                        petEditorViewModel.petSpecies.value = Species.Canine.num
                    }
                }
            }
        }
        speciesDialog = SpinnerDialog(activity = this, title = resources.getString(R.string.create_pet_spinner_dialog_select_species), list = speciesList, listener = speciesListener, lifecycleOwner = this@PetEditorActivity)
        speciesDialog.show()
    }

    fun petWeightLBOnClick() {
        petEditorBinding.petEditorWeightUnitLb.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.lb_selected))
        petEditorBinding.petEditorWeightUnitKg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.kilogram_unselected))
        petEditorViewModel.petWeightUnit.value = WeightUnit.Libra.num
    }

    fun petWeightKGOnClick() {
        petEditorBinding.petEditorWeightUnitLb.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.lb_unselected))
        petEditorBinding.petEditorWeightUnitKg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.kilogram_selected))
        petEditorViewModel.petWeightUnit.value = WeightUnit.Kilogram.num
    }

    private fun setDiabetesTypesCheckBoxListener() {
        petEditorBinding.petEditorDiabetesTypeNaCheckbox.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                petEditorBinding.petEditorDiabetesTypeNaCheckbox.isChecked = true
                petEditorBinding.petEditorDiabetesType1Checkbox.isChecked = false
                petEditorBinding.petEditorDiabetesType2Checkbox.isChecked = false
                petEditorViewModel.petDiabetesType.value = DiabetesType.NA.num
                petEditorBinding.petEditorDiabetesYearLayout.visibility = View.GONE
            } else {
                petEditorViewModel.petDiabetesType.value = null
                petEditorBinding.petEditorDiabetesYearLayout.visibility = View.GONE
            }
        }
        petEditorBinding.petEditorDiabetesType1Checkbox.setOnCheckedChangeListener{ _, isChecked ->
            if(isChecked) {
                petEditorBinding.petEditorDiabetesTypeNaCheckbox.isChecked = false
                petEditorBinding.petEditorDiabetesType1Checkbox.isChecked = true
                petEditorBinding.petEditorDiabetesType2Checkbox.isChecked = false
                petEditorViewModel.petDiabetesType.value = DiabetesType.Type1.num
                petEditorBinding.petEditorDiabetesYearLayout.visibility = View.VISIBLE
            } else {
                petEditorViewModel.petDiabetesType.value = null
                petEditorBinding.petEditorDiabetesYearLayout.visibility = View.GONE
            }
        }
        petEditorBinding.petEditorDiabetesType2Checkbox.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                petEditorBinding.petEditorDiabetesTypeNaCheckbox.isChecked = false
                petEditorBinding.petEditorDiabetesType1Checkbox.isChecked = false
                petEditorBinding.petEditorDiabetesType2Checkbox.isChecked = true
                petEditorViewModel.petDiabetesType.value = DiabetesType.Type2.num
                petEditorBinding.petEditorDiabetesYearLayout.visibility = View.VISIBLE
            } else {
                petEditorViewModel.petDiabetesType.value = null
                petEditorBinding.petEditorDiabetesYearLayout.visibility = View.GONE
            }
        }
    }

    fun petGenderOnClick() {
        val genderList = arrayListOf(Gender.Male.name, Gender.Female.name)
        val genderListener = object: SpinnerDialogAdapter.SpinnerDialogRecyclerViewOptionListener {
            override fun onClicked(option: String) {
                genderDialog.dismiss()
                when (option) {
                    Gender.Male.name -> {
                        petEditorBinding.petEditorGenderText.text =  Gender.Male.name
                        petEditorViewModel.petGender.value = Gender.Male.num
                    }
                    Gender.Female.name -> {
                        petEditorBinding.petEditorGenderText.text = Gender.Female.name
                        petEditorViewModel.petGender.value = Gender.Female.num
                    }
                    else -> {
                        petEditorBinding.petEditorGenderText.text =  Gender.Male.name
                        petEditorViewModel.petGender.value = Gender.Male.num
                    }
                }
            }
        }
        genderDialog = SpinnerDialog(activity = this, title = resources.getString(R.string.create_pet_spinner_dialog_select_gender), list = genderList, listener = genderListener, lifecycleOwner = this@PetEditorActivity)
        genderDialog.show()
    }

    fun petBirthDateOnClick() {
        val datePickerDialog = DatePickerDialog(this, R.style.CustomDatePickerDialog, DatePickerDialog.OnDateSetListener {
                _, year, month, dayOfMonth ->
            petEditorBinding.petEditorBirthText.text = setDateFormat(year, month, dayOfMonth)
            petEditorViewModel.petBirthday.value = setDateFormat(year, month, dayOfMonth)
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE))
        datePickerDialog.show()
    }

    private fun setDateFormat(year: Int, monthOfYear: Int, dayOfMonth: Int): String {
        return (year.toString() + "/"
                + (monthOfYear + 1).toString() + "/"
                + dayOfMonth.toString())
    }

    private fun baseEventHandler(event: BaseEvent) {
        petEditorViewModel.isProcessing.value = false
        when (event) {
            is EditPetEvent.Success -> {
                when (event.status) {
                    Code.EDIT_PET_REQUEST_SUCCESS -> {
                        Log.d(TAG, "finish petInfo:${event.petInfo}")
                        LiveEventBus.get(RecordFragment.LIVE_EVENT_BUS_PET_INFO_UPDATE_STICKY_KEY).post(event.petInfo)
                        this.finish()
                    }
                    Code.EDIT_PET_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS -> {
                        petEditorViewModel.checkPetInfo()
                        Log.d(TAG, "uri:${event.uri}")
                    }
                }
            }
            is EditPetEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_connect_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is EditPetEvent.PetNameFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_name_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is EditPetEvent.PetSpeciesFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_species_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is EditPetEvent.PetGlucoseUpperLimitFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_glucose_upper_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is EditPetEvent.PetGlucoseLowerLimitFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_glucose_lower_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is EditPetEvent.PetUpperMoreThanLowerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_upper_more_than_lower_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is EditPetEvent.PetBirthdayAtFuture -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_birthday_at_future_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is EditPetEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_server_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
            is EditPetEvent.UpdateAvatarPhotoFail -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_upload_avatar_photo_fail, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
                petEditorViewModel.checkPetInfo()
            }
            is EditPetEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_PET_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_pet_dialog_other_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
        }
    }
}