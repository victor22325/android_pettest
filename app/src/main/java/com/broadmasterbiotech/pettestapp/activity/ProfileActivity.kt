package com.broadmasterbiotech.pettestapp.activity

import android.app.Activity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.ProfileEvent
import com.broadmasterbiotech.library.viewmodel.ProfileViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.ActivityProfileBinding
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import kotlinx.android.synthetic.main.progressbar_dialog.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.google.firebase.storage.StorageMetadata
import java.io.ByteArrayOutputStream
import android.provider.OpenableColumns
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.bumptech.glide.Glide
import com.jeremyliao.liveeventbus.LiveEventBus


class ProfileActivity: SwipeBackActivity() {

    companion object {

        private val TAG = ProfileActivity::class.java.simpleName

        const val IMAGE_REQUEST_CODE: Int = 1

        const val RESIZE_REQUEST_CODE: Int = 2
    }

    private lateinit var profileBinding: ActivityProfileBinding

    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        profileBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        profileBinding.lifecycleOwner = this@ProfileActivity
        profileViewModel = ViewModelProvider(this@ProfileActivity).get(ProfileViewModel::class.java)
        profileBinding.viewModel = profileViewModel
        profileBinding.activity = this
        profileBinding.profileAdvocateLogoLayout.pettest_advocateLogo_back_layout.setOnClickListener {
            finish()
        }
        profileBinding.loginProgressDialog.progressDialog_content_text.text = resources.getString(R.string.progressbar_content_loading_text)
        profileViewModel.profileLiveEvent.observe(this@ProfileActivity, Observer { event -> event?.let { baseEventHandler(it) } })
        profileViewModel.uploadLiveEvent.observe( this@ProfileActivity, Observer { event -> event?.let { uploadEventHandler(it) } })
        profileViewModel.getAccountInfo()
        profileViewModel.userPhotoLiveEvent.observe(this@ProfileActivity, Observer { event -> event?.let {
            profileBinding.profileAvatarImage.setImageDrawable(it)
        } })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.d(TAG, "onBackPressed")
    }

    fun saveOnClick() {
        profileBinding.loginProgressDialog.progressDialog_content_text.text = resources.getString(R.string.progressbar_content_saving_text)
        profileViewModel.saveOnClick()
    }

    fun pickerAvatar() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        val destIntent = Intent.createChooser(intent, null)
        startActivityForResult(destIntent, IMAGE_REQUEST_CODE)
    }

    private fun baseEventHandler(event: BaseEvent) {
        profileViewModel.isProcessing.value = false
        when (event) {
            is ProfileEvent.Success -> {
                when(event.status) {
                    Code.PROFILE_REQUEST_SUCCESS -> {
                        finish()
                    }
                    Code.PROFILE_REQUEST_GET_ACCOUNT_INFO_SUCCESS -> {
                        Log.d(TAG, "Get Account Info is Successful")
                    }
                    Code.PROFILE_REQUEST_UPDATE_PASSWORD_SUCCESS -> {
                        MyCustomDialog(this)
                            .setTitle(getString(R.string.profile_dialog_update_password_success_title))
                            .setTitleStyleBold()
                            .setMessage(getString(R.string.profile_dialog_update_password_success_content))
                            .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                            .builder()
                            .show()
                    }
                }
            }
            is ProfileEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.PROFILE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.profile_dialog_connect_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is ProfileEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.PROFILE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.profile_dialog_server_error, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_logout)) {profileViewModel.logout()}
                    .builder()
                    .show()
            }
            is ProfileEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.PROFILE_ACCOUNT_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.profile_dialog_other_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_logout)) {
                        profileViewModel.logout()
                        finish()
                    }
                    .builder()
                    .show()
            }
            is ProfileEvent.NewPasswordFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.PROFILE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.profile_dialog_new_password_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is ProfileEvent.ConfirmFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.PROFILE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.profile_dialog_confirm_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is ProfileEvent.ReadAccountDataFail -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.PROFILE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.profile_dialog_read_account_data_fail))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
            is ProfileEvent.ReAuthFail -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.PROFILE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.profile_dialog_change_password_re_auth_fail, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
            is ProfileEvent.UpdatePasswordFail -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.PROFILE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.profile_dialog_update_change_password_fail, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
        }
    }

    private fun uploadEventHandler(event: BaseEvent) {
        when (event) {
            is ProfileEvent.Success -> {
                when (event.status) {
                    Code.PROFILE_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS -> {
                        LiveEventBus.get(PetTestMainActivity.LIVE_EVENT_BUS_NAVIGATION_AVATAR_PHOTO_UPDATE_STICK_KEY).post(event.uri)
                        Glide.with(this@ProfileActivity).load(event.uri).into(profileBinding.profileAvatarImage)
                        profileViewModel.checkAccountInfo()
                    }
                }
            }
            is ProfileEvent.UploadAvatarPhotoFail -> {
                Glide.with(this@ProfileActivity).load(ContextCompat.getDrawable(this@ProfileActivity, R.drawable.user)).into(profileBinding.profileAvatarImage)
                profileViewModel.checkAccountInfo()
                Toast.makeText(this, getString(R.string.profile_dialog_upload_avatar_photo_fail, event.message), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode != Activity.RESULT_OK) {
            return
        } else {
            when (requestCode) {
                IMAGE_REQUEST_CODE -> {
                    if(data!=null) {
                        resizeImage(data.data!!)
                    }
                }
                RESIZE_REQUEST_CODE -> {
                    if (data!= null) {
                        Glide.with(this@ProfileActivity).load(data.data).into(profileBinding.profileAvatarImage)
                        val uri = data.data!!
                        val contentResolver = this.contentResolver
                        val bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(uri))
                        val byteArrayOutputStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
                        val imageData = byteArrayOutputStream.toByteArray()
                        val storageMetadata = StorageMetadata.Builder()
                            .setCustomMetadata("accountInfo", queryFileName(uri))
                            .setContentType("image/jpg")
                            .build()
                        profileViewModel.uploadFileName = queryFileName(uri)
                        profileViewModel.uploadImageData = imageData
                        profileViewModel.uploadMetadata = storageMetadata
                    }
                }
            }
        }
    }

    private fun queryFileName(uri: Uri): String {
        var name = ""
        this.contentResolver.query(uri, null, null, null, null)?.use { cursor->
            if(cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
            }
        }
        return name
    }

    private fun resizeImage(uri: Uri) {
        Log.d(TAG, "resizeImage path:${uri.path}")
        try {
            val intent = Intent("com.android.camera.action.CROP")
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            intent.setDataAndType(uri, "image/*")
            intent.putExtra("crop", "true")
            intent.putExtra("aspectX", 1)
            intent.putExtra("aspectY", 1)
            intent.putExtra("outputX", 500)
            intent.putExtra("outputY", 500)
            intent.putExtra("scale", true)
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG)
            intent.putExtra("noFaceDetection", true)
            intent.putExtra("return-data", false)
            startActivityForResult(intent, RESIZE_REQUEST_CODE)
        } catch (e: Exception) {
            Log.d(TAG, "resizeImage exception:${e.message}")
        }
    }
}