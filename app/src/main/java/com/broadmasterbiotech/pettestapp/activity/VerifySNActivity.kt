package com.broadmasterbiotech.pettestapp.activity


import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.VerifySNEvent
import com.broadmasterbiotech.library.viewmodel.VerifySNViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.databinding.ActivityVerifyBinding
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import kotlinx.android.synthetic.main.progressbar_dialog.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity

class VerifySNActivity: SwipeBackActivity() {

    companion object {

        private val TAG = VerifySNActivity::class.java.simpleName
    }

    private lateinit var verifyBinding: ActivityVerifyBinding

    private lateinit var verifySNViewModel: VerifySNViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verifyBinding = DataBindingUtil.setContentView(this, R.layout.activity_verify)
        verifyBinding.lifecycleOwner = this@VerifySNActivity
        verifySNViewModel = ViewModelProvider(this@VerifySNActivity).get(VerifySNViewModel::class.java)
        verifyBinding.viewModel = verifySNViewModel
        verifySNViewModel.getSN()
        verifySNViewModel.verifySNLiveEvent.observe(this@VerifySNActivity, Observer{ isSuccessful ->
            isSuccessful?.let { this.verifyEventHandler(it) }
        })
        verifyBinding.loginProgressDialog.progressDialog_content_text.text = resources.getString(R.string.progressbar_content_verifying_text)
        verifyBinding.verifySNAdvocateLogoLayout.pettest_advocateLogo_back_layout.visibility = View.GONE
    }

    private fun verifyEventHandler(event: BaseEvent) {
        verifySNViewModel.isProcessing.value = false
        when (event) {
            is VerifySNEvent.Success -> {
                val intent = Intent(this@VerifySNActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
            is VerifySNEvent.Fail -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.VERIFY_SN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.verifySN_fail))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is VerifySNEvent.SNisEmpty -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.VERIFY_SN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.verifySN_empty))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is VerifySNEvent.SNisNull -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.VERIFY_SN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.verifySN_null))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}