package com.broadmasterbiotech.pettestapp.activity

import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.database.reminder.ReminderInfo
import com.broadmasterbiotech.library.viewmodel.ReminderViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.databinding.ActivityEditReminderBinding
import com.broadmasterbiotech.pettestapp.dialog.SelectDateDialog
import com.broadmasterbiotech.pettestapp.dialog.SelectTimeDialog
import com.broadmasterbiotech.pettestapp.enums.reminder.NotificationType
import com.broadmasterbiotech.pettestapp.enums.reminder.RepeatType
import com.broadmasterbiotech.pettestapp.fragment.ReminderListFragment
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity
import java.text.SimpleDateFormat
import java.util.*

class EditReminderActivity: SwipeBackActivity() {

    companion object {
        val TAG = EditReminderActivity::class.java.simpleName
    }

    private lateinit var activityEditReminderBinding: ActivityEditReminderBinding
    private lateinit var reminderViewModel: ReminderViewModel
    private lateinit var reminderInfo: ReminderInfo
    private val calendar: Calendar = Calendar.getInstance()
    private lateinit var timePicker: TimePicker
    private lateinit var datePicker: DatePicker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intentExtra = intent.getParcelableExtra<ReminderInfo>(ReminderListFragment.INTENT_EXTRA_EDIT_REMINDER_INFO_KEY)
        if(intentExtra == null) {
            MyCustomDialog(this)
                .setCancelable(true)
                .setTitle(R.string.edit_reminder_intent_extra_error_title)
                .setTitleStyleBold()
                .setMessage(R.string.edit_reminder_intent_extra_error_message)
                .setPositiveButton(getString(R.string.my_custom_dialog_ok_text)) {
                    finish()
                }
                .builder()
                .show()
        } else {
            reminderInfo = intentExtra
        }
        activityEditReminderBinding =  DataBindingUtil.setContentView(this, R.layout.activity_edit_reminder)
        reminderViewModel = ViewModelProvider(this@EditReminderActivity).get(ReminderViewModel::class.java)
        activityEditReminderBinding.activity = this
        activityEditReminderBinding.lifecycleOwner = this
        activityEditReminderBinding.flag = reminderInfo.type
        activityEditReminderBinding.viewModel = reminderViewModel
        activityEditReminderBinding.editReminderTopBarLayout.pettest_advocateLogo_back_layout.setOnClickListener { finish() }
        activityEditReminderBinding.editReminderTopBarLayout.pettest_advocateLogo_ok_layout.visibility = View.VISIBLE
        activityEditReminderBinding.editReminderTopBarLayout.pettest_advocateLogo_ok_layout.setOnClickListener {
            reminderViewModel.updateReminderOnClick(reminderInfo)
            finish()
        }
        if(reminderInfo.date!!.isNotEmpty()) {
            val dateSplit = reminderInfo.date?.split(getString(R.string.simple_date_format_date_split))
            calendar.set(Calendar.YEAR, dateSplit!![0].toInt())
            calendar.set(Calendar.MONTH, dateSplit[1].toInt())
            calendar.set(Calendar.DAY_OF_MONTH, dateSplit[2].toInt())
        }
        val timeSplit = reminderInfo.time.split(getString(R.string.simple_date_format_time_split))
        calendar.set(Calendar.HOUR_OF_DAY, timeSplit[0].toInt())
        calendar.set(Calendar.MINUTE, timeSplit[1].toInt())
        timePicker = TimePicker(this)
        datePicker = DatePicker(this)
        timePicker.hour = calendar.get(Calendar.HOUR_OF_DAY)
        timePicker.minute = calendar.get(Calendar.MINUTE)
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        when (reminderInfo.type) {
            ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG -> {
                activityEditReminderBinding.editReminderTitleIcon.setImageResource(R.drawable.medicine_reminder)
                activityEditReminderBinding.editReminderTimeText.text = getTimeString(timePicker)
                activityEditReminderBinding.editReminderNotificationTypeText.text = getNotificationTypeString(reminderInfo.notificationType)
                activityEditReminderBinding.editReminderRepeatTypeText.text = getRepeatTypeString(reminderInfo.repeat!!)
                reminderViewModel.initialInsulin(getTimeString(timePicker), RepeatType.Never.num, NotificationType.None.num, reminderInfo.label)
            }
            ReminderListFragment.CREATE_VETERINARIAN_REMINDER_FLAG -> {
                activityEditReminderBinding.editReminderTitleIcon.setImageResource(R.drawable.clinic_reminder_title)
                activityEditReminderBinding.editReminderDateText.text = getDateString(datePicker)
                activityEditReminderBinding.editReminderTimeText.text = getTimeString(timePicker)
                activityEditReminderBinding.editReminderNotificationTypeText.text = getNotificationTypeString(reminderInfo.notificationType)
                reminderViewModel.initialVeterinarian(getDateString(datePicker), getTimeString(timePicker), NotificationType.None.num, reminderInfo.label)
            }
        }
    }

    private val selectDateListener = object : SelectDateDialog.Listener {
        override fun datePickerOnClick(date: String) {
            when(reminderInfo.type) {
                ReminderListFragment.CREATE_VETERINARIAN_REMINDER_FLAG -> reminderViewModel.reminderDateSingleLiveEvent.value = date
            }
            activityEditReminderBinding.editReminderDateText.text = date
            val dateSplit = date.split(resources.getString(R.string.simple_date_format_date_split))
            datePicker.updateDate(dateSplit[0].toInt(), dateSplit[1].toInt()-1, dateSplit[2].toInt())
        }
    }

    fun dateOptionOnClick() {
        val selectDateDialog = SelectDateDialog(this, datePicker, selectDateListener, this)
        selectDateDialog.show()
    }

    private val selectTimeListener = object : SelectTimeDialog.Listener {
        override fun timePickerOnClick(time: String) {
            when(reminderInfo.type) {
                ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG -> reminderViewModel.reminderTimeSingleLiveEvent.value = time
                ReminderListFragment.CREATE_VETERINARIAN_REMINDER_FLAG -> reminderViewModel.reminderTimeSingleLiveEvent.value = time
            }
            activityEditReminderBinding.editReminderTimeText.text = time
            val timeSplit = time.split(resources.getString(R.string.simple_date_format_time_split))
            timePicker.hour = timeSplit[0].toInt()
            timePicker.minute = timeSplit[1].toInt()
        }
    }

    fun timeOptionOnClick() {
        val selectTimeDialog = SelectTimeDialog(this, timePicker, selectTimeListener, this)
        selectTimeDialog.show()
    }

    fun repeatOptionOnClick() {
        val repeatList = ArrayList<String>()
        repeatList.add(RepeatType.Never.name)
        repeatList.add(RepeatType.Everyday.name)
        MyCustomDialog(this, resources.getString(R.string.my_custom_dialog_bottom_text))
            .setBottomTitle(resources.getString(R.string.create_reminder_repeat_type_title_text))
            .setBottomTitleColor(R.color.colorBlack)
            .setListView(repeatList) { _, _, position, _ ->
                when(position) {
                    RepeatType.Never.num -> {
                        activityEditReminderBinding.editReminderRepeatTypeText.text = RepeatType.Never.name
                        reminderViewModel.reminderRepeatTypeSingleLiveEvent.value = RepeatType.Never.num
                    }
                    RepeatType.Everyday.num -> {
                        activityEditReminderBinding.editReminderRepeatTypeText.text = RepeatType.Everyday.name
                        reminderViewModel.reminderRepeatTypeSingleLiveEvent.value = RepeatType.Everyday.num
                    }
                }
            }
            .setBottomNegativeButton(resources.getString(R.string.my_custom_dialog_cancel_text)) { }
            .setBottomNegativeButtonColor(R.color.myCustomDialogColorBlue)
            .builder()
            .show()
    }

    fun typeOptionOnClick() {
        val notificationTypeList = ArrayList<String>()
        notificationTypeList.add(NotificationType.None.name)
        notificationTypeList.add(NotificationType.Notification.name)
        notificationTypeList.add(NotificationType.NotificationSound.name)
        MyCustomDialog(this, resources.getString(R.string.my_custom_dialog_bottom_text))
            .setBottomTitle(resources.getString(R.string.create_reminder_notification_type_title_text))
            .setBottomTitleColor(R.color.colorBlack)
            .setListView(notificationTypeList) { _, _, position, _ ->
                when(position) {
                    NotificationType.None.num -> {
                        activityEditReminderBinding.editReminderNotificationTypeText.text = NotificationType.None.name
                        if(reminderInfo.type == ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG) {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.None.num
                        } else {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.None.num
                        }
                    }
                    NotificationType.Notification.num -> {
                        activityEditReminderBinding.editReminderNotificationTypeText.text = NotificationType.Notification.name
                        if(reminderInfo.type == ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG) {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.Notification.num
                        } else {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.Notification.num
                        }
                    }
                    NotificationType.NotificationSound.num -> {
                        activityEditReminderBinding.editReminderNotificationTypeText.text = NotificationType.NotificationSound.name
                        if(reminderInfo.type == ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG) {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.NotificationSound.num
                        } else {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.NotificationSound.num
                        }
                    }
                }
            }
            .setBottomNegativeButton(resources.getString(R.string.my_custom_dialog_cancel_text)) { }
            .setBottomNegativeButtonColor(R.color.myCustomDialogColorBlue)
            .builder()
            .show()
    }

    private fun getDateString(datePicker: DatePicker): String {
        val dateCalendar = Calendar.getInstance()
        dateCalendar.set(Calendar.YEAR, datePicker.year)
        dateCalendar.set(Calendar.MONTH, datePicker.month)
        dateCalendar.set(Calendar.DAY_OF_MONTH, datePicker.dayOfMonth)
        val dateSimpleDateFormat = SimpleDateFormat(resources.getString(R.string.simple_date_format_year_month_date), Locale.US)
        return dateSimpleDateFormat.format(dateCalendar.time)
    }

    private fun getTimeString(timePicker: TimePicker): String {
        val timeCalendar = Calendar.getInstance()
        timePicker.hour.let { timeCalendar.set(Calendar.HOUR_OF_DAY, it) }
        timePicker.minute.let { timeCalendar.set(Calendar.MINUTE, it) }
        val timeSimpleDateFormat = SimpleDateFormat(resources.getString(R.string.simple_date_format_hour_minute), Locale.US)
        return timeSimpleDateFormat.format(timeCalendar.time)
    }

    private fun getRepeatTypeString(repeatType: Int): String {
        return when(repeatType) {
            RepeatType.Never.num -> {
                RepeatType.Never.name
            }
            RepeatType.Everyday.num -> {
                RepeatType.Everyday.name
            }
            else -> {
                RepeatType.Never.name
            }
        }
    }

    private fun getNotificationTypeString(notificationType: Int): String {
        return when(notificationType) {
            NotificationType.None.num -> {
                NotificationType.None.name
            }
            NotificationType.Notification.num -> {
                NotificationType.Notification.name
            }
            NotificationType.NotificationSound.num -> {
                NotificationType.NotificationSound.name
            }
            else -> {
                NotificationType.None.name
            }
        }
    }
}