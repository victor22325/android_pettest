package com.broadmasterbiotech.pettestapp.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.CreatePetEvent
import com.broadmasterbiotech.library.viewmodel.CreatePetViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.adapter.SpinnerDialogAdapter.SpinnerDialogRecyclerViewOptionListener
import com.broadmasterbiotech.pettestapp.databinding.ActivityCreatePetBinding
import com.broadmasterbiotech.pettestapp.dialog.SpinnerDialog
import com.broadmasterbiotech.pettestapp.enums.pet.DiabetesType
import com.broadmasterbiotech.pettestapp.enums.pet.Gender
import com.broadmasterbiotech.pettestapp.enums.pet.Species
import com.broadmasterbiotech.pettestapp.enums.pet.WeightUnit
import com.bumptech.glide.Glide
import com.google.firebase.storage.StorageMetadata
import com.jeremyliao.liveeventbus.LiveEventBus
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import kotlinx.android.synthetic.main.progressbar_dialog.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity
import java.io.ByteArrayOutputStream
import java.util.*

class CreatePetActivity: SwipeBackActivity() {

    companion object {

        private val TAG = CreatePetActivity::class.java.simpleName

        const val IMAGE_REQUEST_CODE: Int = 1
        const val RESIZE_REQUEST_CODE: Int = 2
    }

    private lateinit var createPetBinding: ActivityCreatePetBinding
    private lateinit var createPetViewMode: CreatePetViewModel
    private lateinit var speciesDialog: SpinnerDialog
    private lateinit var genderDialog: SpinnerDialog
    private val calendar: Calendar = Calendar.getInstance()
    private var status: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intentInt = intent.getIntExtra(PetTestMainActivity.INTENT_EXTRA_SELECT_PET_DIALOG_TO_CREATE_PET_KEY, 0)
        status = intentInt
        createPetViewMode = ViewModelProvider(this@CreatePetActivity).get(CreatePetViewModel::class.java)
        createPetBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_pet)
        createPetBinding.lifecycleOwner = this
        createPetBinding.activity = this@CreatePetActivity
        createPetBinding.viewModel = createPetViewMode
        createPetBinding.createPetProgressDialog.progressDialog_content_text.text = resources.getString(R.string.progressbar_content_creating_text)
        createPetBinding.createPetTopBarLayout.pettest_advocateLogo_back_layout.setOnClickListener { finish() }
        createPetViewMode.createPetLiveEvent.observe(this@CreatePetActivity, Observer { event -> event?.let { baseEventHandler(it) } })
        setDiabetesTypesCheckBoxListener()
    }

    private fun baseEventHandler(event: BaseEvent) {
        createPetViewMode.isProcessing.value = false
        when (event) {
            is CreatePetEvent.Success -> {
                when (event.status) {
                    Code.CREATE_PET_REQUEST_SUCCESS -> {
                        when (status) {
                            PetTestMainActivity.BUNDLE_SELECT_PET_DIALOG_TO_CREATE_PET_VALUE -> {
                                LiveEventBus.get(PetTestMainActivity.LIVE_EVENT_BUS_PET_INFO_LIST_UPDATE_STICKY_KEY).post(event.petInfoList)
                                finish()
                            }
                            else -> {
                                val intent = Intent(this@CreatePetActivity, PetTestMainActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                        }
                    }
                    Code.CREATE_PET_REQUEST_AVATAR_PHOTO_UPLOAD_SUCCESS -> {
                        createPetViewMode.checkPetInfo()
                        Log.d(TAG, "uri:${event.uri}")
                    }
                }
            }
            is CreatePetEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_connect_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreatePetEvent.PetNameFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_name_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreatePetEvent.PetSpeciesFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_species_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreatePetEvent.PetGlucoseUpperLimitFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_glucose_upper_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreatePetEvent.PetGlucoseLowerLimitFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_glucose_lower_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreatePetEvent.PetUpperMoreThanLowerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_upper_more_than_lower_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreatePetEvent.PetBirthdayAtFutureError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_birthday_at_future_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreatePetEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_server_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
            is CreatePetEvent.PetAlreadyExist -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_pet_already_exist_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreatePetEvent.ServerHaveNoData -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_server_have_no_data))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
            is CreatePetEvent.UpdateAvatarPhotoFail -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_upload_avatar_photo_fail))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
                createPetViewMode.checkPetInfo()
            }
            is CreatePetEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_PET_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_pet_dialog_other_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
        }
    }

    private fun setDiabetesTypesCheckBoxListener() {
        createPetBinding.createPetDiabetesTypeNaCheckbox.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                createPetBinding.createPetDiabetesTypeNaCheckbox.isChecked = true
                createPetBinding.createPetDiabetesType1Checkbox.isChecked = false
                createPetBinding.createPetDiabetesType2Checkbox.isChecked = false
                createPetViewMode.petDiabetesType.value = DiabetesType.NA.num
                createPetBinding.createPetDiabetesYearLayout.visibility = View.GONE
            } else {
                createPetViewMode.petDiabetesType.value = null
                createPetBinding.createPetDiabetesYearLayout.visibility = View.GONE
            }
        }
        createPetBinding.createPetDiabetesType1Checkbox.setOnCheckedChangeListener{ _, isChecked ->
            if(isChecked) {
                createPetBinding.createPetDiabetesTypeNaCheckbox.isChecked = false
                createPetBinding.createPetDiabetesType1Checkbox.isChecked = true
                createPetBinding.createPetDiabetesType2Checkbox.isChecked = false
                createPetViewMode.petDiabetesType.value = DiabetesType.Type1.num
                createPetBinding.createPetDiabetesYearLayout.visibility = View.VISIBLE
            } else {
                createPetViewMode.petDiabetesType.value = null
                createPetBinding.createPetDiabetesYearLayout.visibility = View.GONE
            }
        }
        createPetBinding.createPetDiabetesType2Checkbox.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                createPetBinding.createPetDiabetesTypeNaCheckbox.isChecked = false
                createPetBinding.createPetDiabetesType1Checkbox.isChecked = false
                createPetBinding.createPetDiabetesType2Checkbox.isChecked = true
                createPetViewMode.petDiabetesType.value = DiabetesType.Type2.num
                createPetBinding.createPetDiabetesYearLayout.visibility = View.VISIBLE
            } else {
                createPetViewMode.petDiabetesType.value = null
                createPetBinding.createPetDiabetesYearLayout.visibility = View.GONE
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.d(TAG,"onBackPressed")
    }

    fun speciesOnClick() {
        val speciesList = arrayListOf(Species.Canine.name, Species.Feline.name)
        val speciesListener = object: SpinnerDialogRecyclerViewOptionListener {
            override fun onClicked(option: String) {
                speciesDialog.dismiss()
                when (option) {
                    Species.Canine.name -> {
                        createPetBinding.createPetSpeciesText.text = Species.Canine.name
                        createPetViewMode.petSpecies.value = Species.Canine.num
                    }
                    Species.Feline.name -> {
                        createPetBinding.createPetSpeciesText.text = Species.Feline.name
                        createPetViewMode.petSpecies.value = Species.Feline.num
                    }
                    else -> {
                        createPetBinding.createPetSpeciesText.text = Species.Canine.name
                        createPetViewMode.petSpecies.value = Species.Canine.num
                    }
                }
            }
        }
        speciesDialog = SpinnerDialog(activity = this, title = resources.getString(R.string.create_pet_spinner_dialog_select_species), list = speciesList, listener = speciesListener, lifecycleOwner = this@CreatePetActivity)
        speciesDialog.show()
    }

    fun petWeightLBOnClick() {
        createPetBinding.createPetWeightUnitLb.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.lb_selected))
        createPetBinding.createPetWeightUnitKg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.kilogram_unselected))
        createPetViewMode.petWeightUnit.value = WeightUnit.Libra.num
    }

    fun petWeightKGOnClick() {
        createPetBinding.createPetWeightUnitLb.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.lb_unselected))
        createPetBinding.createPetWeightUnitKg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.kilogram_selected))
        createPetViewMode.petWeightUnit.value = WeightUnit.Kilogram.num
    }

    fun petGenderOnClick() {
        val genderList = arrayListOf(Gender.Male.name, Gender.Female.name)
        val genderListener = object: SpinnerDialogRecyclerViewOptionListener {
            override fun onClicked(option: String) {
                genderDialog.dismiss()
                when (option) {
                    Gender.Male.name -> {
                        createPetBinding.createPetGenderText.text =  Gender.Male.name
                        createPetViewMode.petGender.value = Gender.Male.num
                    }
                    Gender.Female.name -> {
                        createPetBinding.createPetGenderText.text = Gender.Female.name
                        createPetViewMode.petGender.value = Gender.Female.num
                    }
                    else -> {
                        createPetBinding.createPetGenderText.text =  Gender.Male.name
                        createPetViewMode.petGender.value = Gender.Male.num
                    }
                }
            }
        }
        genderDialog = SpinnerDialog(activity = this, title = resources.getString(R.string.create_pet_spinner_dialog_select_gender), list = genderList, listener = genderListener, lifecycleOwner = this@CreatePetActivity)
        genderDialog.show()
    }

    fun petBirthDateOnClick() {
        val datePickerDialog = DatePickerDialog(this, R.style.CustomDatePickerDialog, DatePickerDialog.OnDateSetListener {
                _, year, month, dayOfMonth ->
            createPetBinding.createPetBirthText.text = setDateFormat(year, month, dayOfMonth)
            createPetViewMode.petBirthday.value = setDateFormat(year, month, dayOfMonth)
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE))
        datePickerDialog.show()
    }

    private fun setDateFormat(year: Int, monthOfYear: Int, dayOfMonth: Int): String {
        return (year.toString() + "/"
                + (monthOfYear + 1).toString() + "/"
                + dayOfMonth.toString())
    }

    fun pickerAvatar() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        val destIntent = Intent.createChooser(intent, null)
        startActivityForResult(destIntent, IMAGE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode != Activity.RESULT_OK) {
            return
        } else {
            when (requestCode) {
                IMAGE_REQUEST_CODE -> {
                    if(data!=null) {
                        resizeImage(data.data!!)
                    }
                }
                RESIZE_REQUEST_CODE -> {
                    if (data!= null) {
                        Glide.with(this@CreatePetActivity).load(data.data).into(createPetBinding.createPetAvatarImage)
                        val uri = data.data!!
                        val contentResolver = this.contentResolver
                        val bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(uri))
                        val byteArrayOutputStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
                        val imageData = byteArrayOutputStream.toByteArray()
                        val storageMetadata = StorageMetadata.Builder()
                            .setCustomMetadata("petInfo", queryFileName(uri))
                            .setContentType("image/jpg")
                            .build()
                        createPetViewMode.uploadFileName = queryFileName(uri)
                        createPetViewMode.uploadImageData = imageData
                        createPetViewMode.uploadMetadata = storageMetadata
                    }
                }
            }
        }
    }

    private fun queryFileName(uri: Uri): String {
        var name = ""
        this.contentResolver.query(uri, null, null, null, null)?.use { cursor->
            if(cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
            }
        }
        return name
    }

    private fun resizeImage(uri: Uri) {
        Log.d(TAG, "resizeImage path:${uri.path}")
        try {
            val intent = Intent("com.android.camera.action.CROP")
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            intent.setDataAndType(uri, "image/*")
            intent.putExtra("crop", "true")
            intent.putExtra("aspectX", 1)
            intent.putExtra("aspectY", 1)
            intent.putExtra("outputX", 500)
            intent.putExtra("outputY", 500)
            intent.putExtra("scale", true)
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG)
            intent.putExtra("noFaceDetection", true)
            intent.putExtra("return-data", false)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            startActivityForResult(intent, RESIZE_REQUEST_CODE)
        } catch (e: Exception) {
            Log.d(TAG, "resizeImage exception:${e.message}")
        }
    }

}