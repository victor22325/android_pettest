package com.broadmasterbiotech.pettestapp.activity

import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.viewmodel.ReminderViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.databinding.ActivityCreateReminderBinding
import com.broadmasterbiotech.pettestapp.dialog.SelectDateDialog
import com.broadmasterbiotech.pettestapp.dialog.SelectTimeDialog
import com.broadmasterbiotech.pettestapp.enums.reminder.NotificationType
import com.broadmasterbiotech.pettestapp.enums.reminder.RepeatType
import com.broadmasterbiotech.pettestapp.fragment.ReminderListFragment
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CreateReminderActivity: SwipeBackActivity() {

    companion object {
        val TAG = CreateReminderActivity::class.java.simpleName
    }

    private lateinit var activityCreateReminderBinding: ActivityCreateReminderBinding
    private lateinit var reminderViewModel: ReminderViewModel
    private var flag: Int = ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG
    private val calendar: Calendar = Calendar.getInstance()
    private lateinit var timePicker: TimePicker
    private lateinit var datePicker: DatePicker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intentInt = intent.getIntExtra(ReminderListFragment.INTENT_EXTRA_CREATE_REMINDER_TYPE_KEY, ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG)
        flag = intentInt
        activityCreateReminderBinding =  DataBindingUtil.setContentView(this, R.layout.activity_create_reminder)
        reminderViewModel = ViewModelProvider(this@CreateReminderActivity).get(ReminderViewModel::class.java)
        activityCreateReminderBinding.activity = this
        activityCreateReminderBinding.lifecycleOwner = this
        activityCreateReminderBinding.flag = this.flag
        activityCreateReminderBinding.viewModel = reminderViewModel
        activityCreateReminderBinding.createReminderTopBarLayout.pettest_advocateLogo_back_layout.setOnClickListener { finish() }
        activityCreateReminderBinding.createReminderTopBarLayout.pettest_advocateLogo_ok_layout.visibility = View.VISIBLE
        activityCreateReminderBinding.createReminderTopBarLayout.pettest_advocateLogo_ok_layout.setOnClickListener {
            reminderViewModel.createReminderOnClick(flag)
            finish()
        }
        timePicker = TimePicker(this)
        datePicker = DatePicker(this)
        timePicker.hour = calendar.get(Calendar.HOUR_OF_DAY)
        timePicker.minute = calendar.get(Calendar.MINUTE)
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        when (flag) {
            ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG -> {
                activityCreateReminderBinding.createReminderTitleIcon.setImageResource(R.drawable.medicine_reminder)
                activityCreateReminderBinding.createReminderTimeText.text = getTimeString(timePicker)
                activityCreateReminderBinding.createReminderNotificationTypeText.text = NotificationType.None.name
                activityCreateReminderBinding.createReminderRepeatTypeText.text = RepeatType.Never.name
                reminderViewModel.initialInsulin(getTimeString(timePicker), RepeatType.Never.num, NotificationType.None.num)
            }
            ReminderListFragment.CREATE_VETERINARIAN_REMINDER_FLAG -> {
                activityCreateReminderBinding.createReminderTitleIcon.setImageResource(R.drawable.clinic_reminder_title)
                activityCreateReminderBinding.createReminderDateText.text = getDateString(datePicker)
                activityCreateReminderBinding.createReminderTimeText.text = getTimeString(timePicker)
                activityCreateReminderBinding.createReminderNotificationTypeText.text = NotificationType.None.name
                reminderViewModel.initialVeterinarian(getDateString(datePicker), getTimeString(timePicker), NotificationType.None.num)
            }
        }
    }

    private val selectDateListener = object : SelectDateDialog.Listener {
        override fun datePickerOnClick(date: String) {
            when(flag) {
                ReminderListFragment.CREATE_VETERINARIAN_REMINDER_FLAG -> reminderViewModel.reminderDateSingleLiveEvent.value = date
            }
            activityCreateReminderBinding.createReminderDateText.text = date
            val dateSplit = date.split(resources.getString(R.string.simple_date_format_date_split))
            datePicker.updateDate(dateSplit[0].toInt(), dateSplit[1].toInt()-1, dateSplit[2].toInt())
        }
    }

    fun dateOptionOnClick() {
        val selectDateDialog = SelectDateDialog(this, datePicker, selectDateListener, this)
        selectDateDialog.show()
    }

    private val selectTimeListener = object : SelectTimeDialog.Listener {
        override fun timePickerOnClick(time: String) {
            when(flag) {
                ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG -> reminderViewModel.reminderTimeSingleLiveEvent.value = time
                ReminderListFragment.CREATE_VETERINARIAN_REMINDER_FLAG -> reminderViewModel.reminderTimeSingleLiveEvent.value = time
            }
            activityCreateReminderBinding.createReminderTimeText.text = time
            val timeSplit = time.split(resources.getString(R.string.simple_date_format_time_split))
            timePicker.hour = timeSplit[0].toInt()
            timePicker.minute = timeSplit[1].toInt()
        }
    }

    fun timeOptionOnClick() {
        val selectTimeDialog = SelectTimeDialog(this, timePicker, selectTimeListener, this)
        selectTimeDialog.show()
    }

    fun repeatOptionOnClick() {
        val repeatList = ArrayList<String>()
        repeatList.add(RepeatType.Never.name)
        repeatList.add(RepeatType.Everyday.name)
        MyCustomDialog(this, resources.getString(R.string.my_custom_dialog_bottom_text))
            .setBottomTitle(resources.getString(R.string.create_reminder_repeat_type_title_text))
            .setBottomTitleColor(R.color.colorBlack)
            .setListView(repeatList) { _, _, position, _ ->
                when(position) {
                    RepeatType.Never.num -> {
                        activityCreateReminderBinding.createReminderRepeatTypeText.text = RepeatType.Never.name
                        reminderViewModel.reminderRepeatTypeSingleLiveEvent.value = RepeatType.Never.num
                    }
                    RepeatType.Everyday.num -> {
                        activityCreateReminderBinding.createReminderRepeatTypeText.text = RepeatType.Everyday.name
                        reminderViewModel.reminderRepeatTypeSingleLiveEvent.value = RepeatType.Everyday.num
                    }
                }
            }
            .setBottomNegativeButton(resources.getString(R.string.my_custom_dialog_cancel_text)) { }
            .setBottomNegativeButtonColor(R.color.myCustomDialogColorBlue)
            .builder()
            .show()
    }

    fun typeOptionOnClick() {
        val notificationTypeList = ArrayList<String>()
        notificationTypeList.add(NotificationType.None.name)
        notificationTypeList.add(NotificationType.Notification.name)
        notificationTypeList.add(NotificationType.NotificationSound.name)
        MyCustomDialog(this, resources.getString(R.string.my_custom_dialog_bottom_text))
            .setBottomTitle(resources.getString(R.string.create_reminder_notification_type_title_text))
            .setBottomTitleColor(R.color.colorBlack)
            .setListView(notificationTypeList) { _, _, position, _ ->
                when(position) {
                    NotificationType.None.num -> {
                        activityCreateReminderBinding.createReminderNotificationTypeText.text = NotificationType.None.name
                        if(flag == ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG) {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.None.num
                        } else {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.None.num
                        }
                    }
                    NotificationType.Notification.num -> {
                        activityCreateReminderBinding.createReminderNotificationTypeText.text = NotificationType.Notification.name
                        if(flag == ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG) {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.Notification.num
                        } else {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.Notification.num
                        }
                    }
                    NotificationType.NotificationSound.num -> {
                        activityCreateReminderBinding.createReminderNotificationTypeText.text = NotificationType.NotificationSound.name
                        if(flag == ReminderListFragment.CREATE_INSULIN_REMINDER_FLAG) {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.NotificationSound.num
                        } else {
                            reminderViewModel.reminderNotificationTypeSingleLiveEvent.value = NotificationType.NotificationSound.num
                        }
                    }
                }
            }
            .setBottomNegativeButton(resources.getString(R.string.my_custom_dialog_cancel_text)) { }
            .setBottomNegativeButtonColor(R.color.myCustomDialogColorBlue)
            .builder()
            .show()
    }

    private fun getDateString(datePicker: DatePicker): String {
        val dateCalendar = Calendar.getInstance()
        dateCalendar.set(Calendar.YEAR, datePicker.year)
        dateCalendar.set(Calendar.MONTH, datePicker.month)
        dateCalendar.set(Calendar.DAY_OF_MONTH, datePicker.dayOfMonth)
        val dateSimpleDateFormat = SimpleDateFormat(resources.getString(R.string.simple_date_format_year_month_date), Locale.US)
        return dateSimpleDateFormat.format(dateCalendar.time)
    }

    private fun getTimeString(timePicker: TimePicker): String {
        val timeCalendar = Calendar.getInstance()
        timePicker.hour.let { timeCalendar.set(Calendar.HOUR_OF_DAY, it) }
        timePicker.minute.let { timeCalendar.set(Calendar.MINUTE, it) }
        val timeSimpleDateFormat = SimpleDateFormat(resources.getString(R.string.simple_date_format_hour_minute), Locale.US)
        return timeSimpleDateFormat.format(timeCalendar.time)
    }
}