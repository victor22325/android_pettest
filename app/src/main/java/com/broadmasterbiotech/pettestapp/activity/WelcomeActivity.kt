package com.broadmasterbiotech.pettestapp.activity

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.viewmodel.WelcomeViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.ActivityWelcomBinding
import me.imid.swipebacklayout.lib.app.SwipeBackActivity

class WelcomeActivity : SwipeBackActivity() {

    companion object {

        private val TAG = WelcomeActivity::class.java.simpleName
        private const val REQUEST_CODE_PERMISSION = 1000
    }

    private lateinit var welcomeBinding: ActivityWelcomBinding

    private lateinit var welcomeViewModel: WelcomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        welcomeBinding = DataBindingUtil.setContentView(this@WelcomeActivity, R.layout.activity_welcom)
        welcomeBinding.lifecycleOwner = this
        welcomeViewModel = ViewModelProvider(this).get(WelcomeViewModel::class.java)
        welcomeViewModel.toNextPage.observe(this, Observer { toNextPage ->
            toNextPage?.let {
                if (it) {
                    Log.d(TAG, "goto VerifySNActivity")
                    val intent = Intent(this@WelcomeActivity, VerifySNActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        })
        initPermission()
    }

    private fun initPermission() {
        Log.d(TAG, "initPermission")
        var permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        permission = if (permission == PackageManager.PERMISSION_GRANTED) ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) else permission
        if(permission != PackageManager.PERMISSION_GRANTED)
            requestPermission()
        else
            welcomeViewModel.startGoToNextPage()
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this@WelcomeActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG,"Already grant permission")
                welcomeViewModel.startGoToNextPage()
            } else {
                AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage(R.string.dialog_grant_permission_title)
                    .setPositiveButton(R.string.dialog_grant_permission_btn_yes) { _, _ -> requestPermission() }
                    .setNegativeButton(R.string.dialog_grant_permission_btn_no) { _, _ -> finish() }
                    .show()
            }
        }
    }
}