package com.broadmasterbiotech.pettestapp.activity

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.CreateAccountEvent
import com.broadmasterbiotech.library.viewmodel.CreateAccountViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.databinding.ActivityCreateAccountBinding
import kotlinx.android.synthetic.main.progressbar_dialog.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity

class CreateAccountActivity: SwipeBackActivity() {

    companion object {

        private val TAG = CreateAccountActivity::class.java.simpleName
    }

    private lateinit var createAccountBinding: ActivityCreateAccountBinding

    private lateinit var createAccountViewModel: CreateAccountViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createAccountBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_account)
        createAccountViewModel = ViewModelProvider(this@CreateAccountActivity).get(CreateAccountViewModel::class.java)
        createAccountBinding.lifecycleOwner = this
        createAccountBinding.activity = this
        createAccountBinding.viewModel = createAccountViewModel
        createAccountBinding.loginProgressDialog.progressDialog_content_text.text = resources.getString(R.string.progressbar_content_creating_text)
        createAccountViewModel.createAccountLiveEvent.observe(this@CreateAccountActivity, Observer { event -> event?.let { baseEventHandler(it) } } )
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.d(TAG, "onBackPressed")
    }

    private fun baseEventHandler(event: BaseEvent) {
        createAccountViewModel.isProcessing.value = false
        when (event) {
            is CreateAccountEvent.Success -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.create_account_dialog_success_title))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_account_dialog_success_content))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
            is CreateAccountEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_account_dialog_connect_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreateAccountEvent.EmailFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_account_dialog_email_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreateAccountEvent.PasswordFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_account_dialog_password_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreateAccountEvent.ConfirmFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_account_dialog_confirm_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is CreateAccountEvent.SendCertificationEmailFail -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_account_dialog_certification_letter_send_fail, event.message))
                    .setPositiveButton(getString(R.string.error_dialog_builder_positive)) {
                        createAccountViewModel.sendCertificationEmail()
                        finish()
                    }
                    .setNegativeButton(getString(R.string.error_dialog_builder_negative)) {finish()}
                    .builder()
                    .show()
            }
            is CreateAccountEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_ACCOUNT_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_account_dialog_server_error, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) { finish() }
                    .builder()
                    .show()
            }
            is CreateAccountEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.CREATE_ACCOUNT_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.create_account_dialog_other_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) { finish() }
                    .builder()
                    .show()
            }
        }
    }
}