package com.broadmasterbiotech.pettestapp.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.LoginEvent
import com.broadmasterbiotech.library.viewmodel.LoginViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.databinding.ActivityLoginBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import kotlinx.android.synthetic.main.progressbar_dialog.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity

class LoginActivity: SwipeBackActivity() {

    companion object {

        private val TAG = LoginActivity::class.java.simpleName

        private const val RC_SIGN_IN = 1

        private const val requestIdToken = "261653557470-42lrsn6s62erheoq9tba6ac145knsr2m.apps.googleusercontent.com"
    }

    private lateinit var loginBinding: ActivityLoginBinding

    private lateinit var loginViewModel: LoginViewModel

    private lateinit var googleSignInOptions: GoogleSignInOptions

    private lateinit var googleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        loginBinding.googleSignInBtn.setOnClickListener { googleSignIn() }
        loginBinding.lifecycleOwner = this@LoginActivity
        loginViewModel = ViewModelProvider(this@LoginActivity).get(LoginViewModel::class.java)
        loginBinding.viewModel = loginViewModel
        loginBinding.activity = this
        loginBinding.loginProgressDialog.progressDialog_content_text.text = resources.getString(R.string.progressbar_content_loading_text)
        loginViewModel.getEmailAddressAndPassword()
        loginViewModel.loginLiveEvent.observe(this@LoginActivity, Observer { event -> event?.let { baseEventHandler(it) } })
        loginViewModel.isProcessing.observe(this@LoginActivity, Observer { event ->event?.let { boolean->
            loginBinding.googleSignInBtn.isEnabled = !boolean
        } })
        loginBinding.loginAdvocateLogoLayout.pettest_advocateLogo_back_layout.visibility = View.GONE
    }

    private fun baseEventHandler(event: BaseEvent) {
        loginViewModel.isProcessing.value = false
        when(event) {
            is LoginEvent.Success -> {
                Log.d(TAG, "Login Success status:${event.status}, uuid:${event.uuid}")
                when (event.status) {
                    Code.LOGIN_REQUEST_SUCCESS -> {
                        Log.d(TAG, "email:${loginViewModel.mFireBaseAuth.currentUser?.email}, displayName:${loginViewModel.mFireBaseAuth.currentUser?.displayName}")
                        val intent = Intent(this@LoginActivity, PetTestMainActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    Code.LOGIN_REQUEST_SUCCESS_NEED_CREATE_PET -> {
                        val intent = Intent(this@LoginActivity, CreatePetActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    Code.LOGIN_SEND_CERTIFICATION_EMAIL_SUCCESS -> {
                        MyCustomDialog(this)
                            .setTitle(getString(R.string.login_dialog_certification_email_title))
                            .setTitleStyleBold()
                            .setMessage(getString(R.string.login_dialog_certification_email_content))
                            .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {loginViewModel.logout()}
                            .builder()
                            .show()
                    }
                }
            }
            is LoginEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.LOGIN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.login_dialog_connect_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {loginViewModel.logout()}
                    .builder()
                    .show()
            }
            is LoginEvent.EmailFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.LOGIN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.login_dialog_email_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is LoginEvent.PasswordFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.LOGIN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.login_dialog_password_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is LoginEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.LOGIN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.login_dialog_server_error, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {loginViewModel.logout()}
                    .builder()
                    .show()
            }
            is LoginEvent.FireBaseAuthSignInError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.LOGIN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.login_dialog_sign_in_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {loginViewModel.logout()}
                    .builder()
                    .show()
            }
            is LoginEvent.EmailNotCertified -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.LOGIN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.login_dialog_email_certified_error))
                    .setPositiveButton(getString(R.string.error_dialog_builder_positive)) {loginViewModel.sendCertificationEmail()}
                    .setNegativeButton(getString(R.string.error_dialog_builder_negative)) {loginViewModel.logout()}
                    .builder()
                    .show()
            }
            is LoginEvent.SendCertificationEmailFail -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.LOGIN_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.login_dialog_send_certification_email_fail))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {loginViewModel.logout()}
                    .builder()
                    .show()
            }
            is LoginEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.LOGIN_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.login_dialog_other_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_logout)) {
                        loginViewModel.logout()
                        finish()
                    }
                    .builder()
                    .show()
            }
        }
    }

    private fun googleSignIn() {
        loginViewModel.isProcessing.value = true
        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(requestIdToken)
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "requestCode:$requestCode, resultCode:$resultCode, data:$data")
        if(requestCode == RC_SIGN_IN) {
            try {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                val account = task.getResult(ApiException::class.java)
                loginViewModel.fireBaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                Log.d(TAG, "onActivityResult exception:${e.message}")
                this.baseEventHandler(LoginEvent.ServerError(status = Code.LOGIN_SERVER_ERROR, message = e.message))
            }
        }
    }

    fun createAccountOnClick() {
        val intent = Intent(this@LoginActivity, CreateAccountActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        Log.d(TAG, "onBackPressed")
        loginViewModel.logout()
        finish()
    }
}