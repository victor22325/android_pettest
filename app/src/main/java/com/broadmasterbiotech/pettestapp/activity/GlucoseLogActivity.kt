package com.broadmasterbiotech.pettestapp.activity

import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.event.*
import com.broadmasterbiotech.library.viewmodel.GlucoseLogViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.adapter.GlucoseLogAdapter
import com.broadmasterbiotech.pettestapp.databinding.ActivityGlucoseLogBinding
import com.broadmasterbiotech.pettestapp.dialog.CreateGlucoseDialog
import com.broadmasterbiotech.pettestapp.dialog.EditGlucoseDialog
import com.broadmasterbiotech.pettestapp.enums.SelectPetSwipeOption
import com.broadmasterbiotech.pettestapp.fragment.RecordFragment
import com.jeremyliao.liveeventbus.LiveEventBus
import com.yanzhenjie.recyclerview.OnItemMenuClickListener
import com.yanzhenjie.recyclerview.SwipeMenuCreator
import com.yanzhenjie.recyclerview.SwipeMenuItem
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import kotlinx.android.synthetic.main.progressbar_dialog.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GlucoseLogActivity: SwipeBackActivity() {

    companion object {

        private val TAG = GlucoseLogActivity::class.java.simpleName
    }

    private lateinit var activityGlucoseLogBinding: ActivityGlucoseLogBinding
    private lateinit var glucoseLogViewModel: GlucoseLogViewModel
    private lateinit var glucoseLogAdapter: GlucoseLogAdapter
    private lateinit var addGlucoseLogDialog: CreateGlucoseDialog
    private lateinit var editGlucoseDialog: EditGlucoseDialog
    private lateinit var glucoseInfoList: ArrayList<GlucoseInfo>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityGlucoseLogBinding = DataBindingUtil.setContentView(this, R.layout.activity_glucose_log)
        glucoseLogViewModel = ViewModelProvider(this@GlucoseLogActivity).get(GlucoseLogViewModel::class.java)
        activityGlucoseLogBinding.lifecycleOwner = this
        activityGlucoseLogBinding.activity = this
        activityGlucoseLogBinding.viewModel = glucoseLogViewModel
        activityGlucoseLogBinding.glucoseLogTopBarLayout.pettest_advocateLogo_back_layout.setOnClickListener { finish() }
        activityGlucoseLogBinding.glucoseLogProgressDialog.progressDialog_content_text.text = resources.getString(R.string.progressbar_content_loading_text)
        val swipeMenuCreator = SwipeMenuCreator { _, rightMenu, _ ->
            val editItem = SwipeMenuItem(applicationContext)
            editItem.text = resources.getString(R.string.select_pet_dialog_swipe_edit_btn)
            editItem.setTextColorResource(R.color.colorWhite)
            editItem.textSize = 20
            editItem.setBackgroundColorResource(R.color.colorSelectPetEdit)
            editItem.height = ViewGroup.LayoutParams.MATCH_PARENT
            editItem.width = 220
            rightMenu.addMenuItem(editItem)
            val deleteItem = SwipeMenuItem(applicationContext)
            deleteItem.text = resources.getString(R.string.select_pet_dialog_swipe_delete_btn)
            deleteItem.setTextColorResource(R.color.colorWhite)
            deleteItem.textSize = 20
            deleteItem.setBackgroundColorResource(R.color.colorSelectPetDelete)
            deleteItem.height = ViewGroup.LayoutParams.MATCH_PARENT
            deleteItem.width = 220
            rightMenu.addMenuItem(deleteItem)
        }
        val itemMenuClickListener = OnItemMenuClickListener { menuBridge, adapterPosition ->
            menuBridge.closeMenu()
            when (menuBridge.position) {
                SelectPetSwipeOption.Edit.num -> {
                    editGlucoseDialog = EditGlucoseDialog(this, glucoseLogViewModel, glucoseInfoList[adapterPosition], this)
                    editGlucoseDialog.show()
                }
                SelectPetSwipeOption.Delete.num -> {
                    MyCustomDialog(this)
                        .setTitle(resources.getString(R.string.glucose_log_swipe_delete_dialog_title))
                        .setTitleStyleBold()
                        .setMessage(resources.getString(R.string.glucose_log_swipe_delete_dialog_content, glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name, "${glucoseInfoList[adapterPosition].year}/${glucoseInfoList[adapterPosition].month}/${glucoseInfoList[adapterPosition].day}", glucoseInfoList[adapterPosition].value.toString()))
                        .setPositiveButton(resources.getString(R.string.error_dialog_builder_positive)) {
                            activityGlucoseLogBinding.glucoseLogProgressDialog.progressDialog_content_text.text = resources.getString(R.string.progressbar_content_deleting_text)
                            glucoseLogViewModel.deleteGlucoseInfo(glucoseInfoList[adapterPosition])
                        }
                        .setNegativeButton(resources.getString(R.string.error_dialog_builder_negative)) {}
                        .builder()
                        .show()
                }
            }
        }
        activityGlucoseLogBinding.glucoseLogRecyclerView.setOnItemMenuClickListener(itemMenuClickListener)
        activityGlucoseLogBinding.glucoseLogRecyclerView.setSwipeMenuCreator(swipeMenuCreator)
        glucoseInfoList = glucoseLogViewModel.petTestRepository.glucoseListLiveEvent.value!!
        glucoseLogAdapter = GlucoseLogAdapter(glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value!!, glucoseLogAdapterListener)
        glucoseLogViewModel.petTestRepository.glucoseListLiveEvent.value!!.sort()
        glucoseLogAdapter.submitList(glucoseLogViewModel.petTestRepository.glucoseListLiveEvent.value!!)
        activityGlucoseLogBinding.glucoseLogRecyclerView.adapter = glucoseLogAdapter
        glucoseLogViewModel.glucoseLogSyncLiveEvent.observe(this, Observer { glucoseLogSyncEventHandler(it)} )
        glucoseLogViewModel.glucoseLogAddLiveEvent.observe(this, Observer { glucoseLogAddEventHandler(it)} )
        glucoseLogViewModel.glucoseLogDeleteEvent.observe(this, Observer { glucoseLogDeleteEventHandler(it)} )
        glucoseLogViewModel.glucoseLogEditEvent.observe(this, Observer { glucoseLogEditEventHandler(it) })
    }

    private val glucoseLogAdapterListener = object: GlucoseLogAdapter.GlucoseLogAdapterListener {
        override fun onClick(glucoseInfo: GlucoseInfo) {
            Log.d(TAG, "glucoseInfo onClick:$glucoseInfo")
        }
    }

    fun addGlucoseOnClick() {
        addGlucoseLogDialog = CreateGlucoseDialog(this, glucoseLogViewModel,this)
        addGlucoseLogDialog.show()
    }

    private fun updateFromToDate(glucoseInfoList: ArrayList<GlucoseInfo>) {
        Log.d(TAG, "size:${glucoseInfoList.size}")
        if(glucoseInfoList.size == 0) {
            activityGlucoseLogBinding.fromDate = ""
            activityGlucoseLogBinding.toDate = ""
        } else {
            val formCalendar = Calendar.getInstance()
            formCalendar.set(Calendar.YEAR, glucoseInfoList[0].year.toInt())
            formCalendar.set(Calendar.MONTH, glucoseInfoList[0].month.toInt()-1)
            formCalendar.set(Calendar.DAY_OF_MONTH, glucoseInfoList[0].day.toInt())
            formCalendar.set(Calendar.HOUR_OF_DAY, glucoseInfoList[0].hour.toInt())
            formCalendar.set(Calendar.MINUTE, glucoseInfoList[0].minute.toInt())
            activityGlucoseLogBinding.fromDate = SimpleDateFormat(getString(R.string.simple_date_format_month_date_year), Locale.US).format(formCalendar.timeInMillis)
            if(glucoseInfoList.size > 2) {
                val toCalendar = Calendar.getInstance()
                toCalendar.set(Calendar.YEAR, glucoseInfoList[glucoseInfoList.size-1].year.toInt())
                toCalendar.set(Calendar.MONTH, glucoseInfoList[glucoseInfoList.size-1].month.toInt()-1)
                toCalendar.set(Calendar.DAY_OF_MONTH, glucoseInfoList[glucoseInfoList.size-1].day.toInt())
                toCalendar.set(Calendar.HOUR_OF_DAY, glucoseInfoList[glucoseInfoList.size-1].hour.toInt())
                toCalendar.set(Calendar.MINUTE, glucoseInfoList[glucoseInfoList.size-1].minute.toInt())
                activityGlucoseLogBinding.toDate = SimpleDateFormat(getString(R.string.simple_date_format_month_date_year), Locale.US).format(toCalendar.timeInMillis)
            } else {
                activityGlucoseLogBinding.toDate = SimpleDateFormat(getString(R.string.simple_date_format_month_date_year), Locale.US).format(formCalendar.timeInMillis)
            }
        }
    }

    private fun glucoseLogSyncEventHandler(event: BaseEvent) {
        glucoseLogViewModel.isBusy.value = false
        when(event) {
            is GlucoseLogSyncEvent.Success -> {
                Log.d(TAG, "Sync ${glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name}'s glucose data success")
                updateFromToDate(event.glucoseInfoList!!)
            }
            is GlucoseLogSyncEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.SYNC_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.sync_glucose_dialog_server_error, glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
            is GlucoseLogSyncEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.SYNC_GLU_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.sync_glucose_dialog_other_error, glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
        }
    }

    private fun glucoseLogAddEventHandler(event: BaseEvent) {
        when(event) {
            is GlucoseLogAddEvent.Success -> {
                LiveEventBus.get(RecordFragment.LIVE_EVENT_BUS_GLUCOSE_INFO_LIST_UPDATE_STICKY_KEY).post(event.glucoseInfoList)
                updateFromToDate(event.glucoseInfoList)
                glucoseLogAdapter.submitList(event.glucoseInfoList)
                MyCustomDialog(this)
                    .setMessage(getString(R.string.add_glucose_dialog_success, glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {addGlucoseLogDialog.dismiss()}
                    .builder()
                    .show()
            }
            is GlucoseLogAddEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.ADD_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.add_glucose_dialog_connect_error, glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is GlucoseLogAddEvent.GlucoseValueFormatError-> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.ADD_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.add_glucose_dialog_value_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is GlucoseLogAddEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.ADD_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.add_glucose_dialog_server_error, glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is GlucoseLogAddEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.ADD_GLU_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.add_glucose_dialog_other_error, glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {addGlucoseLogDialog.dismiss()}
                    .builder()
                    .show()
            }
        }
    }

    private fun glucoseLogDeleteEventHandler(event: BaseEvent) {
        glucoseLogViewModel.isBusy.value = false
        when(event) {
            is GlucoseLogDeleteEvent.Success -> {
                LiveEventBus.get(RecordFragment.LIVE_EVENT_BUS_GLUCOSE_INFO_LIST_UPDATE_STICKY_KEY).post(event.glucoseInfoList)
                updateFromToDate(event.glucoseInfoList)
                glucoseLogAdapter.submitList(event.glucoseInfoList)
                MyCustomDialog(this)
                    .setMessage(getString(R.string.glucose_log_swipe_delete_dialog_success_dialog_message, glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.name, event.deletedGlucoseInfo.value.toString()))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is GlucoseLogDeleteEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.DELETE_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.glucose_log_swipe_delete_dialog_connect_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is GlucoseLogDeleteEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.DELETE_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.glucose_log_swipe_delete_dialog_server_error, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
            is GlucoseLogDeleteEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.DELETE_GLU_TYPE.toString(), event.status.toString()))
                    .setMessage(getString(R.string.glucose_log_swipe_delete_dialog_other_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
        }
    }

    private fun glucoseLogEditEventHandler(event: BaseEvent) {
        when(event) {
            is GlucoseLogEditEvent.Success -> {
                Log.d(TAG, "Edit glucose :${event.editGlucoseInfo?.createTime} is successful")
                LiveEventBus.get(RecordFragment.LIVE_EVENT_BUS_GLUCOSE_INFO_LIST_UPDATE_STICKY_KEY).post(event.glucoseInfoList)
                updateFromToDate(event.glucoseInfoList!!)
                glucoseLogAdapter.submitList(event.glucoseInfoList)
                editGlucoseDialog.dismiss()
            }
            is GlucoseLogEditEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_glucose_dialog_connect_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is GlucoseLogEditEvent.GlucoseValueFormatError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_glucose_dialog_value_format_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is GlucoseLogEditEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_GLU_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_glucose_dialog_server_error, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()

            }
            is GlucoseLogEditEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.EDIT_GLU_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.edit_glucose_dialog_other_error))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {finish()}
                    .builder()
                    .show()
            }
        }
    }
}