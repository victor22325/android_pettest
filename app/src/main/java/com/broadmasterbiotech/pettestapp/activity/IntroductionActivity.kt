package com.broadmasterbiotech.pettestapp.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.IntroductionSyncEvent
import com.broadmasterbiotech.library.viewmodel.IntroductionViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.adapter.NewsListAdapter
import com.broadmasterbiotech.pettestapp.databinding.ActivityIntroductionBinding
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity

class IntroductionActivity: SwipeBackActivity() {

    companion object {
        private val TAG = IntroductionActivity::class.java.simpleName
    }

    private lateinit var introductionBinding: ActivityIntroductionBinding
    private lateinit var introductionViewModel: IntroductionViewModel
    private lateinit var introductionListAdapter: NewsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        introductionBinding = DataBindingUtil.setContentView(this, R.layout.activity_introduction)
        introductionBinding.lifecycleOwner = this
        introductionViewModel = ViewModelProvider(this@IntroductionActivity).get(IntroductionViewModel::class.java)
        introductionBinding.viewModel = introductionViewModel
        introductionViewModel.syncInstructions()
        introductionBinding.introductionAdvocateLogoLayout.pettest_advocateLogo_back_layout.setOnClickListener {
            finish()
        }
        introductionViewModel.syncIntroductionLiveEvent.observe(this@IntroductionActivity, Observer{ event -> event?.let { baseEventHandler(it) }})
        introductionListAdapter = NewsListAdapter(this, introductionListListener)
        introductionBinding.introductionRecyclerView.adapter = introductionListAdapter
    }

    private val introductionListListener = object : NewsListAdapter.NewsInfoAdapterListener {
        override fun onClick(newsInfo: NewsInfo) {
            Log.d(TAG, "introduction listListener title:${newsInfo.title}, subTitle:${newsInfo.subTitle}, date:${newsInfo.date}")
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(Uri.parse(newsInfo.contentUri),"video/*")
            startActivity(Intent.createChooser(intent, null))
        }
    }

    private fun baseEventHandler(event: BaseEvent) {
        introductionViewModel.isProcessing.value = false
        when(event) {
            is IntroductionSyncEvent.Success -> {
                event.introductionList.sort()
                introductionListAdapter.submitList(event.introductionList)
            }
            is IntroductionSyncEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.SYNC_INTRODUCTION.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.sync_introduction_dialog_connect_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
            is IntroductionSyncEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.SYNC_INTRODUCTION.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.sync_introduction_dialog_server_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
            is IntroductionSyncEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.SYNC_INTRODUCTION.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.sync_introduction_dialog_other_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
        }
    }
}