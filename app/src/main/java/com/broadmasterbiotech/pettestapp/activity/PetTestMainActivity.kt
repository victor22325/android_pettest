package com.broadmasterbiotech.pettestapp.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.viewmodel.PetTestViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.adapter.PetTestMainViewPagerAdapter
import com.broadmasterbiotech.pettestapp.adapter.SelectPetDialogAdapter
import com.broadmasterbiotech.pettestapp.databinding.ActivityPettestDrawerLayoutBinding
import com.broadmasterbiotech.pettestapp.dialog.AboutDialog
import com.broadmasterbiotech.pettestapp.dialog.BirthdayDialog
import com.broadmasterbiotech.pettestapp.dialog.SelectPetDialog
import com.bumptech.glide.Glide
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jeremyliao.liveeventbus.LiveEventBus
import kotlinx.android.synthetic.main.navigation_header.view.*


class PetTestMainActivity: AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener{

    companion object {

        private val TAG = PetTestMainActivity::class.java.simpleName

        const val navigation_bottom_record = 0
        const val navigation_bottom_notes = 1
        const val navigation_bottom_reminder = 2
        const val navigation_bottom_blog = 3
        const val navigation_bottom_settings = 4

        const val LIVE_EVENT_BUS_NAVIGATION_AVATAR_PHOTO_UPDATE_STICK_KEY = "LiveEventBusNavigationAvatarPhotoUpdateStickyKey"
        const val LIVE_EVENT_BUS_PET_INFO_LIST_UPDATE_STICKY_KEY = "LiveEventBusPetInfoListUpdateStickyKey"

        const val INTENT_EXTRA_SELECT_PET_DIALOG_TO_CREATE_PET_KEY = "IntentExtraSelectPetDialogToCreatePetKey"
        const val BUNDLE_SELECT_PET_DIALOG_TO_CREATE_PET_VALUE = 20
    }

    private lateinit var activityPetTestDrawerLayoutBinding: ActivityPettestDrawerLayoutBinding

    private lateinit var petTestViewModel: PetTestViewModel

    private lateinit var fragmentManager: FragmentManager

    private lateinit var petTestMainViewPagerAdapter: PetTestMainViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityPetTestDrawerLayoutBinding = DataBindingUtil.setContentView(this, R.layout.activity_pettest_drawer_layout)
        petTestViewModel = ViewModelProvider(this@PetTestMainActivity).get(PetTestViewModel::class.java)
        activityPetTestDrawerLayoutBinding.lifecycleOwner = this
        activityPetTestDrawerLayoutBinding.viewModel = petTestViewModel
        activityPetTestDrawerLayoutBinding.pettestMain.activity = this
        activityPetTestDrawerLayoutBinding.pettestMain.viewModel = petTestViewModel

        activityPetTestDrawerLayoutBinding.pettestMain.pettestBottomNavigationView.itemIconTintList = null
        activityPetTestDrawerLayoutBinding.pettestMain.pettestBottomNavigationView.setOnNavigationItemSelectedListener(this)

        fragmentManager = supportFragmentManager
        petTestMainViewPagerAdapter = PetTestMainViewPagerAdapter(fragmentManager)
        activityPetTestDrawerLayoutBinding.pettestMain.pettestViewPager.adapter = petTestMainViewPagerAdapter
        activityPetTestDrawerLayoutBinding.pettestMain.pettestViewPager.currentItem = navigation_bottom_record
        activityPetTestDrawerLayoutBinding.pettestMain.pettestViewPager.addOnPageChangeListener(mainViewPagerListener)

        setUpNavigation()
        petTestViewModel.getUserPhoto()
        petTestViewModel.userPhotoLiveEvent.observe( this@PetTestMainActivity, Observer { event -> event?.let {
            Glide.with(this@PetTestMainActivity).load(it).into(activityPetTestDrawerLayoutBinding.pettestNavigationView.getHeaderView(0).nav_header_user_image)
        } })

        LiveEventBus.get(LIVE_EVENT_BUS_NAVIGATION_AVATAR_PHOTO_UPDATE_STICK_KEY).observeSticky(this@PetTestMainActivity, Observer { url->
            Glide.with(this@PetTestMainActivity).load(url).into(activityPetTestDrawerLayoutBinding.pettestNavigationView.getHeaderView(0).nav_header_user_image)
        })

        LiveEventBus.get(LIVE_EVENT_BUS_PET_INFO_LIST_UPDATE_STICKY_KEY).observeSticky(this@PetTestMainActivity, Observer { petInfoList->
            if(petInfoList is ArrayList<*>) {
                val list: ArrayList<PetInfo> = petInfoList.filterIsInstance<PetInfo>() as ArrayList<PetInfo>
                petTestViewModel.petTestRepository.petInfoListLiveEvent.value = list
            }
        })

        if(petTestViewModel.checkIsBirthday(getString(R.string.simple_date_format_month_date))) {
            val birthdayDialog = BirthdayDialog(this@PetTestMainActivity, this)
            birthdayDialog.show()
        }
    }

    private fun setUpNavigation() {
        //selectPet
        var petInfoList = ArrayList<PetInfo>()
        if(petTestViewModel.petTestRepository.petInfoListLiveEvent.value!=null) {
            petInfoList = petTestViewModel.petTestRepository.petInfoListLiveEvent.value!!
        }

        val selectPetListener = object: SelectPetDialog.SelectPetListener {
            override fun onClick() {
                val intent = Intent(this@PetTestMainActivity, CreatePetActivity::class.java)
                intent.putExtra(INTENT_EXTRA_SELECT_PET_DIALOG_TO_CREATE_PET_KEY, BUNDLE_SELECT_PET_DIALOG_TO_CREATE_PET_VALUE)
                startActivity(intent)
            }
        }
        val selectPetAdapterListener = object: SelectPetDialogAdapter.SelectPetAdapterListener {
            override fun onClick(petInfo: PetInfo) {
                petTestViewModel.pickPetInfo(petInfo = petInfo)
            }
        }
        val selectPetDialog = SelectPetDialog(this@PetTestMainActivity, petTestViewModel, petInfoList, selectPetListener, selectPetAdapterListener,this)
        petTestViewModel.petTestRepository.petInfoListLiveEvent.observe(this@PetTestMainActivity, Observer { event -> event?.let {
            selectPetDialog.updatePetList(it)
        }})

        activityPetTestDrawerLayoutBinding.pettestNavigationView.getHeaderView(0).nav_header_user_image.setImageDrawable(getDrawable(R.drawable.user))
        activityPetTestDrawerLayoutBinding.pettestNavigationView.getHeaderView(0).nav_header_user_name.text = getDisplayName()
        activityPetTestDrawerLayoutBinding.pettestNavigationView.getHeaderView(0).nav_header_user_email.text = petTestViewModel.mFireBaseAuth.currentUser?.email
        activityPetTestDrawerLayoutBinding.pettestNavigationView.setNavigationItemSelectedListener { menuItem->
            when (menuItem.itemId) {
                R.id.navigation_menu_profile -> startActivity(Intent(this@PetTestMainActivity, ProfileActivity::class.java))
                R.id.navigation_menu_select_pet -> selectPetDialog.show()
                R.id.navigation_menu_introduction -> startActivity(Intent(this@PetTestMainActivity, IntroductionActivity::class.java))
                R.id.navigation_menu_about -> {
                    val aboutDialog = AboutDialog(this, this)
                    aboutDialog.show()
                }
                R.id.navigation_menu_logout -> {
                    finish()
                    startActivity(Intent(this@PetTestMainActivity, LoginActivity::class.java))
                }
                R.id.navigation_menu_exit -> finish()
            }
            activityPetTestDrawerLayoutBinding.pettestDrawerLayout.closeDrawers()
            false
        }
        activityPetTestDrawerLayoutBinding.pettestMain.pettestNavigationBtnLayout.setOnClickListener {
            activityPetTestDrawerLayoutBinding.pettestDrawerLayout.openDrawer(GravityCompat.START)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.d(TAG, "onBackPressed")
        petTestViewModel.logout()
    }

    private fun getDisplayName(): String {
        return if(petTestViewModel.petTestRepository.accountInfo?.lastName.isNullOrEmpty()) {
            if(petTestViewModel.mFireBaseAuth.currentUser?.displayName.isNullOrEmpty()) {
                petTestViewModel.mFireBaseAuth.currentUser?.email!!.trim().split("@")[0]
            } else {
                petTestViewModel.mFireBaseAuth.currentUser?.displayName!!
            }
        } else {
            return petTestViewModel.petTestRepository.accountInfo?.lastName!!
        }
    }

    private val mainViewPagerListener = object : ViewPager.OnPageChangeListener {

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        }

        override fun onPageSelected(position: Int) {
            activityPetTestDrawerLayoutBinding.pettestMain.pettestBottomNavigationView.selectedItemId = when (position) {
                navigation_bottom_record -> R.id.navigation_menu_record
                navigation_bottom_notes -> R.id.navigation_menu_notes
                navigation_bottom_reminder -> R.id.navigation_menu_reminder
                navigation_bottom_blog -> R.id.navigation_menu_blog
                navigation_bottom_settings -> R.id.navigation_menu_setting
                else -> R.id.navigation_menu_record
            }
        }

        override fun onPageScrollStateChanged(state: Int) {
        }
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        activityPetTestDrawerLayoutBinding.pettestMain.pettestViewPager.currentItem = when (menuItem.itemId) {
            R.id.navigation_menu_record -> navigation_bottom_record
            R.id.navigation_menu_notes -> navigation_bottom_notes
            R.id.navigation_menu_reminder -> navigation_bottom_reminder
            R.id.navigation_menu_blog -> navigation_bottom_blog
            R.id.navigation_menu_setting -> navigation_bottom_settings
            else -> navigation_bottom_record
        }
        return true
    }
}