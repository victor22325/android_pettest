package com.broadmasterbiotech.pettestapp.activity

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.ActivityBrowseNewsBinding
import com.broadmasterbiotech.pettestapp.fragment.NewsListFragment
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_pettest_top_bar.view.*
import me.imid.swipebacklayout.lib.app.SwipeBackActivity

class NewsBrowserActivity: SwipeBackActivity() {

    companion object {
        val TAG = NewsBrowserActivity::class.java.simpleName
    }

    private lateinit var activityBrowserNewsBinding: ActivityBrowseNewsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val newsInfo = intent.getParcelableExtra<NewsInfo>(NewsListFragment.INTENT_EXTRA_NEWS_LIST_TO_BROWSER_KEY)
        Log.d(TAG, "newsInfo title:${newsInfo?.title} ,date:${newsInfo?.date}")
        activityBrowserNewsBinding = DataBindingUtil.setContentView(this@NewsBrowserActivity, R.layout.activity_browse_news)
        activityBrowserNewsBinding.lifecycleOwner = this
        activityBrowserNewsBinding.newsInfo = newsInfo
        Glide.with(this@NewsBrowserActivity).load(newsInfo?.contentUri).into(activityBrowserNewsBinding.browseNewsImage)
        activityBrowserNewsBinding.browseNewsTopBarLayout.pettest_advocateLogo_back_layout.setOnClickListener { finish() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}