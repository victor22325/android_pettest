package com.broadmasterbiotech.pettestapp.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.CreateNewsEvent
import com.broadmasterbiotech.library.viewmodel.CreateNewsViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.databinding.ActivityCreateNewsBinding
import com.bumptech.glide.Glide
import com.google.firebase.storage.StorageMetadata
import me.imid.swipebacklayout.lib.app.SwipeBackActivity
import java.io.ByteArrayOutputStream

class CreateNewsActivity: SwipeBackActivity() {

    companion object {
        val TAG = CreateNewsActivity::class.java.simpleName

        const val IMAGE_REQUEST_CODE: Int = 1
        const val RESIZE_REQUEST_CODE: Int = 2
    }

    private lateinit var activityCreateNewsBinding: ActivityCreateNewsBinding
    private lateinit var createNewsViewMode: CreateNewsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityCreateNewsBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_news)
        createNewsViewMode = ViewModelProvider(this@CreateNewsActivity).get(CreateNewsViewModel::class.java)
        activityCreateNewsBinding.lifecycleOwner = this
        activityCreateNewsBinding.activity = this
        activityCreateNewsBinding.viewModel = createNewsViewMode
        activityCreateNewsBinding.createNewsTopBarLayout.setOnClickListener { finish() }
        createNewsViewMode.createNewsLiveEvent.observe(this, Observer { event -> event?.let { baseEventHandler(it) }})
    }

    private fun baseEventHandler(event: BaseEvent) {
        createNewsViewMode.isProcessing.value = false
        when (event) {
            is CreateNewsEvent.Success -> {
                when (event.status) {
                    Code.CREATE_NEWS_REQUEST_SUCCESS -> {
                        MyCustomDialog(this)
                            .setMessage(resources.getString(R.string.create_news_dialog_success))
                            .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                            .builder()
                            .show()
                    }
                    Code.CREATE_NEWS_REQUEST_PHOTO_UPLOAD_SUCCESS -> {
                        createNewsViewMode.checkNewsInfo()
                    }
                }
            }
            is CreateNewsEvent.ConnectError -> {
                MyCustomDialog(this)
                    .setTitle(event.status.toString())
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.create_news_dialog_connect_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
            is CreateNewsEvent.TitleFormatError -> {
                MyCustomDialog(this)
                    .setTitle(event.status.toString())
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.create_news_dialog_title_format_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
            is CreateNewsEvent.DateFormatError -> {
                MyCustomDialog(this)
                    .setTitle(event.status.toString())
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.create_news_dialog_date_format_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
            is CreateNewsEvent.ImageHaveNoData -> {
                MyCustomDialog(this)
                    .setTitle(event.status.toString())
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.create_news_dialog_upload_photo_have_no_data))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
            is CreateNewsEvent.ServerError -> {
                MyCustomDialog(this)
                    .setTitle(event.status.toString())
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.create_news_dialog_server_error, event.message))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) { finish() }
                    .builder()
                    .show()
            }
            is CreateNewsEvent.ImageUploadFail -> {
                MyCustomDialog(this)
                    .setTitle(event.status.toString())
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.create_news_dialog_upload_photo_fail, event.message))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) { finish() }
                    .builder()
                    .show()
            }
            is CreateNewsEvent.ServerSaveFail -> {
                MyCustomDialog(this)
                    .setTitle(event.status.toString())
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.create_news_dialog_upload_photo_fail, event.message))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) { finish() }
                    .builder()
                    .show()
            }
            is CreateNewsEvent.OtherError -> {
                MyCustomDialog(this)
                    .setTitle(event.status.toString())
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.create_news_dialog_other_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) { finish() }
                    .builder()
                    .show()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun cancelOnClick() {
        finish()
    }

    fun pickerPhoto() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        val destIntent = Intent.createChooser(intent, null)
        startActivityForResult(destIntent, IMAGE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode != Activity.RESULT_OK) {
            return
        } else {
            when (requestCode) {
                IMAGE_REQUEST_CODE -> {
                    if(data!=null) {
                        resizeImage(data.data!!)
                    }
                }
                RESIZE_REQUEST_CODE -> {
                    if (data!= null) {
                        Glide.with(this@CreateNewsActivity).load(data.data).into(activityCreateNewsBinding.createNewsPhotoImage)
                        val uri = data.data!!
                        val contentResolver = this.contentResolver
                        val bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(uri))
                        val byteArrayOutputStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
                        val imageData = byteArrayOutputStream.toByteArray()
                        val storageMetadata = StorageMetadata.Builder()
                            .setCustomMetadata("newsInfo", queryFileName(uri))
                            .setContentType("image/jpg")
                            .build()
                        createNewsViewMode.uploadFileName = queryFileName(uri)
                        createNewsViewMode.uploadImageData = imageData
                        createNewsViewMode.uploadMetadata = storageMetadata
                    }
                }
            }
        }
    }

    private fun queryFileName(uri: Uri): String {
        var name = ""
        this.contentResolver.query(uri, null, null, null, null)?.use { cursor->
            if(cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
            }
        }
        return name
    }

    private fun resizeImage(uri: Uri) {
        Log.d(TAG, "resizeImage path:${uri.path}")
        try {
            val intent = Intent("com.android.camera.action.CROP")
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            intent.setDataAndType(uri, "image/*")
            intent.putExtra("crop", "true")
            intent.putExtra("aspectX", 1)
            intent.putExtra("aspectY", 1)
            intent.putExtra("outputX", 500)
            intent.putExtra("outputY", 500)
            intent.putExtra("scale", true)
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG)
            intent.putExtra("noFaceDetection", true)
            intent.putExtra("return-data", false)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            startActivityForResult(intent, RESIZE_REQUEST_CODE)
        } catch (e: Exception) {
            Log.d(TAG, "resizeImage exception:${e.message}")
        }
    }
}