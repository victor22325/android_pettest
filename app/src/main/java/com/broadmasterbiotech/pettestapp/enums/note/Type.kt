package com.broadmasterbiotech.pettestapp.enums.note

enum class Type(val num: Int) {
    Meal(0),
    Exercise(1),
    Note(2),
    Medical(3)
}