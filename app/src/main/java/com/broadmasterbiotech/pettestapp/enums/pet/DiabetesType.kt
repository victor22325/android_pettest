package com.broadmasterbiotech.pettestapp.enums.pet

enum class DiabetesType(val num: Int) {
    NA(0),
    Type1(1),
    Type2(2),
}