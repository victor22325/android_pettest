package com.broadmasterbiotech.pettestapp.enums.reminder

enum class RepeatType(val num: Int) {
    Never(0),
    Everyday(1)
}