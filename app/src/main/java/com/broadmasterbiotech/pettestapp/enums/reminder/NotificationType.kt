package com.broadmasterbiotech.pettestapp.enums.reminder

enum class NotificationType(val num: Int) {
    None(0),
    Notification(1),
    NotificationSound(2)
}