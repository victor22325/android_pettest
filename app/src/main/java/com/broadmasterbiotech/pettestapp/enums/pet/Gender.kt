package com.broadmasterbiotech.pettestapp.enums.pet

enum class Gender(val num: Int) {
    Male(0),
    Female(1)
}