package com.broadmasterbiotech.pettestapp.enums.reminder

enum class Type(val num: Int) {
    Insulin(0),
    Veterinarian(1)
}