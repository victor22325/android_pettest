package com.broadmasterbiotech.pettestapp.enums.pet

enum class Species(val num: Int) {
    Canine(0),
    Feline(1)
}