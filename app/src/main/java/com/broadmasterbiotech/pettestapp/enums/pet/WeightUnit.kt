package com.broadmasterbiotech.pettestapp.enums.pet

enum class WeightUnit(val num: Int) {
    Kilogram(0),
    Libra(1)
}