package com.broadmasterbiotech.pettestapp.enums.reminder

enum class Status(val num: Int) {
    Off(0),
    On(1)
}