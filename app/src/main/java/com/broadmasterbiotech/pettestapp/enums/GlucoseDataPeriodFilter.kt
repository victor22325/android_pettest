package com.broadmasterbiotech.pettestapp.enums

enum class GlucoseDataPeriodFilter(val num: Int) {
    AllData(0),
    OneMonth(1),
    ThreeMonth(2)
}