package com.broadmasterbiotech.pettestapp.enums

enum class SelectPetSwipeOption(val num: Int) {
    Edit(0),
    Delete(1)
}