package com.broadmasterbiotech.pettestapp.object.MyDialog;

public class StringItemBean {

    /**
     * ListView的Item內容
     */
    private String itemStr;

    public StringItemBean(String str){
        this.itemStr = str;
    }

    public String getItemStr() {
        return itemStr;
    }

    public void setItemStr(String itemStr) {
        this.itemStr = itemStr;
    }
}