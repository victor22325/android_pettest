package com.broadmasterbiotech.pettestapp.`object`

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import com.broadmasterbiotech.pettestapp.R
import java.util.*

class ErrorDialogBuilder(private val context: Context) {

    companion object {

        private val TAG = ErrorDialogBuilder::class.java.simpleName
    }

    private var title: String? = null

    private var errorCode: String? = ""

    private var hasTitle: Boolean? = false

    private var message: String? = null

    private var neutralText: String? = null

    private var hasNeutralButton: Boolean? = false

    private var neutralOnClickListener: DialogInterface.OnClickListener? = null

    private var positiveText: String? = null

    private var hasPositiveButton: Boolean? = false

    private var positiveOnClickListener: DialogInterface.OnClickListener? = null

    private var negativeText: String? = null

    private var hasNegativeButton: Boolean? = false

    private var negativeOnClickListener: DialogInterface.OnClickListener? = null

    private var cancelEnable: Boolean? = false

    init {
        title = context.getString(R.string.error_dialog_builder_title)
        message = context.getString(R.string.error_dialog_builder_content)
        neutralText = context.getString(R.string.error_dialog_builder_neutral)
        positiveText = context.getString(R.string.error_dialog_builder_positive)
        negativeText = context.getString(R.string.error_dialog_builder_negative)
    }

    fun create():Dialog {
        val builder = AlertDialog.Builder(context)
        if(hasTitle!!) {
            builder.setTitle(title)
        } else {
            builder.setTitle("")
        }
        if(errorCode?.isNotEmpty()!!) {
            builder.setTitle("$errorCode")
        }
        builder.setMessage("$message")
        if (!hasNeutralButton!! && !hasPositiveButton!! && !hasNegativeButton!!) {
            builder.setNeutralButton(neutralText, neutralOnClickListener)
        } else {
            if (hasPositiveButton!!)
                builder.setPositiveButton(positiveText, positiveOnClickListener)
            if (hasNeutralButton!!)
                builder.setNeutralButton(neutralText, neutralOnClickListener)
            if (hasNegativeButton!!)
                builder.setNegativeButton(negativeText, negativeOnClickListener)
        }
        builder.setCancelable(cancelEnable!!)
        return builder.create()
    }

    fun title(title: String): ErrorDialogBuilder {
        hasTitle = true
        this.title = title
        return this
    }

    fun message(message: String): ErrorDialogBuilder {
        this.message = message
        return this
    }

    fun neutralButton(text: String, onClickListener: DialogInterface.OnClickListener): ErrorDialogBuilder {
        neutralText = text
        neutralOnClickListener = onClickListener
        hasNeutralButton = true
        return this
    }

    fun positiveButton(text: String, onClickListener: DialogInterface.OnClickListener): ErrorDialogBuilder {
        positiveText = text
        positiveOnClickListener = onClickListener
        hasPositiveButton = true
        return this
    }

    fun negativeButton(text: String, onClickListener: DialogInterface.OnClickListener): ErrorDialogBuilder {
        negativeText = text
        negativeOnClickListener = onClickListener
        hasNegativeButton = true
        return this
    }

    fun setNoTitle(): ErrorDialogBuilder {
        hasTitle = false
        return this
    }

    fun errorCode(type: Int, code: Int): ErrorDialogBuilder {
        errorCode = String.format(Locale.US, "%d%d", type, code)
        return this
    }
}