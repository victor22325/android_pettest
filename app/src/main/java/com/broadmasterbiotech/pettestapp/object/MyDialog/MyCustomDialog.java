package com.broadmasterbiotech.pettestapp.object.MyDialog;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.text.TextPaint;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.broadmasterbiotech.pettestapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyCustomDialog {

    private Context mContext;                       //上下文

    private View mDialogLayout;                     //彈窗佈局

    private boolean mCancelable = true;             //返回鍵、窗體外區域是否可點選取消，預設可以

    private View.OnClickListener positiveButton;    //positive區域監聽器
    private View.OnClickListener negativeButton;    //negative區域監聽器

    private AdapterView.OnItemClickListener mItemClickListener;  //item的點選事件

    private Dialog dialog;                          //構建的彈窗

    private int mCustomAnim;                        //自定義動畫

    private boolean mIsShowTitle;                   //是否顯示標題，預設不顯示
    private boolean mIsShowNegativeButton;          //是否顯示negative區域按鈕，預設不顯示
    private boolean mIsShowPositiveButton;          //是否顯示positive區域按鈕，預設不顯示
    private boolean mIsShowListView;                //是否在內容區顯示ListView，預設不顯示
    private boolean mIsHaveCustomAnim;              //是否含有自定義的動畫效果

    private boolean mIsShowBottomTitle;             //是否顯示底部彈窗標題，預設不顯示
    private boolean mIsShowBottomNegativeButton;    //是否顯示底部彈窗的negative區域按鈕，預設不顯示
    private boolean mIsBottomDialog;                //是否是底部彈窗，預設中間彈窗

    private MyCustomAdapter mAdapter;                     //Adapter，設配自定義的資料
    private ArrayList mDataList;         //資料來源，顯示的文字

    public static final String BOTTOM = "BOTTOM";   //底部彈窗標誌

    private String TAG = "MyCustomDialog";

    /**
     * 中間彈窗，建構函式
     *
     * @param context 上下文
     */
    public MyCustomDialog(Context context) {
        this.mContext = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDialogLayout = inflater.inflate(R.layout.my_custom_dialog_layout, null);
    }

    /**
     * 中間彈窗，建構函式，需要傳入自定義動畫的ID，若傳入0，則代表無動畫
     *
     * @param context    上下文
     * @param customAnim 自定義的動畫效果ID
     */
    public MyCustomDialog(Context context, int customAnim) {
        this(context);
        mCustomAnim = customAnim;
        mIsHaveCustomAnim = true;
    }

    /**
     * 底部彈窗，建構函式，需要傳入String型別引數，BOTTOM，才會顯示底部Dialog
     *
     * @param context 上下文
     * @param gravity 位置，String型別，必須是"BOTTOM"才會顯示底部Dialog
     */
    public MyCustomDialog(Context context, String gravity) {
        this.mContext = context;
        if (gravity.equals(BOTTOM)) {
            mIsBottomDialog = true;
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDialogLayout = inflater.inflate(R.layout.my_custom_bottom_dialog_layout, null);
    }

    /**
     * 都不彈窗，建構函式，需要傳入String型別引數，BOTTOM，才會顯示底部Dialog；自定義動畫效果
     *
     * @param context    上下文
     * @param customAnim 自定義的動畫效果
     * @param gravity    位置，String型別，必須是"BOTTOM"才會顯示底部Dialog
     */
    public MyCustomDialog(Context context, int customAnim, String gravity) {
        this.mContext = context;
        if (gravity.equals(BOTTOM)) {
            mIsBottomDialog = true;
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDialogLayout = inflater.inflate(R.layout.my_custom_bottom_dialog_layout, null);
        mCustomAnim = customAnim;
        mIsHaveCustomAnim = true;
    }

    /**
     * 能否返回鍵取消和點選彈窗外部區域取消
     * 中間、底部彈窗共用
     *
     * @param boolean1 true 代表可以取消，false 不可取消
     * @return this
     */
    public MyCustomDialog setCancelable(Boolean boolean1) {
        this.mCancelable = boolean1;
        return this;
    }

    /**
     * 中間彈窗，設定標題 int型 不常用，適配更多型別可過載多個該方法，引數型別不同即可
     *
     * @param title int型引數
     * @return this
     */
    public MyCustomDialog setTitle(int title) {
        mIsShowTitle = true;
        ((TextView) mDialogLayout.findViewById(R.id.title)).setText(title);
        return this;
    }

    /**
     * 中間彈窗，設定標題 String型 最常用
     *
     * @param title String型引數
     * @return this
     */
    public MyCustomDialog setTitle(String title) {
        mIsShowTitle = true;
        ((TextView) mDialogLayout.findViewById(R.id.title)).setText(title);
        return this;
    }

    /**
     * 中間彈窗，設定標題文字大小
     *
     * @param size 大小值，int型
     * @return this
     */
    public MyCustomDialog setTitleSize(int size) {
        ((TextView) mDialogLayout.findViewById(R.id.title)).setTextSize(size);
        return this;
    }

    /**
     * 中間彈窗，設定標題文字顏色
     *
     * @param color 顏色
     * @return this
     */
    public MyCustomDialog setTitleColor(int color) {
        ((TextView) mDialogLayout.findViewById(R.id.title)).setTextColor(mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 中間彈窗，設定標題字型為粗體
     *
     * @return this
     */
    public MyCustomDialog setTitleStyleBold(){
        TextView tv = (TextView)mDialogLayout.findViewById(R.id.title);
        TextPaint tp = tv.getPaint();
        tp.setFakeBoldText(true);
        return this;
    }

    /**
     * 中間彈窗，設定標題區域的背景顏色 不常用
     *
     * @param color 顏色
     * @return this
     */
    public MyCustomDialog setTitleBackgroundColor(int color) {
        mDialogLayout.findViewById(R.id.title_background).setBackgroundColor(mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 中間彈窗，設定內容，int型，不常用，適配更多型別可過載多個該方法，引數型別不同即可
     *
     * @param message int型引數
     * @return this
     */
    public MyCustomDialog setMessage(int message) {
        ((TextView) mDialogLayout.findViewById(R.id.message)).setText(message);
        return this;
    }

    /**
     * 中間彈窗，設定內容，String型，最常用
     *
     * @param message String型資訊
     * @return this
     */
    public MyCustomDialog setMessage(String message) {
        ((TextView) mDialogLayout.findViewById(R.id.message)).setText(message);
        return this;
    }

    /**
     * 中間彈窗，設定內容的文字顏色
     *
     * @param color 文字顏色
     * @return this
     */
    public MyCustomDialog setMessageColor(int color){
        ((TextView) mDialogLayout.findViewById(R.id.message)).setTextColor(
                mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 中間彈窗，設定內容區域的背景色
     *
     * @param color 背景色
     * @return this
     */
    public MyCustomDialog setMessageBackground(int color){
        mDialogLayout.findViewById(R.id.content).setBackgroundColor(
                mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 中間彈窗，設定negative區域的文字和點選事件，一般為"取消"
     * 同AlertDialog.Builder的設定名稱相同
     *
     * @param negativeText 按鈕文字
     * @param listener     監聽器
     * @return this
     */
    public MyCustomDialog setNegativeButton(String negativeText, View.OnClickListener listener) {
        mIsShowNegativeButton = true;
        ((TextView) mDialogLayout.findViewById(R.id.negative)).setText(negativeText);
        this.negativeButton = listener;
        return this;
    }

    /**
     * 中間彈窗，設定negative區域顯示文字的顏色，如藍色的"取消"文字
     * 多數APP如網購APP，在某個商品瀏覽頁面，不希望使用者退出又必須要給出退出提示時，多將negative設定為顯眼的顏色
     * 而positive設定為暗色。所以該方法還是使用比較常見的
     *
     * @param color 顏色
     * @return this
     */
    public MyCustomDialog setNegativeButtonColor(int color) {
        ((TextView) mDialogLayout.findViewById(R.id.negative)).setTextColor(
                mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 中間彈窗，設定negative文字的大小
     *
     * @param size 文字大小
     * @return this
     */
    public MyCustomDialog setNegativeButtonTextSize(int size){
        ((TextView) mDialogLayout.findViewById(R.id.negative)).setTextSize(size);
        return this;
    }

    /**
     * 中間彈窗，設定negative文字字型為粗體
     *
     * @return this
     */
    public MyCustomDialog setNegativeButtonStyleBold(){
        TextView tv = (TextView) mDialogLayout.findViewById(R.id.negative);
        TextPaint tp = tv.getPaint();
        tp.setFakeBoldText(true);
        return this;
    }

    /**
     * 中間彈窗，設定positive區域的文字和點選事件，一般為"確定"
     * 同AlertDialog.Builder的設定名稱相同
     *
     * @param positiveText 按鈕文字
     * @param listener     監聽器
     * @return this
     */
    public MyCustomDialog setPositiveButton(String positiveText, View.OnClickListener listener) {
        mIsShowPositiveButton = true;
        ((TextView) mDialogLayout.findViewById(R.id.positive)).setText(positiveText);
        this.positiveButton = listener;
        return this;
    }

    /**
     * 中間彈窗，設定positive區域顯示文字的顏色，如藍色的"確定"文字
     *
     * @param color 顏色
     * @return this
     */
    public MyCustomDialog setPositiveButtonColor(int color) {
        ((TextView) mDialogLayout.findViewById(R.id.positive)).setTextColor(
                mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 中間彈窗，設定positive區域顯示文字的大小
     *
     * @param size 文字大小
     * @return this
     */
    public MyCustomDialog setPositiveButtonSize(int size){
        ((TextView) mDialogLayout.findViewById(R.id.positive)).setTextSize(size);
        return this;
    }

    /**
     * 中間彈窗，設定positive文字字型為粗體
     *
     * @return this
     */
    public MyCustomDialog setPositiveButtonStyleBold(){
        TextView tv = (TextView) mDialogLayout.findViewById(R.id.positive);
        TextPaint tp = tv.getPaint();
        tp.setFakeBoldText(true);
        return this;
    }

    /**
     * 中間彈窗，重新設定內容的顯示控制元件
     * 預設的Dialog只有一個顯示的TextView，替換顯示控制元件可以實現顯示更豐富的內容，如圖片 + 文字。
     *
     * @param v 要顯示的控制元件
     * @return this
     */
    public MyCustomDialog setView(View v) {
        ((FrameLayout) mDialogLayout.findViewById(R.id.sv)).removeAllViews();
        //進行判斷，否則第二次彈出Dialog時會報異常
        //異常：java.lang.IllegalStateException: The specified child already has a parent.
        //     You must call removeView() on the child's parent first.
        ViewGroup parent = (ViewGroup) v.getParent();
        if (parent != null) {
            parent.removeAllViews();
        }
        ((FrameLayout) mDialogLayout.findViewById(R.id.sv)).addView(v);
        return this;
    }

    /**
     * 設定顯示內容為ListView，傳入要顯示的陣列和監聽事件
     * 顯示預設顏色
     * 中間彈窗和底部彈窗共用
     *
     * @param data     資料來源，陣列，String型別
     * @param listener item的監聽事件，短按型別
     * @return this
     */
    public MyCustomDialog setListView(String[] data, AdapterView.OnItemClickListener listener) {
        setListView(Arrays.asList(data), listener);
        return this;
    }

    /**
     * 設定顯示內容為ListView，傳入要顯示的list和監聽事件
     * 顯示預設顏色
     * 中間彈窗和底部彈窗共用
     *
     * @param list     資料來源，list，String型別
     * @param listener item的監聽事件，短按型別
     * @return this
     */
    public MyCustomDialog setListView(List<String> list, AdapterView.OnItemClickListener listener) {
        mItemClickListener = listener;
        mIsShowListView = true;
        mDataList = new ArrayList<>();
        for (String str : list) {
            mDataList.add(new StringItemBean(str));
        }
        mAdapter = new MyCustomAdapter(mContext, R.layout.my_custom_dialog_string_item_layout, mDataList);
        return this;
    }

    /**
     * 設定顯示內容為ListView，可以設定item的顏色，且可以分別設定
     * 資料來源型別：陣列，陣列
     * 中間彈窗和底部彈窗共用
     *
     * @param data     資料來源，陣列，String型別
     * @param colors   顏色資料來源，陣列，Integer型別
     * @param listener item的監聽事件，短按型別
     * @return this
     */
    public MyCustomDialog setListView(String[] data, Integer[] colors, AdapterView.OnItemClickListener listener) {
        setListView(Arrays.asList(data), Arrays.asList(colors), listener);
        return this;
    }

    /**
     * 設定顯示內容為ListView，可以設定item的顏色，且可以分別設定
     * 資料來源型別：List，陣列
     * 中間彈窗和底部彈窗共用
     *
     * @param list     資料來源，List，String型別
     * @param colors   顏色資料來源，陣列，Integer型別
     * @param listener item的監聽事件，短按型別
     * @return this
     */
    public MyCustomDialog setListView(List<String> list, Integer[] colors, AdapterView.OnItemClickListener listener) {
        setListView(list, Arrays.asList(colors), listener);
        return this;
    }

    /**
     * 設定顯示內容為ListView，可以設定item的顏色，且可以分別設定
     * 資料來源型別：陣列，List
     * 中間彈窗和底部彈窗共用
     *
     * @param data     資料來源，陣列，String型別
     * @param colors   顏色資料來源，List，Integer型別
     * @param listener item的監聽事件，短按型別
     * @return this
     */
    public MyCustomDialog setListView(String[] data, List<Integer> colors, AdapterView.OnItemClickListener listener) {
        setListView(Arrays.asList(data), colors, listener);
        return this;
    }

    /**
     * 設定顯示內容為ListView，可以設定item的顏色，且可以分別設定
     * 資料來源型別：List，List，
     * 不管傳入的資料來源和顏色資料來源的型別是陣列還是List，最後都要使用這個方法進行設定
     * 中間彈窗和底部彈窗共用
     *
     * @param list     資料來源，List，String型別
     * @param colors   顏色資料來源，List，Integer型別
     * @param listener item的監聽事件
     * @return this
     */
    public MyCustomDialog setListView(List<String> list, List<Integer> colors, AdapterView.OnItemClickListener listener) {
        mIsShowListView = true;
        mItemClickListener = listener;
        mDataList = new ArrayList<>();
        for (String str : list) {
            mDataList.add(new StringItemBean(str));
        }
        mAdapter = new MyCustomAdapter(mContext, R.layout.my_custom_dialog_string_item_layout, mDataList, colors);
        return this;
    }

    /**
     * 底部彈窗，重新設定內容的顯示控制元件
     *
     * @param v 要顯示的控制元件
     * @return this
     */
    public MyCustomDialog setBottomView(View v) {
        ((LinearLayout) mDialogLayout.findViewById(R.id.list_content)).removeAllViews();
        ViewGroup parent = (ViewGroup) v.getParent();
        if (parent != null) {
            parent.removeAllViews();
        }
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        int height = wm.getDefaultDisplay().getHeight();
        if (getMsgListViewHeight((ListView) v) > height / 5 * 3){
            //如果List的高度大於螢幕高度的4/5
            LinearLayout.LayoutParams LayoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,height / 5 * 3);
            mDialogLayout.findViewById(R.id.list_content).setLayoutParams(LayoutParams);
        }
        ((LinearLayout) mDialogLayout.findViewById(R.id.list_content)).addView(v);
        return this;
    }

    /**
     * 底部彈窗，設定標題
     *
     * @param title 整型標題
     * @return this
     */
    public MyCustomDialog setBottomTitle(int title) {
        mIsShowBottomTitle = true;
        ((TextView) mDialogLayout.findViewById(R.id.bottom_title)).setText(title);
        return this;
    }

    /**
     * 底部彈窗，設定標題
     *
     * @param title String型標題
     * @return this
     */
    public MyCustomDialog setBottomTitle(String title) {
        mIsShowBottomTitle = true;
        ((TextView) mDialogLayout.findViewById(R.id.bottom_title)).setText(title);
        return this;
    }

    /**
     * 底部彈窗，設定標題文字的顏色
     *
     * @param color 文字顏色
     * @return this
     */
    public MyCustomDialog setBottomTitleColor(int color) {
        ((TextView) mDialogLayout.findViewById(R.id.bottom_title)).setTextColor(
                mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 設定底部彈窗標題文字的文字大小
     *
     * @param size 文字大小
     * @return this
     */
    public MyCustomDialog setBottomTitleSize(int size){
        ((TextView) mDialogLayout.findViewById(R.id.bottom_title)).setTextSize(size);
        return this;
    }

    /**
     * 底部彈窗，設定標題區域的背景色
     *
     * @param color 背景色
     * @return this
     */
    public MyCustomDialog setBottomTitleBackground(int color) {
        mIsShowBottomTitle = true;
        mDialogLayout.findViewById(R.id.bottom_title_content).setBackgroundColor(
                mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 底部彈窗，設定Negative的文字和點選事件，點選事件可為null
     *
     * @param negativeText 文字，如"取消"
     * @param listener     negative區域的監聽事件
     * @return this
     */
    public MyCustomDialog setBottomNegativeButton(String negativeText, View.OnClickListener listener) {
        mIsShowBottomNegativeButton = true;
        ((TextView) mDialogLayout.findViewById(R.id.bottom_negative)).setText(negativeText);
        this.negativeButton = listener;
        return this;
    }

    /**
     * 底部彈窗，設定negative文字的顏色
     *
     * @param color 文字顏色
     * @return this
     */
    public MyCustomDialog setBottomNegativeButtonColor(int color) {
        ((TextView) mDialogLayout.findViewById(R.id.bottom_negative)).setTextColor(
                mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 底部彈窗，設定negative文字的字型大小
     *
     * @param size 文字大小
     * @return this
     */
    public MyCustomDialog setBottomNegativeButtonSize(int size){
        ((TextView) mDialogLayout.findViewById(R.id.bottom_negative)).setTextSize(size);
        return this;
    }

    /**
     * 設定底部彈窗negative文字的字型為粗體
     *
     * @return this
     */
    public MyCustomDialog setBottomNegativeButtomStyleBold(){
        TextView tv = (TextView) mDialogLayout.findViewById(R.id.bottom_negative);
        TextPaint tp = tv.getPaint();
        tp.setFakeBoldText(true);
        return this;
    }

    /**
     * 設定底部彈窗negative區域的背景色
     *
     * @param color 背景色
     * @return this
     */
    public MyCustomDialog setBottomNegativeButtonBackground(int color) {
        mDialogLayout.findViewById(R.id.bottom_negative_content).setBackgroundColor(
                mContext.getResources().getColor(color));
        return this;
    }

    /**
     * 獲取List的總高度
     * @param mMessageCenterLv ListView
     * @return this
     */
    private int getMsgListViewHeight(ListView mMessageCenterLv) {
        //ListView總高度
        int totalHeight = 0;
        ListAdapter listAdapter = mMessageCenterLv.getAdapter();
        if (listAdapter == null) {
            return totalHeight;
        }
        int height = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, mMessageCenterLv);
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mMessageCenterLv.getWidth(), View.MeasureSpec.AT_MOST);
            listItem.measure(desiredWidth, 0);
            height += (listItem.getMeasuredHeight());
            Log.d(TAG, "每項item的高度："+listItem.getMeasuredHeight());
        }
        totalHeight = height + (mMessageCenterLv.getDividerHeight() * (listAdapter.getCount() - 1));
        return totalHeight;
    }

    /**
     * 構建窗體，所有鏈式呼叫都在這裡進行集中整理
     *
     * @return 構建完畢的窗體
     */
    public Dialog builder() {
        dialog = new Dialog(mContext, R.style.MyDialogTheme);
        dialog.setCancelable(mCancelable);
        dialog.addContentView(mDialogLayout, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        //如果是中間彈窗
        if (!mIsBottomDialog) {
            //如果沒有設定Title
            if (!mIsShowTitle) {
                mDialogLayout.findViewById(R.id.title_background).setVisibility(View.GONE);
            }
            //如果設定顯示了ListView
            if (mIsShowListView) {
                ListView listView = new ListView(mContext);
                LinearLayout.LayoutParams listLayoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                listView.setLayoutParams(listLayoutParams);
                listView.setAdapter(mAdapter);
                int list_height = getMsgListViewHeight(listView);   //獲取ListView的高度

                Log.v(TAG, "List的總高度為："+list_height);
                WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
                int height = wm.getDefaultDisplay().getHeight();
                Log.d(TAG, "螢幕高度：" + height);
                if (list_height > height*3/5) {
                    list_height = height*3/5;
                }
                LinearLayout.LayoutParams LayoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, list_height);
                mDialogLayout.findViewById(R.id.sv).setLayoutParams(LayoutParams);

                setView(listView);
                if (mItemClickListener != null) {
                    listView.setOnItemClickListener((parent, view, position, id) -> {
                        mItemClickListener.onItemClick(parent, view, position, id);
                        dialog.dismiss();
                    });
                } else {
                    listView.setOnItemClickListener((parent, view, position, id) -> dialog.dismiss());
                }
            }
            //如果設定了negative區域的按鈕
            if (mIsShowNegativeButton) {
                if (negativeButton != null) {
                    mDialogLayout.findViewById(R.id.negative).setOnClickListener(v -> {
                        negativeButton.onClick(v);
                        dialog.dismiss();
                    });
                } else {
                    mDialogLayout.findViewById(R.id.negative).setOnClickListener(v -> dialog.dismiss());
                }
            } else {
                mDialogLayout.findViewById(R.id.negative).setVisibility(View.GONE);
                mDialogLayout.findViewById(R.id.line3).setVisibility(View.GONE);
            }
            //如果設定了positive區域的按鈕
            if (mIsShowPositiveButton) {
                if (positiveButton != null) {
                    mDialogLayout.findViewById(R.id.positive).setOnClickListener(v -> {
                        positiveButton.onClick(v);
                        dialog.dismiss();
                    });
                } else {
                    mDialogLayout.findViewById(R.id.positive).setOnClickListener(v -> dialog.dismiss());
                }
            } else {
                mDialogLayout.findViewById(R.id.positive).setVisibility(View.GONE);
                mDialogLayout.findViewById(R.id.line3).setVisibility(View.GONE);
            }
            mDialogLayout.findViewById(R.id.negative).setOnClickListener(v -> {
                if (negativeButton != null) {
                    negativeButton.onClick(v);
                }
                dialog.dismiss();
            });
            //如果有自定義的動畫效果傳入，就顯示傳入的動畫效果，否則顯示預設效果，另外：傳入0，無動畫
            if (mIsHaveCustomAnim) {
                if (mCustomAnim != 0) { //設定顯示dialog的顯示動畫
                    dialog.getWindow().setWindowAnimations(mCustomAnim);
                }
            } else {                    //設定預設dialog的顯示動畫
                dialog.getWindow().setWindowAnimations(R.style.DialogInAndOutAnim);
            }
        } else { //是底部彈窗
            //如果沒有設定底部彈窗標題
            if (!mIsShowBottomTitle) {
                mDialogLayout.findViewById(R.id.bottom_title_content).setVisibility(View.GONE);
            }
            //如果設定了顯示ListView
            if (mIsShowListView) {
                ListView listView = new ListView(mContext);
                LinearLayout.LayoutParams listLayoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                listView.setLayoutParams(listLayoutParams);
                listView.setAdapter(mAdapter);
                setBottomView(listView);
                if (mItemClickListener != null) {
                    listView.setOnItemClickListener((parent, view, position, id) -> {
                        mItemClickListener.onItemClick(parent, view, position, id);
                        dialog.dismiss();
                    });
                } else {
                    listView.setOnItemClickListener((parent, view, position, id) -> dialog.dismiss());
                }
            }
            //如果設定了顯示底部Negative按鈕
            if (mIsShowBottomNegativeButton) {
                if (negativeButton != null) {
                    mDialogLayout.findViewById(R.id.bottom_negative_content).setOnClickListener(v -> {
                        negativeButton.onClick(v);
                        dialog.dismiss();
                    });
                } else {
                    mDialogLayout.findViewById(R.id.bottom_negative_content).setOnClickListener(v -> dialog.dismiss());
                }
            } else {
                mDialogLayout.findViewById(R.id.bottom_negative_content).setVisibility(View.GONE);
            }
            //如果有自定義的動畫效果傳入，就顯示傳入的動畫效果，否則顯示預設效果，另外：傳入0，無動畫
            if (mIsHaveCustomAnim) {
                if (mCustomAnim != 0) { //設定顯示底部dialog的顯示動畫
                    dialog.getWindow().setWindowAnimations(mCustomAnim);
                }
            } else {                    //設定預設底部dialog的顯示動畫
                dialog.getWindow().setWindowAnimations(R.style.BottomDialogInAndOutAnim);
            }
            Window dialogWindow = dialog.getWindow();
            dialogWindow.setGravity(Gravity.BOTTOM);
        }

        return dialog;
    }
}
