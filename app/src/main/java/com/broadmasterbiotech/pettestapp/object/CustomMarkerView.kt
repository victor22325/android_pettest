package com.broadmasterbiotech.pettestapp.`object`

import android.content.Context
import android.widget.TextView
import com.broadmasterbiotech.pettestapp.R
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF

class CustomMarkerView(context: Context?, layoutResource: Int, private val valueFormatter: ValueFormatter) : MarkerView(context, layoutResource) {
    private val dateTextView: TextView = findViewById(R.id.linechart_markview_date)
    private val valueTextView: TextView = findViewById(R.id.linechart_markview_value)

    override fun refreshContent(e: Entry, highlight: Highlight) {
        dateTextView.text = valueFormatter.getFormattedValue(e.x)
        valueTextView.text = "value:" + e.y.toInt().toString()
        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }
}