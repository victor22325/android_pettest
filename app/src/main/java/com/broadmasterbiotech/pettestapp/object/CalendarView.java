package com.broadmasterbiotech.pettestapp.object;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.broadmasterbiotech.pettestapp.R;
import com.broadmasterbiotech.pettestapp.util.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarView extends View {

    private static final int NUM_COLUMNS    =   7;

    private static final int NUM_ROWS       =   6;

    private List<String> mOptionalDates = new ArrayList<>();
    private List<String> mGlucoseDates = new ArrayList<>();
    private List<String> mNotesDates = new ArrayList<>();

    private int mBgColor = ContextCompat.getColor(getContext(), R.color.colorNotesCalendarMinorBackground);

    private int mDayNormalColor = ContextCompat.getColor(getContext(), R.color.colorBlack);

    private int mDayOtherMonthColor = ContextCompat.getColor(getContext(), R.color.colorNotesCalendarOtherMonthTextColor);

    private int mDayTodayTextColor = ContextCompat.getColor(getContext(), R.color.colorNotesCalendarTodayTextColor);

    private int mDayTextSize = 14;
    Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);

    private DisplayMetrics mMetrics;
    private Paint mPaint;
    private int mCurYear;
    private int mCurMonth;
    private int mCurDate;

    private int mSelYear;
    private int mSelMonth;
    private int mSelDate;

    private int mTempYear;
    private int mTempMonth;
    private int mTempDate;

    private int mColumnSize;
    private int mRowSize;
    private int[][] mDays = new int[6][7];

    private int mMonthDays;

    private int mWeekNumber;

    private Bitmap mBgOptBitmap;

    private Bitmap mBgNotOptBitmap;

    private Bitmap mBgGluBitmap;
    private Bitmap mBgNotGluBitmap;

    private Bitmap mBgNotesBitmap;
    private Bitmap mBgNotNotesBitmap;

    private Bitmap mBG_None;
    private Bitmap mBG_Not_None;

    public CalendarView(Context context) {
        super(context);
        init();
    }

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        mMetrics = getResources().getDisplayMetrics();

        mPaint = new Paint();

        Calendar calendar = Calendar.getInstance();
        mCurYear    =   calendar.get(Calendar.YEAR);
        mCurMonth   =   calendar.get(Calendar.MONTH);
        mCurDate    =   calendar.get(Calendar.DATE);
        setSelYTD(mCurYear, mCurMonth, mCurDate);
        setTempYTD(mCurYear, mCurMonth, mCurDate);

        mBgOptBitmap      = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bg_course_all);
        mBgNotOptBitmap   = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bg_all);
        mBgGluBitmap      = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bg_course_blood);
        mBgNotGluBitmap   = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bg_blood);
        mBgNotesBitmap    = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bg_course_pencil);
        mBgNotNotesBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bg_pencil);
        mBG_None          = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bg_course_optional);
        mBG_Not_None      = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bg_course_not_optional);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        initSize();

        mPaint.setColor(mBgColor);
        canvas.drawRect(0, 0, getWidth(), getHeight(), mPaint);

        mPaint.setTextSize(mDayTextSize * mMetrics.scaledDensity);
        mPaint.setTypeface(font);

        String dayStr;

        mMonthDays = DateUtils.getMonthDays(mTempYear, mTempMonth);

        mWeekNumber = DateUtils.getFirstDayWeek(mTempYear, mTempMonth);

        for(int day = 0; day < NUM_ROWS * NUM_COLUMNS; day++) {
            int column  =  day % 7;
            int row     =  day / 7;
            String getDate;
            if(day < (mWeekNumber-1)) {
                int previousMonthDays = DateUtils.getMonthDays(mTempYear, mTempMonth-1);
                mDays[row][column] = previousMonthDays - (mWeekNumber-1) + 1 + day;
                dayStr = String.valueOf(previousMonthDays - (mWeekNumber-1) + 1 + day);
                getDate = getTempData(mTempYear, mTempMonth-1, mDays[row][column]);
                mPaint.setColor(mDayOtherMonthColor);
            } else if(day > ((mMonthDays-1) + (mWeekNumber-1))) {
                mDays[row][column] = day - mMonthDays + 1 - (mWeekNumber-1);
                dayStr = String.valueOf(day - mMonthDays + 1 - (mWeekNumber-1));
                getDate = getTempData(mTempYear, mTempMonth+1, mDays[row][column]);
                mPaint.setColor(mDayOtherMonthColor);
            } else {
                mDays[row][column] = day + 1 - (mWeekNumber-1);
                dayStr = String.valueOf(day + 1 - (mWeekNumber-1));
                getDate = getTempData(mTempYear, mTempMonth, mDays[row][column]);

                if(mDays[row][column] == mCurDate && mTempMonth == mCurMonth && mTempYear == mCurYear) {
                    mPaint.setColor(mDayTodayTextColor);
                } else {
                    mPaint.setColor(mDayNormalColor);
                }
            }
            int startX = (int) (mColumnSize * column + (mColumnSize - mPaint.measureText(dayStr)) / 2);
            int startY = (int) (mRowSize * row + mRowSize / 2 - (mPaint.ascent() + mPaint.descent()) / 2);

            if(mOptionalDates.contains(getDate) || mGlucoseDates.contains(getDate)|| mNotesDates.contains(getDate)){
                String selDate = getTempData(mSelYear, mSelMonth, mSelDate);
                if(!selDate.contains(getDate)){
                    if(mGlucoseDates.contains(getDate) && !mNotesDates.contains(getDate)){
                        canvas.drawBitmap(mBgNotGluBitmap, startX - (mBgNotOptBitmap.getWidth() / 3), startY - (mBgNotOptBitmap.getHeight() / 2), mPaint);
                    } else if(!mGlucoseDates.contains(getDate) && mNotesDates.contains(getDate)){
                        canvas.drawBitmap(mBgNotNotesBitmap, startX - (mBgNotOptBitmap.getWidth() / 3), startY - (mBgNotOptBitmap.getHeight() / 2), mPaint);
                    } else {
                        canvas.drawBitmap(mBgNotOptBitmap, startX - (mBgNotOptBitmap.getWidth() / 3), startY - (mBgNotOptBitmap.getHeight() / 2), mPaint);
                    }
                } else{
                    if(mGlucoseDates.contains(getDate) && !mGlucoseDates.contains(getDate)){
                        canvas.drawBitmap(mBgGluBitmap, startX - (mBgNotOptBitmap.getWidth() / 3), startY - (mBgNotOptBitmap.getHeight() / 2), mPaint);
                    } else if(!mGlucoseDates.contains(getDate) && mNotesDates.contains(getDate)){
                        canvas.drawBitmap(mBgNotesBitmap, startX - (mBgNotOptBitmap.getWidth() / 3), startY - (mBgNotOptBitmap.getHeight() / 2), mPaint);
                    } else {
                        canvas.drawBitmap(mBgOptBitmap, startX - (mBgOptBitmap.getWidth() / 3), startY - (mBgOptBitmap.getHeight() / 2), mPaint);
                    }
                }
                canvas.drawText(dayStr, startX, startY, mPaint);
            }
            else{
                String selDate = getTempData(mSelYear, mSelMonth, mSelDate);
                if(!selDate.contains(getDate)){
                    canvas.drawBitmap(mBG_Not_None, startX - (mBgNotOptBitmap.getWidth() / 3), startY - (mBgNotOptBitmap.getHeight() / 2), mPaint);
                } else{
                    canvas.drawBitmap(mBG_None, startX - (mBgNotOptBitmap.getWidth() / 3), startY - (mBgNotOptBitmap.getHeight() / 2), mPaint);
                }
                canvas.drawText(dayStr, startX, startY, mPaint);
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        float upX, upY;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_UP:
                upX = event.getX();
                upY = event.getY();
                int col = (int)Math.floor(upX / 150); //demon 100
                int row = (int)Math.floor(upY / 100); //demon 80
                onClick(col, row);
                break;
        }
        return true;
    }

    public void onClick(int col, int row){
        int position = (row)*NUM_COLUMNS + (col+1);
        if(position < mWeekNumber) {
            int year = mSelYear;
            int month = mSelMonth - 1;
            int date = mDays[row][col];
            if(month < 0) {
                year = year -1;
                month = month + 12;
            }
            setSelYTD(year, month, date);
            setTempYTD(year, month, date);
        } else if(position > mWeekNumber + mMonthDays -1) {
            int year = mSelYear;
            int month = mSelMonth + 1;
            int date = mDays[row][col];
            if(month > 11) {
                year = year + 1;
                month = month % 12;
            }
            setSelYTD(year, month, date);
            setTempYTD(year, month, date);
        } else {
            setSelYTD(mSelYear, mSelMonth, mDays[row][col]);
        }
        if(mListener != null){
            mListener.onClickDateListener(mSelYear, (mSelMonth + 1), mSelDate);
        }
        invalidate();
    }

    public void goSelectToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        mSelYear = calendar.get(Calendar.YEAR);
        mSelMonth = calendar.get(Calendar.MONTH);
        mSelDate = calendar.get(Calendar.DAY_OF_MONTH);
        setSelYTD(mSelYear, mSelMonth, mSelDate);
        setTempYTD(mSelYear, mSelMonth, mSelDate);
        if(mListener != null){
            mListener.onClickDateListener(mSelYear, (mSelMonth + 1), mSelDate);
        }
        invalidate();
    }

    private void initSize() {
        mColumnSize = getWidth() / NUM_COLUMNS;
        mRowSize = getHeight() / NUM_ROWS;
    }

    public void setOptionalDate(List<String> dates){
        this.mOptionalDates = dates;
    }
    public void setGlucoseDate(List<String> dates){
        this.mGlucoseDates = dates;
    }
    public void setNotesDate(List<String> dates){
        this.mNotesDates= dates;
    }
    public void refreshCalendar(){
        invalidate();
    }

    public void setSelYTD(int year, int month, int date){
        this.mSelYear   =   year;
        this.mSelMonth  =   month;
        this.mSelDate   =   date;
    }
    public void setTempYTD(int year, int month, int date){
        this.mTempYear  =   year;
        this.mTempMonth =   month;
        this.mTempDate  =   date;
    }

    public int getSelYear(){
        return mSelYear;
    }
    public int getSelMonth(){
        return mSelMonth;
    }
    public int getSelDate(){
        return mSelDate;
    }

    public void setLastMonth(){
        int year    =   mTempYear;
        int month   =   mTempMonth;
        int day     =   mTempDate;
        if(month == 0){
            year = mTempYear-1;
            month = 11;
        }else if(DateUtils.getMonthDays(year, month) == day){
            month = month-1;
            day = DateUtils.getMonthDays(year, month);
        }else{
            month = month-1;
        }
        setTempYTD(year,month,day);
        invalidate();
    }

    public void setNextMonth(){
        int year    =   mTempYear;
        int month   =   mTempMonth;
        int day     =   mTempDate;
        if(month == 11){
            year = mTempYear+1;
            month = 0;
        }else if(DateUtils.getMonthDays(year, month) == day){
            month = month + 1;
            day = DateUtils.getMonthDays(year, month);
        }else{
            month = month + 1;
        }
        setTempYTD(year,month,day);
        invalidate();
    }

    public void setSelectMonth(int year ,int month){
        int day  = mTempDate;
        setSelYTD(year,month,day);
        invalidate();
    }

    public String getDate(){
        String data;
        if((mSelMonth + 1) < 10){
            data = mSelYear + "-0" + (mSelMonth + 1);
        }else{
            data = mSelYear + "-" + (mSelMonth + 1);
        }
        return data;
    }

    public String getDateShow(){
        String data,month="";
        String[] selectMonth=new String[]{"January","February","March","April","May","June","July","August","September","October","November","December"};
        for(int i=0;i<mTempMonth+1;i++){
            if(mTempMonth==i){
                month=selectMonth[i];
            }
        }
        data =month+" "+mTempYear;
        return data;
    }

    private String getTempData(int year, int month, int date){
        String monty, day;
        month = (month + 1);

        if(month > 12) {
            month = month % 12;
            year = year + 1;
        }
        if(month == 0) {
            month = 12;
            year = year -1;
        }

        if((month) < 10) {
            monty = "0" + month;
        }else{
            monty = String.valueOf(month);
        }

        if((date) < 10){
            day = "0" + (date);
        }else{
            day = String.valueOf(date);
        }
        return year + "/" + monty + "/" + day;
    }

    private OnClickListener mListener;

    public interface OnClickListener{
        void onClickDateListener(int year, int month, int day);
    }

    public void setOnClickDate(OnClickListener listener){
        this.mListener = listener;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        recyclerBitmap(mBgOptBitmap);
        recyclerBitmap(mBgNotOptBitmap);
    }

    private void recyclerBitmap(Bitmap bitmap) {
        if(bitmap != null && !bitmap.isRecycled()){
            bitmap.recycle();
        }
    }
}
