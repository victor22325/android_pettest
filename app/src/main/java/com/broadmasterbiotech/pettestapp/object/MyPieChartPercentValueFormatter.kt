package com.broadmasterbiotech.pettestapp.`object`

import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.DecimalFormat

class MyPieChartPercentValueFormatter : ValueFormatter() {

    private val format = DecimalFormat("###,###,##0.0")

    override fun getFormattedValue(value: Float): String {
        return format.format(value) + " %"
    }
}