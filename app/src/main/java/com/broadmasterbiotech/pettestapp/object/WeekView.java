package com.broadmasterbiotech.pettestapp.object;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import androidx.core.content.ContextCompat;
import com.broadmasterbiotech.pettestapp.R;

public class WeekView extends View {

    private String[] mWeeks = {"Sum", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

    private int mWeekTextSize = 14;

    private int mWeekTextColor = Color.WHITE;

    private int mWeekBgColor = ContextCompat.getColor(getContext(), R.color.colorNotesCalendarMainBackground);

    Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
    private DisplayMetrics mMetrics;
    private final Paint mPaint;

    public WeekView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mMetrics = getResources().getDisplayMetrics();

        mPaint = new Paint();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);

        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        if (heightMode == MeasureSpec.AT_MOST) {
            heightSize = mMetrics.densityDpi * 30;
        }
        if (widthMode == MeasureSpec.AT_MOST) {
            widthSize = mMetrics.densityDpi * 300;
        }
        setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        mPaint.setColor(mWeekBgColor);
        canvas.drawRect(0, 0, width, height, mPaint);

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextSize(mWeekTextSize * mMetrics.scaledDensity);
        mPaint.setColor(mWeekTextColor);
        mPaint.setTypeface(font);
        int columnWidth = width / 7;
        for(int i = 0; i < mWeeks.length; i++){
            String text = mWeeks[i];
            int fontWidth = (int) mPaint.measureText(text);
            int startX = columnWidth * i + (columnWidth - fontWidth) / 2;
            int startY = (int) (height / 2 - (mPaint.ascent() + mPaint.descent()) / 2);
            canvas.drawText(text, startX, startY, mPaint);
        }
    }

    public void setWeeks(String[] weeks){
        this.mWeeks = weeks;
    }

    public void setWeekTextSize(int size){
        this.mWeekTextSize = size;
    }

    public void setWeekBgColor(int color){
        this.mWeekBgColor = color;
    }

    public void setWeekTextColor(int color){
        this.mWeekTextColor = color;
    }
}