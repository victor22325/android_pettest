package com.broadmasterbiotech.pettestapp.object.MyDialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.broadmasterbiotech.pettestapp.R;

import java.util.List;

public class MyCustomAdapter extends ArrayAdapter<StringItemBean> {

    private int mResourceId;            //資源ID，佈局檔案
    private List<Integer> mColorsList;  //顏色list
    private boolean mIsHaveColor;       //是否傳入了顏色

    private Context mContext;           //上下文

    public MyCustomAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<StringItemBean> objects) {
        super(context, resource, objects);
        mResourceId = resource;
        mContext = context;
    }

    public MyCustomAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<StringItemBean> objects, List<Integer> colors) {
        super(context, resource, objects);
        mResourceId = resource;
        mColorsList = colors;
        mContext = context;
        mIsHaveColor = true;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //獲取當前item的例項
        StringItemBean stringItemBean = getItem(position);

        View view;
        ViewHolder viewHolder;

        if (convertView == null) {    //快取為空
            //載入佈局
            view = LayoutInflater.from(getContext()).inflate(mResourceId, parent, false);
            //只有在快取為空的情況下，才會建立ViewHolder例項
            viewHolder = new ViewHolder();
            //關聯控制元件
            viewHolder.item = (TextView) view.findViewById(R.id.my_custom_list_item);
            //將ViewHolder儲存到view中
            view.setTag(viewHolder);
        } else {                    //快取不為空
            view = convertView;
            //重新獲取ViewHolder
            viewHolder = (ViewHolder) view.getTag();
        }

        //設定顯示內容
        if (mIsHaveColor){
            viewHolder.item.setText(stringItemBean.getItemStr());
            viewHolder.item.setTextColor(mContext.getResources().getColor(mColorsList.get(position)));
        }else{
            viewHolder.item.setText(stringItemBean.getItemStr());
        }

        return view;
    }

    //內部類ViewHolder
    private class ViewHolder {
        TextView item;
    }
}