package com.broadmasterbiotech.pettestapp.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.NewsListSyncEvent
import com.broadmasterbiotech.library.viewmodel.NewsViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.activity.NewsBrowserActivity
import com.broadmasterbiotech.pettestapp.adapter.NewsListAdapter
import com.broadmasterbiotech.pettestapp.databinding.FragmentNewsListBinding

class NewsListFragment: BaseFragment() {

    companion object {

        private val TAG = NewsListFragment::class.java.simpleName

        fun newInstance(): NewsListFragment {
            return NewsListFragment()
        }

        const val INTENT_EXTRA_NEWS_LIST_TO_BROWSER_KEY = "IntentExtraNewsListToBrowserKey"
    }

    private lateinit var fragmentNewsListBinding: FragmentNewsListBinding
    private lateinit var newsViewModel: NewsViewModel
    private lateinit var newsListAdapter: NewsListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentNewsListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_list, null, false)
        newsViewModel = ViewModelProvider(this).get(NewsViewModel::class.java)
        fragmentNewsListBinding.lifecycleOwner = this
        fragmentNewsListBinding.activity = this
        fragmentNewsListBinding.viewModel = newsViewModel
        newsListAdapter = NewsListAdapter(activity!!, newListListener)
        newsViewModel.syncCurrentNews()
        newsViewModel.syncNewsLiveEvent.observe(viewLifecycleOwner, Observer {event -> event?.let { baseEventHandler(it) }})
        newsViewModel.petTestRepository.newsInfoListLiveEvent.observe(viewLifecycleOwner , Observer { list->
            list.sort()
            newsListAdapter.submitList(list)
        })
        fragmentNewsListBinding.newsFragmentRecyclerView.adapter = newsListAdapter
        return fragmentNewsListBinding.root
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    private val newListListener = object : NewsListAdapter.NewsInfoAdapterListener {
        override fun onClick(newsInfo: NewsInfo) {
            Log.d(TAG, "newsInfo title:${newsInfo.title}, subTitle:${newsInfo.subTitle}, date:${newsInfo.date}")
            val intent = Intent(activity, NewsBrowserActivity::class.java)
            intent.putExtra(INTENT_EXTRA_NEWS_LIST_TO_BROWSER_KEY, newsInfo)
            activity?.startActivity(intent)
        }
    }

    private fun baseEventHandler(event: BaseEvent) {
        newsViewModel.isProcessing.value = false
        when(event) {
            is NewsListSyncEvent.Success -> {
                event.newsInfoList.sort()
                newsListAdapter.submitList(event.newsInfoList)
            }
            is NewsListSyncEvent.ConnectError -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.SYNC_NEW.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.sync_news_list_connect_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
            is NewsListSyncEvent.ServerError -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.SYNC_NEW.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.sync_news_list_server_error, event.message))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) {}
                    .builder()
                    .show()
            }
            is NewsListSyncEvent.OtherError -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.SYNC_NEW.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(resources.getString(R.string.sync_news_list_other_error))
                    .setPositiveButton(resources.getString(R.string.my_custom_dialog_ok_text)) { activity?.finish() }
                    .builder()
                    .show()
            }
        }
    }
}