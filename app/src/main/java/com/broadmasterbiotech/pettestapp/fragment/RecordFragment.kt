package com.broadmasterbiotech.pettestapp.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.viewmodel.RecordViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.CustomMarkerView
import com.broadmasterbiotech.pettestapp.`object`.MyPieChartPercentValueFormatter
import com.broadmasterbiotech.pettestapp.activity.GlucoseLogActivity
import com.broadmasterbiotech.pettestapp.databinding.FragmentRecordBinding
import com.broadmasterbiotech.pettestapp.enums.pet.Species
import com.bumptech.glide.Glide
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.jeremyliao.liveeventbus.LiveEventBus
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RecordFragment: BaseFragment() {

    companion object {

        private val TAG = RecordFragment::class.java.simpleName

        fun newInstance(): RecordFragment {
            return RecordFragment()
        }

        const val LIVE_EVENT_BUS_PET_INFO_UPDATE_STICKY_KEY = "LiveEventBusPetInfoUpdateStickyKey"
        const val LIVE_EVENT_BUS_GLUCOSE_INFO_LIST_UPDATE_STICKY_KEY = "LiveEventBusGlucoseInfoListUpdateStickyKey"
    }

    private lateinit var fragmentRecordBinding: FragmentRecordBinding

    private lateinit var recordViewModel: RecordViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fragmentRecordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_record, null, false)
        fragmentRecordBinding.lifecycleOwner = this
        fragmentRecordBinding.activity = this
        recordViewModel = ViewModelProvider(this@RecordFragment).get(RecordViewModel::class.java)
        fragmentRecordBinding.viewModel = recordViewModel
        fragmentRecordBinding.petInfo = recordViewModel.petTestRepository.petInfoLiveEvent.value
        fragmentRecordBinding.age = getAge(recordViewModel.petTestRepository.petInfoLiveEvent.value?.birth)
        if(recordViewModel.petTestRepository.petInfoLiveEvent.value!=null) {
            updatePetPhoto(recordViewModel.petTestRepository.petInfoLiveEvent.value!!)
        }
        recordViewModel.petTestRepository.petInfoLiveEvent.observe(viewLifecycleOwner, Observer { petInfo->
            fragmentRecordBinding.petInfo = petInfo
            fragmentRecordBinding.age = getAge(petInfo.birth)
            updatePetPhoto(petInfo)
        })

        LiveEventBus.get(LIVE_EVENT_BUS_PET_INFO_UPDATE_STICKY_KEY).observeSticky(this@RecordFragment, Observer { petInfo->
            if(petInfo is PetInfo) {
                fragmentRecordBinding.petInfo = petInfo
                fragmentRecordBinding.age = getAge(petInfo.birth)
                Glide.with(this@RecordFragment).load(petInfo.avatarPhoto).into(fragmentRecordBinding.petAvatarImage)
                if(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!.isNotEmpty()) {
                    fragmentRecordBinding.recordLineChart.axisLeft.removeAllLimitLines()
                    fragmentRecordBinding.recordLineChart.axisLeft.addLimitLine(getUpperLimitLine(petInfo))
                    fragmentRecordBinding.recordLineChart.axisLeft.addLimitLine(getLowerLimitLine(petInfo))

                    val pieData = getPieData(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!, petInfo)
                    pieData.setDrawValues(true)
                    pieData.setValueFormatter(MyPieChartPercentValueFormatter())
                    pieData.setValueTextSize(16f)
                    setPieChart(fragmentRecordBinding.recordPieChart, pieData)
                }
            }
        })

        LiveEventBus.get(LIVE_EVENT_BUS_GLUCOSE_INFO_LIST_UPDATE_STICKY_KEY).observeSticky(this@RecordFragment, Observer { glucoseInfoList->
            if(glucoseInfoList is ArrayList<*>) {
                val list: ArrayList<GlucoseInfo> = glucoseInfoList.filterIsInstance<GlucoseInfo>() as ArrayList<GlucoseInfo>
                updateChart(recordViewModel.filterGlucoseDataPeriod(list))
            }
        })

        recordViewModel.petTestRepository.glucoseListLiveEvent.observe(viewLifecycleOwner, Observer { glucoseInfoList ->
            updateChart(recordViewModel.filterGlucoseDataPeriod(glucoseInfoList))
        })

        if(recordViewModel.petTestRepository.glucoseListLiveEvent.value!=null) {
            if(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!.size > 0) {
                updateChart(recordViewModel.filterGlucoseDataPeriod(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!))
            }
        }
        return fragmentRecordBinding.root
    }

    private fun getAge(birth: String?): String {
        if(birth == null) {
            return ""
        } else {
            if (birth.isEmpty()) {
                return ""
            }
            val birthCalendar = Calendar.getInstance()
            birthCalendar.time = SimpleDateFormat("yyyy/MM/dd", Locale.US).parse(birth)!!
            val nowCalendar = Calendar.getInstance()
            nowCalendar.time = Date(System.currentTimeMillis())
            if(birthCalendar.after(nowCalendar)) {
                return "Can't be born in the future"
            }
            var age = nowCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR)
            if(nowCalendar.get(Calendar.DAY_OF_YEAR) < birthCalendar.get(Calendar.DAY_OF_YEAR)) {
                age -= 1
            }
            return age.toString()
        }
    }

    private fun updatePetPhoto(petInfo: PetInfo) {
        if(petInfo.avatarPhoto!!.isNotEmpty()) {
            Glide.with(this@RecordFragment).load(petInfo.avatarPhoto).into(fragmentRecordBinding.petAvatarImage)
        } else {
            if(petInfo.species == Species.Canine.num) {
                Glide.with(this@RecordFragment).load(R.drawable.dog_list).into(fragmentRecordBinding.petAvatarImage)
            } else {
                Glide.with(this@RecordFragment).load(R.drawable.cat_list).into(fragmentRecordBinding.petAvatarImage)
            }
        }
    }

    private fun updateChart(glucoseInfoList: ArrayList<GlucoseInfo>) {
        glucoseInfoList.sort()
        fragmentRecordBinding.recordLineChart.clear()
        fragmentRecordBinding.dataCount = glucoseInfoList.size
        if(glucoseInfoList.isNotEmpty()) {

            fragmentRecordBinding.recordFromToDateText.text = resources.getString(R.string.record_blood_period,
                                                              SimpleDateFormat(getString(R.string.simple_date_format_month_date_year), Locale.US).format(getEarliestCalendar(glucoseInfoList).timeInMillis),
                                                              SimpleDateFormat(getString(R.string.simple_date_format_month_date_year), Locale.US).format(getLatestCalendar(glucoseInfoList).timeInMillis))

            //line
            setLineChart(fragmentRecordBinding.recordLineChart, glucoseInfoList)

            //pie
            val pieData = getPieData(glucoseInfoList, recordViewModel.petTestRepository.petInfoLiveEvent.value!!)
            pieData.setDrawValues(true)
            pieData.setValueFormatter(MyPieChartPercentValueFormatter())
            pieData.setValueTextSize(16f)
            setPieChart(fragmentRecordBinding.recordPieChart, pieData)
        }
    }

    fun createLogActivity() {
        val intent = Intent(activity, GlucoseLogActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    private fun getEarliestCalendar(glucoseInfoList: ArrayList<GlucoseInfo>): Calendar {
        val earliestCalendar = Calendar.getInstance()
        earliestCalendar.set(Calendar.YEAR, glucoseInfoList[0].year.toInt())
        earliestCalendar.set(Calendar.MONTH, glucoseInfoList[0].month.toInt()-1)
        earliestCalendar.set(Calendar.DAY_OF_MONTH, glucoseInfoList[0].day.toInt())
        earliestCalendar.set(Calendar.HOUR_OF_DAY, glucoseInfoList[0].hour.toInt())
        earliestCalendar.set(Calendar.MINUTE, glucoseInfoList[0].minute.toInt())
        return earliestCalendar
    }

    private fun getLatestCalendar(glucoseInfoList: ArrayList<GlucoseInfo>): Calendar {
        val latestCalendar = Calendar.getInstance()
        latestCalendar.set(Calendar.YEAR, glucoseInfoList[glucoseInfoList.size-1].year.toInt())
        latestCalendar.set(Calendar.MONTH, glucoseInfoList[glucoseInfoList.size-1].month.toInt()-1)
        latestCalendar.set(Calendar.DAY_OF_MONTH, glucoseInfoList[glucoseInfoList.size-1].day.toInt())
        latestCalendar.set(Calendar.HOUR_OF_DAY, glucoseInfoList[glucoseInfoList.size-1].hour.toInt())
        latestCalendar.set(Calendar.MINUTE, glucoseInfoList[glucoseInfoList.size-1].minute.toInt())
        return latestCalendar
    }

    private fun getLineData(glucoseInfoList: ArrayList<GlucoseInfo>): LineData {

        val aMinuteInMilliSecond = 60 * 1000

        val chartData = ArrayList<Entry>()
        for(i in 0 until glucoseInfoList.size) {
            val tempCalendar = Calendar.getInstance()
            tempCalendar.set(Calendar.YEAR, glucoseInfoList[i].year.toInt())
            tempCalendar.set(Calendar.MONTH, glucoseInfoList[i].month.toInt()-1)
            tempCalendar.set(Calendar.DAY_OF_MONTH, glucoseInfoList[i].day.toInt())
            tempCalendar.set(Calendar.HOUR_OF_DAY, glucoseInfoList[i].hour.toInt())
            tempCalendar.set(Calendar.MINUTE, glucoseInfoList[i].minute.toInt())
            val diff = (tempCalendar.timeInMillis - getEarliestCalendar(glucoseInfoList).timeInMillis)/aMinuteInMilliSecond.toFloat()
            chartData.add(Entry(diff, glucoseInfoList[i].value.toFloat()))
        }
        val lineDataSet = LineDataSet(chartData, resources.getString(R.string.add_glucose_blood_value_hint))

        lineDataSet.color = ContextCompat.getColor(context!!, R.color.colorRecordGlucoseLineChartMainColor)
        lineDataSet.lineWidth = 2.75f
        lineDataSet.circleRadius = 3f
        lineDataSet.circleColors = getCircleColorList(glucoseInfoList)
        lineDataSet.setDrawCircleHole(false)
        lineDataSet.valueTextSize = 12f
        lineDataSet.setDrawFilled(true)
        //line chart label high
        lineDataSet.formLineWidth = 4f
        //line chart label width
        lineDataSet.formSize = 30f
        lineDataSet.mode = LineDataSet.Mode.LINEAR
        if(glucoseInfoList.size > 7) {
            lineDataSet.setDrawValues(false)
        }
        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(lineDataSet)
        return LineData(dataSets)
    }

    private fun getCircleColorList(glucoseInfoList: ArrayList<GlucoseInfo>): List<Int> {
        val list = ArrayList<Int>()
        for(i in 0..glucoseInfoList.size) {
            list.add(ContextCompat.getColor(context!!, R.color.colorRecordGlucoseLineChartMainColor))
        }
        return list
    }

    private fun setLineChart(lineChart: LineChart, glucoseInfoList: ArrayList<GlucoseInfo>) {
        lineChart.description.isEnabled = false
        val xAxis = lineChart.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.axisMinimum = 0f
        xAxis.granularity = 1f
        xAxis.valueFormatter = object: ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                val labelCalendar = getEarliestCalendar(glucoseInfoList)
                labelCalendar.add(Calendar.MINUTE, value.toInt())
                return SimpleDateFormat(getString(R.string.simple_date_format_month_date), Locale.US).format(labelCalendar.timeInMillis)
            }
        }

        val leftYAxis = lineChart.axisLeft
        leftYAxis.addLimitLine(getUpperLimitLine(recordViewModel.petTestRepository.petInfoLiveEvent.value!!))
        leftYAxis.addLimitLine(getLowerLimitLine(recordViewModel.petTestRepository.petInfoLiveEvent.value!!))
        val rightYAxis = lineChart.axisRight
        leftYAxis.axisMinimum = 0f
        rightYAxis.axisMinimum = 0f

        val legend = lineChart.legend
        legend.form = Legend.LegendForm.LINE
        legend.textSize = 20f
        legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT

        val lineCharMarkerView = CustomMarkerView(context!!, R.layout.linechart_markview_layout,  xAxis.valueFormatter)
        lineChart.marker = lineCharMarkerView

        lineChart.data = getLineData(glucoseInfoList)
        lineChart.notifyDataSetChanged()
        lineChart.invalidate()
    }

    private fun getUpperLimitLine(petInfo: PetInfo): LimitLine {
        val upperLimitLine = LimitLine(petInfo.glucoseUpper.toFloat(), resources.getString(R.string.create_pet_glucose_limit_upper))
        upperLimitLine.lineColor = ContextCompat.getColor(context!!, R.color.colorRecordGlucoseLineChartUpperLineColor)
        upperLimitLine.textColor = ContextCompat.getColor(context!!, R.color.colorRecordGlucoseLineChartUpperLineColor)
        upperLimitLine.enableDashedLine(2f,3f,0f)
        return upperLimitLine
    }

    private fun getLowerLimitLine(petInfo: PetInfo): LimitLine {
        val lowerLimitLine = LimitLine(petInfo.glucoseLower.toFloat(), resources.getString(R.string.create_pet_glucose_limit_lower))
        lowerLimitLine.lineColor = ContextCompat.getColor(context!!, R.color.colorRecordGlucoseLineChartLowerLineColor)
        lowerLimitLine.textColor = ContextCompat.getColor(context!!, R.color.colorRecordGlucoseLineChartLowerLineColor)
        lowerLimitLine.enableDashedLine(2f,3f,0f)
        return lowerLimitLine
    }

    private fun getPieData(glucoseInfoList: ArrayList<GlucoseInfo>, petInfo: PetInfo): PieData {
        val chartData = ArrayList<PieEntry>()
        var belowCount = 0f
        var withInCount = 0f
        var aboveCount = 0f
        for(i in 0 until glucoseInfoList.size) {
            when {
                glucoseInfoList[i].value < petInfo.glucoseLower.toInt() -> {
                    belowCount++
                }
                glucoseInfoList[i].value > petInfo.glucoseUpper.toInt() -> {
                    aboveCount++
                }
                else -> {
                    withInCount++
                }
            }
        }

        val colorList = ArrayList<Int>()
        if(((belowCount/glucoseInfoList.size)*100).toDouble().roundTo1DecimalPlaces().toFloat() > 0) {
            chartData.add(PieEntry(((belowCount/glucoseInfoList.size)*100).toDouble().roundTo1DecimalPlaces().toFloat(), resources.getString(R.string.record_pie_chart_below_text)))
            colorList.add(ContextCompat.getColor(context!!,R.color.colorRecordGlucosePieChartLowerColor))
        }
        if(((withInCount/glucoseInfoList.size)*100).toDouble().roundTo1DecimalPlaces().toFloat() > 0) {
            chartData.add(PieEntry(((withInCount/glucoseInfoList.size)*100).toDouble().roundTo1DecimalPlaces().toFloat(), resources.getString(R.string.record_pie_chart_within_text)))
            colorList.add(ContextCompat.getColor(context!!,R.color.colorRecordGlucosePieChartWithInColor))
        }
        if(((aboveCount/glucoseInfoList.size)*100).toDouble().roundTo1DecimalPlaces().toFloat() > 0) {
            chartData.add(PieEntry(((aboveCount/glucoseInfoList.size)*100).toDouble().roundTo1DecimalPlaces().toFloat(), resources.getString(R.string.record_pie_chart_above_text)))
            colorList.add(ContextCompat.getColor(context!!,R.color.colorRecordGlucosePieChartUpperColor))
        }
        val pieDataSet = PieDataSet(chartData, resources.getString(R.string.record_pie_chart_label_text))
        pieDataSet.colors = colorList
        return PieData(pieDataSet)
    }

    private fun setPieChart(pieChart: PieChart, pieData: PieData) {
        pieChart.holeRadius = 0f
        pieChart.transparentCircleRadius = 0f
        pieChart.description.isEnabled = false
        val legend = pieChart.legend
        legend.isEnabled = true
        legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        legend.orientation = Legend.LegendOrientation.HORIZONTAL
        legend.xEntrySpace = 12f
        legend.formToTextSpace = 5f
        legend.form = Legend.LegendForm.SQUARE
        legend.formSize = 16f
        legend.textSize = 12f
        legend.isWordWrapEnabled = true
        pieChart.setUsePercentValues(true)
        pieChart.data = pieData
        pieChart.notifyDataSetChanged()
        pieChart.invalidate()
    }

    private fun Double.roundTo1DecimalPlaces() = BigDecimal(this).setScale(1, BigDecimal.ROUND_HALF_UP).toDouble()

    fun allDataOnClick(period: Int) {
        recordViewModel.currentGlucoseDataPeriod.value = period
        if(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!.isNotEmpty()) {
            updateChart(recordViewModel.filterGlucoseDataPeriod(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!))
        }
    }

    fun oneMonthOnClick(period: Int) {
        recordViewModel.currentGlucoseDataPeriod.value = period
        if(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!.isNotEmpty()) {
            updateChart(recordViewModel.filterGlucoseDataPeriod(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!))
        }
    }

    fun threeMonthOnClick(period: Int) {
        recordViewModel.currentGlucoseDataPeriod.value = period
        if(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!.isNotEmpty()) {
            updateChart(recordViewModel.filterGlucoseDataPeriod(recordViewModel.petTestRepository.glucoseListLiveEvent.value!!))
        }
    }
}