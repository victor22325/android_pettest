package com.broadmasterbiotech.pettestapp.fragment

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.database.reminder.ReminderInfo
import com.broadmasterbiotech.library.receiver.ReminderReceiver
import com.broadmasterbiotech.library.viewmodel.ReminderViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.activity.CreateReminderActivity
import com.broadmasterbiotech.pettestapp.activity.EditReminderActivity
import com.broadmasterbiotech.pettestapp.adapter.ReminderListAdapter
import com.broadmasterbiotech.pettestapp.databinding.FragmentReminderListBinding
import com.yanzhenjie.recyclerview.OnItemMenuClickListener
import com.yanzhenjie.recyclerview.SwipeMenuCreator
import com.yanzhenjie.recyclerview.SwipeMenuItem
import java.util.*
import kotlin.collections.ArrayList

class ReminderListFragment: BaseFragment() {

    companion object {

        private val TAG = ReminderListFragment::class.java.simpleName

        fun newInstance(): ReminderListFragment {
            return ReminderListFragment()
        }

        const val INTENT_EXTRA_CREATE_REMINDER_TYPE_KEY = "IntentExtraCreateReminderTypeKey"
        const val INTENT_EXTRA_EDIT_REMINDER_INFO_KEY = "IntentExtraEditReminderInfoKey"
        const val CREATE_INSULIN_REMINDER_FLAG = 0
        const val CREATE_VETERINARIAN_REMINDER_FLAG = 1
    }

    private lateinit var fragmentReminderListBinding: FragmentReminderListBinding
    private lateinit var lifecycleOwner: LifecycleOwner
    private lateinit var reminderViewModel: ReminderViewModel
    private lateinit var reminderInfoList: ArrayList<ReminderInfo>
    private var reminderListAdapter: ReminderListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentReminderListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reminder_list, null, false)
        fragmentReminderListBinding.lifecycleOwner = this
        fragmentReminderListBinding.activity = this
        fragmentReminderListBinding.reminderSwipeRefreshLayout.setOnRefreshListener {
            fragmentReminderListBinding.reminderSwipeRefreshLayout.isRefreshing = false
            reminderViewModel.getCurrentReminderList()
        }
        lifecycleOwner = this
        reminderViewModel = ViewModelProvider(this@ReminderListFragment).get(ReminderViewModel::class.java)
        reminderViewModel.getCurrentReminderList()
        reminderViewModel.petTestRepository.reminderInfoListLiveEvent.observe(viewLifecycleOwner, Observer { list->
            reminderInfoList = list
            reminderListAdapter?.submitList(list)
            reminderListAdapter?.notifyDataSetChanged()
        })
        val swipeMenuCreator = SwipeMenuCreator { _, rightMenu, _ ->
            val deleteItem = SwipeMenuItem(activity)
            deleteItem.text = resources.getString(R.string.edit_reminder_swipe_delete_button_text)
            deleteItem.setTextColorResource(R.color.colorWhite)
            deleteItem.textSize = 20
            deleteItem.setBackgroundColorResource(R.color.colorSelectPetDelete)
            deleteItem.height = ViewGroup.LayoutParams.MATCH_PARENT
            deleteItem.width = 220
            rightMenu.addMenuItem(deleteItem)
        }
        val itemMenuClickListener = OnItemMenuClickListener { menuBridge, adapterPosition ->
            menuBridge.closeMenu()
            MyCustomDialog(activity)
                .setCancelable(true)
                .setTitle(getString(R.string.delete_reminder_swipe_dialog_warning_title))
                .setTitleStyleBold()
                .setMessage(getString(R.string.delete_reminder_swipe_dialog_message))
                .setPositiveButton(getString(R.string.error_dialog_builder_positive)) {
                    reminderViewModel.deleteReminderOnClick(reminderInfoList[adapterPosition])
                }
                .setNegativeButton(getString(R.string.error_dialog_builder_negative)) { }
                .builder()
                .show()
        }
        fragmentReminderListBinding.reminderFragmentRecyclerView.setSwipeMenuCreator(swipeMenuCreator)
        fragmentReminderListBinding.reminderFragmentRecyclerView.setOnItemMenuClickListener(itemMenuClickListener)
        reminderListAdapter = ReminderListAdapter(reminderListAdapterListener)
        fragmentReminderListBinding.reminderFragmentRecyclerView.adapter = reminderListAdapter
        return fragmentReminderListBinding.root
    }

    private val reminderListAdapterListener = object : ReminderListAdapter.ReminderListAdapterListener {
        override fun onClick(reminderInfo: ReminderInfo) {
            val intent = Intent(activity, EditReminderActivity::class.java)
            intent.putExtra(INTENT_EXTRA_EDIT_REMINDER_INFO_KEY, reminderInfo)
            startActivity(intent)
        }

        override fun onSwitch(reminderInfo: ReminderInfo, isChecked: Boolean) {
            val checked: Int = if(isChecked) { 1 } else { 0 }
            if(reminderInfo.status != checked) {
                reminderViewModel.switchUpdateReminder(reminderInfo, isChecked)
                val calendar = Calendar.getInstance()
                if(reminderInfo.date?.isNotEmpty()!!) {
                    val dateSplit = reminderInfo.date?.split(resources.getString(R.string.simple_date_format_date_split))
                    dateSplit?.get(0)?.toInt()?.let { calendar.set(Calendar.YEAR, it) }
                    dateSplit?.get(1)?.toInt()?.let { calendar.set(Calendar.MONTH, it - 1) }
                    dateSplit?.get(2)?.toInt()?.let { calendar.set(Calendar.DAY_OF_MONTH, it) }
                }
                val timeSplit = reminderInfo.time.split(resources.getString(R.string.simple_date_format_time_split))
                calendar.set(Calendar.HOUR_OF_DAY, timeSplit[0].toInt())
                calendar.set(Calendar.MINUTE, timeSplit[1].toInt())
                if(isChecked) {
                    registerAlarm(calendar, reminderInfo)
                } else {
                    unRegisterAlarm(reminderInfo)
                }
            }
        }
    }

    fun createReminderOnClick() {
        val reminderList = ArrayList<String>()
        reminderList.add(resources.getString(R.string.reminder_list_type_selector_insulin_medicine_reminder_text))
        reminderList.add(resources.getString(R.string.reminder_list_type_selector_veterinarian_reminder_text))
        MyCustomDialog(activity)
            .setTitle(resources.getString(R.string.reminder_list_type_selector_title))
            .setListView(reminderList) { _, _, position, _ ->
                when (reminderList[position]) {
                    resources.getString(R.string.reminder_list_type_selector_insulin_medicine_reminder_text) -> {
                        val intent = Intent(activity, CreateReminderActivity::class.java)
                        intent.putExtra(INTENT_EXTRA_CREATE_REMINDER_TYPE_KEY, CREATE_INSULIN_REMINDER_FLAG)
                        startActivity(intent)
                    }
                    resources.getString(R.string.reminder_list_type_selector_veterinarian_reminder_text) -> {
                        val intent = Intent(activity, CreateReminderActivity::class.java)
                        intent.putExtra(INTENT_EXTRA_CREATE_REMINDER_TYPE_KEY, CREATE_VETERINARIAN_REMINDER_FLAG)
                        startActivity(intent)
                    }
                }
            }
            .setNegativeButton(resources.getString(R.string.my_custom_dialog_cancel_text)) { }
            .setNegativeButtonColor(R.color.myCustomDialogColorBlue)
            .setNegativeButtonStyleBold()
            .builder()
            .show()
    }

    private fun registerAlarm(calendar: Calendar, reminderInfo: ReminderInfo) {
        val intent = Intent(context, ReminderReceiver::class.java)
        intent.action = ReminderReceiver.REMINDER_RECEIVED_ACTION
        intent.addCategory(reminderInfo.time)
        intent.putExtra(ReminderReceiver.REMINDER_RECEIVED_LABEL_KEY, reminderInfo.label)
        intent.putExtra(ReminderReceiver.REMINDER_RECEIVED_NOTIFICATION_TYPE_KEY, reminderInfo.notificationType)
        val pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
    }

    private fun unRegisterAlarm(reminderInfo: ReminderInfo) {
        val intent = Intent(context, ReminderReceiver::class.java)
        intent.action = ReminderReceiver.REMINDER_RECEIVED_ACTION
        intent.addCategory(reminderInfo.time)
        intent.putExtra(ReminderReceiver.REMINDER_RECEIVED_LABEL_KEY, reminderInfo.label)
        intent.putExtra(ReminderReceiver.REMINDER_RECEIVED_NOTIFICATION_TYPE_KEY, reminderInfo.notificationType)
        val pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.cancel(pendingIntent)
    }

    override fun onBackPressed(): Boolean {
        return false
    }
}