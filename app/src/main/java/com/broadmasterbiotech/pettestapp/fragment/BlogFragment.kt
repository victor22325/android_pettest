package com.broadmasterbiotech.pettestapp.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.FragmentBlogBinding


class BlogFragment : BaseFragment() {

    companion object {
        val TAG = BlogFragment::class.java.simpleName

        fun newInstance(): BlogFragment {
            return BlogFragment()
        }
    }

    private lateinit var fragmentBlogBinding: FragmentBlogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentBlogBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_blog, null, false)
        val spannableString = SpannableString(activity?.resources?.getString(R.string.blog_text_link))
        spannableString.setSpan(UnderlineSpan(), 0, spannableString.length, 0)
        fragmentBlogBinding.blogTextLink.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://shoppettest.com/blog/")
            startActivity(intent)
        }
        fragmentBlogBinding.blogTextLink.text = spannableString
        return fragmentBlogBinding.root
    }

    override fun onBackPressed(): Boolean {
        return false
    }
}