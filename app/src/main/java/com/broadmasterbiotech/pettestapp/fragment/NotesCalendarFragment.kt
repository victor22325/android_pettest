package com.broadmasterbiotech.pettestapp.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.database.note.NoteInfo
import com.broadmasterbiotech.library.viewmodel.NotesCalendarViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.ErrorDialogBuilder
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.adapter.NotesCalendarAdapter
import com.broadmasterbiotech.pettestapp.databinding.FragmentNotesCalendarBinding
import com.broadmasterbiotech.pettestapp.dialog.CreateNoteDialog
import com.broadmasterbiotech.pettestapp.dialog.EditNoteDialog
import com.yanzhenjie.recyclerview.OnItemMenuClickListener
import com.yanzhenjie.recyclerview.SwipeMenuCreator
import com.yanzhenjie.recyclerview.SwipeMenuItem
import java.util.*

class NotesCalendarFragment: BaseFragment() {

    companion object {

        private val TAG = NotesCalendarFragment::class.java.simpleName

        fun newInstance(): NotesCalendarFragment {
            return NotesCalendarFragment()
        }
    }

    private lateinit var fragmentNotesCalendarBinding: FragmentNotesCalendarBinding
    private lateinit var notesCalendarViewModel: NotesCalendarViewModel
    private var notesCalendarAdapter: NotesCalendarAdapter? = null
    private lateinit var date: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentNotesCalendarBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notes_calendar, null, false)
        fragmentNotesCalendarBinding.lifecycleOwner = this
        fragmentNotesCalendarBinding.activity = this
        notesCalendarViewModel = ViewModelProvider(this@NotesCalendarFragment).get(NotesCalendarViewModel::class.java)
        fragmentNotesCalendarBinding.notesCalendarMouthsYearText.text = fragmentNotesCalendarBinding.notesCalendarCalendarView.dateShow
        fragmentNotesCalendarBinding.notesCalendarCalendarView.setOnClickDate { year, month, day ->
            fragmentNotesCalendarBinding.notesCalendarMouthsYearText.text = fragmentNotesCalendarBinding.notesCalendarCalendarView.dateShow
            notesCalendarViewModel.selectedCalendar.set(Calendar.YEAR, year)
            notesCalendarViewModel.selectedCalendar.set(Calendar.MONTH, month-1)
            notesCalendarViewModel.selectedCalendar.set(Calendar.DAY_OF_MONTH, day)
            date = "${notesCalendarViewModel.selectedCalendar.get(Calendar.YEAR)}/${notesCalendarViewModel.getNum(notesCalendarViewModel.selectedCalendar.get(Calendar.MONTH)+1)}/${notesCalendarViewModel.getNum(notesCalendarViewModel.selectedCalendar.get(Calendar.DAY_OF_MONTH))}"
            notesCalendarViewModel.getCurrentNoteList(date)
        }
        date = "${notesCalendarViewModel.selectedCalendar.get(Calendar.YEAR)}/${notesCalendarViewModel.getNum(notesCalendarViewModel.selectedCalendar.get(Calendar.MONTH)+1)}/${notesCalendarViewModel.getNum(notesCalendarViewModel.selectedCalendar.get(Calendar.DAY_OF_MONTH))}"
        notesCalendarViewModel.getCurrentNoteList(date)
        notesCalendarViewModel.noteInfoListSingleLiveEvent.observe(this, androidx.lifecycle.Observer { noteInfoList->
            noteInfoList.sort()
            if(notesCalendarAdapter == null) {
                notesCalendarAdapter = NotesCalendarAdapter(notesCalendarListener)
            }
            notesCalendarAdapter?.submitList(noteInfoList)
            fragmentNotesCalendarBinding.notesCalendarRecyclerView.adapter = notesCalendarAdapter
        })
        notesCalendarViewModel.getAllNotesDateList()
        notesCalendarViewModel.notesDateListSingleLiveEvent.observe(this, androidx.lifecycle.Observer { notesDateList->
            fragmentNotesCalendarBinding.notesCalendarCalendarView.setNotesDate(notesDateList)
            fragmentNotesCalendarBinding.notesCalendarCalendarView.refreshCalendar()
        })
        val swipeMenuCreator = SwipeMenuCreator { _, rightMenu, _ ->
            val deleteItem = SwipeMenuItem(context)
            deleteItem.text = resources.getString(R.string.select_pet_dialog_swipe_delete_btn)
            deleteItem.setTextColorResource(R.color.colorWhite)
            deleteItem.textSize = 20
            deleteItem.setBackgroundColorResource(R.color.colorSelectPetDelete)
            deleteItem.height = ViewGroup.LayoutParams.MATCH_PARENT
            deleteItem.width = 220
            rightMenu.addMenuItem(deleteItem)
        }
        fragmentNotesCalendarBinding.notesCalendarRecyclerView.setSwipeMenuCreator(swipeMenuCreator)
        fragmentNotesCalendarBinding.notesCalendarRecyclerView.setOnItemMenuClickListener(itemMenuClickListener)
        return fragmentNotesCalendarBinding.root
    }

    private val notesCalendarListener = object: NotesCalendarAdapter.NotesCalendarAdapterListener {
        override fun onClick(noteInfo: NoteInfo) {
            val editNoteDialog = EditNoteDialog(activity!!, noteInfo, notesCalendarViewModel, fragmentNotesCalendarBinding.lifecycleOwner!!)
            editNoteDialog.show()
        }
    }

    private val itemMenuClickListener = OnItemMenuClickListener { menuBridge, adapterPosition ->
        menuBridge.closeMenu()
        MyCustomDialog(context!!)
            .setTitle(resources.getString(R.string.delete_note_dialog_title))
            .setMessage(resources.getString(R.string.delete_note_dialog_content, notesCalendarViewModel.noteInfoListSingleLiveEvent.value?.get(adapterPosition)?.topic))
            .setPositiveButton(resources.getString(R.string.error_dialog_builder_positive)) { notesCalendarViewModel.deleteNoteOnClick(notesCalendarViewModel.noteInfoListSingleLiveEvent.value?.get(adapterPosition)!!) }
            .setNegativeButton(resources.getString(R.string.error_dialog_builder_negative)) {}
            .builder()
            .show()
    }

    fun backToTodayOnClick() {
        fragmentNotesCalendarBinding.notesCalendarCalendarView.goSelectToday()
        fragmentNotesCalendarBinding.notesCalendarMouthsYearText.text = fragmentNotesCalendarBinding.notesCalendarCalendarView.dateShow
    }

    fun addNoteOnClick() {
        val createNoteDialog = CreateNoteDialog(activity!!, notesCalendarViewModel.selectedCalendar, notesCalendarViewModel, this)
        createNoteDialog.show()
    }

    fun expandOnClick() {
        if(fragmentNotesCalendarBinding.notesCalendarExpandLayout.isExpanded) {
            fragmentNotesCalendarBinding.notesCalendarExpandLayout.collapse()
            fragmentNotesCalendarBinding.notesCalendarExpandButtonIcon.setImageDrawable(ContextCompat.getDrawable(context!!, R.mipmap.bigdown))
        } else {
            fragmentNotesCalendarBinding.notesCalendarExpandLayout.expand()
            fragmentNotesCalendarBinding.notesCalendarExpandButtonIcon.setImageDrawable(ContextCompat.getDrawable(context!!, R.mipmap.bigup))
        }
    }

    fun previousMonthOnClick() {
        fragmentNotesCalendarBinding.notesCalendarCalendarView.setLastMonth()
        fragmentNotesCalendarBinding.notesCalendarMouthsYearText.text = fragmentNotesCalendarBinding.notesCalendarCalendarView.dateShow

    }

    fun nextMonthOnClick() {
        fragmentNotesCalendarBinding.notesCalendarCalendarView.setNextMonth()
        fragmentNotesCalendarBinding.notesCalendarMouthsYearText.text = fragmentNotesCalendarBinding.notesCalendarCalendarView.dateShow

    }

    override fun onBackPressed(): Boolean {
        return false
    }
}