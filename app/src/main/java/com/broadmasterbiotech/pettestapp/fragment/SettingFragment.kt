package com.broadmasterbiotech.pettestapp.fragment

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothHeadset
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.BlePairEvent
import com.broadmasterbiotech.library.viewmodel.SettingViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.adapter.SelectBluetoothAdapter
import com.broadmasterbiotech.pettestapp.databinding.FragmentSettingBinding
import com.broadmasterbiotech.pettestapp.dialog.BluetoothPairSettingDialog
import com.polidea.rxandroidble2.RxBleClient

class SettingFragment: BaseFragment() {

    companion object {

        private val TAG = SettingFragment::class.java.simpleName

        fun newInstance(): SettingFragment {
            return SettingFragment()
        }

        const val ACTIVITY_RESULT_REQUEST_ENABLE_BT = 10
    }

    private lateinit var fragmentSettingBinding: FragmentSettingBinding
    private lateinit var settingViewModel: SettingViewModel
    private var bluetoothAdapter: BluetoothAdapter? = null
    private lateinit var pairedDevicesAdapter: SelectBluetoothAdapter
    private lateinit var availableDevicesAdapter: SelectBluetoothAdapter
    private var pairedDevicesList = ArrayList<BluetoothDevice>()
    private var availableDevicesList = ArrayList<BluetoothDevice>()
    private lateinit var blDevice: BluetoothDevice

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fragmentSettingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, null, false)
        fragmentSettingBinding.lifecycleOwner = this
        settingViewModel = ViewModelProvider(this@SettingFragment).get(SettingViewModel::class.java)
        fragmentSettingBinding.viewModel = settingViewModel
        setUpBluetoothStatus()
        pairedDevicesAdapter = SelectBluetoothAdapter(activity!!, pairedDevicesList, pairedDevicesListener)
        fragmentSettingBinding.settingPairedDevicesRecyclerView.adapter = pairedDevicesAdapter
        availableDevicesAdapter = SelectBluetoothAdapter(activity!!, availableDevicesList, availableDevicesListener)
        fragmentSettingBinding.settingAvailableDevicesRecyclerView.adapter = availableDevicesAdapter
        fragmentSettingBinding.settingDongleBluetoothPairingSwitch.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                val bluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                bluetoothIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
                this.startActivityForResult(bluetoothIntent, ACTIVITY_RESULT_REQUEST_ENABLE_BT)
                if (bluetoothAdapter?.isDiscovering!!) {
                    bluetoothAdapter?.cancelDiscovery()
                }
            } else {
                closeBLEPairSwitch()
            }
        }
        settingViewModel.blePairSingleLiveEvent.observe(viewLifecycleOwner, Observer{ bleConnectEventHandler(it) })
        return fragmentSettingBinding.root
    }
    private fun closeBLEPairSwitch() {
        fragmentSettingBinding.settingDongleBluetoothPairingSwitch.isChecked = false
        fragmentSettingBinding.settingDongleSearchDevicesLayout.visibility = View.GONE
        settingViewModel.isPaired.value = false
        settingViewModel.bluetoothStatusLiveData.value = false
        settingViewModel.closeBleConnection()
        pairedDevicesList.clear()
        pairedDevicesAdapter.submitList(pairedDevicesList)
        availableDevicesList.clear()
        availableDevicesAdapter.submitList(availableDevicesList)
        bluetoothAdapter?.disable()
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    private val pairedDevicesListener = object: SelectBluetoothAdapter.SelectBluetoothDeviceListener {
        override fun onClick(bluetoothDevice: BluetoothDevice) {
            if(bluetoothAdapter!!.isDiscovering) {
                bluetoothAdapter?.cancelDiscovery()
            }
            blDevice = bluetoothDevice
            settingViewModel.connectToDevice(bluetoothDevice)
        }
    }

    private val availableDevicesListener = object: SelectBluetoothAdapter.SelectBluetoothDeviceListener {
        override fun onClick(bluetoothDevice: BluetoothDevice) {
            if(bluetoothAdapter!!.isDiscovering) {
                bluetoothAdapter?.cancelDiscovery()
            }
            blDevice = bluetoothDevice
            settingViewModel.connectToDevice(bluetoothDevice)
        }
    }

    private fun setUpBluetoothStatus() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if(bluetoothAdapter!=null) {
            settingViewModel.bluetoothStatusLiveData.value = bluetoothAdapter?.isEnabled!!
        } else {
            fragmentSettingBinding.settingDongleBluetoothPairingSwitch.isEnabled = false
            fragmentSettingBinding.settingDongleSearchDevicesLayout.visibility = View.GONE
        }

        val filter = IntentFilter()
        filter.addAction(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        activity?.registerReceiver(bluetoothDiscoveryReceiver, filter)

        // rxAndroidBLE
        settingViewModel.bleClientStatus.observe(viewLifecycleOwner, Observer { status->
            settingViewModel.bluetoothStatusLiveData.value = status == RxBleClient.State.READY
            when(status) {
                RxBleClient.State.BLUETOOTH_NOT_AVAILABLE -> {
                    MyCustomDialog(activity!!)
                        .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), Code.SETTING_BLE_NON_AVAILABLE.toString()))
                        .setMessage(getString(R.string.bluetooth_error_dialog_device_non_support))
                        .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                        .builder()
                        .show()
                }
                RxBleClient.State.LOCATION_PERMISSION_NOT_GRANTED -> {
                    MyCustomDialog(activity!!)
                        .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), Code.SETTING_BLE_LOCAL_PERMISSION_NOT_GRANTED.toString()))
                        .setMessage(getString(R.string.bluetooth_error_dialog_local_permission_not_granted))
                        .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                        .builder()
                        .show()
                }
                RxBleClient.State.BLUETOOTH_NOT_ENABLED -> {
                    MyCustomDialog(activity!!)
                        .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), Code.SETTING_BLE_NOT_ENABLE.toString()))
                        .setMessage(getString(R.string.bluetooth_error_dialog_not_enable))
                        .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                        .builder()
                        .show()
                }
                RxBleClient.State.LOCATION_SERVICES_NOT_ENABLED -> {
                    MyCustomDialog(activity!!)
                        .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), Code.SETTING_BLE_SERVICE_NOT_ENABLE.toString()))
                        .setMessage(getString(R.string.bluetooth_error_dialog_local_service_not_enable))
                        .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                        .builder()
                        .show()
                }
                RxBleClient.State.READY -> {
                    settingViewModel.scanBLEDevice()
                }
                else -> {
                    Log.d(TAG, "bleClientStatus:${status.name}")
                }
            }
        })
    }

    private val bluetoothDiscoveryReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when(intent.action!!) {
                BluetoothDevice.ACTION_FOUND -> {
                    val device: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)!!
                    val deviceName = device.name
                    val deviceHardwareAddress = device.address
                    val isConnected =  bluetoothAdapter?.getProfileConnectionState(BluetoothHeadset.HEADSET) == BluetoothHeadset.STATE_CONNECTED
                    Log.d(TAG, "availableDevices deviceName:$deviceName, address:$deviceHardwareAddress, isConnected:$isConnected")
                    availableDevicesList.add(device)
                    availableDevicesAdapter.submitList(availableDevicesList)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode != Activity.RESULT_OK) {
            return
        } else {
            when (requestCode) {
                ACTIVITY_RESULT_REQUEST_ENABLE_BT -> {
                    fragmentSettingBinding.settingDongleSearchDevicesLayout.visibility = View.VISIBLE
                    bluetoothAdapter?.startDiscovery()
                    val pairedDevices: Set<BluetoothDevice>? = bluetoothAdapter?.bondedDevices
                    pairedDevices?.forEach { device ->
                        val deviceName = device.name
                        val deviceHardwareAddress = device.address
                        val isConnected =  bluetoothAdapter?.getProfileConnectionState(BluetoothHeadset.HEADSET) == BluetoothHeadset.STATE_CONNECTED
                        Log.d(TAG, "pairedDevices deviceName:$deviceName, address:$deviceHardwareAddress, isConnected:$isConnected")
                        pairedDevicesList.add(device)
                        pairedDevicesAdapter.submitList(pairedDevicesList)
                    }
                }
            }
        }
    }

    private fun bleConnectEventHandler(event: BaseEvent) {
        when(event) {
            is BlePairEvent.PairSuccess -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.ble_pair_error_dialog_pair_success_title))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.ble_pair_error_dialog_pair_success_message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {}
                    .builder()
                    .show()
            }
            is BlePairEvent.VerifySuccess -> {
                settingViewModel.closeBleConnection()
                val bluetoothPairSettingDialog = BluetoothPairSettingDialog(activity!!, blDevice, settingViewModel, this)
                bluetoothPairSettingDialog.show()
            }
            is BlePairEvent.SendSNError -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.ble_pair_error_dialog_send_sn_fail, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {closeBLEPairSwitch()}
                    .builder()
                    .show()
            }
            is BlePairEvent.VerifySNError -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.ble_pair_error_dialog_verify_sn_fail))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {closeBLEPairSwitch()}
                    .builder()
                    .show()
            }
            is BlePairEvent.ScanWifiError -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.ble_pair_error_dialog_scan_wifi_fail, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {closeBLEPairSwitch()}
                    .builder()
                    .show()
            }
            is BlePairEvent.SendTokenError -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.ble_pair_error_dialog_send_token_fail, event.message))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {closeBLEPairSwitch()}
                    .builder()
                    .show()
            }
            is BlePairEvent.OtherError -> {
                MyCustomDialog(activity)
                    .setTitle(getString(R.string.my_custom_dialog_builder_title, Code.Type.BLUETOOTH_SETTING_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(getString(R.string.ble_pair_error_dialog_other_fail))
                    .setNegativeButton(getString(R.string.error_dialog_builder_neutral)) {closeBLEPairSwitch()}
                    .builder()
                    .show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(bluetoothDiscoveryReceiver)
        bluetoothAdapter?.cancelDiscovery()
    }
}