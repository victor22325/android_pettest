package com.broadmasterbiotech.pettestapp.fragment

import androidx.fragment.app.Fragment

abstract class BaseFragment: Fragment() {

    abstract fun onBackPressed(): Boolean

}