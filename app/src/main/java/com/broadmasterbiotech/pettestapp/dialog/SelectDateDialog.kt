package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.DatePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.DialogNoteSelectDatePickerBinding
import java.text.SimpleDateFormat
import java.util.*

class SelectDateDialog(val activity: Activity, private val datePicker: DatePicker, val listener: Listener, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {
        val TAG = SelectDateDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogNoteSelectDatePickerBinding: DialogNoteSelectDatePickerBinding

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogNoteSelectDatePickerBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_note_select_date_picker, null, false)
        dialog.setContentView(dialogNoteSelectDatePickerBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialogNoteSelectDatePickerBinding.activity = this
        dialogNoteSelectDatePickerBinding.noteSelectDateDatePicker.updateDate(datePicker.year, datePicker.month, datePicker.dayOfMonth)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }

    fun createNoteDatePickerDoneOnClick() {
        dialog.dismiss()
        val dateCalendar = Calendar.getInstance()
        dialogNoteSelectDatePickerBinding.noteSelectDateDatePicker.year.let { dateCalendar.set(Calendar.YEAR, it) }
        dialogNoteSelectDatePickerBinding.noteSelectDateDatePicker.month.let { dateCalendar.set(Calendar.MONTH, it) }
        dialogNoteSelectDatePickerBinding.noteSelectDateDatePicker.dayOfMonth.let { dateCalendar.set(Calendar.DAY_OF_MONTH, it) }
        val dateSimpleDateFormat = SimpleDateFormat("yyyy/MM/dd", Locale.US)
        listener.datePickerOnClick(dateSimpleDateFormat.format(dateCalendar.time))
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }

    interface Listener {
        fun datePickerOnClick(date: String)
    }
}