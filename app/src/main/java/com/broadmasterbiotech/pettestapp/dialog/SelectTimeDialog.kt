package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.DialogNoteSelectTimePickerBinding
import java.text.SimpleDateFormat
import java.util.*

class SelectTimeDialog(val activity: Activity, private val timePicker: TimePicker, private val listener: Listener, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {
        val TAG = SelectTimeDialog::class.java
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogNoteSelectTimePickerBinding: DialogNoteSelectTimePickerBinding

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogNoteSelectTimePickerBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_note_select_time_picker, null, false)
        dialog.setContentView(dialogNoteSelectTimePickerBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialogNoteSelectTimePickerBinding.activity = this
        dialogNoteSelectTimePickerBinding.noteSelectTimeTimePicker.hour = timePicker.hour
        dialogNoteSelectTimePickerBinding.noteSelectTimeTimePicker.minute = timePicker.minute
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }

    fun createNoteTimePickerDoneOnClick() {
        dialog.dismiss()
        val timeCalendar = Calendar.getInstance()
        dialogNoteSelectTimePickerBinding.noteSelectTimeTimePicker.hour.let { timeCalendar.set(Calendar.HOUR_OF_DAY, it) }
        dialogNoteSelectTimePickerBinding.noteSelectTimeTimePicker.minute.let { timeCalendar.set(Calendar.MINUTE, it) }
        val timeSimpleDateFormat = SimpleDateFormat("HH:mm", Locale.US)
        listener.timePickerOnClick(timeSimpleDateFormat.format(timeCalendar.time))
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }

    interface Listener {
        fun timePickerOnClick(time: String)
    }
}