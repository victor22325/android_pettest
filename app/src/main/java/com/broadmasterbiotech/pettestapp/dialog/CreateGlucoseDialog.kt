package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.library.viewmodel.GlucoseLogViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.DialogAddGlucoseBinding
import com.broadmasterbiotech.pettestapp.enums.pet.Species
import com.bumptech.glide.Glide
import java.util.*

class CreateGlucoseDialog (val activity: Activity, private val glucoseLogViewModel: GlucoseLogViewModel, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {
        val TAG = CreateGlucoseDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogAddGlucoseBinding: DialogAddGlucoseBinding
    private lateinit var datePicker: DatePicker
    private lateinit var timePicker: TimePicker

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogAddGlucoseBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_add_glucose, null, false)
        dialog.setContentView(dialogAddGlucoseBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        datePicker = dialogAddGlucoseBinding.addGlucoseDatePicker
        timePicker = dialogAddGlucoseBinding.addGlucoseTimePicker
        dialogAddGlucoseBinding.activity = this
        dialogAddGlucoseBinding.viewModel = glucoseLogViewModel
        dialogAddGlucoseBinding.petInfo = glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value
        if(glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.avatarPhoto.isNullOrEmpty()) {
            if(glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.species == Species.Canine.num) {
                Glide.with(activity).load(R.drawable.dog_list).into(dialogAddGlucoseBinding.addGlucoseAvatarPhoto)
            } else {
                Glide.with(activity).load(R.drawable.cat_list).into(dialogAddGlucoseBinding.addGlucoseAvatarPhoto)
            }
        } else {
            Glide.with(activity).load(glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.avatarPhoto).into(dialogAddGlucoseBinding.addGlucoseAvatarPhoto)
        }
        dialog.show()
    }

    fun addGlucoseInsertOnClick() {
        glucoseLogViewModel.addGlucoseInsertOnClick(datePicker, timePicker)
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }
}