package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.library.viewmodel.NotesCalendarViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.DialogCreateNoteBinding
import com.broadmasterbiotech.pettestapp.enums.note.Type
import java.text.SimpleDateFormat
import java.util.*

class CreateNoteDialog(val activity: Activity, private val calendar: Calendar, private val notesCalendarViewModel: NotesCalendarViewModel, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {
        val TAG = CreateNoteDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogCreateNoteBinding: DialogCreateNoteBinding
    private var datePicker = DatePicker(activity.application)
    private var timePicker = TimePicker(activity.application)

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogCreateNoteBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_create_note, null, false)
        dialog.setContentView(dialogCreateNoteBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialogCreateNoteBinding.activity = this
        dialogCreateNoteBinding.viewModel = notesCalendarViewModel
        dialogCreateNoteBinding.currentType = Type.Note.num
        notesCalendarViewModel.typeSingleLiveEvent.value = Type.Note.num
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        notesCalendarViewModel.datePickerSingleLiveEvent.value = getDateString(datePicker)
        timePicker.hour = calendar.get(Calendar.HOUR_OF_DAY)
        timePicker.minute = calendar.get(Calendar.MINUTE)
        notesCalendarViewModel.timePickerSingleLiveEvent.value = getTimeString(timePicker)
        notesCalendarViewModel.topicSingleLiveEvent.value = ""
        notesCalendarViewModel.contentSingleLiveEvent.value = ""
        dialogCreateNoteBinding.createNoteSelectDate.text = notesCalendarViewModel.getSelectDate(datePicker)
        dialogCreateNoteBinding.createNoteSelectTime.text = activity.resources.getString(R.string.notes_calendar_select_time, notesCalendarViewModel.getSelectHour(calendar.get(Calendar.HOUR_OF_DAY)), notesCalendarViewModel.getSelectMinute(calendar.get(Calendar.MINUTE)))
        dialog.show()
    }

    fun createNoteDoneOnClick() {
        dialog.dismiss()
        notesCalendarViewModel.createNoteDoneOnClick()
    }

    fun mealTypeOnClick() {
        notesCalendarViewModel.typeSingleLiveEvent.value = Type.Meal.ordinal
        dialogCreateNoteBinding.currentType = Type.Meal.ordinal
    }

    fun exerciseTypeOnClick() {
        notesCalendarViewModel.typeSingleLiveEvent.value = Type.Exercise.ordinal
        dialogCreateNoteBinding.currentType = Type.Exercise.ordinal
    }

    fun noteTypeOnClick() {
        notesCalendarViewModel.typeSingleLiveEvent.value = Type.Note.ordinal
        dialogCreateNoteBinding.currentType = Type.Note.ordinal
    }

    fun medicalTypeOnClick() {
        notesCalendarViewModel.typeSingleLiveEvent.value = Type.Medical.ordinal
        dialogCreateNoteBinding.currentType = Type.Medical.ordinal
    }

    private val selectDateListener = object: SelectDateDialog.Listener {
        override fun datePickerOnClick(date: String) {
            notesCalendarViewModel.datePickerSingleLiveEvent.value = date
            dialogCreateNoteBinding.createNoteSelectDate.text = date
            val dateSplit = date.split(activity.resources.getString(R.string.simple_date_format_date_split))
            datePicker.updateDate(dateSplit[0].toInt(), dateSplit[1].toInt()-1, dateSplit[2].toInt())
        }
    }
    
    fun selectDateOnClick() {
        val datePickerDialog = SelectDateDialog(activity, datePicker, selectDateListener, lifecycleOwner)
        datePickerDialog.show()
    }

    private val selectTimeListener = object: SelectTimeDialog.Listener {
        override fun timePickerOnClick(time: String) {
            notesCalendarViewModel.timePickerSingleLiveEvent.value = time
            dialogCreateNoteBinding.createNoteSelectTime.text = time
            val timeSplit = time.split(activity.resources.getString(R.string.simple_date_format_time_split))
            timePicker.hour = timeSplit[0].toInt()
            timePicker.minute = timeSplit[1].toInt()
        }
    }

    fun selectTimeOnClick() {
        val timePickerDialog = SelectTimeDialog(activity, timePicker, selectTimeListener, lifecycleOwner)
        timePickerDialog.show()
    }

    private fun getDateString(datePicker: DatePicker): String {
        val dateCalendar = Calendar.getInstance()
        dateCalendar.set(Calendar.YEAR, datePicker.year)
        dateCalendar.set(Calendar.MONTH, datePicker.month)
        dateCalendar.set(Calendar.DAY_OF_MONTH, datePicker.dayOfMonth)
        val dateSimpleDateFormat = SimpleDateFormat(activity.resources.getString(R.string.simple_date_format_year_month_date), Locale.US)
        return dateSimpleDateFormat.format(dateCalendar.time)
    }

    private fun getTimeString(timePicker: TimePicker): String {
        val timeCalendar = Calendar.getInstance()
        timePicker.hour.let { timeCalendar.set(Calendar.HOUR_OF_DAY, it) }
        timePicker.minute.let { timeCalendar.set(Calendar.MINUTE, it) }
        val timeSimpleDateFormat = SimpleDateFormat(activity.resources.getString(R.string.simple_date_format_hour_minute), Locale.US)
        return timeSimpleDateFormat.format(timeCalendar.time)
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }
}