package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.DialogBirthdayBinding
import java.util.*

class BirthdayDialog(val activity: Activity, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {
        val TAG = BirthdayDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogBirthdayBinding: DialogBirthdayBinding

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogBirthdayBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_birthday, null, false)
        dialog.setContentView(dialogBirthdayBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }
}