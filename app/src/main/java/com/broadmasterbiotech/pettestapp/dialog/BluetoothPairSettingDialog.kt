package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.bluetooth.BluetoothDevice
import android.content.*
import android.net.wifi.ScanResult
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.library.viewmodel.SettingViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.adapter.SelectWifiAdapter
import com.broadmasterbiotech.pettestapp.databinding.DialogBluetoothPairSettingBinding
import java.util.*

class BluetoothPairSettingDialog(val activity: Activity, private val bluetoothDevice: BluetoothDevice, val viewModel: SettingViewModel, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {
        val TAG = BluetoothPairSettingDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private var dialogBluetoothPairSettingBinding: DialogBluetoothPairSettingBinding? = null
    private lateinit var selectWifiAdapter: SelectWifiAdapter
    private var scanResultList = ArrayList<ScanResult>()

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogBluetoothPairSettingBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_bluetooth_pair_setting, null, false)
        dialog.setContentView(dialogBluetoothPairSettingBinding?.root!!)
        dialogBluetoothPairSettingBinding?.lifecycleOwner = lifecycleOwner
        dialogBluetoothPairSettingBinding?.dialog = this
        dialogBluetoothPairSettingBinding?.viewModel = viewModel
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        selectWifiAdapter = SelectWifiAdapter(activity, scanResultList, scanResultListener)
        dialogBluetoothPairSettingBinding?.blePairSettingUsableWifiRecyclerView?.adapter = selectWifiAdapter
        viewModel.scanResultListSingleLiveEvent.observe(lifecycleOwner, androidx.lifecycle.Observer { scanResultList->
            selectWifiAdapter.submitList(scanResultList)
        })
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialogBluetoothPairSettingBinding?.ssid = ""
        viewModel.scanWifi(activity)
        dialog.show()
    }

    private val scanResultListener = object: SelectWifiAdapter.ScanResultListener {
        override fun onClick(scanResult: ScanResult) {
            Log.d(TAG, "scanResult:${scanResult.SSID}")
            dialogBluetoothPairSettingBinding?.ssid = scanResult.SSID
            viewModel.wifiSSID.value = scanResult.SSID
        }
    }

    fun sendToDongle() {
        viewModel.sendTokenToDongle(bluetoothDevice)
        dialog.cancel()
    }

    override fun dismiss() {
        viewModel.closeScanWifi()
        viewModel.closeSendToken()
        dialog.dismiss()
    }

    override fun cancel() {
        viewModel.closeScanWifi()
        viewModel.closeSendToken()
        dialog.cancel()
    }
}