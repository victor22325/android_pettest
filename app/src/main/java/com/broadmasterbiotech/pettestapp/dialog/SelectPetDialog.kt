package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.library.viewmodel.PetTestViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.adapter.SelectPetDialogAdapter
import com.broadmasterbiotech.pettestapp.databinding.DialogSelectPetBinding
import com.yanzhenjie.recyclerview.OnItemMenuClickListener
import com.yanzhenjie.recyclerview.SwipeMenuCreator
import android.util.Log
import android.view.ViewGroup
import com.broadmasterbiotech.library.code.Code
import com.broadmasterbiotech.library.event.BaseEvent
import com.broadmasterbiotech.library.event.DeletePetEvent
import com.broadmasterbiotech.library.event.PickPetEvent
import com.broadmasterbiotech.pettestapp.`object`.MyDialog.MyCustomDialog
import com.broadmasterbiotech.pettestapp.activity.PetEditorActivity
import com.broadmasterbiotech.pettestapp.enums.SelectPetSwipeOption
import com.yanzhenjie.recyclerview.SwipeMenuItem
import java.util.*
import kotlin.collections.ArrayList

class SelectPetDialog(val activity: Activity, val viewModel: PetTestViewModel, private var petInfoList: ArrayList<PetInfo>, val listener :SelectPetListener, private val adapterListener: SelectPetDialogAdapter.SelectPetAdapterListener, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {

        val TAG = SelectPetDialog::class.java.simpleName

        const val INTENT_EXTRA_SELECT_PET_DIALOG_TO_EDIT_PET_KEY = "IntentExtraSelectPetDialogToEditPetKey"
    }

    private lateinit var dialog: Dialog
    private var dialogSelectPetBinding: DialogSelectPetBinding? = null
    private lateinit var selectPetDialogAdapter: SelectPetDialogAdapter

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogSelectPetBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_select_pet, null, false)
        selectPetDialogAdapter = SelectPetDialogAdapter(activity, petInfoList, adapterListener)
        val swipeMenuCreator = SwipeMenuCreator { _, rightMenu, _ ->
            val editItem = SwipeMenuItem(activity.applicationContext)
            editItem.text = activity.resources.getString(R.string.select_pet_dialog_swipe_edit_btn)
            editItem.setTextColorResource(R.color.colorWhite)
            editItem.textSize = 20
            editItem.setBackgroundColorResource(R.color.colorSelectPetEdit)
            editItem.height = ViewGroup.LayoutParams.MATCH_PARENT
            editItem.width = 220
            rightMenu.addMenuItem(editItem)
            val deleteItem = SwipeMenuItem(activity.applicationContext)
            deleteItem.text = activity.resources.getString(R.string.select_pet_dialog_swipe_delete_btn)
            deleteItem.setTextColorResource(R.color.colorWhite)
            deleteItem.textSize = 20
            deleteItem.setBackgroundColorResource(R.color.colorSelectPetDelete)
            deleteItem.height = ViewGroup.LayoutParams.MATCH_PARENT
            deleteItem.width = 220
            rightMenu.addMenuItem(deleteItem)
        }
        dialogSelectPetBinding?.selectPetRecyclerView?.setSwipeMenuCreator(swipeMenuCreator)
        dialogSelectPetBinding?.selectPetRecyclerView?.setOnItemMenuClickListener(itemMenuClickListener)
        dialogSelectPetBinding?.selectPetRecyclerView?.adapter = selectPetDialogAdapter
        dialogSelectPetBinding?.listener = listener
        dialogSelectPetBinding?.lifecycleOwner = lifecycleOwner
        dialog.setContentView(dialogSelectPetBinding?.root!!)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
        viewModel.selectPickPetLiveData.observe(lifecycleOwner, androidx.lifecycle.Observer { pickPetEventHandler(it) })
        viewModel.selectDeletePetLiveData.observe(lifecycleOwner, androidx.lifecycle.Observer { deletePetEventHandler(it) })
    }

    private fun getMenuPositionName(num: Int): String {
        return when (num) {
            SelectPetSwipeOption.Edit.num -> SelectPetSwipeOption.Edit.name
            SelectPetSwipeOption.Delete.num -> SelectPetSwipeOption.Delete.name
            else -> "$num"
        }
    }

    fun updatePetList(petInfoList: ArrayList<PetInfo>) {
        if (dialogSelectPetBinding != null) {
            selectPetDialogAdapter.submitList(petInfoList)
            this.petInfoList = petInfoList
        }
    }

    private val itemMenuClickListener = OnItemMenuClickListener { menuBridge, adapterPosition ->
        menuBridge.closeMenu()
        val direction = menuBridge.direction
        val menuPosition = menuBridge.position
        Log.d(TAG, "petName:${petInfoList[adapterPosition].name}, direction:$direction, menuButton:${getMenuPositionName(menuPosition)}")
        when (menuPosition) {
            SelectPetSwipeOption.Edit.num -> {
                val intent = Intent(activity, PetEditorActivity::class.java)
                intent.putExtra(INTENT_EXTRA_SELECT_PET_DIALOG_TO_EDIT_PET_KEY, petInfoList[adapterPosition])
                activity.startActivity(intent)
            }
            SelectPetSwipeOption.Delete.num -> {
                dialog.dismiss()
                MyCustomDialog(activity)
                    .setTitle(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_title))
                    .setTitleStyleBold()
                    .setMessage(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_content, petInfoList[adapterPosition].name))
                    .setPositiveButton(activity.resources.getString(R.string.error_dialog_builder_positive)) {viewModel.deletePetInfo(petInfoList[adapterPosition])}
                    .setNegativeButton(activity.resources.getString(R.string.error_dialog_builder_negative)) {}
                    .builder()
                    .show()
            }
        }
    }

    private fun pickPetEventHandler(event: BaseEvent) {
        when (event) {
            is PickPetEvent.PickPetSuccess -> {
                viewModel.updatePetInfoToDataBase(petInfoList = event.petInfoList)
            }
            is PickPetEvent.PickPetServerError -> {
                MyCustomDialog(activity)
                    .setTitle(activity.getString(R.string.my_custom_dialog_builder_title, Code.Type.PICK_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_pick_pet_server_error, event.message))
                    .setPositiveButton(activity.resources.getString(R.string.error_dialog_builder_neutral)) {dialog.dismiss()}
                    .builder()
                    .show()
            }
        }
    }

    private fun deletePetEventHandler(event: BaseEvent) {
        when (event) {
            is DeletePetEvent.Success -> {
                MyCustomDialog(activity)
                    .setTitle(R.string.my_custom_dialog_title_text)
                    .setMessage(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_delete_success, event.removePetInfo?.name))
                    .setPositiveButton(activity.resources.getString(R.string.error_dialog_builder_neutral)) {dialog.dismiss()}
                    .builder()
                    .show()
                viewModel.updatePetInfoToDataBase(petInfoList = event.petInfoList)
            }
            is DeletePetEvent.CurrentPetError -> {
                MyCustomDialog(activity)
                    .setTitle(activity.getString(R.string.my_custom_dialog_builder_title, Code.Type.DELETE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_delete_current_pet_error))
                    .setNegativeButton(activity.resources.getString(R.string.error_dialog_builder_neutral)) {dialog.dismiss()}
                    .builder()
                    .show()
            }
            is DeletePetEvent.ServerError -> {
                MyCustomDialog(activity)
                    .setTitle(activity.getString(R.string.my_custom_dialog_builder_title, Code.Type.DELETE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_server_error, event.message))
                    .setNegativeButton(activity.resources.getString(R.string.error_dialog_builder_neutral)) {dialog.dismiss()}
                    .builder()
                    .show()
            }
            is DeletePetEvent.IsExistError -> {
                MyCustomDialog(activity)
                    .setTitle(activity.getString(R.string.my_custom_dialog_builder_title, Code.Type.DELETE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_exist_error, event.message))
                    .setNegativeButton(activity.resources.getString(R.string.error_dialog_builder_neutral)) {dialog.dismiss()}
                    .builder()
                    .show()
            }
            is DeletePetEvent.PetInfoError -> {
                MyCustomDialog(activity)
                    .setTitle(activity.getString(R.string.my_custom_dialog_builder_title, Code.Type.DELETE_PET_TYPE.toString(), event.status!!.toString()))
                    .setTitleStyleBold()
                    .setMessage(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_delete_info_error, event.message))
                    .setNegativeButton(activity.resources.getString(R.string.error_dialog_builder_neutral)) {dialog.dismiss()}
                    .builder()
                    .show()
            }
            is DeletePetEvent.OtherError -> {
                MyCustomDialog(activity)
                    .setTitle(activity.getString(R.string.my_custom_dialog_builder_title, Code.Type.DELETE_PET_TYPE.toString(), event.status.toString()))
                    .setTitleStyleBold()
                    .setMessage(activity.resources.getString(R.string.select_pet_dialog_swipe_delete_dialog_other_error))
                    .setNegativeButton(activity.resources.getString(R.string.error_dialog_builder_neutral)) {dialog.dismiss()}
                    .builder()
                    .show()
            }
        }
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }

    interface SelectPetListener {
        fun onClick()
    }
}