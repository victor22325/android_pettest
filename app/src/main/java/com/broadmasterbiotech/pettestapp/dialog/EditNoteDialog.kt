package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.library.database.note.NoteInfo
import com.broadmasterbiotech.library.viewmodel.NotesCalendarViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.DialogEditNoteBinding
import com.broadmasterbiotech.pettestapp.enums.note.Type
import java.util.*

class EditNoteDialog(val activity: Activity, private val noteInfo: NoteInfo, private val notesCalendarViewModel: NotesCalendarViewModel, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {
        val TAG = EditNoteDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogEditGlucoseBinding: DialogEditNoteBinding
    private var datePicker = DatePicker(activity.application)
    private var timePicker = TimePicker(activity.application)
    private var calendar: Calendar = Calendar.getInstance()

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogEditGlucoseBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_edit_note, null, false)
        dialog.setContentView(dialogEditGlucoseBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialogEditGlucoseBinding.activity = this
        dialogEditGlucoseBinding.viewModel = notesCalendarViewModel
        dialogEditGlucoseBinding.currentType = noteInfo.type
        notesCalendarViewModel.editCreateTimeSingleLiveEvent.value = noteInfo.createTime
        notesCalendarViewModel.editDatePickerSingleLiveEvent.value = noteInfo.date
        notesCalendarViewModel.editTimePickerSingleLiveEvent.value = noteInfo.time
        notesCalendarViewModel.editTopicSingleLiveEvent.value = noteInfo.topic
        notesCalendarViewModel.editMediaUriLiveEvent.value = noteInfo.mediaUri
        val dateSplit = noteInfo.date.split("/")
        calendar.set(Calendar.YEAR, dateSplit[0].toInt())
        calendar.set(Calendar.MONTH, dateSplit[1].toInt()-1)
        calendar.set(Calendar.DAY_OF_MONTH, dateSplit[2].toInt())
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        dialogEditGlucoseBinding.editNoteSelectDate.text = notesCalendarViewModel.getSelectDate(datePicker)
        val timeSplit = noteInfo.time.split(":")
        calendar.set(Calendar.HOUR_OF_DAY, timeSplit[0].toInt())
        calendar.set(Calendar.MINUTE, timeSplit[1].toInt())
        timePicker.hour = calendar.get(Calendar.HOUR_OF_DAY)
        timePicker.minute = calendar.get(Calendar.MINUTE)
        dialogEditGlucoseBinding.editNoteSelectTime.text = activity.resources.getString(R.string.notes_calendar_select_time, notesCalendarViewModel.getSelectHour(calendar.get(Calendar.HOUR_OF_DAY)), notesCalendarViewModel.getSelectMinute(calendar.get(Calendar.MINUTE)))
        notesCalendarViewModel.editContentSingleLiveEvent.value = noteInfo.content
        notesCalendarViewModel.editTypeSingleLiveEvent.value = noteInfo.type
        dialog.show()
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }

    fun saveOnClick() {
        dialog.dismiss()
        notesCalendarViewModel.editNoteDoneOnClick(noteInfo)
    }

    private val selectDateListener = object: SelectDateDialog.Listener {
        override fun datePickerOnClick(date: String) {
            notesCalendarViewModel.editDatePickerSingleLiveEvent.value = date
            dialogEditGlucoseBinding.editNoteSelectDate.text = date
            val dateSplit = date.split(activity.resources.getString(R.string.simple_date_format_date_split))
            datePicker.updateDate(dateSplit[0].toInt(), dateSplit[1].toInt()-1, dateSplit[2].toInt())
        }
    }

    fun selectDateOnClick() {
        val datePickerDialog = SelectDateDialog(activity, datePicker, selectDateListener, lifecycleOwner)
        datePickerDialog.show()
    }

    private val selectTimeListener = object: SelectTimeDialog.Listener {
        override fun timePickerOnClick(time: String) {
            notesCalendarViewModel.editTimePickerSingleLiveEvent.value = time
            dialogEditGlucoseBinding.editNoteSelectTime.text = time
            val timeSplit = time.split(activity.resources.getString(R.string.simple_date_format_time_split))
            timePicker.hour = timeSplit[0].toInt()
            timePicker.minute = timeSplit[1].toInt()
        }
    }

    fun selectTimeOnClick() {
        val timePickerDialog = SelectTimeDialog(activity, timePicker, selectTimeListener, lifecycleOwner)
        timePickerDialog.show()
    }

    fun mealTypeOnClick() {
        notesCalendarViewModel.editTypeSingleLiveEvent.value = Type.Meal.ordinal
        dialogEditGlucoseBinding.currentType = Type.Meal.ordinal
    }

    fun exerciseTypeOnClick() {
        notesCalendarViewModel.editTypeSingleLiveEvent.value = Type.Exercise.ordinal
        dialogEditGlucoseBinding.currentType = Type.Exercise.ordinal
    }

    fun noteTypeOnClick() {
        notesCalendarViewModel.editTypeSingleLiveEvent.value = Type.Note.ordinal
        dialogEditGlucoseBinding.currentType = Type.Note.ordinal
    }

    fun medicalTypeOnClick() {
        notesCalendarViewModel.editTypeSingleLiveEvent.value = Type.Medical.ordinal
        dialogEditGlucoseBinding.currentType = Type.Medical.ordinal
    }
}