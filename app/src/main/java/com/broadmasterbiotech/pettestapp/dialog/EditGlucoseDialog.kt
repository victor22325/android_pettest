package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.viewmodel.GlucoseLogViewModel
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.DialogEditGlucoseBinding
import com.broadmasterbiotech.pettestapp.enums.pet.Species
import com.bumptech.glide.Glide
import java.util.*

class EditGlucoseDialog(val activity: Activity, private val glucoseLogViewModel: GlucoseLogViewModel, private val glucoseInfo: GlucoseInfo, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {

        private val TAG = EditGlucoseDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogEditGlucoseBinding: DialogEditGlucoseBinding
    private lateinit var datePicker: DatePicker
    private lateinit var timePicker: TimePicker

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogEditGlucoseBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_edit_glucose, null, false)
        dialog.setContentView(dialogEditGlucoseBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialogEditGlucoseBinding.activity = this
        dialogEditGlucoseBinding.viewModel = glucoseLogViewModel
        dialogEditGlucoseBinding.petInfo = glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value
        dialogEditGlucoseBinding.glucoseInfo = glucoseInfo
        datePicker = dialogEditGlucoseBinding.editGlucoseDatePicker
        timePicker = dialogEditGlucoseBinding.editGlucoseTimePicker
        glucoseLogViewModel.editGlucoseValue.value = glucoseInfo.value.toString()
        dialogEditGlucoseBinding.editGlucoseDatePicker.updateDate(glucoseInfo.year.toInt(), glucoseInfo.month.toInt()-1, glucoseInfo.day.toInt())
        dialogEditGlucoseBinding.editGlucoseTimePicker.hour = glucoseInfo.hour.toInt()
        dialogEditGlucoseBinding.editGlucoseTimePicker.minute = glucoseInfo.minute.toInt()
        if(glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.avatarPhoto.isNullOrEmpty()) {
            if(glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.species == Species.Canine.num) {
                Glide.with(activity).load(R.drawable.dog_list).into(dialogEditGlucoseBinding.editGlucoseAvatarPhoto)
            } else {
                Glide.with(activity).load(R.drawable.cat_list).into(dialogEditGlucoseBinding.editGlucoseAvatarPhoto)
            }
        } else {
            Glide.with(activity).load(glucoseLogViewModel.petTestRepository.petInfoLiveEvent.value?.avatarPhoto).into(dialogEditGlucoseBinding.editGlucoseAvatarPhoto)
        }
        dialog.show()
    }

    fun editGlucoseSaveOnClick() {
        glucoseLogViewModel.editGlucoseSaveOnClick(glucoseInfo, datePicker, timePicker)
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.dismiss()
    }

}