package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.pettestapp.BuildConfig
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.DialogAboutBinding
import java.util.*

class AboutDialog(val activity: Activity, val lifecycleOwner: LifecycleOwner): DialogInterface {

    companion object {
        val TAG = AboutDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogAboutBinding: DialogAboutBinding

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogAboutBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_about, null, false)
        dialog.setContentView(dialogAboutBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialogAboutBinding.version = BuildConfig.VERSION_NAME
        dialogAboutBinding.lifecycleOwner = lifecycleOwner
        dialog.show()
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }

}