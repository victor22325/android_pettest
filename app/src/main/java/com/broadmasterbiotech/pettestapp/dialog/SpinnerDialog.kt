package com.broadmasterbiotech.pettestapp.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.adapter.SpinnerDialogAdapter
import com.broadmasterbiotech.pettestapp.databinding.DialogSpinnerSelectorBinding
import java.util.*
import kotlin.collections.ArrayList

class SpinnerDialog(val activity:Activity, private val title:String, val list:ArrayList<String>,val listener: SpinnerDialogAdapter.SpinnerDialogRecyclerViewOptionListener, val lifecycleOwner: LifecycleOwner): DialogInterface{

    companion object {

        private val TAG = SpinnerDialog::class.java.simpleName
    }

    private lateinit var dialog: Dialog
    private lateinit var dialogSpinnerSelectorBinding: DialogSpinnerSelectorBinding

    fun show() {
        dialog = Dialog(activity, R.style.CustomSpinnerDialog)
        dialogSpinnerSelectorBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_spinner_selector, null, false)
        dialogSpinnerSelectorBinding.dialogSpinnerSelectorTitle.text = title
        dialogSpinnerSelectorBinding.dialogSpinnerSelectorRecyclerView.adapter = SpinnerDialogAdapter(list, listener)
        dialogSpinnerSelectorBinding.lifecycleOwner = lifecycleOwner
        dialog.setContentView(dialogSpinnerSelectorBinding.root)
        Objects.requireNonNull<Window>(dialog.window).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }

    override fun dismiss() {
        dialog.dismiss()
    }

    override fun cancel() {
        dialog.cancel()
    }
}