package com.broadmasterbiotech.pettestapp.adapter

import android.util.SparseArray
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.broadmasterbiotech.pettestapp.activity.PetTestMainActivity
import com.broadmasterbiotech.pettestapp.fragment.*

class PetTestMainViewPagerAdapter(fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    companion object {

        private val TAG = PetTestMainViewPagerAdapter::class.java.simpleName

        private const val mainPagerSize = 5
    }

    private val mFragments = SparseArray<BaseFragment>()

    override fun getCount(): Int {
        return mainPagerSize
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            PetTestMainActivity.navigation_bottom_record -> RecordFragment.newInstance()
            PetTestMainActivity.navigation_bottom_notes -> NotesCalendarFragment.newInstance()
            PetTestMainActivity.navigation_bottom_reminder -> ReminderListFragment.newInstance()
            PetTestMainActivity.navigation_bottom_blog -> BlogFragment.newInstance()
            PetTestMainActivity.navigation_bottom_settings -> SettingFragment.newInstance()
            else -> RecordFragment.newInstance()
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as BaseFragment
        mFragments.put(position, fragment)
        return fragment
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
        mFragments.remove(position)
    }

}