package com.broadmasterbiotech.pettestapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.broadmasterbiotech.library.database.glucose.GlucoseInfo
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.RecyclerViewGlucoseLogBinding

class GlucoseLogAdapter(private val petInfo: PetInfo, private val glucoseLogAdapterLister: GlucoseLogAdapterListener): ListAdapter<GlucoseInfo, GlucoseLogAdapter.ItemViewHolder>(GlucoseInfoDiffCallback()) {

    companion object {

        private val TAG = GlucoseLogAdapter::class.java.simpleName

        class GlucoseInfoDiffCallback : DiffUtil.ItemCallback<GlucoseInfo>() {

            override fun areItemsTheSame(oldItem: GlucoseInfo, newItem: GlucoseInfo): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: GlucoseInfo, newItem: GlucoseInfo): Boolean {
                return oldItem == newItem
            }

            override fun getChangePayload(oldItem: GlucoseInfo, newItem: GlucoseInfo): Any? {
                return newItem
            }
        }
    }

    inner class ItemViewHolder(var binding: RecyclerViewGlucoseLogBinding): RecyclerView.ViewHolder(binding.root) {

        fun bindTo(glucoseInfo: GlucoseInfo, glucoseUpper: Int, glucoseLower: Int, listener: GlucoseLogAdapterListener) {
            binding.glucoseInfo = glucoseInfo
            binding.glucoseUpper = glucoseUpper
            binding.glucoseLower = glucoseLower
            binding.listener = listener
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = DataBindingUtil.inflate<RecyclerViewGlucoseLogBinding>(LayoutInflater.from(parent.context), R.layout.recycler_view_glucose_log, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val glucoseInfo = getItem(position)
        holder.bindTo(glucoseInfo, petInfo.glucoseUpper.toInt(), petInfo.glucoseLower.toInt(), glucoseLogAdapterLister)
    }

    interface GlucoseLogAdapterListener {
        fun onClick(glucoseInfo: GlucoseInfo)
    }
}