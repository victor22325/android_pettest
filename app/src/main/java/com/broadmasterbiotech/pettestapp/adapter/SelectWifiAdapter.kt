package com.broadmasterbiotech.pettestapp.adapter

import android.app.Activity
import android.net.wifi.ScanResult
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ListAdapter
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.RecyclerViewScanResultBinding

class SelectWifiAdapter(val activity: Activity, private var scanResultList: ArrayList<ScanResult>, private val scanResultListener: ScanResultListener): ListAdapter<ScanResult, SelectWifiAdapter.ItemViewHolder>(ScanResultDiffCallback()) {

    companion object {

        private val TAG = SelectWifiAdapter::class.java.simpleName

        class ScanResultDiffCallback : DiffUtil.ItemCallback<ScanResult>() {

            override fun areItemsTheSame(oldItem: ScanResult, newItem: ScanResult): Boolean {
                return oldItem.BSSID == newItem.BSSID
            }

            override fun areContentsTheSame(oldItem: ScanResult, newItem: ScanResult): Boolean {
                return oldItem.BSSID == newItem.BSSID
            }

            override fun getChangePayload(oldItem: ScanResult, newItem: ScanResult): Any? {
                return newItem.BSSID
            }
        }
    }

    inner class ItemViewHolder(var binding: RecyclerViewScanResultBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindScanResult(scanResult: ScanResult, listener: ScanResultListener) {
            binding.scanResult = scanResult
            binding.listener = listener
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectWifiAdapter.ItemViewHolder {
        val binding = DataBindingUtil.inflate<RecyclerViewScanResultBinding>(
            LayoutInflater.from(parent.context), R.layout.recycler_view_scan_result, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val scanResult = getItem(position)
        holder.bindScanResult(scanResult, scanResultListener)
    }

    override fun getItem(position: Int): ScanResult {
        return scanResultList[position]
    }

    override fun getItemCount(): Int {
        return scanResultList.size
    }

    override fun submitList(list: MutableList<ScanResult>?) {
        this.scanResultList = list as ArrayList<ScanResult>
        super.submitList(scanResultList)
    }

    interface ScanResultListener {
        fun onClick(scanResult: ScanResult)
    }
}
