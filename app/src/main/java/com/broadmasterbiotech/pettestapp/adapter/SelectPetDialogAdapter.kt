package com.broadmasterbiotech.pettestapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.broadmasterbiotech.library.database.pet.PetInfo
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.RecyclerViewSelectPetDialogBinding
import com.broadmasterbiotech.pettestapp.enums.pet.Species
import com.bumptech.glide.Glide

class SelectPetDialogAdapter(val activity: Activity, private var petInfoList: ArrayList<PetInfo>, val listener: SelectPetAdapterListener): ListAdapter<PetInfo, SelectPetDialogAdapter.ItemViewHolder>(PetInfoDiffCallback()) {

    companion object {

        private val TAG = SelectPetDialogAdapter::class.java.simpleName

        class PetInfoDiffCallback : DiffUtil.ItemCallback<PetInfo>() {

            override fun areItemsTheSame(oldItem: PetInfo, newItem: PetInfo): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: PetInfo, newItem: PetInfo): Boolean {
                return oldItem == newItem
            }

            override fun getChangePayload(oldItem: PetInfo, newItem: PetInfo): Any? {
                return newItem
            }
        }
    }

    inner class ItemViewHolder(var binding: RecyclerViewSelectPetDialogBinding): RecyclerView.ViewHolder(binding.root) {

        fun bindTo(petInfo: PetInfo, listener: SelectPetAdapterListener) {
            binding.petInfo = petInfo
            binding.listener = listener
        }

        fun bindAvatarPhoto(petInfo: PetInfo) {
            if(petInfo.avatarPhoto?.isNotEmpty()!!) {
                Glide.with(activity).load(petInfo.avatarPhoto)
                    .into(binding.selectPetDialogPetAvatarPhoto)
            } else {
                if(petInfo.species == Species.Canine.num) {
                    Glide.with(activity).load(R.drawable.dog_list)
                        .into(binding.selectPetDialogPetAvatarPhoto)
                } else {
                    Glide.with(activity).load(R.drawable.cat_list)
                        .into(binding.selectPetDialogPetAvatarPhoto)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = DataBindingUtil.inflate<RecyclerViewSelectPetDialogBinding>(LayoutInflater.from(parent.context), R.layout.recycler_view_select_pet_dialog, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val petInfo = getItem(position)
        holder.bindTo(petInfo, listener)
        holder.bindAvatarPhoto(petInfo)
    }

    override fun getItem(position: Int): PetInfo {
        return petInfoList[position]
    }

    override fun getItemCount(): Int {
        return petInfoList.size
    }

    override fun submitList(list: MutableList<PetInfo>?) {
        this.petInfoList = list as ArrayList<PetInfo>
        super.submitList(petInfoList)
        notifyDataSetChanged()
    }

    interface SelectPetAdapterListener {
        fun onClick(petInfo: PetInfo)
    }
}