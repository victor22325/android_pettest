package com.broadmasterbiotech.pettestapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.AndroidViewModel
import androidx.recyclerview.widget.RecyclerView
import com.broadmasterbiotech.pettestapp.databinding.RecyclerViewSpinnerDialogBinding

class SpinnerDialogAdapter(private val arrayList: ArrayList<String>, private val listener: SpinnerDialogRecyclerViewOptionListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {

        private val TAG = SpinnerDialogAdapter::class.java.simpleName
    }

    private lateinit var recyclerViewSpinnerDialogBinding: RecyclerViewSpinnerDialogBinding

    interface SpinnerDialogRecyclerViewOptionListener {

        fun onClicked(option: String)
    }

    inner class OptionBindingViewHolder(private val binding: RecyclerViewSpinnerDialogBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(option: String, listener: SpinnerDialogRecyclerViewOptionListener) {
            binding.option = option
            binding.listener = listener
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        recyclerViewSpinnerDialogBinding = RecyclerViewSpinnerDialogBinding.inflate(inflater, parent, false)
        return OptionBindingViewHolder(recyclerViewSpinnerDialogBinding)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is OptionBindingViewHolder) {
            holder.bind(option = arrayList[position], listener = listener)
        }
    }
}