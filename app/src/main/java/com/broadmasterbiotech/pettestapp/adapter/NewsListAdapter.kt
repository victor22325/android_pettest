package com.broadmasterbiotech.pettestapp.adapter

import android.app.Activity
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.broadmasterbiotech.library.database.news.NewsInfo
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.RecyclerViewNewsListBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


class NewsListAdapter(val activity: Activity, private var newsInfoAdapterListener: NewsInfoAdapterListener): ListAdapter<NewsInfo, NewsListAdapter.ItemViewHolder>(NewsDiffCallback()) {

    companion object {
        val TAG = NewsListAdapter::class.java.simpleName

        class NewsDiffCallback : DiffUtil.ItemCallback<NewsInfo>() {
            override fun areItemsTheSame(oldItem: NewsInfo, newItem: NewsInfo): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(oldItem: NewsInfo, newItem: NewsInfo): Boolean {
                return oldItem.title == newItem.title
            }
        }
    }

    private lateinit var binding: RecyclerViewNewsListBinding

    inner class ItemViewHolder(var binding: RecyclerViewNewsListBinding): RecyclerView.ViewHolder(binding.root) {
        fun bindTo(newsInfo: NewsInfo, listener: NewsInfoAdapterListener) {
            binding.newsInfo = newsInfo
            binding.listener = listener
        }

        fun bindPhoto(thumbnailUri: String?) {
            if(thumbnailUri?.isNotEmpty()!!) {
                Log.d(TAG, "thumbnailUri:$thumbnailUri")
                Glide.with(activity).load(thumbnailUri).into(binding.newsThumbnailPhoto)
            } else {
                Glide.with(activity).load(R.drawable.ic_launcher).into(binding.newsThumbnailPhoto)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.recycler_view_news_list, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val newsInfo = getItem(position)
        holder.bindTo(newsInfo, newsInfoAdapterListener)
        holder.bindPhoto(newsInfo.thumbnailUri)
    }

    interface NewsInfoAdapterListener {
        fun onClick(newsInfo: NewsInfo)
    }
}