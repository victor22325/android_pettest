package com.broadmasterbiotech.pettestapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.broadmasterbiotech.library.database.reminder.ReminderInfo
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.RecyclerViewReminderListItemBinding

class ReminderListAdapter(private var reminderListAdapterListener: ReminderListAdapterListener): ListAdapter<ReminderInfo, ReminderListAdapter.ItemViewHolder>(ReminderDiffCallback()) {

    companion object {

        val TAG = ReminderListAdapter::class.java.simpleName

        class ReminderDiffCallback : DiffUtil.ItemCallback<ReminderInfo>() {
            override fun areItemsTheSame(oldItem: ReminderInfo, newItem: ReminderInfo): Boolean {
                return oldItem.createTime!! == newItem.createTime
            }

            override fun areContentsTheSame(oldItem: ReminderInfo, newItem: ReminderInfo): Boolean {
                return oldItem.createTime!! == newItem.createTime
            }

            override fun getChangePayload(oldItem: ReminderInfo, newItem: ReminderInfo): Any? {
                return newItem
            }
        }
    }

    private lateinit var binding: RecyclerViewReminderListItemBinding

    inner class ItemViewHolder(var binding: RecyclerViewReminderListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bindTo(reminderInfo: ReminderInfo, listener: ReminderListAdapterListener) {
            binding.reminderInfo = reminderInfo
            binding.listener = listener
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.recycler_view_reminder_list_item, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val reminderInfo = getItem(position)
        holder.bindTo(reminderInfo, reminderListAdapterListener)
        binding.reminderListItemSwitch.setOnCheckedChangeListener { _, isChecked ->
            reminderListAdapterListener.onSwitch(reminderInfo, isChecked)
        }
    }

    interface ReminderListAdapterListener {
        fun onClick(reminderInfo: ReminderInfo)
        fun onSwitch(reminderInfo: ReminderInfo, isChecked: Boolean)
    }
}