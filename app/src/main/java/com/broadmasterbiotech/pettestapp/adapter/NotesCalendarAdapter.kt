package com.broadmasterbiotech.pettestapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.broadmasterbiotech.library.database.note.NoteInfo
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.RecyclerViewNotesCalendarBinding
import com.broadmasterbiotech.pettestapp.enums.note.Type

class NotesCalendarAdapter(private var notesCalendarAdapterListener: NotesCalendarAdapterListener): ListAdapter<NoteInfo, NotesCalendarAdapter.ItemViewHolder>(NoteDiffCallback()) {

    companion object {

        val TAG = NotesCalendarAdapter::class.java.simpleName

        class NoteDiffCallback : DiffUtil.ItemCallback<NoteInfo>() {
            override fun areItemsTheSame(oldItem: NoteInfo, newItem: NoteInfo): Boolean {
                return oldItem.createTime!! == newItem.createTime
            }

            override fun areContentsTheSame(oldItem: NoteInfo, newItem: NoteInfo): Boolean {
                return oldItem.createTime!! == newItem.createTime
            }
        }
    }

    private lateinit var binding: RecyclerViewNotesCalendarBinding

    inner class ItemViewHolder(var binding: RecyclerViewNotesCalendarBinding): RecyclerView.ViewHolder(binding.root) {
        fun bindTo(noteInfo: NoteInfo, adapter: NotesCalendarAdapter, listener: NotesCalendarAdapterListener) {
            binding.noteInfo = noteInfo
            binding.listener = listener
            binding.adapter = adapter
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.recycler_view_notes_calendar, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val noteInfo = getItem(position)
        binding.notesCalendarRecyclerViewItemTypeIcon.setImageResource(getNoteTypeDrawable(noteInfo))
        holder.bindTo(noteInfo!!, this, notesCalendarAdapterListener)
    }

    interface NotesCalendarAdapterListener {
        fun onClick(noteInfo: NoteInfo)
    }

    fun getDate(date: String): String {
        val splitList = date.split("/")
        val convertDate = when (splitList[1]) {
            "01" -> "Jan"
            "02" -> "Feb"
            "03" -> "Mar"
            "04" -> "Apr"
            "05" -> "May"
            "06" -> "Jun"
            "07" -> "Jul"
            "08" -> "Aug"
            "09" -> "Sep"
            "10" -> "Oct"
            "11" -> "Nov"
            "12" -> "Dec"
            else -> ""
        }
        return "$convertDate/${splitList[2]}"
    }

    private fun getNoteTypeDrawable(noteInfo: NoteInfo): Int {
        return when (noteInfo.type) {
            Type.Meal.num -> R.mipmap.ic_diningroom
            Type.Exercise.num -> R.mipmap.ic_running
            Type.Note.num -> R.mipmap.ic_book
            Type.Medical.num -> R.mipmap.ic_pill
            else -> R.mipmap.ic_book
        }
    }
}