package com.broadmasterbiotech.pettestapp.adapter

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothHeadset
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.broadmasterbiotech.pettestapp.R
import com.broadmasterbiotech.pettestapp.databinding.RecyclerViewSelectBluetoothDialogBinding

class SelectBluetoothAdapter(val activity: Activity, private var bluetoothDeviceList: ArrayList<BluetoothDevice>, private val bluetoothDeviceListener: SelectBluetoothDeviceListener): ListAdapter<BluetoothDevice, SelectBluetoothAdapter.ItemViewHolder>(BluetoothDeviceDiffCallback()) {

    companion object {

        private val TAG = SelectBluetoothAdapter::class.java.simpleName

        class BluetoothDeviceDiffCallback : DiffUtil.ItemCallback<BluetoothDevice>() {

            override fun areItemsTheSame(oldItem: BluetoothDevice, newItem: BluetoothDevice): Boolean {
                return oldItem.address == newItem.address
            }

            override fun areContentsTheSame(oldItem: BluetoothDevice, newItem: BluetoothDevice): Boolean {
                return oldItem.address == newItem.address
            }

            override fun getChangePayload(oldItem: BluetoothDevice, newItem: BluetoothDevice): Any? {
                return newItem.address
            }
        }
    }

    inner class ItemViewHolder(var binding: RecyclerViewSelectBluetoothDialogBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindingDevice(bluetoothDevice: BluetoothDevice, connectStatus: Boolean, listener: SelectBluetoothDeviceListener) {
            binding.bluetoothDevice = bluetoothDevice
            binding.connectedStatus = connectStatus
            binding.listener = listener
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = DataBindingUtil.inflate<RecyclerViewSelectBluetoothDialogBinding>(LayoutInflater.from(parent.context), R.layout.recycler_view_select_bluetooth_dialog, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val bluetoothDevice = getItem(position)
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        holder.bindingDevice(bluetoothDevice,  bluetoothAdapter?.getProfileConnectionState(BluetoothHeadset.HEADSET) == BluetoothHeadset.STATE_CONNECTED, bluetoothDeviceListener)
    }

    override fun getItem(position: Int): BluetoothDevice {
        return bluetoothDeviceList[position]
    }

    override fun getItemCount(): Int {
        return bluetoothDeviceList.size
    }

    override fun submitList(list: MutableList<BluetoothDevice>?) {
        this.bluetoothDeviceList = list as ArrayList<BluetoothDevice>
        super.submitList(bluetoothDeviceList)
        notifyDataSetChanged()
    }

    interface SelectBluetoothDeviceListener {
        fun onClick(bluetoothDevice: BluetoothDevice)
    }
}